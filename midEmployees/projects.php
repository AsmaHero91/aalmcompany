<?php
session_start();
if ($_SESSION['levelid']) {
    //successfully login
} else {
    header("Location: ../login.html");
}
?>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>قسم المشاريع</title>
        <!-- Tell the browser to be responsive to screen width -->
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Select2 for select multible -->
        <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <!-- DataTables styles with filters -->
        <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
        <!-- SweetAlert2 and Toast -->
        <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <!-- SweetAlert2 and Toast -->
        <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="../dist/css/responsiveTable.css">
        <link rel="stylesheet" href="../dist/css/blogModal.css">
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> -->
           <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

        </head>
    <style>
        .bootstrap-switch .bootstrap-switch-handle-off,
        .bootstrap-switch .bootstrap-switch-handle-on,
        .bootstrap-switch .bootstrap-switch-label {
            line-height: 1.8;
        }

        #nationality {
            width: 100px;
        }

        #experience {
            width: 100px;
        }

        #status {
            width: 100px;
        }

        .greendot {
            height: 25px;
            width: 25px;
            background-color: #2ECC40;
            border-radius: 50%;
            display: inline-block;
        }

    </style>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-sm-inline-block">
            <a href="../index.php" class="nav-link">البداية</a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown al">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments" id="cc"></i>
              <span class="badge badge-danger navbar-badge" id="countAlert"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

          <div class="dropdown-divider"></div>
                                          </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item">
              <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="../index.php" class="brand-link">
          <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
          <span class="brand-text font-weight-light">PIONEER</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a  id="userName" href="#" class="d-block"></a>
              <a href="#" id="userStatus"></a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                   <li class="nav-item">
                     <a href="../index.php" class="nav-link">
                       <i class="nav-icon fas fa-tachometer-alt"></i>
                       <p>
                         لوحة التحكم
                       </p>
                     </a>
                   </li>


              <li class="nav-item">
                <a href="../employees/profile.php" class="nav-link">
                  <i class="nav-icon fas fa-id-card-alt"></i>
                  <p>
                    البروفايل
                  </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../employees/requests.php" class="nav-link">
                  <i class="nav-icon fas fa-list-ol"></i>
                  <p>
                    الطلبات
                  </p>
                </a>
              </li>



              <li class="nav-item has-treeview opened menu-open" id="midadmin-section" style="display:none;">
                <a href="#" class="nav-link active">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    أقسام الإدارة المتوسطة
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../midEmployees/projects.php" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم المشاريع</p>
                    </a>
                  </li>

                </ul>

              </li>



              <li class="nav-item has-treeview" id="admin-section" style="display:none;">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                     أقسام الإدارة العليا
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/employees.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم الموارد البشرية</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/others.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم العملاء والمقاولين</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/AdminRequests.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم خصائص المعاملات</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">

                <li class="nav-item">
    <a href="../sms/form.php" class="nav-link">
    <i class="nav-icon fas fa-sms"></i>
    <p>
    SMS
    </p>
    </a>
    </li>
    </ul>

              </li>

              <li class="nav-item">
     <a id="logout" class="nav-link">
       <i class="nav-icon  ion-log-out"></i>
       <p>خروج</p>
     </a>
     </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>


            <!-- end sidebars aside-->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 10px;">
                                <div class="card card-primary card-tabs">
                                    <div class="card-header p-0 pt-1">
                                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                            <li class="nav-item"> <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">المشاريع</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">الطلبات</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">المستندات</a> </li>
                                        </ul>
                                    </div>
                                    <!-- /.card secondary-->
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-one-tabContent">
                                            <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                                                        <!-- to take all space width of row-->
                                                                        <div class="row">
                                                                        <div class="col-12">
                                                                                <div class="card-body" style="overflow-x:auto;">
                                                                                    <table id="projectTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                        <thead>
                                                                                            <tr>

                                                                                              <th class="no-sort">
                                                                                                  <button data-id1="projectTable" class="addbutton btn btn-s btn-success" type="button" name="button">+</button>
                                                                                                  <br>#
                                                                                                  <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                                                <th width="10%">كود المشروع</th>
                                                                                                <th width="10%">اسم المشروع</th>
                                                                                                <th width="35%">القائدين</th>
                                                                                                <th width="35%">أعضاء الفريق</th>
                                                                                                <th width="35%"> نسبة الإنجاز</th>
                                                                                                <th width="35%">المهام المنجزة</th>
                                                                                                <th width="35%">العملاء</th>
                                                                                                <th width="35%">تاريخ الإنشاء</th>
                                                                                                <th></th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody id="projectTbody">

                                                                                        </tbody>
                                                                                    </table>
                                                                                    <br />
                                                                                    <div class="card card-secondary">
                                                                                        <div class="card-body">
                                                                                            <label>الصفوف المحددة</label>
                                                                                            <div>
                                                                                                <button type="button" name="deleteSelect" data-id1="projectTable" class="deleteSelect btn btn-danger" style="margin-bottom: 10px; height: 35px;">حذف المحدد</button>
                                                                                                <button type="button" class="savebutton btn btn-info" data-id1="projectTable" style="margin-bottom: 10px; height: 35px;">حفظ المضاف</button>
                                                                                                <button type="button" class="viewbutton btn btn-info"  style="margin-bottom: 10px; height: 35px;"> رؤية التفاصيل</button>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- /.card-secondary -->
                                                                                </div>
                                                                                <!-- /.card-body -->
                                                                            </div>
                                                                            <!-- /.card -->
                                                                        </div>
                                                                        <!-- /.col-12 -->
                                                                    </div>

                                            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                              <!-- to take all space width of row-->
                                              <div class="row">
                                              <div class="col-12">
                                                      <div class="card-body" style="overflow-x:auto;">
                                                          <table id="requestsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                              <thead>
                                                                  <tr>

                                                                    <th class="no-sort">
                                                                        <br># </th>
                                                                      <th width="10%">رقم المهمة</th>
                                                                      <th width="10%">اسم المهمة</th>
                                                                      <th width="35%">الوصف</th>
                                                                      <th width="35%">المرسل</th>
                                                                      <th width="35%">مرسل إلى</th>
                                                                      <th width="35%">تاريخ الإنشاء</th>
                                                                      <th width="35%">تاريخ التسليم</th>
                                                                      <th width="50%">الحالة</th>
                                                                      <th width="35%">كود البروجكت</th>
                                                                      <th width="35%">المستندات</th>
                                                                      <th class="no-sort"></th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody id="requestsTbody">

                                                              </tbody>
                                                          </table>
                                                          <br />

                                                          <!-- /.card-secondary -->
                                                      </div>
                                                      <!-- /.card-body -->
                                                  </div>
                                                  <!-- /.card -->
                                              </div>
                                            </div>

                                            <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
                                              <!-- to take all space width of row-->
                                              <div class="row">
                                              <div class="col-12">
                                                      <div class="card-body" style="overflow-x:auto;">
                                                          <table id="documentsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                              <thead>
                                                                  <tr>

                                                                    <th class="no-sort">
                                                                        <br>#</th>
                                                                      <th width="10%">كود البروجكت</th>
                                                                      <th width="10%">المرسل</th>
                                                                      <th width="35%">المستندات</th>
                                                                      <th width="35%">تاريخ الإنشاء</th>
                                                                      <th width="35%">العنوان</th>

                                                                      <th class="no-sort"></th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody id="documentsTbody">

                                                              </tbody>
                                                          </table>
                                                          <br />

                                                          <!-- /.card-secondary -->
                                                      </div>
                                                      <!-- /.card-body -->
                                                  </div>
                                                  <!-- /.card -->
                                              </div>
                                            </div>
                                            <!-- /.content-wrapper -->
                                            <!-- Control Sidebar -->
                                            <aside class="control-sidebar control-sidebar-dark">
                                                <!-- Control sidebar content goes here -->
                                            </aside>
                                            <!-- /.control-sidebar -->
                                        </div>
                                        <!-- ./wrapper -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
                </section>
            </div>
            <!-- Modal window pop up when click update or add records -->

        </div>

    </body>
    <footer class="main-footer">
      <strong>Copyright &copy; 2020 PIONEER</strong>
      All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.0.0
      </div>
    </footer>
    </html>
    <!-- jQuery -->
    <script src="../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- script for tables to have entries and search-->
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- Bootstrap Switch -->
    <script src="../plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <!-- Datepicker -->
    <script src="../plugins/gijgo/js/gijgo.min.js"></script>
    <!-- Select2 -->
    <script src="../plugins/select2/js/select2.full.min.js"></script>

    <!-- Bootstrap4 Duallistbox -->
    <script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <!-- InputMask -->
    <script src="../plugins/moment/moment.min.js"></script>
    <script src="../plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- date-range-picker -->
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- ChartJS -->
    <script src="../plugins/chart.js/Chart.min.js"></script>
    <script src="../dist/js/requestsActions.js"></script>
    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="../dist/js/requestsActions.js"></script>
    <script src="../dist/js/unified.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
   <script  src='../dist/js/pdfmake-master/build/pdfmake.min.js'></script>
   <script  src='../dist/js/pdfmake-master/build/vfs_fonts.js'></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
   <script  src="../dist/js/requestsActions.js"></script>
    <script>
    function showMessageError(maessageResponse){
      Swal.fire({
        position: 'center',
        type: 'error',
        title: maessageResponse,
        showConfirmButton: false,
        timer: 1500
      })
    }
        $("input[data-bootstrap-switch]").each(function () {
            $(this).bootstrapSwitch('state', false); //to switch it off by default
        });
        $('.checkall').change(function () {
            $('.checkboxtd').each(function () {
                if ($('.checkall').is(':checked')) {
                    $(this).attr("disabled", false);
                    $(this).prop("checked", true);
                }
                else {
                    $(this).prop("checked", false);
                }
            });
        });

        $(function () {
          $('a#custom-tabs-one-home-tab').click();
        //console.log('hasbeen clicked');
        defineSelect2();

        })
        let firstClickedHomeTab = true;
        $("a#custom-tabs-one-home-tab").click(function(){
            showsettingtbody("","projectTable");


      //


        console.log('hasbeen clicked');
        });
        $("a#custom-tabs-one-profile-tab").click(function(){
            showsettingtbody("","requestsTable");

        console.log('hasbeen clicked');
        });
        $("a#custom-tabs-one-settings-tab").click(function(){
            showsettingtbody("","documentsTable");

        console.log('hasbeen clicked');
        });
        let values;
        $(".savebutton").click(function(){

              /////////begin of procesing add records rows number data
              values = [];
              addRecords = [];
              var tableName = $(this).data("id1");
              var rowCount;
                        if (tableName == 'projectTable') {

                        rowCount = $('#projectTbody tr').length; //to count of current table
                      }
                      var sel = $('select[name="projectTable_length"] option:selected').val();
                      var selint = parseInt(sel);
                      var realExitRecords;

                      if (rowCount > 0)
                      {
                        realExitRecords = rowCount - addRecordsRowsNo.length;
                        console.log(realExitRecords+'real exit');

                      }else{
                        realExitRecords = 0;
                      }
                    var diff;
                    if(rowCount > addRecordsRowsNo.length ){
                    diff = rowCount - addRecordsRowsNo.length; //10 displayed records - 4 which is added = 6 the real exit records in the table.
                  }else{
                    diff = addRecordsRowsNo.length - rowCount;
                  }
                    if(addRecordsRowsNo.length + diff > sel){
                      maessageResponse = 'لا تستطيع حفظ أكثر من المحدد من الحقول';
                      Swal.fire({
                          position: 'center'
                          , type: 'error'
                          , title: maessageResponse
                          , showConfirmButton: false
                          , timer: 1500
                      })

                      return;
                    }

                    var table = document.getElementById("" + tableName + "");
              if (typeof addRecordsRowsNo != "undefined" && addRecordsRowsNo != null && addRecordsRowsNo.length != null && addRecordsRowsNo.length > 0) {
                  //if table name
                  if (tableName == 'projectTable') {

                    var options = [];
                    var m = 0;
                    var r = realExitRecords + 1;
                      addRecordsRowsNo.forEach(myFunction);

                      function myFunction(value) {
                        options[m] = new Array(7);
                                for (i = 0; i < 7; i++) {

                                  if (i == 4) {
                                      // get the selected country from current <tr>
                                      var d = $(table.rows[r]).find('td:eq(4) select[name="select2TeamMembers"]').val(); //to take the value of combo
                                      options[m][4] = d.join(',');

                                      var selections =  $(table.rows[r]).find('td:eq(4) select[name="select2TeamMembers"]').select2('data');
                                      var selectedEmployees = [];
                                      var t = 0;
                                      selections.forEach(function(value){
                                        selectedEmployees.push(selections[t].text);
                                        t++;
                                      });


                                      let string = selectedEmployees.toString();
                                      options[m][5] = string;
                                      $(table.rows[r]).find('td:eq(4) input[name="select2TeamMembers"]').val(string);

                                      var dd = $(table.rows[r]).find('td:eq(4) select[name="select2Contractors"]').val(); //to take the value of combo
                                      options[m][6] = dd.join(',');
                                    //  var text2 = selectedContractors; //to take the value of combo
                                    var selections2 =  $(table.rows[r]).find('td:eq(4) select[name="select2Contractors"]').select2('data');

                                    var selectedContractors = [];
                                    var t = 0;
                                    selections2.forEach(function(value){
                                      selectedContractors.push(selections2[t].text);
                                      t++;
                                    });


                                    let string2 = selectedContractors.toString();
                                    options[m][7] = string2;
                                    $(table.rows[r]).find('td:eq(4) input[name="select2Contractors"]').val(string2);
                                  }
                                  if (i == 7) {
                                      // get the selected country from current <tr>
                                      var d = $(table.rows[r]).find('td:eq(7) select[name="select2Customers"]').val(); //to take the value of combo
                                      options[m][8] = d.join(',');

                                      var selections =  $(table.rows[r]).find('td:eq(7) select[name="select2Customers"]').select2('data');
                                      var selectedCustomers = [];
                                      var t = 0;
                                      selections.forEach(function(value){
                                        selectedCustomers.push(selections[t].text);
                                        t++;
                                      });


                                      let string = selectedCustomers.toString();
                                      options[m][9] = string;
                                      $(table.rows[r]).find('td:eq(8) input[name="select2Customers"]').val(string);

                                  }
                                  if (i == 3) {
                                      // get the selected country from current <tr>
                                      // get the selected country from current <tr>
                                      var d = $(table.rows[r]).find('td:eq(3) select[name="select2leadby"]').val(); //to take the value of combo
                                      options[m][10] = d.join(',');

                                      var selections =  $(table.rows[r]).find('td:eq(3) select[name="select2leadby"]').select2('data');
                                      var selectedLeaders = [];
                                      var t = 0;
                                      selections.forEach(function(value){
                                        selectedLeaders.push(selections[t].text);
                                        t++;
                                      });


                                      let string = selectedLeaders.toString();
                                      options[m][11] = string;
                                      $(table.rows[r]).find('td:eq(3) input[name="select2leadby"]').val(string);


                                  }
                                  if (i == 1 || i == 2){

                                  options[m][i] = table.rows[r].cells[i].innerText;
                                }
                              }
                          m++;
                          r++;
                }
                addRecords.push(options); //assumin we have 4 cells in our example
                console.log(addRecords+'sss');
                      }
                    }


            if(addRecords.lenght == 0){
              maessageResponse = 'لم تضف أي بيانات بعد';
              Swal.fire({
                  position: 'center'
                  , type: 'error'
                  , title: maessageResponse
                  , showConfirmButton: false
                  , timer: 1500
              })
              return;
            }
            //////////////end of processing add records rows number
            var addRecordsData = addRecords;
            var action = 'saveMultibleRecords';
            if (tableName == 'projectTable') {
                var tableNameData = "projectTable";
                if (typeof addRecords != "undefined" && addRecords != null && addRecords.length != null && addRecords.length > 0) {


                $.ajax({
                    url: '../dist/php/projects.php'
                    , method: "POST"
                    , data: {
                        addRecordsData: addRecordsData
                        , action: action
                        , tableNameData: tableNameData
                    }
                    , success: function (data) { //the data return in json format from db to be used in the modal.
                        // on load of the page: switch to the currently selected tab
                        //  var hash = window.location.hash;
                        if (data == 0) {
                            maessageResponse = 'تم الحفظ بنجاح';
                            Swal.fire({
                                position: 'center'
                                , type: 'success'
                                , title: maessageResponse
                                , showConfirmButton: true
                                , timer: 1500
                            })
                            initializeArrays();
                      window.location.reload();
                            //  $('a[href=\'' + '#custom-content2-below-settings' + '\']').click();
                            return;
                        }else{
                          maessageResponse = 'لم يتم الحفظ بنجاح تأكد من بعض البيانات المدخلة';
                          Swal.fire({
                              position: 'center'
                              , type: 'error'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                         addRecords = [];
                        }


                    }
                });
              }else{
                maessageResponse = 'لم تضف صفوف جديدة بعد ';
                Swal.fire({
                    position: 'center'
                    , type: 'error'
                    , title: maessageResponse
                    , showConfirmButton: false
                    , timer: 1500
                })
              }
            }

        });
        function endEdition() {
                // We get the cell
                var el = $(this);
                var myTable = defineTableWithCounter('#projectTable');

                el.attr('contenteditable', 'false');
                el.off('blur', endEdition); // To prevent another bind to this function
            }

        var addRecords = [];
        var addRecordsRowsNo = [];
        var editedRecords = [];

        function initializeArrays(){
          addRecords = [];
          addRecordsRowsNo = [];
          editedRecords = [];
        }
        function showsettingtbody(myselection,tableName) {
var myselection = getChechedValues("projectTable");
var element;

if(myselection.length == 1){
  element =  myselection;
}


          if(tableName == "projectTable"){

            initializeArrays();
            $('#projectTbody').html('')

        //    defineSelect2();

            //  htmltoAppend.innerHTML = '';
            var action = 'showProjects';
            $.ajax({
                url: '../dist/php/projects.php'
                , type: 'POST'
                , data: {
                action: action
                , }
             , dataType: "json"
                , success: function (data) {
              //


                          var t =  defineTableWithCounter('#projectTable');
                          t.clear().draw();
                    var myData = data;
                    if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
                        var count = 0;

                        myData.forEach(function (rowd) {

                          count = count + 1;
                          // var t = $('#projectTable').DataTable({
                          //   retrieve: true

                          var row = t.row.add(['<td class="#">' + count + ' </td>'
                          , '<td class="idcard">' + rowd.projectCode + '</td>'
                              , '<td class="name">' + rowd.pname + '</td>'
                              , '<td class="m1">' + rowd.leadbyName + '</td>'
                              , '<td class="m2">' + rowd.teamempname + '</td>'
                              , '<td class="email">' + rowd.progress + '</td>'
                              , '<td class="notes">' + rowd.tasksAcheived + '</td>'
                              , '<td class="notes">' + rowd.customers + '</td>'
                              , '<td class="notes">' + rowd.dateCreated + '</td>'
                              , '<td></td>'
                          , ]).draw().node();


                          $(row).find('td:eq(3)').html('<input type="text" name="select2leadby" value="'+rowd.leadbyName+'"><br><select class="select2leadby" name="select2leadby" multiple="multiple" data-placeholder="اختر القائدين" style="width: 150px;"></select>');


                          $(row).find('td:eq(4)').html('<input type="text" name="select2TeamMembers" value="'+rowd.teamempname+'"><select class="select2TeamMembers" name="select2TeamMembers" multiple="multiple" data-placeholder="اختر الموظفين" style="width: 150px;"></select></br></br><input type="text" name="select2Contractors" value="'+rowd.teamcontraname+'"><select class="select2Contractors" name="select2Contractors" multiple="multiple" data-placeholder="اختر المقاولين" style="width: 150px;"></select>');

                    //      $(row).find('td:eq(5)').html('<select class="select2Contractors" multiple="multiple" data-placeholder="اختر المقاولين" style="width: 300px;"></select>');
                          $(row).find('td:eq(7)').html('<input type="text" name="select2Customers" value="'+rowd.customername+'"><br><select class="select2Customers" name="select2Customers" multiple="multiple" data-placeholder="اختر العملاء" style="width: 150px;"></select>');
                          $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.projectCode + '" data-id1="'+rowd.teamemp+'"  type="checkbox">' + count + '</label>');

                          $(row).find('td:eq(9)').html('<button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="projectTable"  data-id4="'+rowd.teamemp+'" data-id5="'+rowd.teamcontra+'" data-id6="'+rowd.customers+'" data-id7="'+rowd.leadby+'" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="projectTable" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

//$(row).find('select.select2Customers')[0].val(JSON.parse('[' +"1076609104" + ']'));
  //$(row).find('td:eq(8) .select2TeamMembers').val(JSON.parse('[' +"1076609104" + ']'));

                        //  $(row).find('td:eq(10)').html('<button type="button" class="btn btn-warning btn-s update" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id1="'+rowd.id+'"> <span class="ion-edit" aria-hidden="true"></span> </button><button type="button" name="btn btn-danger btn-s btn_delete" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id2="'+rowd.id+'" data-id3="'+rowd.documents+'" class="btn btn-s btn-danger btn_delete"><span class="ion-trash-a"'+' aria-hidden="true"></span> </button><button type="button" name="btn btn-danger btn-s btn_delete" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id4="'+rowd.id+'" class="btn btn-s btn-info btn_chat"><span class="ion-chatboxes" aria-hidden="true"></span> </button>');
                        $(row).find('td:eq(3) select').prop('disabled', true);
                        $(row).find('td:eq(4) select').prop('disabled', true);
                        $(row).find('td:eq(7) select').prop('disabled', true);

            row.id = count;

                          return row;

                        });

                        if(firstClickedHomeTab){
                        firstClickedHomeTab = false;
                        console.log('it is first click');
                      }else{
                        defineSelect2();


                      }
                      //  defineSelect2();

                        $('#projectTable_paginate').click(function () {

                      defineSelect2();

                      console.log('hi');
                                });
                    }
                    else {

                        var t =  defineTableWithCounter("#projectTable");
                        t.clear().draw();
                    }
                }
            });
            }
          if(tableName == "requestsTable"){

            initializeArrays();
            $('#requestsTbody').html('')

            //  htmltoAppend.innerHTML = '';
            var action = 'showAllRequests';
            var t = defineTableWithCounter('#requestsTable');
                        t.clear().draw();;
            $.ajax({
                url: '../dist/php/projects.php'
                , type: 'POST'
                , data: {
                  element: element,
                action: action
                , }
             , dataType: "json"
                , success: function (data) {
                  var t = defineTableWithCounter('#requestsTable');
                            t.clear().draw();
                    var myData = data;
                    let resultNewData = myData.map(function (o) {
                        //o.priority = "asma";
                        switch (o.status) {
                        case 'STATUS_SUSPENDED':
                            o.status = 'معلق';
                            break;
                            case 'STATUS_ACTIVE':
                                o.status = 'نشط';
                                break;
                        case 'STATUS_DONE':
                            o.status = 'مكتمل';
                            break;
                        case 'STATUS_FAILED':
                            o.status = 'مرفوض';
                            break;
                        }
                        return o;
                    });
                    if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
                        var count = 0;



                        resultNewData.forEach(function (rowd) {


                          count = count + 1;
                          // var t = $('#projectTable').DataTable({
                          //   retrieve: true
                          // })

                          var arraydString = arrayhtml;

                          var arrayd = [];
                          var arrayhtml = [];
                          var value = rowd.documents
                          var valueString = value.toString();
                          var dlink = '../upload/projects/projectsrequests/'
                          console.log(valueString);
                          strx = valueString.split(',');
                          arrayd = arrayd.concat(strx);
                          //formattedDocuments.split(',');
                          for (element of arrayd) {
                            var el = element.trim();
                            var fairel = el.substring(el.lastIndexOf('+') + 1);
                              arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + fairel.trim() + '</a>')
                              console.log(element.trim());
                          }

                          var arraydString = arrayhtml;
                          var row = t.row.add(['<td class="#">' + count + ' </td>'
                          , '<td class="idcard">' + rowd.taskNo + '</td>'
                              , '<td class="name">' + rowd.taskName + '</td>'
                              , '<td class="m1">' + rowd.description + '</td>'
                              , '<td class="email">' + rowd.requester + '</td>'
                              , '<td class="notes">' + rowd.assignedTo + '</td>'
                              , '<td class="notes">' + rowd.dateCreated + '</td>'
                              , '<td class="notes">' + rowd.deadline + '</td>'
                              , '<td class="notes">' + rowd.status + '</td>'
                              , '<td class="notes">' + rowd.projectCode + '</td>'
                              , '<td class="notes">' + rowd.documents + '</td>'
                              , '<td></td>'
                          , ]).draw().node();


$(row).find('td:eq(10)').html(arraydString.join());
                      //    $(row).find('td:eq(11)').html('<button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="requestsTable" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="requestsTable" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

            row.id = count;

                          return row;

                        });



            }      else {

                          var t = defineTableWithCounter('#requestsTable');
                                t.clear().draw();

                  }


}
});
}
if(tableName == "documentsTable"){

  initializeArrays();
  $('#requestsTbody').html('')

  var t = defineTableWithCounter('#documentsTable');
        t.clear().draw();
//    defineSelect2();

  //  htmltoAppend.innerHTML = '';
  var action = 'showAllDocuments';
  $.ajax({
      url: '../dist/php/projects.php'
      , type: 'POST'
      , data: {
        element: element,
      action: action
      , }
   , dataType: "json"
      , success: function (data) {
    //
    var t = $('#documentsTable').DataTable({
        "order": []
        , "columnDefs": [{
            "targets": [0]
            , "orderable": false
        , }]
        , retrieve: true
        , processing: true
    , });
          var myData = data;

          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
              var count = 0;


              myData.forEach(function (rowd) {


                count = count + 1;
                // var t = $('#projectTable').DataTable({
                //   retrieve: true
                // })

                var arrayd = [];
                var arrayhtml = [];
                var value = rowd.documents
                var valueString = value.toString();
                var dlink = '../upload/projects/projectsdocuments/'
                console.log(valueString);
                strx = valueString.split(',');
                arrayd = arrayd.concat(strx);
                //formattedDocuments.split(',');
                for (element of arrayd) {
                  var el = element.trim();
                  var fairel = el.substring(el.lastIndexOf('+') + 1);
                    arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + fairel + '</a>')
                    console.log(element.trim());
                }

                var arraydString = arrayhtml;
                var row = t.row.add(['<td class="#">' + count + ' </td>'
                , '<td class="idcard">' + rowd.projectCode + '</td>'
                    , '<td class="name">' + rowd.sender + '</td>'
                    , '<td class="m1">' + rowd.documents + '</td>'
                    , '<td class="email">' + rowd.dateCreated + '</td>'
                    , '<td class="notes">' + rowd.title + '</td>'
                    , '<td></td>'
                , ]).draw().node();
$(row).find('td:eq(6)').html(arraydString.join());


              //  $(row).find('td:eq(6)').html('<button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="documentsTable" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="documentsTable" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

//$(row).find('select.select2Customers')[0].val(JSON.parse('[' +"1076609104" + ']'));
//$(row).find('td:eq(8) .select2TeamMembers').val(JSON.parse('[' +"1076609104" + ']'));

              //  $(row).find('td:eq(10)').html('<button type="button" class="btn btn-warning btn-s update" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id1="'+rowd.id+'"> <span class="ion-edit" aria-hidden="true"></span> </button><button type="button" name="btn btn-danger btn-s btn_delete" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id2="'+rowd.id+'" data-id3="'+rowd.documents+'" class="btn btn-s btn-danger btn_delete"><span class="ion-trash-a"'+' aria-hidden="true"></span> </button><button type="button" name="btn btn-danger btn-s btn_delete" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id4="'+rowd.id+'" class="btn btn-s btn-info btn_chat"><span class="ion-chatboxes" aria-hidden="true"></span> </button>');

  row.id = count;

                return row;

              });


            //  defineSelect2();

              $('#documentsTable_paginate').click(function () {
                var t = $('#documentsTable').DataTable({
                    "order": []
                    , "columnDefs": [{
                        "targets": [0]
                        , "orderable": false
                    , }]
                    , retrieve: true
                    , processing: true
                , });


            console.log('hi');
                      });

  }      else {

            var t = $("#documentsTable").DataTable({
                retrieve: true
            });
            t.clear().draw();
        }

}
});
}
}
          let editedItems = [];
          let canEditProjectd;
          $('.viewbutton').click(function () {

              var element = getChechedValues("projectTable");
              if (element == null || element == 'undefined' || element.length == 0){
showMessageError('لا تستطيع رؤية التفاصيل من غير اختيارالمشروع المراد');
                return;
              }
              var action = 'checkpermession';
              $.ajax({
                  url: '../dist/php/projects.php'
                  , type: 'POST'
                  , data: {
                      element: element
                      , action: action
                  , }
                  , success: function (data) {

                    if (data == 0) {
                        canEditProjectd = true;
                        let myselection = getChechedValues("projectTable");
                        localStorage.setItem('myselection', myselection);
                        localStorage.setItem('canEditProjectd', true);
                        window.open('gantt.html','popup');   // Opens a new window

}
if (data == 1) {
  canEditProjectd = false;
  let myselection = getChechedValues("projectTable");
  localStorage.setItem('myselection', myselection);
  localStorage.setItem('canEditProjectd', false);
  window.open('gantt.html','popup');   // Opens a new window

}
if (data == -1) {
    showMessageError('غير مصرح لك رؤية تفاصيل هذا المشروع ، من  فضلك قم بالتواصل مع المدير');
    return;

}}
              });


});
          $(document).on('click', '.editbtn', function () {
                  var currentTD = $(this).parents('tr').find('td');
                  console.log('edit is caled');
                  if ($(this).html() == 'Edit') {
                      editedItems[0] = $(this).data("id2");
let employeesids = $(this).data("id4");
let contractorsids = $(this).data("id5");
let customersids = $(this).data("id6");
let leadersids = $(this).data("id7");
let employeeSelections = JSON.parse("[" + employeesids + "]");
let contractorsSelections = JSON.parse("[" + '"'+contractorsids+'"' +"]");
let customersSelections = JSON.parse("[" + '"'+customersids+'"' +"]");
let leadersSelections = JSON.parse("[" + leadersids + "]");
                      //console.log($(this).data("id2"));
                      $.each(currentTD, function () {
                          $(this).prop('contenteditable', true),
                            $(this).find('select').prop('disabled', false);

                            $(this).find('select[name="select2TeamMembers"]').val(employeeSelections);
                            $(this).find('select[name="select2TeamMembers"]').trigger( "change" );
                            $(this).find('select[name="select2Contractors"]').val(contractorsSelections);
                            $(this).find('select[name="select2Contractors"]').trigger( "change" );
                            $(this).find('select[name="select2Customers"]').val(customersSelections);
                            $(this).find('select[name="select2Customers"]').trigger( "change" );
                            $(this).find('select[name="select2leadby"]').val(leadersSelections);
                            $(this).find('select[name="select2leadby"]').trigger( "change" );

                      });
                      $(this).html('Save');
                  }
                  else {
                      $(this).html('Edit');
                      var idttoEdit = $(this).parents('tr').index() + 1;
                      console.log(idttoEdit);
                      console.log('saveiscall');
                      var tableName =  $(this).data("id3");
                      if(tableName == "projectTable"){
                        editsettingtbody(idttoEdit, 'projectTable');
                      }

                      $.each(currentTD, function () {
                          $(this).prop('contenteditable', false),
                          $(this).find('select').prop('disabled', true);

                      });

                      var editedRecordsData = editedRecords;
                      var action = "editRecords";
                      var editedItemsData = editedItems;
                      console.log("edited"+ editedItems);
                      console.log("records"+ editedRecordsData);

                      if(tableName == "projectTable"){
                        ///this is for our table stting 3 table


                        var tableNameData = "projectTable";
                        $.ajax({
                            url: '../dist/php/projects.php'
                            , type: 'POST'
                            , data: {
                                editedItemsData: editedItemsData
                                , editedRecordsData: editedRecordsData
                                , action: action
                                ,tableNameData: tableNameData
                            , }
                            , dataType: "json"
                            , success: function (data) {
                              if (data == 0) {
                                  maessageResponse = 'تم التعديل بنجاح';
                                  Swal.fire({
                                      position: 'center'
                                      , type: 'success'
                                      , title: maessageResponse
                                      , showConfirmButton: false
                                      , timer: 1500
                                  })

                                  var t = $('#projectTbody').html('');
                                  showsettingtbody("","projectTable");

                              }
                              else {
                                  maessageResponse = 'لم يتم التعديل بنجاح ، تأكد من البيانات المدخلة';
                                  Swal.fire({
                                      position: 'center'
                                      , type: 'error'
                                      , title: maessageResponse
                                      , showConfirmButton: false
                                      , timer: 1500
                                  })
                              }


                            }
                        });
                  }
              }
          });
          function  editsettingtbody(idttoEdit, tableName){
            editedRecords = [];
                        var values = [];
                        var table = document.getElementById("" + tableName + "");
                        if (idttoEdit != null || idttoEdit != 'undefined') {
                            //if table name

                            if (tableName == 'projectTable') {
                              var options = [];
                              var j = 0;

                                          for (i = 0; i < 8; i++) { //assumin we have 9 cells in our example
                                              //  var rowNo = table.rows[addRecordsRowssingle].id;
                                              if (i == 4) {
                                                  // get the selected country from current <tr>
                                                  var d = $(table.rows[idttoEdit]).find('td:eq(4) select[name="select2TeamMembers"]').val(); //to take the value of combo
                                                  options[4] = d.join(',');

                                                  var selections =  $(table.rows[idttoEdit]).find('td:eq(4) select[name="select2TeamMembers"]').select2('data');
                                                  var selectedEmployees = [];
                                                  var m = 0;
                                                  selections.forEach(function(value){
                                                    selectedEmployees.push(selections[m].text);
                                                    m++;
                                                  });


                                                  let string = selectedEmployees.toString();
                                                  options[5] = string;
                                                  $(table.rows[idttoEdit]).find('td:eq(4) input[name="select2TeamMembers"]').val(string);

                                                  var dd = $(table.rows[idttoEdit]).find('td:eq(4) select[name="select2Contractors"]').val(); //to take the value of combo
                                                  options[6] = dd.join(',');
                                                //  var text2 = selectedContractors; //to take the value of combo
                                                var selections2 =  $(table.rows[idttoEdit]).find('td:eq(4) select[name="select2Contractors"]').select2('data');

                                                var selectedContractors = [];
                                                var m = 0;
                                                selections2.forEach(function(value){
                                                  selectedContractors.push(selections2[m].text);
                                                  m++;
                                                });


                                                let string2 = selectedContractors.toString();
                                                options[7] = string2;
                                                $(table.rows[idttoEdit]).find('td:eq(4) input[name="select2Contractors"]').val(string2);
                                              }
                                              if (i == 7) {
                                                  // get the selected country from current <tr>
                                                  var d = $(table.rows[idttoEdit]).find('td:eq(7) select[name="select2Customers"]').val(); //to take the value of combo
                                                  options[8] = d.join(',');

                                                  var selections =  $(table.rows[idttoEdit]).find('td:eq(7) select[name="select2Customers"]').select2('data');
                                                  var selectedCustomers = [];
                                                  var m = 0;
                                                  selections.forEach(function(value){
                                                    selectedCustomers.push(selections[m].text);
                                                    m++;
                                                  });


                                                  let string = selectedCustomers.toString();
                                                  options[9] = string;
                                                  $(table.rows[idttoEdit]).find('td:eq(8) input[name="select2Customers"]').val(string);

                                              }
                                              if (i == 3) {
                                                  // get the selected country from current <tr>
                                                  // get the selected country from current <tr>
                                                  var d = $(table.rows[idttoEdit]).find('td:eq(3) select[name="select2leadby"]').val(); //to take the value of combo
                                                  options[10] = d.join(',');

                                                  var selections =  $(table.rows[idttoEdit]).find('td:eq(3) select[name="select2leadby"]').select2('data');
                                                  var selectedLeaders = [];
                                                  var m = 0;
                                                  selections.forEach(function(value){
                                                    selectedLeaders.push(selections[m].text);
                                                    m++;
                                                  });


                                                  let string = selectedLeaders.toString();
                                                  options[11] = string;
                                                  $(table.rows[idttoEdit]).find('td:eq(3) input[name="select2leadby"]').val(string);


                                              }
                                              if(i == 2){
                                                options[1] =$(table.rows[idttoEdit]).find('td:eq(2)').html();

                                              }


                                              j++;

                                          }
                                          editedRecords.push(options);
                                          console.log("edited records are "+ options);

                                }

                    }
            }
        var myjsonC = [];
        var myjsonE = [];
        var myjsonO = [];
        var selectedTeam;
function fetchSelectCustomers(){
return $.ajax({
  url: '../dist/php/fetchSelect.php'
  , type: 'POST'
  , data: {
  action: "fetchSelectCustomers"
  }
  , success: function (data) {

  if (data != -1){
  var myObj = JSON.parse(data);
  var m = 0;
  myObj.forEach(row => {
  myjsonC[m] = {id:row.customerid,text:row.customerName}; //convert numbers to ["1077","1022"]
  m++;
  });
}
}
});

}
function fetchSelectContractors(){
return $.ajax({
  url: '../dist/php/fetchSelect.php'
  , type: 'POST'
  , data: {
  action: "fetchSelectContractors"
  }
  , success: function (data) {

  if (data != -1){
  var myObj = JSON.parse(data);
  var m = 0;
  myObj.forEach(row => {
  myjsonO[m] = {id:row.customerid,text:row.customerName}; //convert numbers to ["1077","1022"]
  m++;
  });
}
}
});

}
function fetchSelectEmployees(){
  return $.ajax({
  url: '../dist/php/fetchSelect.php'
  , type: 'POST'
  , data: {
  action: "fetchSelectEmployees"
  }
  , success: function (data) {

  if (data != -1){
  var myObj = JSON.parse(data);
  var m = 0;
  myObj.forEach(row => {
  myjsonE[m] = {id:row.id,text:row.name}; //convert numbers to ["1077","1022"]
  m++;
  });

}
}
  });
}

        var array = []; // initialize array to be used inside file upload
        $(document).ready(function () {
          $.when(fetchSelectCustomers(),fetchSelectEmployees(),fetchSelectContractors()).done(function(a1,a2,a3){ ///call all select together
        //    $('.select2').select2()
          defineSelect2();
});

            $('#joinDate').datepicker({
                format: 'yyyy-mm-dd'
            });
            // defineTableWithCounter('#projectTable');

          });



        $(document).on('click', '.deletebtn', function (event) {
          event.preventDefault();
            var deletedItem;
            var deleteidt = $(this).data("id1");

            var tableName = $(this).data("id3");
            var table = document.getElementById("" + tableName + "");
            var t = defineTable("#" + tableName + "");
            var j = 0;
            var table = document.getElementById("" + tableName + "");
            var isFoundinAddRecords = false;
            addRecordsRowsNo.forEach(function (addSingleRecordNo) {
                if (addSingleRecordNo == deleteidt) {
                    console.log('is exit before delete please from added records');
                    const index = addRecordsRowsNo.indexOf(addSingleRecordNo);
                    if (index > -1) {
                        addRecordsRowsNo.splice(index, 1);
                        //  event.target.closest('tr').remove();
                      //  t.rows[deleteindex].remove(); //to remove row
                      isFoundinAddRecords = true;
                    }
                }
                j++;
            });
            if (isFoundinAddRecords){
              t.row(['#'+ deleteidt]).remove().draw();
              isFoundinAddRecords = false;
              return;
            }
                    deletedItem = $(this).data("id2");

            let action = "DeleteOneItem";
            if (deletedItem != null && deletedItem != 'undefined') {
              var tableNameData;
              if (tableName == "projectTable"){
  tableNameData = "projectTable";
              }

                $.ajax({
                    url: '../dist/php/projects.php'
                    , type: 'POST'
                    , data: {
                        deletedItem
                        , action
                        , tableNameData
                    }
                    , //contentType: "application/json",
                    success: function (data) {
                        if (data == 0) {
                          maessageResponse = 'تم الحذف بنجاح';
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                            t.row(['#'+ deleteidt]).remove().draw(true);
                            event.target.closest('tr').remove();

                        }
                        else {
                            maessageResponse = 'لم يتم الحذف بنجاح';
                            Swal.fire({
                                position: 'center'
                                , type: 'error'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 1500
                            })
                        }
                    }
                });
            }
        });
        $(document).on('click','.deleteSelect', function () { //to click only one time to insert records and update edited values only
            var tableName = $(this).data("id1");
            var action = 'deleteMultibleRecords';
            console.log('deleteAllisCalled');
            var deleteItemsData = getChechedValues(tableName);
            console.log(deleteItemsData + 'deleteItemsData');
            if(tableName == "projectTable"){
              /////////begin of procesing delete records rows number data
              var tableNameData = "projectTable";
              $.ajax({
                  url: '../dist/php/projects.php'
                  , method: "POST"
                  , data: {
                      deleteItemsData: deleteItemsData
                      , action: action
                      , tableNameData: tableNameData
                  }
                  , dataType: "text"
                  , success: function (data) { //the data return in json format from db to be used in the modal.
                      deleteItemsData = [];
                      if (data == 0) {
                          maessageResponse = 'تم حذف العناصر المحددة بنجاح'
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                          var t = $('#projectTbody').html('');
                          showsettingtbody("","projectTable");
                      }
                  }
              });


            }
          });
    $(document).on('click', 'tr:not(:has(".editbtn") , :has("th"), :has(".dataTables_empty"),:has("a"))', function () {
        var currentID = $(this)[0].rowIndex;
        $(this).attr('contenteditable', 'true');
        var el = $(this);
        el.focus();
        $(this).blur(endEdition);
    });
showsettingtbody("","projectTable");
function prependZero(number) {
  number++;
    if (number <= 9) return "000" + number;
    else if (number <= 99) return "00" + number;
    else if (number <= 999) return "0" + number;
    else return number;
}
function extractNumberFromText(string){
  var matches = string.match(/(\d+)/);
  if (matches){
    return matches[0];
  }
}
    function handlefetchSelectCustomers(){
    return myjsonC;
    }
    function handlefetchSelectEmployees(){
    return myjsonE;
    }
    function handlefetchSelectContractors(){
    return myjsonO;
    }
    function defineSelect2(){
    //   $('.select2').select2()

        $('.select2Customers').select2({
          data:handlefetchSelectCustomers(),
        multiple: true,
        });
        $('.select2leadby').select2({
          data:handlefetchSelectEmployees(),
        multiple: true,
        });

        $('.select2TeamMembers').select2({
          data:handlefetchSelectEmployees(),
        multiple: true,
        });
        $('.select2Contractors').select2({
          data:handlefetchSelectContractors(),
        multiple: true,
        });


        $('.select2Contractors').change(function () {
        console.log("select2 is filled");
          var selections = $('.select2Contractors').val();
          selectedContractors = selections;
        });
        $('.select2leadby').change(function () {
        console.log("select2 is filled");
          var selections = $('.select2Contractors').val();
          selectedLeaders = selections;
        });

        $('.select2TeamMembers').change(function () {
        console.log("select2 is filled");
          var selections = $('.select2TeamMembers').val();
          selectedTeam = selections;
        });
        // $('.selectLeaders').change(function () {
        // console.log("select2 is filled");
        //   var selections = $('.selectLeaders').val();
        //   selectedCustomers = selections;
        // });

        $('.select2Customers').change(function () {
        console.log("select2 is filled");
          var selections = $('.select2Customers').val();
          selectedCustomers = selections;
        });


    }
    $('.addbutton').click(function () { //to click only one time
      var tableName = $(this).data("id1");
      var rowCount;
      var sel;
      if (tableName == 'projectTable') {

      sel = $('select[name="projectTable_length"] option:selected').val();
      rowCount = $('#projectTable tr').length;
    }
      var selint = parseInt(sel);
      var diff;

    if(addRecordsRowsNo.length > 0){
      if(rowCount > addRecordsRowsNo.length ){
      diff = rowCount - addRecordsRowsNo.length; //10 displayed records - 4 which is added = 6 the real exit records in the table.
    }else{
      diff = addRecordsRowsNo.length - rowCount;
    }
      if(addRecordsRowsNo.length + diff == sel){
maessageResponse = 'لاتستطيع الإضافة قبل حفظ البيانات المضافة حالياً';
Swal.fire({
  position: 'center'
  , type: 'error'
  , title: maessageResponse
  , showConfirmButton: false
  , timer: 1500
})
return;
}
}
            if (tableName == 'projectTable') {
              //these all calulcation because of the integrity of request counter
                var t =  defineTable('#projectTable');
                var table = document.getElementById("projectTable");
                var counter = t.rows().count() + 1; //countAllRows
              var rowCount = $('#projectTable tr').length; //to count of current table

              if (rowCount >= sel){
        var diff =   counter - rowCount ;
        console.log(diff+'diff');
            if(diff > 1){
              maessageResponse = 'لا تستطيع الإضافة انتقل الى الصفحة التالية لسلامة البيانات';
              Swal.fire({
                  position: 'center'
                  , type: 'error'
                  , title: maessageResponse
                  , showConfirmButton: false
                  , timer: 1500
              })

              return;
            }
          }
          var reqcounter;
          if (counter == 1){ //th consider as row
            reqcounter = extractNumberFromText("proj000");
          } else{
            reqcounter = extractNumberFromText($('#projectTbody tr:last-child').find('td:eq(1)')[0].innerText);

          }

                var row = t.row.add([
                    counter
                    , 'proj' + prependZero(reqcounter)
                    , ''
                    , ''
                    , ''
                    , '0'
                    , ''
                    , ''
                    , ''
                    , '<button class="deletebtn btn btn-danger btn-sm" data-id1="' + counter + '" data-id3="projectTable">حذف</button>'
                ]).draw(false).node();
               row.id = counter;

               $(row).find('td:eq(3)').html('<input type="text" name="select2leadby" value=""><br><select class="select2leadby" name="select2leadby" multiple="multiple" data-placeholder="اختر القائدين" style="width: 150px;"></select>');


               $(row).find('td:eq(4)').html('<input type="text" name="select2TeamMembers" value=""><select class="select2TeamMembers" name="select2TeamMembers" multiple="multiple" data-placeholder="اختر الموظفين" style="width: 150px;"></select></br></br><input type="text" name="select2Contractors" value=""><select class="select2Contractors" name="select2Contractors" multiple="multiple" data-placeholder="اختر المقاولين" style="width: 150px;"></select>');

             //      $(row).find('td:eq(5)').html('<select class="select2Contractors" multiple="multiple" data-placeholder="اختر المقاولين" style="width: 300px;"></select>');
               $(row).find('td:eq(7)').html('<input type="text" name="select2Customers" value=""><br><select class="select2Customers" name="select2Customers" multiple="multiple" data-placeholder="اختر العملاء" style="width: 150px;"></select>');

                                        // $(row).find('td:eq(8)').html('<button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="projectTable"  data-id4="'+rowd.teamemp+'" data-id5="'+rowd.teamcontra+'" data-id6="'+rowd.customers+'" data-id7="'+rowd.leadby+'" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="projectTable" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

                addRecordsRowsNo.push(row.id);
                t.page('last').draw('page');
                defineSelect2();
                $('html, body').animate({
                    scrollTop: $(row)[0].offsetTop
                }, 500);
                ///var currentTD = t.rows(counter).find('td');
            }
          });
        /*
         * -------
         * Here we will create multible select, daterange,
         */
        $(function () {
            //Initialize Select2 Elements
            //$('.select2').select2()

                //Money Euro
            $('[data-mask]').inputmask()
                //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()]
                        , 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')]
                        , 'Last 7 Days': [moment().subtract(6, 'days'), moment()]
                        , 'Last 30 Days': [moment().subtract(29, 'days'), moment()]
                        , 'This Month': [moment().startOf('month'), moment().endOf('month')]
                        , 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                    , startDate: moment().subtract(29, 'days')
                    , endDate: moment()
                }, function (start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                })
                //Bootstrap Duallistbox
        })
        $("a#vert-tabs-profile-tab").hide();


        function getChechedValues(tableName) {
            var selectedChecked = [];
            var t = $("#" + tableName + "").DataTable({
                retrieve: true
            });

            var filteredInputsCheckboxes = $("#" + tableName + "").find("input[name='checkboxtd']:checked");
            $.each($(filteredInputsCheckboxes), function () {
                selectedChecked.push($(this).val());
                console.log($(this).val());
            //    t.row($(this).closest('tr')).remove().draw(); //to remove row
            });
            //how to look for a specific cell in datatable jquery t.cell(['#12'],0).data() 0 is index , #12 is number of row id

            //console.log("My selected values are: " + selectedChecked);
            return selectedChecked;
        }

    </script>
