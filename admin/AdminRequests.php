<?php
session_start();
if ($_SESSION['levelid']) {
    //successfully login
} else {
    header("Location: ../login.html");
}
?>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>إدارة الطلبات</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Select2 for select multible -->
        <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="../dist/css/modalStyle.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- Google Font: Source Sans Pro -->
        <!-- DataTables styles with filters -->
        <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
        <!-- SweetAlert2 and Toast -->
        <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <link rel="stylesheet" href="../dist/css/responsiveTable.css">
        <link rel="stylesheet" href="../dist/css/blogModal.css">
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> -->
           <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

      </head>
    <style media="screen">
        #description {
            background: url('../dist/img/ynxjD.png') repeat-y;
            font: normal 14px verdana;
            line-height: 25px;
            padding: 2px 10px;
            border: solid 1px #ddd;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
        }

        /* .modal {
            position: absolute;
            overflow: scroll;
        } */

        .documentdeletespan {
            display: inline-block;
            margin-right: 10px;
        }

        #documentsu {
            display: flex;
            column-gap: 25px;
            margin-top: 0em;
        }

        #documentsu>div {
            flex: 1 1 0px;
            text-align: center;
            border: 4px solid white;
        }

        article {
            width: 100%;
            display: flex;
            align-items: center;
            text-align: center;
            float: middle;
            justify-content: center;
            padding-top: 2%;
            padding-bottom: 15%;
        }

        checkbox {
            width: 48px;
            height: 48px;
        }
        .responsive {
          display: flex;
          flex-direction: column;
      width: 70%;
      padding: 7.5;

      }
      .responsive {
        display: flex;
        flex-direction: column;
    width: 70%;
    padding: 7.5;

    }
      @media screen and (max-width: 600px) {
        .responsive {
          display: flex;
          flex-direction: column;
      width: 100%;
      padding: 7.5;

      }
}
    </style>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-sm-inline-block">
            <a href="../index.php" class="nav-link">البداية</a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown al">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments" id="cc"></i>
              <span class="badge badge-danger navbar-badge" id="countAlert"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

          <div class="dropdown-divider"></div>
                                          </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item">
              <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="../index.php" class="brand-link">
          <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
          <span class="brand-text font-weight-light">PIONEER</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a  id="userName" href="#" class="d-block"></a>
              <a href="#" id="userStatus"></a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                   <li class="nav-item">
                     <a href="../index.php" class="nav-link">
                       <i class="nav-icon fas fa-tachometer-alt"></i>
                       <p>
                         لوحة التحكم
                       </p>
                     </a>
                   </li>


              <li class="nav-item">
                <a href="../employees/profile.php" class="nav-link">
                  <i class="nav-icon fas fa-id-card-alt"></i>
                  <p>
                    البروفايل
                  </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../employees/requests.php" class="nav-link">
                  <i class="nav-icon fas fa-list-ol"></i>
                  <p>
                    الطلبات
                  </p>
                </a>
              </li>



              <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    أقسام الإدارة المتوسطة
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../midEmployees/projects.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم المشاريع</p>
                    </a>
                  </li>

                </ul>

              </li>



              <li class="nav-item has-treeview opened menu-open" id="admin-section" style="display:none;">
                <a href="#" class="nav-link active">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                     أقسام الإدارة العليا
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/employees.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم الموارد البشرية</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/others.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم العملاء والمقاولين</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/AdminRequests.php" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم خصائص المعاملات</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">

                <li class="nav-item">
    <a href="../sms/form.php" class="nav-link">
    <i class="nav-icon fas fa-sms"></i>
    <p>
    SMS
    </p>
    </a>
    </li>
    </ul>

              </li>
              <li class="nav-item">
            <a href="../employees/news.php" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
            آخر الأخبار
            <span class="badge badge-info right" id="blogsCounting"></span>
            </p>
            </a>
            </li>

              <li class="nav-item">
     <a id="logout" class="nav-link">
       <i class="nav-icon  ion-log-out"></i>
       <p>خروج</p>
     </a>
     </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                              <h1>الطلبات</h1> </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item active">طلبات المستخدم</li>
                                    <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <!-- Profile Image -->
                                <div class="card card-primary card-outline">
                                    <div class="card-body box-profile">
                                        <div class="text-center"> <img id="imageProfile" class="profile-user-img img-fluid img-circle" src="" alt="User profile picture"> </div>
                                        <h3 class="profile-username text-center"></h3>
                                        <p class="text-muted text-center"><span id="jobTitle-department"></span></p>
                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item"> <b>الطلبات الجديدة</b>
                                                <a class="float-right" id="new">
                                                      </a>
                                            </li>
                                            <li class="list-group-item"> <b>الطلبات المعالجة</b>
                                                <a class="float-right" id="handeled">

                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                            <div class="responsive">
                                <div class="card card-primary card-tabs">
                                    <div class="card-body">
                                      <!-- here subtab of requests begin -->
                                                <ul class="nav nav-tabs" id="custom-content-tab" role="tablist">
                                                    <li class="nav-item"> <a class="nav-link active" id="custom-content1-settings-tab" data-toggle="pill" href="#custom-content1-settings" role="tab" aria-controls="custom-content1-settings-tab" aria-selected="true">الطلبات</a> </li>
                                                    <li class="nav-item"> <a class="nav-link" id="custom-content2-settings-tab" data-toggle="pill" href="#custom-content2-settings" role="tab" aria-controls="custom-content2-settings-tab" aria-selected="false">تاريخ الطلبات</a> </li>
                                                    <li class="nav-item"> <a class="nav-link" id="custom-content3-settings-tab" data-toggle="pill" href="#custom-content3-settings" role="tab" aria-controls="custom-content3-settings-tab" aria-selected="false">أنواع الطلبات</a> </li>
                                                    <li class="nav-item"> <a class="nav-link" id="custom-content4-settings-tab" data-toggle="pill" href="#custom-content4-settings" role="tab" aria-controls="custom-content4-settings-tab" aria-selected="false">سير عمل الطلب</a> </li>
                                                </ul>
                                                <div class="tab-content" id="custom-content-tabContent">
                                                    <div class="tab-pane fade show active" id="custom-content1-settings" role="tabpanel" aria-labelledby="custom-content1-settings-tab">
                                                        <br>
                                                        <!-- Main content -->
                                                        <section class="content">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <!-- put table here -->
                                                                    <!-- table-->
                                                                    <!-- put table here -->
                                                                    <!-- table-->
                                                                    <div style="overflow-x:auto;">
                                                                        <table id="settingstable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                            <thead>
                                                                                <tr>
                                                                                  <th><br>#
                                                                                  <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                                    <th width="10%">مرجع الطلب</th>
                                                                                    <th width="20%">نوع الطلب</th>
                                                                                    <th width="20%">صاحب الطلب</th>
                                                                                    <th width="35%">المستندات</th>
                                                                                    <th width="35%">تاريخ الانشاء</th>
                                                                                    <th width="35%">وصف الطلب</th>
                                                                                    <th width="35%">حالة الطلب</th>
                                                                                    <th></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody id="tbodysettingstable">

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <!-- /.table 1 content -->
                                                    </div>
                                                    <div class="tab-pane fade show" id="custom-content2-settings" role="tabpanel" aria-labelledby="custom-content2-settings-tab">

                                                        <br>
                                                        <div style="overflow-x:auto;" id="settingstableOneDiv">
                                                            <table id="settingstableOne" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th width="5%">رقم المرجع</th>
                                                                        <th width="10%">الاسم</th>
                                                                        <th width="20%">التصرف المراد</th>
                                                                        <th width="35%">المستندات</th>
                                                                        <th width="45%">تاريخ الإنشاء</th>
                                                                        <th width="35%">التاريخ المستحق</th>
                                                                        <th width="35%">الملاحظات</th>
                                                                        <th width="35%">مرسل إلى</th>
                                                                        <th width="35%">الحالة</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="tbodysettingsOne"> </tbody>
                                                            </table>
                                                        </div>
                                                        <div id="secondcontent-settingstableOne"> </div>
                                                    </div>
                                                    <div class="tab-pane fade show" id="custom-content3-settings" role="tabpanel" aria-labelledby="custom-content3-settings-tab">
                                                        <br>
                                                        <div style="overflow-x:auto;">
                                                            <table id="settingstableTwo" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="no-sort">
                                                                            <button data-id1="settingstableTwo" class="addbutton btn btn-s btn-success" type="button" name="button">+</button>
                                                                            <br>#
                                                                            <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                        <th width="10%">كود الطلب</th>
                                                                        <th width="20%">نوع الطلب</th>
                                                                        <th width="20%">أولوية الطلب</th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="setting2tbody"> </tbody>
                                                            </table>
                                                            <button data-id1="settingstableTwo" class="deletebuttonAll btn btn-danger" type="button" name="button">حذف المحدد</button>
                                                            <button data-id1="settingstableTwo" class="savebutton btn btn-s btn-success ion-download" type="button" name="button">حفظ المضاف</button>
                                                            <!-- <button id="report" data-id1="settingstableTwo" class="btn btn-s btn-primary ion-download" type="button" name="button"></button> -->
                                                            <!-- /.table 1 content -->
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade show" id="custom-content4-settings" role="tabpanel" aria-labelledby="custom-content4-settings-tab">
                                                        <br>
                                                        <div style="overflow-x:auto;">
                                                            <table id="settingstableThree" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="no-sort">
                                                                            <button data-id1="settingstableThree" class="addbutton btn btn-s btn-success" type="button" name="button">+</button>
                                                                            <br>#
                                                                            <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                        <th width="20%">نوع الطلب</th>
                                                                        <th width="20%"> ترتيب العملية</th>
                                                                        <th width="20%">يرسل إلى </th>
                                                                        <th width="20%">الأمر المطلوب </th>
                                                                        <th width="20%"> عدد الأيام المتوقع </th>
                                                                        <th></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="setting3tbody"> </tbody>
                                                            </table>
                                                            <button data-id1="settingstableThree" class="deletebuttonAll btn btn-danger" type="button" name="button">حذف المحدد</button>
                                                            <button  data-id1="settingstableThree" class="savebutton btn btn-s btn-success ion-download" type="button" name="button">حفظ المضاف</button>
                                                            <!-- /.table 1 content -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
                <!-- UnifedModals -->
                <div class="modal custom fade" id="blogModal">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">

                          <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
                           <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                          </button> -->

                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                           </div>
                        <div class="modal-body">

                 <div class="card bg-light">
                     <!-- Grid row -->
                     <div class="row">

                       <!-- Grid column -->
                       <div class="col-12">

                         <!-- Exaple 1 -->
                         <div class="card example-1 scrollbar-ripe-malinka">
                           <div class="card-body"  id="MainDivPostsm">

                           </div>
                         </div>
                         <!-- Exaple 1 -->

                       </div>
                       <!-- Grid column -->



                     </div>
                     <!-- Grid row -->

                   </div>
                 </div>
                </div>

                </div>
                </div>
                <!-- ./blog Modal -->

                <div class="custom modal fade" id="chatModal">
                 <div class="modal-dialog">
                   <div class="modal-content">
                         <div class="modal-header">

                           <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
                            <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                             <i class="fas fa-times"></i>
                           </button> -->

                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                            </div>

                     <div class="modal-body">
                 <!-- /.card-header -->
                 <div class="card direct-chat direct-chat-primary">
                 <div class="card-body">
                   <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
                <div class="image">
                <img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                <a href="#" class="d-block"  id="receivernamem"></a>
                <a href="#" id="statusr2"></a>
                </div>
                </div>
                <hr>
                   <!-- Conversations are loaded here -->
                   <div class="direct-chat-messages" id="direct-chat-messages2">


                   </div>
                   <!--/.direct-chat-messages-->

                   <!-- Contacts are loaded here -->
                   <div class="direct-chat-contacts" >
                     <ul class="contacts-list" id="direct-chat-contacts">
                       <!-- End Contact Item -->
                     </ul>
                     <!-- /.contacts-list -->
                   </div>
                   <!-- /.direct-chat-pane -->
                 </div>
                 <!-- /.card-body -->
                 <div class="card-footer">
                   <form action="#" method="post" id="myForm2">
                     <div class="input-group">
                       <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
                       <input type="hidden" name="action" value="sendChat" class="form-control">
                       <span class="input-group-append">
                         <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
                       </span>
                     </div>
                   </form>
                 </div>
                 <!-- /.card-footer-->
                </div>
                </div>
                </div>

                </div>
                </div>
                <!--/.direct-chat modal -->
                <!-- /.Unified Modals -->


                <!-- /.control-sidebar -->
                <div class="custom modal fade" id="recordModal">
                    <div class="modal-dialog">
                        <form id="recordForm" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">تحرير السجل</h4> </div>
                                <!-- /.modal-header -->
                                <div class="modal-body">
                                    <div class="form-group" id="selectAssignedtoDiv">
                                        <label for="select2Emp" class="control-label">يرسل إلى: <small>(مطلوب)</small></label>
                                        <select class="select2Emp" multiple="multiple" data-placeholder="اختر الموظفين" style="width: 100%;"> </select>
                                    </div>
                                    <div class="form-group" id="actionreqInputDiv">
                                        <label for="actionRequiredm" class="control-label">التصرف المطلوب: <small>(مطلوب)</small></label>
                                        <input type="text" name="actionRequiredm" id="actionRequiredm" value=""> </div>
                                    <div class="form-group" id="durationInputDiv">
                                        <label for="duration" class="control-label">عدد الأيام: <small>(مطلوب)</small></label>
                                        <input type="text" name="duration" id="duration" value="" data-inputmask='"mask": "99"' data-mask required> </div>
                                    <div class="form-group" id="returncheckboxDiv">
                                        <label for="sourceEmpck" class="control-label">إرجاع إلى صاحب الطلب<small>(اختياري)</small></label>
                                        <input type="checkbox" class="form-control" name="sourceEmpck" id="sourceEmpck" value="" style="height: 30px; width: 30px;"> </div>
                                    <div class="form-group">
                                                  <input type="hidden" id="reqid" name="reqid" class="form-control" readonly="readonly" value="">

                                    </div>

                                    <div class="form-group">
                                        <label for="description" class="control-label">الوصف :<small>(اختياري)</small></label>
                                        <textarea name="description" id="description" class="form-control" placeholder="اكتب الوصف هنا"> </textarea>
                                    </div>

                                    <div id="selectFileDiv">

                                <form method='post' action='' enctype="multipart/form-data" id="selectFileForm"> اختار ملف :
                                    <input type='file' name='userfile[]' id='userfile' class='form-control' multiple>
                                    <input type='hidden' class='btn btn-info' value='Upload' id='btn_upload'>
                                  </form>
                                </div>

                                  <div id="documentsm"> </div>

                                    <div class="form-group" id="checkboxDiv">
                                        <label for="completeck" class="control-label">اكمال<small>(اختياري)</small></label>
                                        <input type="checkbox" class="form-control" name="completeck" id="completeck" value="" style="height: 30px; width: 30px;"> </div>
                                </div>
                                <!-- /.modal-body -->
                                <div class="modal-footer">
                                    <!-- the value taken id value to update later-->
                                    <input type="hidden" name="actionm" id="actionm" value="" />
                                    <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                                    <input type="submit" name="save" id="save" class="btn btn-info" value="Save" /> </div>
                            </div>
                            <!-- /.modal-content -->
                        </form>
                    </div>
                    <!-- /.modal-dialog -->
                </div>
              </div>
<!-- ./wrapper -->

                <!-- here should end all of cardbody -->
                <!--*************************************************************-->
                <!-- ************************************************8 -->

                <footer class="main-footer">
                  <strong>Copyright &copy; 2020 PIONEER</strong>
                  All rights reserved.
                  <div class="float-right d-none d-sm-inline-block">
                    <b>Version</b> 3.0.0
                  </div>
                </footer>
    </body>

    </html>
    <!-- Select2 -->
    <!-- jQuery -->
    <script src="../plugins/jquery/jquery.min.js"></script>
    <script src="../dist/js/modalStyle.js"></script>
    <script src="../dist/js/unified.js"></script>


    <!-- Bootstrap 4 -->
    <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>

    <!-- AdminLTE for demo purposes -->
    <!-- Select2 -->
    <script src="../plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="../plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>

    <script src="../plugins/char-counter/jquery.charcounter.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 and Toast -->
    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"></script> -->
   <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
   <script  src='../dist/js/pdfmake-master/build/pdfmake.min.js'></script>
   <script  src='../dist/js/pdfmake-master/build/vfs_fonts.js'></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
   <script  src="../dist/js/requestsActions.js"></script>
   <script  src="../dist/js/upload.js"></script>

    <script>
    var stop = false;
    function getChechedValues(tableName) {
        var selectedChecked = [];
        var t = defineTable("#" + tableName + "");

        var filteredInputsCheckboxes = $("#" + tableName + "").find("input[name='checkboxtd']:checked");
        $.each($(filteredInputsCheckboxes), function () {
            selectedChecked.push($(this).val());
          //  t.row($(this).closest('tr')).remove().draw(); //to remove row
        });
        //how to look for a specific cell in datatable jquery t.cell(['#12'],0).data() 0 is index , #12 is number of row id

        //console.log("My selected values are: " + selectedChecked);
        return selectedChecked;
    }
  loadDatas();
  function loadDatas(){
    var action = 'loadSessionData';
    $.ajax({
      url: '../dist/php/sessionData.php',
      type: 'POST',
      data: {
          action: action
      }
      , dataType:"json"
      , success: function(data) {
    //to conver it to Json after contactit as string  to be used together
    $('#imageProfile')[0].src = '../dist/img/'+ ''+currentUserPic+'';

    }
     });
  }
        var count = 0;
        var text = '';
        var arrayhtmlContent = [];
        var dataArray = [];
        var dataArray2 = [];

        // declinedTableDiv
        let firstClickedStrt = false;
        let firstClickedttab = false;
        let selectedEmployees;
        let isClickedstart = false;


        var myhtmlella;
        var myhtmlella2;
        var table;
        var myHistoryHtml = [];

        function handleJSONObject(jsonarrays,tableName) {
            var count = 0;
            var m = 0;
            var j = 0;
            jsonarrays['data1'].forEach(row => {
                count = count + 1;
                // ["a","b","c","d"]
                var arrayd = [];
                var arrayhtml = [];
                var value = row.documents
                var valueString = value.toString();
                var dlink = '../upload/requests/'
                strx = valueString.split(',');
                arrayd = arrayd.concat(strx);
                //formattedDocuments.split(',');
                for (element of arrayd) {
                    arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + element.trim() + '</a>')
                }
                var arraydString = arrayhtml;
                var t;

                if (tableName == "settingstableOne"){
                 t = defineTable('#settingstableOne');
               }
                    //formattedDocuments.split(',');
                var row = t.row.add(['<td class="#">' + count + ' </td>'
                    , '<td class="ref">' + row.reqid + '</td>'
                    , '<td class="namem">' + row.employeeName + '</td>'
                    , '<td class="documentsm">' + arraydString + '</td>'
                    , '<td class="actiondid">' + row.actionRequired + '</td>'
                    , '<td class="dateCreated">' + row.dateCreated + '</td>'
                    , '<td class="deadline">' + row.deadline + '</td>'
                    , '<td class="notes">' + row.notes + '</td>'
                    , '<td class="assignedTom">' + jsonarrays['data2'][j] + '</td>', //for assignedto
                    '<td class="status">' + row.status + '</td>'
                    , '<td></td>'
                , ]).draw().node();
                //$(row).find('td:eq(8)').addClass('assignedTom'); //to add class or mainpulate specific cell
                j++;
                return row;
            });
        }

        function handleJSONObjectTimeline(jsonarrays) {
            var count = 0;
            var j = 0;
            jsonarrays['data1'].forEach(row => {
                count = count + 1;
                if (j == 0) {
                    text = '<div class="time-label"><span class="bg-danger"><i class="far fa-clock"> البداية</i></span></div><div><i class="fas fa-clock bg-warning"></i><div class="timeline-item" style="width: 50%"> <span class="time"><i class="far fa-clock"></i> المتوقع: <span>' + row.durationInDays + '</span> أيام</span><h3 class="timeline-header">' + row.actionStatement + ' <a href="#profileModal" id="assignedTopr">' + jsonarrays['data2'][j] + '</a></h3></div></div>';
                    arrayhtmlContent.push(text);

                }
                if (j > 0 && j < jsonarrays['data1'].length ) {
                    text = '<div><i class="fas fa-clock bg-warning"></i><div class="timeline-item" style="width: 50%"> <span class="time"><i class="far fa-clock"></i> المتوقع: <span>' + row.durationInDays + '</span> أيام</span><h3 class="timeline-header">' + row.actionStatement + ' <a href="#profileModal" id="assignedTopr">' + jsonarrays['data2'][j] + '</a></h3></div></div>';
                    arrayhtmlContent.push(text);

                }
                if (count == jsonarrays['data1'].length) {
                    text = '<div><i class="far fa-flag bg-green"></i></div>';
                    arrayhtmlContent.push(text);
                }
                j++;
                //arrayhtmlContent.push(text);
                text = '';
                return arrayhtmlContent;
            });
        }


        var myjson = [];
function fetchSelect(){
  var action = "fetchSelectRequests";
  return $.ajax({
  url: '../dist/php/fetchSelect.php'
  , type: 'POST'
  , data: {
  action: action
  }
  , success: function (data) {


  if (data != -1){
  var myObj = JSON.parse(data);
  var m = 0;
  myObj.forEach(row => {
  myjson[m] = {id:row.id,text:row.name}; //convert numbers to ["1077","1022"]
  m++;
  });
  //  var jsonarrays = data;
  }

}

  });
}
let result;

function handlefetchSelect(){
result = myjson;
return myjson;
}
function defineSelect2(){
    // the code here will be executed when all four ajax requests resolve.
    // a1, a2, a3 and a4 are lists of length 3 containing the response text,
    // status, and jqXHR object for each of the four ajax calls respectively.
    // wait until the promise returns us a value
    $('.select2Emp').select2({
      data:handlefetchSelect(),
    multiple: true,
    });


    $('.select2Emp').change(function () {
      var selections = $('.select2Emp').val();
      selectedEmployees = selections;
    });


}

        function initializeArrays(){
          addRecords = [];
          addRecordsRowsNo = [];
          editedRecords = [];
        }
        $(document).ready(function () {
          $.when(fetchSelect()).done(function(a1){
            $('.select2').select2()
            defineSelect2();
});
            //    $('.select2').select2()
            var myactiveTab = localStorage.getItem('currentActiveTab');
            if (myactiveTab == 'custom-content3-settings-tab') {
                $('a#custom-content3-settings-tab').click();
                initializeArrays();
            }

            if (myactiveTab == 'custom-content4-settings-tab') {
                $('a#custom-content4-settings-tab').click();


                initializeArrays();
            }


            $('[data-mask]').inputmask() //to run input mask


        });






        var maessageResponse;
        var arrFiles = []; // initialize array to be used when click "upload" button after select a file and when click "upload file" to open uploadModal
        var linktoFileUpload = '../../upload/requests/';
        $(document).ready(function () {


        var selectionsEmployees;

        var idg;
        var reqidg;
        var selectRequestg;
        var assignedToToInsert;
          $(document).on('click', '.ccaction', function () {
        // $('.ccaction').click(function () {
            $('#recordModal').modal('show');
            $('#recordForm')[0].reset();
            $('#actionm').val('AddCustomActions'); //from modal footer to send
            $('.modal-title').html("<i></i> إضافة نشاط مخصص");
            $('#save').val('إرسال');
          // idg = $(this).data("id1");
            reqidg = $(this).data("id2");
            selectRequestg = $(this).data("id3");
            assignedToToInsert = $(this).data("id5");
            document.getElementById("actionRequiredm").setAttribute("required", "");
            document.getElementById("duration").setAttribute("required", "");
            document.getElementById("checkboxDiv").style.display = 'block';
            document.getElementById("selectAssignedtoDiv").style.display = 'block';
            document.getElementById("actionreqInputDiv").style.display = 'block';
            document.getElementById("durationInputDiv").style.display = 'block';
            document.getElementById("returncheckboxDiv").style.display = 'block';
            document.getElementById("selectFileDiv").style.display = 'block';
        });

        $("#recordModal").on('submit', '#recordForm', function (event) {
            event.preventDefault();
            $('#save').attr('disabled', false);
            var modalTitle = document.getElementById("actionm").value;

            if (modalTitle === 'AddCustomActions') {
                var actionRequiredm = document.getElementById("actionRequiredm").value;
                var duration = document.getElementById("duration").value;
                var description = document.getElementById("description").value;
                var action = 'AddCustomActions';
                var atLeastOneIsChecked = $('input[name="sourceEmpck"]:checked').length;
                if (atLeastOneIsChecked > 0) {
                    if (typeof selectedEmployees != "undefined" && selectedEmployees != null && selectedEmployees.length != null && selectedEmployees.length > 0) {
                        selectionsEmployees = selectedEmployees.join(',') + ',' + assignedToToInsert;
                    }
                    else {
                        selectionsEmployees = assignedToToInsert;
                    }
                }
                else {
                    selectionsEmployees = selectedEmployees.join(',');
                }
                var completeIsChecked = $('input[name="completeck"]:checked').length;
                if (completeIsChecked > 0) {
                    checkedValue = '1';
                }
                else {
                    checkedValue = '';
                }


                     var fd = new FormData();
                     var uploadedFiles = document.querySelectorAll("button.documentdelete");
                     var allFilesNames = [];
                     uploadedFiles.forEach((item) => {
                       var el = item.getAttribute('data-filename');
                     allFilesNames.push(el);
                     });
                     //this the uploaded files before
                     fd.append('allFilesNames', allFilesNames);
                             fd.append('description', description);
                             fd.append('idg', idg);
                             fd.append('reqidg', reqidg);
                             fd.append('selectRequestg', selectRequestg);
                             fd.append('action', action);
                             fd.append('checkedValue', checkedValue);
                             fd.append('selectionsEmployees', selectionsEmployees);
                             fd.append('duration', duration);
                             fd.append('actionRequiredm', actionRequiredm);

                $.ajax({
                  url: '../dist/php/ajax_action.php',
                  type: 'post',
                  data: fd
                  ,contentType: false,
                  processData: false,
                  success: function(data) {
                        if (data == 0) {
                                $('#recordModal').modal('hide');
                                $('#recordForm')[0].reset();
                                $('#save').attr('disabled', false);
                                maessageResponse = 'الطلب تم إرساله بشكل صحيح';
                                Swal.fire({
                                    position: 'center'
                                    , type: 'success'
                                    , title: maessageResponse
                                    , showConfirmButton: false
                                    , timer: 3000
                                })
                                window.location.reload();
                            }
                            else if (data == -1) {
                                maessageResponse = 'لم يتم طلبك بشكل صحيح تأكد من البيانات المدخله ثم حاول مرة أخرى';
                                Swal.fire({
                                    position: 'center'
                                    , type: 'error'
                                    , title: maessageResponse
                                    , showConfirmButton: false
                                    , timer: 3000
                                })
                            } else{
                              maessageResponse = data;
                              Swal.fire({
                                  position: 'center'
                                  , type: 'error'
                                  , title: maessageResponse
                                  , showConfirmButton: true
                              })

                            }
                    }
                });
            }
        });

        $("a#custom-content1-settings-tab").on("shown.bs.tab", function (e) {
            localStorage.setItem('currentActiveTab', 'custom-content1-settings-tab');
          //  $('#setting2tbody').innerHTML = '';
          e.preventDefault();
        //  $('#tbodysettingstable').innerHTML = '';
        });
        $("a#custom-content2-settings-tab").on("shown.bs.tab", function (e) {
            localStorage.setItem('currentActiveTab', 'custom-content2-settings-tab');
            //var id = $(e.target).attr("href").substr(1);
            //window.location.hash = id;
            e.preventDefault();
            showsetting2tbody('settingstableOne');

        });
        $("a#custom-content3-settings-tab").on("shown.bs.tab", function (e) {
            localStorage.setItem('currentActiveTab', 'custom-content3-settings-tab');
              e.preventDefault();


        });
        $("a#custom-content4-settings-tab").on("shown.bs.tab", function (e) {
            localStorage.setItem('currentActiveTab', 'custom-content3-settings-tab');
              e.preventDefault();

              showsetting2tbody("settingstableThree");

        });

        $('a#custom-content4-settings-tab').click(function (e) { //to click only one time
            localStorage.setItem('currentActiveTab', 'custom-content4-settings-tab');
            e.preventDefault();
            $(this).tab('show');
            showsetting2tbody("settingstableThree");

          //  alert('hi');
            // store the currently selected tab in the hash value
        });


showsetting2tbody('settingstable');
showsetting2tbody('settingstableOne');
showsetting2tbody('settingstableTwo');
showsetting2tbody('settingstableThree');

        $('.deletebuttonAll').click(function () { //to click only one time to insert records and update edited values only
            var tableName = $(this).data("id1");
            var action = 'deleteMultibleRecords';
            var deleteItemsData = getChechedValues(tableName);
            if(tableName == "settingstableTwo"){
              /////////begin of procesing delete records rows number data
              var tableNameData = "requesttype";
              $.ajax({
                  url: '../dist/php/requests.php'
                  , method: "POST"
                  , data: {
                      deleteItemsData: deleteItemsData
                      , action: action
                      , tableNameData: tableNameData
                  }
                  , dataType: "text"
                  , success: function (data) { //the data return in json format from db to be used in the modal.
                      deleteItemsData = [];
                      if (data == 0) {
                          maessageResponse = 'تم حذف العناصر المحددة بنجاح'
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                          var t = $('#setting2tbody').html('');
                          showsetting2tbody("settingstableTwo");
                      }
                  }
              });


            }
            if(tableName == "settingstableThree"){
              /////////begin of procesing delete records rows number data
              var tableNameData = "requestprocesses";
              $.ajax({
                  url: '../dist/php/requests.php'
                  , method: "POST"
                  , data: {
                      deleteItemsData: deleteItemsData
                      , action: action
                      , tableNameData: tableNameData
                  }
                  , dataType: "text"
                  , success: function (data) { //the data return in json format from db to be used in the modal.
                      deleteItemsData = [];
                      if (data == 0) {
                          maessageResponse = 'تم حذف العناصر المحددة بنجاح'
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                          showsetting2tbody("settingstableThree");

                      }else if(data == -1) {


                      }
                  }
              });


            }

        });
        $('.savebutton').click(function () { //to click only one time to insert records and update edited values only
            /////////begin of procesing add records rows number data
            values = [];
            addRecords = [];
            var tableName = $(this).data("id1");
            var rowCount;
                      if (tableName == 'settingstableTwo') {

                      rowCount = $('#setting2tbody tr').length; //to count of current table
                    }
                    if (tableName == 'settingstableThree') {

                    rowCount = $('#setting3tbody tr').length; //to count of current table
                  }

                    var sel = $('select[name="settingstableTwo_length"] option:selected').val();
                    var selint = parseInt(sel);
                    var realExitRecords;

                    if (rowCount > 0)
                    {
                      realExitRecords = rowCount - addRecordsRowsNo.length;

                    }else{
                      realExitRecords = 0;
                    }
                  var diff;
                  if(rowCount > addRecordsRowsNo.length ){
                  diff = rowCount - addRecordsRowsNo.length; //10 displayed records - 4 which is added = 6 the real exit records in the table.
                }else{
                  diff = addRecordsRowsNo.length - rowCount;
                }
                  if(addRecordsRowsNo.length + diff > sel){
                    maessageResponse = 'لا تستطيع حفظ أكثر من المحدد من الحقول';
                    Swal.fire({
                        position: 'center'
                        , type: 'error'
                        , title: maessageResponse
                        , showConfirmButton: false
                        , timer: 1500
                    })

                    return;
                  }

                  var table = document.getElementById("" + tableName + "");
            if (typeof addRecordsRowsNo != "undefined" && addRecordsRowsNo != null && addRecordsRowsNo.length != null && addRecordsRowsNo.length > 0) {
                //if table name
                if (tableName == 'settingstableTwo') {

                  var options = [];
                  var m = 0;
                  var r = realExitRecords + 1;
                    addRecordsRowsNo.forEach(myFunction);

                    function myFunction(value) {
                      options[m] = new Array(4);
                              for (i = 0; i < 4; i++) {
                                if (i == 3){
                                  var d  = $(table.rows[r]).find('td:eq(3) select').val(); //to take the value of combo
                                  options[m][i] = d;
                                } else{
                                  options[m][i] = table.rows[r].cells[i].innerText;

                                }

                            }

                        m++;
                        r++;
              }
              addRecords.push(options); //assumin we have 4 cells in our example
                    }

                if (tableName == 'settingstableThree') {
                  var options = [];
                  var m = 0;
                  var r = realExitRecords + 1;
                    addRecordsRowsNo.forEach(myFunction);

                    function myFunction(value) {
                      options[m] = new Array(6);
                              for (i = 0; i < 6; i++) { //assumin we have 4 cells in our example
                                  //  var rowNo = table.rows[addRecordsRowssingle].id;
                                  if (i == 3) {
                                      // get the selected country from current <tr>
                                      var d = $(table.rows[r]).find('td:eq(3) select').val(); //to take the value of combo
if (d != null && d != 'undefined' && d != ""){
  let selectValue = d.join(',');
    options[m][i] = selectValue;
}else{
  //to make sure assigned to is not empty
stop = true;
  break;
}


                                  }

                                  else {
                                      options[m][i] = table.rows[r].cells[i].innerText;

                                  }


                              }
                              m++;
                              r++;
                    }
                    addRecords.push(options);

                  //  console.log(addRecords + 'addRecords');
                    if (stop == true){
                    maessageResponse = 'لا بد من التأكد من اختيار المرسل إليه لتكون البيانات سليمة';
                    Swal.fire({
                        position: 'center'
                        , type: 'error'
                        , title: maessageResponse
                        , showConfirmButton: false
                        , timer: 1500
                    })
                    stop = false;
                    window.location.reload();
                    return;
}
                }
            }else{
              maessageResponse = 'لم تضف أي بيانات بعد';
              Swal.fire({
                  position: 'center'
                  , type: 'error'
                  , title: maessageResponse
                  , showConfirmButton: false
                  , timer: 1500
              })
              return;
            }
            //////////////end of processing add records rows number
            var addRecordsData = addRecords;
            var action = 'saveMultibleRecords';
            if (tableName == 'settingstableTwo') {
              console.log(addRecordsData);
                var tableNameData = "requesttype";
                $.ajax({
                    url: '../dist/php/requests.php'
                    , method: "POST"
                    , data: {
                        addRecordsData: addRecordsData
                        , action: action
                        , tableNameData: tableNameData
                    }
                    , success: function (data) { //the data return in json format from db to be used in the modal.
                        // on load of the page: switch to the currently selected tab
                        //  var hash = window.location.hash;
                        if (data == 0) {
                            maessageResponse = 'تم الحفظ بنجاح';
                            Swal.fire({
                                position: 'center'
                                , type: 'success'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 1500
                            })
                            initializeArrays();
                            var t = $('#setting2tbody').html('');
                            showsetting2tbody("settingstableTwo");
                            //  $('a[href=\'' + '#custom-content2-below-settings' + '\']').click();
                            return;
                        }else{
                          maessageResponse = 'لم يتم الحفظ بنجاح تأكد من بعض البيانات المدخلة';
                          Swal.fire({
                              position: 'center'
                              , type: 'error'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                         addRecords = [];
                        }


                    }
                });
            }
            if (tableName == 'settingstableThree') {
                tableNameData = "requestprocesses";
                $.ajax({
                    url: '../dist/php/requests.php'
                    , method: "POST"
                    , data: {
                        addRecordsData: addRecordsData
                        , action: action
                        , tableNameData: tableNameData
                    }
                        , success: function (data) { //the data return in json format from db to be used in the modal.
                            // on load of the page: switch to the currently selected tab
                            //  var hash = window.location.hash;
                            if (data == 0) {
                                maessageResponse = 'تم الحفظ بنجاح';
                                Swal.fire({
                                    position: 'center'
                                    , type: 'success'
                                    , title: maessageResponse
                                    , showConfirmButton: false
                                    , timer: 2000
                                })
                                initializeArrays();
                                var t = $('#setting3tbody').html('');
                                showsetting2tbody("settingstableThree");
                                defineTable('#settingstableThree');
                            //    var t = $('#setting3tbody').html('')

                                //  $('a[href=\'' + '#custom-content2-below-settings' + '\']').click();
                              //  return;
                            }else{
                              maessageResponse = 'لم يتم الحفظ بنجاح تأكد من بعض البيانات المدخلة';
                              Swal.fire({
                                  position: 'center'
                                  , type: 'error'
                                  , title: maessageResponse
                                  , showConfirmButton: false
                                  , timer: 1500
                              })
                             addRecords = [];
                            }


                        }
                });
            }
        });

        function prependZero(number) {
          number++;
            if (number <= 9) return "000" + number;
            else if (number <= 99) return "00" + number;
            else if (number <= 999) return "0" + number;
            else return number;
        }
        function extractNumberFromText(string){
          var matches = string.match(/(\d+)/);
          if (matches){
            return matches[0];
          }
        }
        $('.addbutton').click(function () { //to click only one time
          var sel = $('select[name="settingstableTwo_length"] option:selected').val();
          var selint = parseInt(sel);
          var diff;
          var rowCount;
          var tableName = $(this).data("id1");
          if (tableName == 'settingstableTwo') {
rowCount = $('#setting2tbody tr').length;
          }
          if (tableName == 'settingstableThree') {
rowCount = $('#setting3tbody tr').length;
          }
        if(addRecordsRowsNo.length > 0){
          if(rowCount > addRecordsRowsNo.length ){
          diff = rowCount - addRecordsRowsNo.length; //10 displayed records - 4 which is added = 6 the real exit records in the table.
        }else{
          diff = addRecordsRowsNo.length - rowCount;
        }
          if(addRecordsRowsNo.length + diff == sel){
  maessageResponse = 'لاتستطيع الإضافة قبل حفظ البيانات المضافة حالياً';
  Swal.fire({
      position: 'center'
      , type: 'error'
      , title: maessageResponse
      , showConfirmButton: false
      , timer: 1500
  })
  return;
}
}
                if (tableName == 'settingstableTwo') {
                  //these all calulcation because of the integrity of request counter
                    var t =  defineTable('#settingstableTwo');
                    var table = document.getElementById("settingstableTwo");
                    var counter = t.rows().count() + 1; //countAllRows
                  var rowCount = $('#setting2tbody tr').length; //to count of current table

                  if (rowCount >= sel){
            var diff =   counter - rowCount ;
                if(diff > 1){
                  maessageResponse = 'لا تستطيع الإضافة انتقل الى الصفحة التالية لسلامة البيانات';
                  Swal.fire({
                      position: 'center'
                      , type: 'error'
                      , title: maessageResponse
                      , showConfirmButton: false
                      , timer: 1500
                  })

                  return;
                }
              }
              var reqcounter;
              if (counter == 1){ //th consider as row
                reqcounter = extractNumberFromText("req000");
              } else{
                reqcounter = extractNumberFromText($('#setting2tbody tr:last-child').find('td:eq(1)')[0].innerText);

              }


                    var row = t.row.add([
                        counter
                        , 'req' + prependZero(reqcounter)
                        , ''
                        , '0'
                        , '<button class="deletebtn btn btn-danger btn-sm" data-id1="' + counter + '" data-id3="settingstableTwo">Delete</button>'
                    ]).draw(false).node();
                   row.id = counter;
                   $(row).find('td:eq(3)').html('<select class="selectPriority" style="width: 300px;" ><option value="0">منخفض</option><option value="1">متوسط</option><option value="2">عالي</option></select>');

                    addRecordsRowsNo.push(row.id);
                    t.page('last').draw('page');
                    $('html, body').animate({
                        scrollTop: $(row)[0].offsetTop
                    }, 500);
                    ///var currentTD = t.rows(counter).find('td');
                }
                if (tableName == 'settingstableThree') {
                  let myselection = getChechedValues('settingstableTwo');//because the falue is taken from these checkbox
                  if (myselection == null || myselection == "undefined" || myselection == "") {
                    maessageResponse = 'لم تقم باختيار الطلب بعد';
                    Swal.fire({
                        position: 'center'
                        , type: 'error'
                        , title: maessageResponse
                        , showConfirmButton: false
                        , timer: 1500
                    })
                    return;
                  }
                    var t = defineTable('#settingstableThree');
                    //    defineSelect2();
                    var counter = t.rows().count() + 1;
                    var rowCount = $('#setting3tbody tr').length; //to count of current table
              var diff = counter - 1 - rowCount;
                  if(diff == 1){
                    maessageResponse = 'لا تستطيع الإضافة انتقل الى الصفحة التالية لسلامة البيانات';
                    Swal.fire({
                        position: 'center'
                        , type: 'error'
                        , title: maessageResponse
                        , showConfirmButton: false
                        , timer: 1500
                    })
                    return;
                  }
                    var table = document.getElementById("settingstableThree");
                    var initcounter = t.rows().count();
                    if (typeof myselection != "undefined" && myselection != null && myselection != [] && myselection.length != null && myselection.length > 0) {
                        var counter = t.rows().count() + 1;
                        var row = t.row.add([
                            counter
                            , myselection
                            , ''
                            , ''
                            , ''
                            , '1'
                            , ''
                        ,]).draw(false).node();
                        //  text = '<tr><td class="#"></td><td class="selectRequest">' + row.requestTCode + '</td><td class="namem">' + row.name + '</td><td class="priority">' + row.priority + '</td><td></td></tr>';
                        $(row).find('td:eq(3)').html('<select class="select2Emp" multiple="multiple" data-placeholder="اختر الموظفين" style="width: 300px;">');
                        $(row).find('td:eq(3)').css('width', '300px');
                        $(row).find('td:eq(6)').html('<button class="deletebtn btn btn-danger btn-sm" data-id1="' + counter + '" data-id3="settingstableThree">Delete</button>');
                        addRecordsRowsNo.push(counter);
                      row.id = counter;
                      //  console.log(row.id);
                        t.page('last').draw('page');
                        $('html, body').animate({
                            scrollTop: $(row)[0].offsetTop
                        }, 500);
                        defineSelect2();

                    }
                    else {
                        maessageResponse = 'لا تستطيع الإضافة من دون إختيار نوع الطلب';
                        Swal.fire({
                            position: 'center'
                            , type: 'error'
                            , title: maessageResponse
                            , showConfirmButton: false
                            , timer: 1500
                        })
                    }
                }

        });
        $(document).on('click', 'tr:not(:has(".editbtn") , :has("th") , :has(".dataTables_empty"),:has("a"))', function () {
            var currentID = $(this)[0].rowIndex;
            $(this).attr('contenteditable', 'true');
            $(this).find('select').prop('disabled', false);


            var el = $(this);
          //  el.focus();
            $(this).blur(endEdition);
        });

        function endEdition() {
            // We get the cell
            var el = $(this);
            var myTable = defineTableWithCounter('#settingstableTwo');

            el.attr('contenteditable', 'false');
            el.off('blur', endEdition); // To prevent another bind to this function
        }
        $('#completeck').change(function () {
            if (this.checked) {
                document.getElementById("actionRequiredm").removeAttribute("required");
                document.getElementById("duration").removeAttribute("required");
                document.getElementById("actionreqInputDiv").style.display = 'none';
                document.getElementById("durationInputDiv").style.display = 'none';
            }
            else {
                if (document.getElementById("actionm").value == 'AddCustomActions') {
                    document.getElementById("actionRequiredm").setAttribute("required", "");
                    document.getElementById("duration").setAttribute("required", "");
                    document.getElementById("actionreqInputDiv").style.display = 'block';
                    document.getElementById("durationInputDiv").style.display = 'block';
                }
            }
        });
        $(document).on('click', '.update', function () {
            var idt = $(this).data("id1");
            var action = 'getRecordRequest'; //it will call the record with selected editbutton id
            $.ajax({
                url: '../dist/php/ajax_action.php'
                , method: "POST"
                , data: {
                    idt: idt
                    , action: action
                }
                , dataType: "json"
                , success: function (data) { //the data return in json format from db to be used in the modal.
                    $('#recordModal').modal('show');
                    $('#description').val(data.notes);
                    $('.modal-title').html("<i></i> رؤية التفاصيل");
                    document.getElementById("save").style.display = 'none';
                    document.getElementById("selectRequest").removeAttribute("required");
                    document.getElementById("actionRequiredm").removeAttribute("required");
                    document.getElementById("duration").removeAttribute("required");
                    document.getElementById("checkboxDiv").style.display = 'none';
                    document.getElementById("selectAssignedtoDiv").style.display = 'none';
                    document.getElementById("actionreqInputDiv").style.display = 'none';
                    document.getElementById("durationInputDiv").style.display = 'none';
                    document.getElementById("returncheckboxDiv").style.display = 'none';
                    document.getElementById("selectRequestDiv").style.display = 'none';
                    document.getElementById("selectFileDiv").style.display = 'none';
                }
            });
        });

        $(document).on('click', '.deletebtn', function (event) {
          event.preventDefault();
            var deletedItem;
            var deleteidt = $(this).data("id1");
//  var deleteindex = $(this).parents('tr').index() + 1;
            //clickedtrid represents table that related to clicked part
            var tableName = $(this).data("id3");
            var table = document.getElementById("" + tableName + "");
            var t = defineTable("#" + tableName + "");
            var j = 0;
            var table = document.getElementById("" + tableName + "");
            var isFoundinAddRecords = false;
            addRecordsRowsNo.forEach(function (addSingleRecordNo) {
                if (addSingleRecordNo == deleteidt) {
                    const index = addRecordsRowsNo.indexOf(addSingleRecordNo);
                    if (index > -1) {
                        addRecordsRowsNo.splice(index, 1);
                        //  event.target.closest('tr').remove();
                      //  t.rows[deleteindex].remove(); //to remove row
                      isFoundinAddRecords = true;
                    }
                }
                j++;
            });
            if (isFoundinAddRecords){
              t.row(['#'+ deleteidt]).remove().draw();
              isFoundinAddRecords = false;
              return;
            }
                    deletedItem = $(this).data("id2");

            let action = "DeleteOneItem";
            if (deletedItem != null && deletedItem != 'undefined') {
              var tableNameData;
              if (tableName == "settingstableTwo"){
  tableNameData = "requesttype";
              }
              if (tableName == "settingstableThree"){
  tableNameData = "requestprocesses";
              }
                $.ajax({
                    url: '../dist/php/requests.php'
                    , type: 'POST'
                    , data: {
                        deletedItem
                        , action
                        , tableNameData
                    }
                    , //contentType: "application/json",
                    success: function (data, e) {
                        if (data == 0) {
                          maessageResponse = 'تم الحذف بنجاح';
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                            t.row(['#'+ deleteidt]).remove().draw(true);

                          //  t.rows[deleteindex].remove(); //to remove row
                        }
                        else {
                            maessageResponse = 'لم يتم الحذف بنجاح';
                            Swal.fire({
                                position: 'center'
                                , type: 'error'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 1500
                            })
                        }
                    }
                });
            }
        });

        function getUnique(array) {
            var uniqueArray = [];
            // Loop through array values
            for (i = 0; i < array.length; i++) {
                if (uniqueArray.indexOf(array[i]) === -1) {
                    uniqueArray.push(array[i]);
                }
            }
            return uniqueArray;
        }
        $(document).on('click', '.editbtn', function () {

                var currentTD = $(this).parents('tr').find('td');

                if ($(this).html() == 'Edit') {
                    editedItems[0] = $(this).data("id2");
                    let assignedToemployee = $(this).data("id4");
                    let assignedtoJson;
                    if (typeof assignedToemployee !== 'undefined'){
                        assignedtoJson = JSON.parse("[" + assignedToemployee + "]");
                    }


                    //console.log($(this).data("id2"));
                    $.each(currentTD, function () {
                        $(this).prop('contenteditable', true),
                          $(this).find('select').prop('disabled', false);
                          if (typeof assignedToemployee !== 'undefined'){

                          $(this).find('select[name="assignedTo"]').val(assignedtoJson);
                          $(this).find('select[name="assignedTo"]').trigger( "change" );
                        }

                    });

                    $(this).html('Save');
                }
                else {
                    $(this).html('Edit');
                    var idttoEdit = $(this).parents('tr').index() + 1;

                    var tableName =  $(this).data("id3")

                    if(tableName == "settingstableTwo"){
                      editsetting2tbody(idttoEdit, 'settingstableTwo');

                    }
                    if(tableName == "settingstableThree"){
                      editsetting2tbody(idttoEdit, 'settingstableThree');
                    }



                    $.each(currentTD, function () {
                        $(this).prop('contenteditable', false),
                        $(this).find('select').prop('disabled', true);

                    });

                    var editedRecordsData = editedRecords;
                    var action = "editRecords";
                    var editedItemsData = editedItems;
                    if(tableName == "settingstableTwo"){
                      var tableNameData = "requesttype";
                    $.ajax({
                        url: '../dist/php/requests.php'
                        , type: 'POST'
                        , data: {
                            editedItemsData: editedItemsData
                            , editedRecordsData: editedRecordsData
                            , action: action
                            ,tableNameData: tableNameData
                        , }
                        , dataType: "text"
                        , success: function (data) {
                          if (data == 0) {
                              maessageResponse = 'تم التعديل بنجاح';
                              Swal.fire({
                                  position: 'center'
                                  , type: 'success'
                                  , title: maessageResponse
                                  , showConfirmButton: false
                                  , timer: 1500
                              })

                              var t = $('#setting2tbody').html('');
                              showsetting2tbody("settingstableTwo");

                          }
                          else {
                              maessageResponse = 'لم يتم التعديل بنجاح ، تأكد من البيانات المدخلة';
                              Swal.fire({
                                  position: 'center'
                                  , type: 'error'
                                  , title: maessageResponse
                                  , showConfirmButton: false
                                  , timer: 1500
                              })
                          }


                        }
                    });
                }
                    if(tableName == "settingstableThree"){
                      ///this is for our table stting 3 table

                      var assignedTovalue = $(this).parents('tr').find('td:eq(3) select').val();

                     if (assignedTovalue == null || assignedTovalue == 'undefined' || assignedTovalue == ""){
  maessageResponse = 'لا بد من التأكد من اختيار المرسل إليه لتكون البيانات سليمة';
  Swal.fire({
      position: 'center'
      , type: 'error'
      , title: maessageResponse
      , showConfirmButton: false
      , timer: 1500
  })
  return;
                    }
                      var tableNameData = "requestprocesses";
                      $.ajax({
                          url: '../dist/php/requests.php'
                          , type: 'POST'
                          , data: {
                              editedItemsData: editedItemsData
                              , editedRecordsData: editedRecordsData
                              , action: action
                              ,tableNameData: tableNameData
                          , }
                          , dataType: "text"
                          , success: function (data) {
                            if (data == 0) {
                                maessageResponse = 'تم التعديل بنجاح';
                                Swal.fire({
                                    position: 'center'
                                    , type: 'success'
                                    , title: maessageResponse
                                    , showConfirmButton: false
                                    , timer: 1500
                                })


                            }
                            else {
                                maessageResponse = 'لم يتم التعديل بنجاح ، تأكد من البيانات المدخلة';
                                Swal.fire({
                                    position: 'center'
                                    , type: 'error'
                                    , title: maessageResponse
                                    , showConfirmButton: false
                                    , timer: 1500
                                })
                            }


                          }
                      });
                }
            }
        });
        var test = [];
        var addRecords = [];
        var editedRecords = [];
        var addRecordsRowsNo = [];
        var deletedRecordsRowsNo = [];
        var editedItems = [];
        var values = [];

        function addsetting2tbody(addRecordsRowsNo, tableName) {
              var table = document.getElementById("" + tableName + "");
            if (tableName === 'settingstableTwo') {
                if (typeof addRecordsRowsNo != "undefined" && addRecordsRowsNo != null && addRecordsRowsNo.length != null && addRecordsRowsNo.length > 0) {
                    addRecordsRowsNo.forEach(function (addRecordsRowssingle) {

                        for (i = 0; i < 4; i++) { //assumin we have 4 cells in our example
                            //  var rowNo = table.rows[addRecordsRowssingle].id;
                            values[i] = table.rows[addRecordsRowssingle].cells[i].innerText;
                            if (i == 3){
                              var d  = $(table.rows[addRecordsRowssingle]).find('td:eq(3) select').val(); //to take the value of combo
                              values[i] = d;
                            }
                        }
                        addRecords.push(values);
                    });
                }
            }
            if (tableName === 'settingstableThree') {
                if (typeof addRecordsRowsNo != "undefined" && addRecordsRowsNo != null && addRecordsRowsNo.length != null && addRecordsRowsNo.length > 0) {
                    addRecordsRowsNo.forEach(function (addRecordsRowssingle) {
                        for (i = 0; i < 4; i++) { //assumin we have 4 cells in our example
                            //  var rowNo = table.rows[addRecordsRowssingle].id;
                            if (i == 3) {
                                // get the selected country from current <tr>
                                var d = $(table.rows[addRecordsRowssingle]).find('td:eq(3) select').val(); //to take the value of combo
                                values[i] = d;
                            }
                            else {
                                values[i] = table.rows[addRecordsRowssingle].cells[i].innerText;
                            }
                        }
                        addRecords.push(values);

                    });
                }
            }
            /////.end of table settingstableTwo
        }

        function editsetting2tbody(idttoEdit, tableName) {
editedRecords = [];
            var values = [];
            var table = document.getElementById("" + tableName + "");

            if (idttoEdit != null || idttoEdit != 'undefined') {
                //if table name
                if(tableName == "settingstableTwo"){
                //  var table = document.getElementById("settingstableTwo");
                  for (i = 0; i < 4; i++) { //assumin we have 4 cells in our example
                    if(i == 3){
                      var m = $(table.rows[idttoEdit]).find('td:eq(3) select').val();
                      values[i] = m;

                    } else {
                      values[i] = table.rows[idttoEdit].cells[i].innerText;
                    }
                      //   console.log(table.rows[idt].cells[i].outerHTML);
                  }

                      editedRecords.push(values);
                }
                if (tableName == 'settingstableThree') {
                  var options = [];

                              for (i = 0; i < 6; i++) { //assumin we have 4 cells in our example
                                  //  var rowNo = table.rows[addRecordsRowssingle].id;
                                  if (i == 3) {
                                      // get the selected country from current <tr>
                                      var d = $(table.rows[idttoEdit]).find('td:eq(3) select').val(); //to take the value of combo
                                      options[i] = d.join(',');
                                  }
                                  else if(i == 1){
                                    var m = $(table.rows[idttoEdit]).find('td:eq(1) input').val();
                                    options[i] = m;

                                  }
                                  else {
                                      options[i] = table.rows[idttoEdit].cells[i].innerText;



                                  }

                              }
                              editedRecords.push(options);

                    }

        }
}

        function showsetting2tbody(tableName) {
          if(tableName == "settingstable"){
            initializeArrays();
              var action = 'showAllRequests';
              $.ajax({
                  url: '../dist/php/requests.php'
                  , type: 'POST'
                  , data: {
                      action: action
                  }
                  , dataType: "json"
                  , success: function (data) {
                    var t = defineTable('#settingstable');
                    t.clear().draw();
                      //to conver it to Json after contactit as string  to be used together
                      var myData = data;
                        if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
                          var j = 0;
                          var count = 0;
                          let resultNewData = myData.map(function (o) {
                              //o.priority = "asma";
                              switch (o.status) {
                                case '0':
                                o.status = "جاري المعالجة";
                                break;
                                case '1':
                                o.status =  "الطلب مكتمل";
                                break;
                                case '2':
                                o.status = "الطلب مرفوض";
                                break;
                              }
                              return o;
                          });

                          resultNewData.forEach(rowd => {
                            count = count + 1;
                            // ["a","b","c","d"]
                            var arrayd = [];
                            var arrayhtml = [];
                            var value = rowd.documents;
                            var valueString = value.toString();
                            var dlink = '../upload/requests/';
                            strx = valueString.split(',');
                            arrayd = arrayd.concat(strx);
                            //formattedDocuments.split(',');
                            for (element of arrayd) {
                              var el = element.trim();
                              var fairel = el.substring(el.lastIndexOf('+') + 1);
                                arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + fairel + '</a>')
                            }
                            var arraydString = arrayhtml;
                            var row = t.row.add(['<td class="#">' + count + ' </td>'
                                , '<td class="ref">' + rowd.reqid + '</td>'
                                , '<td class="reftype">' + rowd.requestTypeName + '</td>'
                                , '<td class="requester">' + rowd.requester + '</td>'
                                , '<td class="documents">' + rowd.documents + '</td>'
                                , '<td class="dataCreated">' + rowd.dataCreated + '</td>'
                                , '<td class="description">' + rowd.description + '</td>'
                                , '<td class="status">' + rowd.status + '</td>'
                                , '<td></td>'
                            , ]).draw().node();
                            $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.reqid + '" type="checkbox">' + count + '</label>');
                            $(row).find('td:eq(4)').html(arraydString.join());

                            if (rowd.status !== "الطلب مكتمل" && rowd.status !== "الطلب مرفوض"){
                              $(row).find('td:eq(8)').html('<button type="button" name="actionBSettings" class="ccaction btn btn-warning btn-s" style="margin-right:10px; margin-bottom:10px; width: 90px; height: 40px;" data-id2="'+rowd.reqid+'" data-id3="'+rowd.requestTCode+'" data-id5="'+rowd.empid+'"> تخصيص </button>');

                            }
                            //$(row).find('td:eq(8)').addClass('assignedTom'); //to add class or mainpulate specific cell
  row.id = count;
  j++;
                            return row;

                          });
                      }
                      else {

                          var t = defineTable('#settingstable');
                          t.clear().draw();
                      }
                  }

            });
          }
    if(tableName == "settingstableOne"){
       let myselectionid = getChechedValues('settingstable');
        var t =  defineTable('#settingstableOne');
        t.clear().draw();
        //  htmltoAppend.innerHTML = '';
        if (typeof myselectionid != "undefined" && myselectionid != null && myselectionid.length != null && myselectionid.length > 0) {
            var element = myselectionid;
            var action = 'passSelectedreqid';
            $.ajax({
                url: '../dist/php/ajax_action.php'
                , type: 'POST'
                , data: {
                    element: myselectionid
                    , action: action
                , }
                , dataType: "json"
                , success: function (data) {
                    var jsonarrays = data;
                   handleJSONObject(jsonarrays,"settingstableOne");
                }
            });
        }else{
          document.getElementById("tbodysettingsOne").innerHTML = '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">لا يوجد معاملات مختارة </td></tr>';

        }
      }
          if(tableName == "settingstableTwo"){
            initializeArrays();
            $('#setting2tbody').html('');
              //setting2tbody this is my tbody id

              var action = 'showRequestTypesTable';
              $.ajax({
                  url: '../dist/php/requests.php'
                  , type: 'POST'
                  , data: {
                      action: action
                  }
                  , dataType: "json"
                  , success: function (data) {
                    var t = defineTableWithCounter('#settingstableTwo');
                    t.clear().draw();
                      //to conver it to Json after contactit as string  to be used together

                      var myData = data;
                      if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {

                          var count = 0;
                          myData.forEach(function (rowd) {
                            count = count + 1;

                            var row = t.row.add(['<td class="#">' + count + ' </td>'
                                , '<td class="ref">' + rowd.requestTCode + '</td>'
                                , '<td class="documentsm">' + rowd.name + '</td>'
                                , '<td class="actiondid">' + rowd.priority + '</td>'
                                , '<td></td>'
                            , ]).draw().node();
                            $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.requestTCode + '" type="checkbox">' + count + '</label>');
                            $(row).find('td:eq(3)').html('<select class="selectPriority" style="width: 100px;" disabled><option value="0">منخفض</option><option value="1">متوسط</option><option value="2">عالي</option></select>')
                            $(row).find('td:eq(3)').find('select').val(rowd.priority);
                            $(row).find('td:eq(4)').html('<button data-id1="' + count + '" data-id2="' + rowd.requestTCode + '" data-id3="settingstableTwo" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.requestTCode + '" data-id3="settingstableTwo" class="deletebtn ion-close btn btn-danger btn-sm"></button>');
                            //$(row).find('td:eq(8)').addClass('assignedTom'); //to add class or mainpulate specific cell
  row.id = count;
                            return row;

                          });
                      }
                      else {
                          var t = defineTableWithCounter("#settingstableTwo");
                          t.clear().draw();
                      }
                  }


          });
        }
        if(tableName == "settingstableThree"){
          initializeArrays();
          var element = getChechedValues('settingstableTwo');



          //  htmltoAppend.innerHTML = '';
          var action = 'showProcessesTable';
          $.ajax({
              url: '../dist/php/ajax_action.php'
              , type: 'POST'
              , data: {
                  element: element
                  , action: action
              , }
              , dataType: "json"
              , success: function (data) {
                try {
                  var t = defineTable('#settingstableThree');
                  t.clear().draw();
                } catch (e) {

                } finally {

                }

                  if (typeof data['data1'] != "undefined" && data['data1'] != null && data['data1'].length != null && data['data1'].length > 0) {

                  var jsonarrays = data;
                  defineSelect2();
                  var count = 0;
                  var j = 0;
                  jsonarrays['data1'].forEach(rowd => {
                      count = count + 1;
                      var t = defineTable('#settingstableThree');

                          if (jsonarrays['data2'][j] == null){
                            jsonarrays['data2'][j] = "";
                          }
                      var row = t.row.add(['<td class="#">' + count + ' </td>'
                          , '<td class="ref">' + rowd.requestTypeName + '</td>'
                          , '<td class="namem">' + rowd.poderNo + '</td>'
                          , '<td class="documentsm">' +  jsonarrays['data2'][j] + '</td>'
                          , '<td class="actiondid">' + rowd.actionStatement + '</td>'
                          , '<td class="dateCreated">' + rowd.durationInDays + '</td>'
                          , ' ']).draw().node();
                      //  text = '<tr><td class="#"></td><td class="selectRequest">' + row.requestTCode + '</td><td class="namem">' + row.name + '</td><td class="priority">' + row.priority + '</td><td></td></tr>';
                      $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.id + '" type="checkbox">' + count + '</label>');
                    //   if(rowd.requestTCode === myselection){
                    //   $(row).find('td:eq(0) input').prop( "checked", true );
                    // }
                      $(row).find('td:eq(1)').html(rowd.requestTypeName + '<input type="hidden" value ="' + rowd.requestTCode + '">')
                      $(row).find('td:eq(3)').html('<input type="text" name="assignedTo" value ="' + jsonarrays['data2'][j] + '" style="background-color:white;" disabled>' + '</br><select class="select2Emp" name="assignedTo" multiple="multiple" data-placeholder="اختر الموظفين" style="width: 300px;"></select>');
                      $(row).find('td:eq(3)').css('width', '300px');
                      $(row).find('td:eq(6)').html('<button data-id1="' + count + '"  data-id2="' + rowd.id + '" data-id3="settingstableThree" data-id4="'+rowd.assignedTo+'" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="settingstableThree" class="deletebtn ion-close btn btn-danger btn-sm"></button>');
                      defineSelect2();
                      $(row).find('td:eq(3) select').prop('disabled', true);
                      j++;
                      row.id = count;

                      return row;
                  });
             defineSelect2();

              }
              }
          });
        }
}
        $('.checkall').change(function () {
            $('.checkboxtd').each(function () {
                if ($('.checkall').is(':checked')) {
                    $(this).attr("disabled", false);
                    $(this).prop("checked", true);
                }
                else {
                    $(this).prop("checked", false);
                }
            });
        });
              });

              $('#btn_upload').click(function() {
                uploadFiles('userfile','#documentsm','../../upload/requests/');

              });

              function countingRequestsHandeled(){
                var action = 'countingRequestsHandeled';
                $.ajax({
                  url: '../dist/php/requests.php',
                  type: 'POST',
                  data: {
                      action: action,
                  }
                  , dataType:"json"
                  , success: function(data) {

              $('a#new').html(data['new']);
              $('a#handeled').html(data['handeled']);


                }
              });

              }
              countingRequestsHandeled();
              $(document).on('click', 'button.documentdelete', function(event) {
                var mine = event.target;
                var formID = $(mine).closest('form')[0].id;
                var link;

              link = '../../upload/requests/';


              var fileToDelete = $(mine).data('filename');
            deleteFile(link,fileToDelete,mine);

              });
    </script>
