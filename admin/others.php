<?php
session_start();
if ($_SESSION['levelid']) {
    //successfully login
} else {
    header("Location: ../login.html");
}
?>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>إدارة العملاء والمقاولين</title>
        <!-- Tell the browser to be responsive to screen width -->
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Select2 for select multible -->
        <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <!-- DataTables styles with filters -->
        <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
        <!-- SweetAlert2 and Toast -->
        <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <!-- SweetAlert2 and Toast -->
        <link rel="stylesheet" href="../dist/css/responsiveTable.css">
        <link rel="stylesheet" href="../dist/css/blogModal.css">
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
         <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> -->
         <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
         <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
         <script  defer src='../dist/js/pdfmake-master/build/pdfmake.min.js'></script>
                   <script defer  src='../dist/js/pdfmake-master/build/vfs_fonts.js'></script>

        </head>
    <style>
        .bootstrap-switch .bootstrap-switch-handle-off,
        .bootstrap-switch .bootstrap-switch-handle-on,
        .bootstrap-switch .bootstrap-switch-label {
            line-height: 1.8;
        }

        #nationality {
            width: 100px;
        }

        #experience {
            width: 100px;
        }

        #status {
            width: 100px;
        }

        .greendot {
            height: 25px;
            width: 25px;
            background-color: #2ECC40;
            border-radius: 50%;
            display: inline-block;
        }
        /* The "responsive" class is added to the topnav with JavaScript when the user clicks on the icon. This class makes the topnav look good on small screens (display the links vertically instead of horizontally) */

    </style>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-sm-inline-block">
            <a href="../index.php" class="nav-link">البداية</a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown al">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments" id="cc"></i>
              <span class="badge badge-danger navbar-badge" id="countAlert"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

          <div class="dropdown-divider"></div>
                                          </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item">
              <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="../index.php" class="brand-link">
          <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
          <span class="brand-text font-weight-light">PIONEER</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a  id="userName" href="#" class="d-block"></a>
              <a href="#" id="userStatus"></a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                   <li class="nav-item">
                     <a href="../index.php" class="nav-link">
                       <i class="nav-icon fas fa-tachometer-alt"></i>
                       <p>
                         لوحة التحكم
                       </p>
                     </a>
                   </li>


              <li class="nav-item">
                <a href="../employees/profile.php" class="nav-link">
                  <i class="nav-icon fas fa-id-card-alt"></i>
                  <p>
                    البروفايل
                  </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../employees/requests.php" class="nav-link">
                  <i class="nav-icon fas fa-list-ol"></i>
                  <p>
                    الطلبات
                  </p>
                </a>
              </li>



              <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    أقسام الإدارة المتوسطة
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../midEmployees/projects.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم المشاريع</p>
                    </a>
                  </li>

                </ul>

              </li>



              <li class="nav-item has-treeview opened menu-open" id="admin-section" style="display:none;">
                <a href="#" class="nav-link active">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                     أقسام الإدارة العليا
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/employees.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم الموارد البشرية</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/others.php" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم العملاء والمقاولين</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/AdminRequests.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم خصائص المعاملات</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">

                <li class="nav-item">
    <a href="../sms/form.php" class="nav-link">
    <i class="nav-icon fas fa-sms"></i>
    <p>
    SMS
    </p>
    </a>
    </li>
    </ul>

              </li>

              <li class="nav-item">
                <a href="../employees/news.php" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
            آخر الأخبار
            <span class="badge badge-info right" id="blogsCounting"></span>
            </p>
            </a>
            </li>

              <li class="nav-item">
     <a id="logout" class="nav-link">
       <i class="nav-icon  ion-log-out"></i>
       <p>خروج</p>
     </a>
     </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>


            <!-- end sidebars aside-->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 10px;">
                                <div class="card card-primary card-tabs">
                                    <div class="card-header p-0 pt-1">
                                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                            <li class="nav-item"> <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">العملاء</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">المقاولين</a> </li>
                                        </ul>
                                    </div>
                                    <!-- /.card secondary-->
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-one-tabContent">
                                            <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                                                        <!-- to take all space width of row-->
                                                                        <div class="row">
                                                                        <div class="col-12">
                                                                                <div class="card-body" style="overflow-x:auto;">
                                                                                    <table id="customersTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                        <thead>
                                                                                            <tr>

                                                                                              <th class="no-sort">
                                                                                                  <button data-id1="customersTable" class="addbutton btn btn-s btn-success" type="button" name="button">+</button>
                                                                                                  <br>#
                                                                                                  <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                                                  <th width="10%">الرقم</th>
                                                                                                  <th width="35%">الاسم</th>

                                                                                                      <th width="50%">جهة العمل</th>

                                                                                                  <th width="50%">الجوال1</th>
                                                                                                  <th width="35%">الجوال2</th>
                                                                                                  <th width="40%"> تاريخ الانضمام</th>
                                                                                                  <th width="35%">الايميل</th>
                                                                                                  <th width="35%">ملاحظات</th>
                                                                                                  <th class="no-sort"></th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody id="customersTbody">

                                                                                        </tbody>
                                                                                    </table>
                                                                                    <br />
                                                                                    <div class="card card-secondary">
                                                                                        <div class="card-body">
                                                                                            <label>الحقول المحددة</label>
                                                                                            <div>
                                                                                                <button type="button" name="deleteSelect" data-id1="customersTable" class="deleteSelect btn btn-danger" style="margin-bottom: 10px; height: 35px;">حذف المحدد</button>
                                                                                                <button type="button" class="savebutton btn btn-info" data-id1="customersTable" style="margin-bottom: 10px; height: 35px;">حفظ المضاف</button>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- /.card-secondary -->
                                                                                </div>
                                                                                <!-- /.card-body -->
                                                                            </div>
                                                                            <!-- /.card -->
                                                                        </div>
                                                                        <!-- /.col-12 -->
                                                                    </div>

                                            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                                                          <!-- to take all space width of row-->
                                                                          <div class="row">
                                                                          <div class="col-12">
                                                                                  <div class="card-body" style="overflow-x:auto;">
                                                                                      <table id="customersTable2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                          <thead>
                                                                                              <tr>

                                                                                                <th class="no-sort">
                                                                                                    <button data-id1="customersTable2" class="addbutton btn btn-s btn-success" type="button" name="button">+</button>
                                                                                                    <br>#
                                                                                                    <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                                                    <th width="10%">الرقم</th>
                                                                                                    <th width="35%">الاسم</th>

                                                                                                        <th width="50%">جهة العمل</th>

                                                                                                    <th width="50%">الجوال1</th>
                                                                                                    <th width="35%">الجوال2</th>
                                                                                                    <th width="40%"> تاريخ الانضمام</th>
                                                                                                    <th width="35%">الايميل</th>
                                                                                                    <th width="35%">ملاحظات</th>
                                                                                                    <th class="no-sort"></th>
                                                                                              </tr>
                                                                                          </thead>
                                                                                          <tbody id="customersTbody2">

                                                                                          </tbody>
                                                                                      </table>
                                                                                      <br />
                                                                                      <div class="card card-secondary">
                                                                                          <div class="card-body">
                                                                                              <label>الحقول المحددة</label>
                                                                                              <div>
                                                                                                  <button type="button" name="deleteSelect" data-id1="customersTable2" class="deleteSelect btn btn-danger" style="margin-bottom: 10px; height: 35px;">حذف المحدد</button>
                                                                                                  <button type="button" class="savebutton btn btn-info" data-id1="customersTable2" style="margin-bottom: 10px; height: 35px;">حفظ المضاف</button>

                                                                                              </div>
                                                                                          </div>
                                                                                      </div>
                                                                                      <!-- /.card-secondary -->
                                                                                  </div>
                                                                                  <!-- /.card-body -->
                                                                              </div>
                                                                              <!-- /.card -->
                                                                          </div>
                                                                          <!-- /.col-12 -->
                                            </div>


                                            <!-- /.content-wrapper -->
                                            <!-- Control Sidebar -->
                                            <aside class="control-sidebar control-sidebar-dark">
                                                <!-- Control sidebar content goes here -->
                                            </aside>
                                            <!-- /.control-sidebar -->
                                        </div>
                                        <!-- ./wrapper -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
                </section>
            </div>
            <!-- Modal window pop up when click update or add records -->

        </div>
        <!-- UnifedModals -->
        <div class="modal custom fade" id="blogModal">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">

                  <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
                   <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button> -->

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                   </div>
                <div class="modal-body">

         <div class="card bg-light">
             <!-- Grid row -->
             <div class="row">

               <!-- Grid column -->
               <div class="col-12">

                 <!-- Exaple 1 -->
                 <div class="card example-1 scrollbar-ripe-malinka">
                   <div class="card-body"  id="MainDivPostsm">

                   </div>
                 </div>
                 <!-- Exaple 1 -->

               </div>
               <!-- Grid column -->



             </div>
             <!-- Grid row -->

           </div>
         </div>
        </div>

        </div>
        </div>
        <!-- ./blog Modal -->

        <div class="custom modal fade" id="chatModal">
         <div class="modal-dialog">
           <div class="modal-content">
                 <div class="modal-header">

                   <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
                    <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                     <i class="fas fa-times"></i>
                   </button> -->

                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    </div>

             <div class="modal-body">
         <!-- /.card-header -->
         <div class="card direct-chat direct-chat-primary">
         <div class="card-body">
           <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
        <div class="image">
        <img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
        <a href="#" class="d-block"  id="receivernamem"></a>
        <a href="#" id="statusr2"></a>
        </div>
        </div>
        <hr>
           <!-- Conversations are loaded here -->
           <div class="direct-chat-messages" id="direct-chat-messages2">


           </div>
           <!--/.direct-chat-messages-->

           <!-- Contacts are loaded here -->
           <div class="direct-chat-contacts" >
             <ul class="contacts-list" id="direct-chat-contacts">
               <!-- End Contact Item -->
             </ul>
             <!-- /.contacts-list -->
           </div>
           <!-- /.direct-chat-pane -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <form action="#" method="post" id="myForm2">
             <div class="input-group">
               <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
               <input type="hidden" name="action" value="sendChat" class="form-control">
               <span class="input-group-append">
                 <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
               </span>
             </div>
           </form>
         </div>
         <!-- /.card-footer-->
        </div>
        </div>
        </div>

        </div>
        </div>
        <!--/.direct-chat modal -->
        <!-- /.Unified Modals -->

    </body>

    </html>
    <!-- jQuery -->
    <script src="../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- script for tables to have entries and search-->
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- Bootstrap Switch -->
    <script src="../plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <!-- Datepicker -->
    <script src="../plugins/gijgo/js/gijgo.min.js"></script>
    <!-- Select2 -->
    <script src="../plugins/select2/js/select2.full.min.js"></script>

    <!-- Bootstrap4 Duallistbox -->
    <script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <!-- InputMask -->
    <script src="../plugins/moment/moment.min.js"></script>
    <script src="../plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- date-range-picker -->
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- ChartJS -->
    <script src="../plugins/chart.js/Chart.min.js"></script>
    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="../dist/js/unified.js"></script>


  <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
  <script  src="../dist/js/requestsActions.js"></script>
  <script   src="//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script   src="//cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
  <script   src="//cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>


    <script>

        $("input[data-bootstrap-switch]").each(function () {
            $(this).bootstrapSwitch('state', false); //to switch it off by default
        });


        $('.checkall').change(function () {
            $('.checkboxtd').each(function () {
                if ($('.checkall').is(':checked')) {
                    $(this).attr("disabled", false);
                    $(this).prop("checked", true);
                }
                else {
                    $(this).prop("checked", false);
                }
            });
        });

        $(function () {
          $('a#custom-tabs-one-home-tab').click();
        //console.log('hasbeen clicked');
        })
        let firstClickedHomeTab = true;
        $("a#custom-tabs-one-home-tab").click(function(){
            showsettingtbody("","customersTable");


      //


        console.log('hasbeen clicked');
        });
        $("a#custom-tabs-one-profile-tab").click(function(){
            showsettingtbody("","customersTable2");
        });

        let values;
        $(".savebutton").click(function(){

              /////////begin of procesing add records rows number data
              values = [];
              addRecords = [];
              var tableName = $(this).data("id1");
              var rowCount;
              var sel;
                        if (tableName == 'customersTable') {

                        rowCount = $('#customersTbody tr').length; //to count of current table
                        sel = $('select[name="customersTable_length"] option:selected').val();

                      }
                      if (tableName == 'customersTable2') {

                      rowCount = $('#customersTbody2 tr').length; //to count of current table
                      sel = $('select[name="customersTable2_length"] option:selected').val();

                    }
                      var selint = parseInt(sel);
                      var realExitRecords;

                      if (rowCount > 0)
                      {
                        realExitRecords = rowCount - addRecordsRowsNo.length;

                      }else{
                        realExitRecords = 0;
                      }
                    var diff;
                    if(rowCount > addRecordsRowsNo.length ){
                    diff = rowCount - addRecordsRowsNo.length; //10 displayed records - 4 which is added = 6 the real exit records in the table.
                  }else{
                    diff = addRecordsRowsNo.length - rowCount;
                  }
                    if(addRecordsRowsNo.length + diff > sel){
                      maessageResponse = 'لا تستطيع حفظ أكثر من المحدد من الحقول';
                      Swal.fire({
                          position: 'center'
                          , type: 'error'
                          , title: maessageResponse
                          , showConfirmButton: false
                          , timer: 1500
                      })

                      return;
                    }

                    var table = document.getElementById("" + tableName + "");
              if (typeof addRecordsRowsNo != "undefined" && addRecordsRowsNo != null && addRecordsRowsNo.length != null && addRecordsRowsNo.length > 0) {
                  //if table name
  if (tableName == 'customersTable') {

                  var options = [];
                  var m = 0;
                  var r = realExitRecords + 1;
                    addRecordsRowsNo.forEach(myFunction);

                    function myFunction(value) {
                      options[m] = new Array(9);
                              for (i = 0; i < 9; i++) {
                                options[m][i] = table.rows[r].cells[i].innerText;
                            }
                        m++;
                        r++;
              }
                addRecords.push(options); //assumin we have 4 cells in our example
                      }
                      if (tableName == 'customersTable2') {

                        var options = [];
                        var m = 0;
                        var r = realExitRecords + 1;
                          addRecordsRowsNo.forEach(myFunction);

                          function myFunction(value) {
                            options[m] = new Array(9);
                                    for (i = 0; i < 9; i++) {
                                      options[m][i] = table.rows[r].cells[i].innerText;
                                  }
                              m++;
                              r++;
                    }
                      addRecords.push(options); //assumin we have 4 cells in our example
                            }


                    }


            if(addRecords.lenght == 0){
              maessageResponse = 'لم تضف أي بيانات بعد';
              Swal.fire({
                  position: 'center'
                  , type: 'error'
                  , title: maessageResponse
                  , showConfirmButton: false
                  , timer: 1500
              })
              return;
            }
            //////////////end of processing add records rows number
            var addRecordsData = addRecords;
            var action = 'saveMultibleRecords';
            if (tableName == 'customersTable' || tableName == 'customersTable2') {
              console.log(addRecordsData);
                var tableNameData = "customersTable";
                $.ajax({
                    url: '../dist/php/others.php'
                    , method: "POST"
                    , data: {
                        addRecordsData: addRecordsData
                        , action: action
                        , tableNameData: tableNameData
                    }
                    , success: function (data) { //the data return in json format from db to be used in the modal.
                        // on load of the page: switch to the currently selected tab
                        //  var hash = window.location.hash;
                        if (data == 0) {
                            maessageResponse = 'تم الحفظ بنجاح';
                            Swal.fire({
                                position: 'center'
                                , type: 'success'
                                , title: maessageResponse
                                , showConfirmButton: true
                                , timer: 1500
                            })
                            initializeArrays();
                      window.location.reload();
                            //  $('a[href=\'' + '#custom-content2-below-settings' + '\']').click();
                            return;
                        }else{
                          maessageResponse = 'لم يتم الحفظ بنجاح تأكد من بعض البيانات المدخلة';
                          Swal.fire({
                              position: 'center'
                              , type: 'error'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                         addRecords = [];
                        }


                    }
                });
            }

        });
        function endEdition() {
                // We get the cell
                var el = $(this);
                var myTable = defineTableWithCounter('#customersTable');

                el.attr('contenteditable', 'false');
                el.off('blur', endEdition); // To prevent another bind to this function
            }

        var addRecords = [];
        var addRecordsRowsNo = [];
        var editedRecords = [];

        function initializeArrays(){
          addRecords = [];
          addRecordsRowsNo = [];
          editedRecords = [];
        }
        function showsettingtbody(myselection,tableName) {
var myselection = getChechedValues("customersTable");
var element;

if(myselection.length == 1){
  element =  myselection;
}


          if(tableName == "customersTable"){

            initializeArrays();
            $('#customersTbody').html('')

        //    defineSelect2();

            //  htmltoAppend.innerHTML = '';
            var action = 'showCustomers';
            $.ajax({
                url: '../dist/php/others.php'
                , type: 'POST'
                , data: {
                action: action
                , }
             , dataType: "json"
                , success: function (data) {
       //


                          var t =  defineTableWithCounter('#customersTable');
                          t.clear().draw();
                    var myData = data;
                    if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
                        var count = 0;

                        myData.forEach(function (rowd) {

                          count = count + 1;
                          // var t = $('#customersTable').DataTable({
                          //   retrieve: true

                          var row = t.row.add(['<td class="#">' + count + ' </td>'
                          , '<td class="idcard">' + rowd.customerid + '</td>'
                              , '<td class="name">' + rowd.customerName + '</td>'
                              , '<td class="m1">' + rowd.workDestination + '</td>'
                              , '<td class="notes">' + rowd.mobile1 + '</td>'
                              , '<td class="notes">' + rowd.mobile2 + '</td>'
                              , '<td class="email">' + rowd.dateJoined + '</td>'
                              , '<td class="notes">' + rowd.email + '</td>'
                              , '<td class="notes">' + rowd.Notes + '</td>'

                              , '<td></td>'
                          , ]).draw().node();

$(row).find('td:eq(2)').html('<a href="../sms/form.php?uid='+btoa(rowd.customerid)+'">'+rowd.customerName+'</a>');
                          $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.customerid + '" data-id1="'+rowd.customerid+'"  type="checkbox">' + count + '</label>');
                          $(row).find('td:eq(9)').html('<button data-id1="' + count + '" data-id2="' + rowd.customerid + '" data-id3="customersTable"  class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.customerid + '" data-id3="customersTable" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

            row.id = count;

                          return row;

                        });


                    }
                    else {

                        var t =  defineTableWithCounter("#customersTable");
                        t.clear().draw();
                    }
                }
            });
            }
            if(tableName == "customersTable2"){

              initializeArrays();
              $('#customersTbody2').html('')

          //    defineSelect2();

              //  htmltoAppend.innerHTML = '';
              var action = 'showContractors';
              $.ajax({
                  url: '../dist/php/others.php'
                  , type: 'POST'
                  , data: {
                  action: action
                  , }
               , dataType: "json"
                  , success: function (data) {
         //

                            var t =  defineTableWithCounter('#customersTable2');
                            t.clear().draw();
                      var myData = data;
                      if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
                          var count = 0;

                          myData.forEach(function (rowd) {

                            count = count + 1;
                            // var t = $('#customersTable').DataTable({
                            //   retrieve: true

                            var row = t.row.add(['<td class="#">' + count + ' </td>'
                            , '<td class="idcard">' + rowd.customerid + '</td>'
                                , '<td class="name">' + rowd.customerName + '</td>'
                                , '<td class="m1">' + rowd.workDestination + '</td>'
                                , '<td class="notes">' + rowd.mobile1 + '</td>'
                                , '<td class="notes">' + rowd.mobile2 + '</td>'
                                , '<td class="email">' + rowd.dateJoined + '</td>'
                                , '<td class="notes">' + rowd.email + '</td>'
                                , '<td class="notes">' + rowd.Notes + '</td>'

                                , '<td></td>'
                            , ]).draw().node();

  $(row).find('td:eq(2)').html('<a href="../sms/form.php?uid='+btoa(rowd.customerid)+'">'+rowd.customerName+'</a>');
                            $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.customerid + '" data-id1="'+rowd.customerid+'"  type="checkbox">' + count + '</label>');
                            $(row).find('td:eq(9)').html('<button data-id1="' + count + '" data-id2="' + rowd.customerid + '" data-id3="customersTable2"  class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.customerid + '" data-id3="customersTable2" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

              row.id = count;

                            return row;

                          });


                      }
                      else {

                          var t =  defineTableWithCounter("#customersTable2");
                          t.clear().draw();
                      }
                  }
              });
              }

}
          let editedItems = [];
          let canEditProjectd;

          $(document).on('click', '.editbtn', function () {
                  var currentTD = $(this).parents('tr').find('td');
                  console.log('edit is caled');
                  if ($(this).html() == 'Edit') {
                      editedItems[0] = $(this).data("id2");
                      //console.log($(this).data("id2"));
                      $.each(currentTD, function () {
                          $(this).prop('contenteditable', true);

                      });
                      $(this).html('Save');
                  }
                  else {
                      $(this).html('Edit');
                      var idttoEdit = $(this).parents('tr').index() + 1;
                      console.log(idttoEdit);
                      console.log('saveiscall');
                      var tableName =  $(this).data("id3");
                      if(tableName == "customersTable"){
                        editsettingtbody(idttoEdit, 'customersTable');
                      }
                      if(tableName == "customersTable2"){
                        editsettingtbody(idttoEdit, 'customersTable2');
                      }

                      $.each(currentTD, function () {
                          $(this).prop('contenteditable', false)

                      });

                      var editedRecordsData = editedRecords;
                      var action = "editRecords";
                      var editedItemsData = editedItems;
                      if(tableName == "customersTable"){
                        ///this is for our table stting 3 table
                        var tableNameData = "customersTable";
                        $.ajax({
                            url: '../dist/php/others.php'
                            , type: 'POST'
                            , data: {
                                editedItemsData: editedItemsData
                                , editedRecordsData: editedRecordsData
                                , action: action
                                ,tableNameData: tableNameData
                            , }
                            , dataType: "json"
                            , success: function (data) {
                              if (data == 0) {
                                  maessageResponse = 'تم التعديل بنجاح';
                                  Swal.fire({
                                      position: 'center'
                                      , type: 'success'
                                      , title: maessageResponse
                                      , showConfirmButton: false
                                      , timer: 1500
                                  })

                                  var t = $('#customersTbody').html('');
                                  showsettingtbody("","customersTable");

                              }
                              else {
                                  maessageResponse = 'لم يتم التعديل بنجاح ، تأكد من البيانات المدخلة';
                                  Swal.fire({
                                      position: 'center'
                                      , type: 'error'
                                      , title: maessageResponse
                                      , showConfirmButton: false
                                      , timer: 1500
                                  })
                              }


                            }
                        });
                  }

                  if(tableName == "customersTable2"){
                    ///this is for our table stting 3 table
                    var tableNameData = "customersTable2";
                    $.ajax({
                        url: '../dist/php/others.php'
                        , type: 'POST'
                        , data: {
                            editedItemsData: editedItemsData
                            , editedRecordsData: editedRecordsData
                            , action: action
                            ,tableNameData: tableNameData
                        , }
                        , dataType: "json"
                        , success: function (data) {
                          if (data == 0) {
                              maessageResponse = 'تم التعديل بنجاح';
                              Swal.fire({
                                  position: 'center'
                                  , type: 'success'
                                  , title: maessageResponse
                                  , showConfirmButton: false
                                  , timer: 1500
                              })

                              var t = $('#customersTbody2').html('');
                              showsettingtbody("","customersTable2");

                          }
                          else {
                              maessageResponse = 'لم يتم التعديل بنجاح ، تأكد من البيانات المدخلة';
                              Swal.fire({
                                  position: 'center'
                                  , type: 'error'
                                  , title: maessageResponse
                                  , showConfirmButton: false
                                  , timer: 1500
                              })
                          }


                        }
                    });
              }
              }
          });
          function  editsettingtbody(idttoEdit, tableName){
            editedRecords = [];
                        var values = [];
                        var table = document.getElementById("" + tableName + "");
                        if (idttoEdit != null || idttoEdit != 'undefined') {
                            //if table name

                            if (tableName == 'customersTable') {
                              var options = [];
                              var j = 0;

                                          for (i = 0; i < 9; i++) { //assumin we have 9 cells in our example
                                            options[j] = table.rows[idttoEdit].cells[i].innerText;

                                              j++;

                                          }
                                          editedRecords.push(options);
                                          console.log("edited records are "+ options);

                                }

                                if (tableName == 'customersTable2') {
                                  var options = [];
                                  var j = 0;

                                              for (i = 0; i < 9; i++) { //assumin we have 9 cells in our example
                                                options[j] = table.rows[idttoEdit].cells[i].innerText;

                                                  j++;

                                              }
                                              editedRecords.push(options);
                                    }

                    }
            }




          defineTableWithCounter('#customersTable');


        $(document).on('click', '.deletebtn', function (event) {
          event.preventDefault();
            var deletedItem;
            var deleteidt = $(this).data("id1");

            var tableName = $(this).data("id3");
            var table = document.getElementById("" + tableName + "");
            var t = defineTable("#" + tableName + "");
            var j = 0;
            var isFoundinAddRecords = false;
            addRecordsRowsNo.forEach(function (addSingleRecordNo) {
                if (addSingleRecordNo == deleteidt) {
                    console.log('is exit before delete please from added records');
                    const index = addRecordsRowsNo.indexOf(addSingleRecordNo);
                    if (index > -1) {
                        addRecordsRowsNo.splice(index, 1);
                        //  event.target.closest('tr').remove();
                      //  t.rows[deleteindex].remove(); //to remove row
                      isFoundinAddRecords = true;
                    }
                }
                j++;
            });
            if (isFoundinAddRecords){
              t.row(['#'+ deleteidt]).remove().draw();
              isFoundinAddRecords = false;
              return;
            }
                    deletedItem = $(this).data("id2");

            let action = "DeleteOneItem";
            if (deletedItem != null && deletedItem != 'undefined') {
              var tableNameData;
              if (tableName == "customersTable"){
  tableNameData = "customersTable";
              }
              if (tableName == "customersTable2"){
  tableNameData = "customersTable2";
              }

                $.ajax({
                    url: '../dist/php/others.php'
                    , type: 'POST'
                    , data: {
                        deletedItem
                        , action
                        , tableNameData
                    }
                    , //contentType: "application/json",
                    success: function (data) {
                        if (data == 0) {
                          maessageResponse = 'تم الحذف بنجاح';
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                            t.row(['#'+ deleteidt]).remove().draw(true);
                            event.target.closest('tr').remove();

                        }
                        else {
                            maessageResponse = 'لم يتم الحذف بنجاح';
                            Swal.fire({
                                position: 'center'
                                , type: 'error'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 1500
                            })
                        }
                    }
                });
            }
        });
        $(document).on('click','.deleteSelect', function () { //to click only one time to insert records and update edited values only
            var tableName = $(this).data("id1");
            var action = 'deleteMultibleRecords';
            var deleteItemsData = getChechedValues(tableName);
            if(tableName == "customersTable"){
              /////////begin of procesing delete records rows number data
              var tableNameData = "customersTable";
              $.ajax({
                  url: '../dist/php/others.php'
                  , method: "POST"
                  , data: {
                      deleteItemsData: deleteItemsData
                      , action: action
                      , tableNameData: tableNameData
                  }
                  , dataType: "text"
                  , success: function (data) { //the data return in json format from db to be used in the modal.
                      deleteItemsData = [];
                      if (data == 0) {
                          maessageResponse = 'تم حذف العناصر المحددة بنجاح'
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                          var t = $('#customersTbody').html('');
                          showsettingtbody("","customersTable");
                      }
                  }
              });


            }
            if(tableName == "customersTable2"){
              /////////begin of procesing delete records rows number data
              var tableNameData = "customersTable2";
              $.ajax({
                  url: '../dist/php/others.php'
                  , method: "POST"
                  , data: {
                      deleteItemsData: deleteItemsData
                      , action: action
                      , tableNameData: tableNameData
                  }
                  , dataType: "text"
                  , success: function (data) { //the data return in json format from db to be used in the modal.
                      deleteItemsData = [];
                      if (data == 0) {
                          maessageResponse = 'تم حذف العناصر المحددة بنجاح'
                          Swal.fire({
                              position: 'center'
                              , type: 'success'
                              , title: maessageResponse
                              , showConfirmButton: false
                              , timer: 1500
                          })
                          var t = $('#customersTbody2').html('');
                          showsettingtbody("","customersTable2");
                      }
                  }
              });


            }
          });
    $(document).on('click', 'tr:not(:has(".editbtn") , :has("th"))', function () {
        var currentID = $(this)[0].rowIndex;
        $(this).attr('contenteditable', 'true');
        var el = $(this);
        el.focus();
        $(this).blur(endEdition);
    });
showsettingtbody("","customersTable");

function prependZero(number) {
  number++;
    if (number <= 9) return "000" + number;
    else if (number <= 99) return "00" + number;
    else if (number <= 999) return "0" + number;
    else return number;
}
function extractNumberFromText(string){
  var matches = string.match(/(\d+)/);
  if (matches){
    return matches[0];
  }
}

    $('.addbutton').click(function () { //to click only one time
      var tableName = $(this).data("id1");
      var rowCount;
      var sel;
      if (tableName == 'customersTable') {

      sel = $('select[name="customersTable_length"] option:selected').val();
      rowCount = $('#customersTable tr').length;
    }
    if (tableName == 'customersTable2') {

    sel = $('select[name="customersTable2_length"] option:selected').val();
    rowCount = $('#customersTable2 tr').length;
  }
      var selint = parseInt(sel);
      var diff;

    if(addRecordsRowsNo.length > 0){
      if(rowCount > addRecordsRowsNo.length ){
      diff = rowCount - addRecordsRowsNo.length; //10 displayed records - 4 which is added = 6 the real exit records in the table.
    }else{
      diff = addRecordsRowsNo.length - rowCount;
    }
      if(addRecordsRowsNo.length + diff == sel){
maessageResponse = 'لاتستطيع الإضافة قبل حفظ البيانات المضافة حالياً';
Swal.fire({
  position: 'center'
  , type: 'error'
  , title: maessageResponse
  , showConfirmButton: false
  , timer: 1500
})
return;
}
}
            if (tableName == 'customersTable') {
              //these all calulcation because of the integrity of request counter
                var t =  defineTable('#customersTable');
                var table = document.getElementById("customersTable");
                var counter = t.rows().count() + 1; //countAllRows
              var rowCount = $('#customersTable tr').length; //to count of current table

              if (rowCount >= sel){
        var diff =   counter - rowCount ;
        console.log(diff+'diff');
            if(diff > 1){
              maessageResponse = 'لا تستطيع الإضافة انتقل الى الصفحة التالية لسلامة البيانات';
              Swal.fire({
                  position: 'center'
                  , type: 'error'
                  , title: maessageResponse
                  , showConfirmButton: false
                  , timer: 1500
              })

              return;
            }
          }
          var reqcounter;
          if (counter == 1){ //th consider as row
            reqcounter = extractNumberFromText("cus000");
          } else{
            reqcounter = extractNumberFromText($('#customersTbody tr:last-child').find('td:eq(1)')[0].innerText);

          }

                var row = t.row.add([
                    counter
                    , 'cus' + prependZero(reqcounter)
                    , ''
                    , ''
                    , ''
                    , ''
                    , ''
                    , ''
                    , ''
                    , '<button class="deletebtn btn btn-danger btn-sm" data-id1="' + counter + '" data-id3="customersTable">حذف</button>'
                ]).draw(false).node();
               row.id = counter;
                            // $(row).find('td:eq(8)').html('<button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="customersTable"  data-id4="'+rowd.teamemp+'" data-id5="'+rowd.teamcontra+'" data-id6="'+rowd.customers+'" data-id7="'+rowd.leadby+'" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="customersTable" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

                addRecordsRowsNo.push(row.id);
                t.page('last').draw('page');
                $('html, body').animate({
                    scrollTop: $(row)[0].offsetTop
                }, 500);
                ///var currentTD = t.rows(counter).find('td');
            }

            if (tableName == 'customersTable2') {
              //these all calulcation because of the integrity of request counter
                var t =  defineTable('#customersTable2');
                var table = document.getElementById("customersTable2");
                var counter = t.rows().count() + 1; //countAllRows
              var rowCount = $('#customersTable2 tr').length; //to count of current table

              if (rowCount >= sel){
        var diff =   counter - rowCount ;
            if(diff > 1){
              maessageResponse = 'لا تستطيع الإضافة انتقل الى الصفحة التالية لسلامة البيانات';
              Swal.fire({
                  position: 'center'
                  , type: 'error'
                  , title: maessageResponse
                  , showConfirmButton: false
                  , timer: 1500
              })

              return;
            }
          }
          var reqcounter;
          if (counter == 1){ //th consider as row
            reqcounter = extractNumberFromText("con000");
          } else{
            reqcounter = extractNumberFromText($('#customersTbody2 tr:last-child').find('td:eq(1)')[0].innerText);

          }

                var row = t.row.add([
                    counter
                    , 'con' + prependZero(reqcounter)
                    , ''
                    , ''
                    , ''
                    , ''
                    , ''
                    , ''
                    , ''
                    , '<button class="deletebtn btn btn-danger btn-sm" data-id1="' + counter + '" data-id3="customersTable2">حذف</button>'
                ]).draw(false).node();
               row.id = counter;
                            // $(row).find('td:eq(8)').html('<button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="customersTable"  data-id4="'+rowd.teamemp+'" data-id5="'+rowd.teamcontra+'" data-id6="'+rowd.customers+'" data-id7="'+rowd.leadby+'" class="editbtn btn btn-warning btn-sm">Edit</button> </br></br><button data-id1="' + count + '" data-id2="' + rowd.projectCode + '" data-id3="customersTable" class="deletebtn ion-close btn btn-danger btn-sm"></button>');

                addRecordsRowsNo.push(row.id);
                t.page('last').draw('page');
                $('html, body').animate({
                    scrollTop: $(row)[0].offsetTop
                }, 500);
                ///var currentTD = t.rows(counter).find('td');
            }
          });
        /*
         * -------
         * Here we will create multible select, daterange,
         */



        function getChechedValues(tableName) {
            var selectedChecked = [];
            var t = $("#" + tableName + "").DataTable({
                retrieve: true
            });

            var filteredInputsCheckboxes = $("#" + tableName + "").find("input[name='checkboxtd']:checked");
            $.each($(filteredInputsCheckboxes), function () {
                selectedChecked.push($(this).val());
            //    t.row($(this).closest('tr')).remove().draw(); //to remove row
            });
            //how to look for a specific cell in datatable jquery t.cell(['#12'],0).data() 0 is index , #12 is number of row id

            //console.log("My selected values are: " + selectedChecked);
            return selectedChecked;
        }

    </script>
