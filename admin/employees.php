<?php
session_start();
if ($_SESSION['levelid']) {
    //successfully login
} else {
    header("Location: ../login.html");
}
?>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>قسم الموارد البشرية</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->

        <!-- Font for add/edit/delete-->
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" /> -->
        <!-- Ionicons -->

        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- My Modal Style -->
        <link rel="stylesheet" href="../dist/css/modalStyle.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- DataTables styles with filters -->
        <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
        <!-- Datepicker -->
        <link rel="stylesheet" href="../plugins/gijgo/css/gijgo.min.css">
      <!-- daterange picker -->
        <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">

          <!-- Select2 for select multible -->
          <!-- SweetAlert2 and Toast -->
          <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
          <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
          <link rel="stylesheet" href="../dist/css/responsiveTable.css">
          <link rel="stylesheet" href="../dist/css/blogModal.css">
          <link href="../dist/css/loadingStyle.css" rel="stylesheet">

          <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> -->
              <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
              <script defer src='../dist/js/pdfmake-master/build/pdfmake.min.js'></script>
              <script defer src='../dist/js/pdfmake-master/build/vfs_fonts.js'></script>
              <script defer src="//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
              <script defer src="//cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
              <script defer src="//cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
              <script defer src="../dist/js/upload.js"></script>

          <style>
          .bootstrap-switch .bootstrap-switch-handle-off,
          .bootstrap-switch .bootstrap-switch-handle-on,
          .bootstrap-switch .bootstrap-switch-label {
              line-height: 1.8;
          }
          .text-offline {
            color: gray;
        }
          #nationality {
              width: 100px;
          }

          #experience {
              width: 100px;
          }

          #status {
              width: 100px;
          }

          .greendot {
              height: 25px;
              width: 25px;
              background-color: #2ECC40;
              border-radius: 50%;
              display: inline-block;
          }

          .documentdeletespan{
    display: inline-block;
    margin-right: 10px;
        }

  #documentsu {
      display: flex;
      column-gap: 25px;
      margin-top: 0em;
  }

  #documentsu > div {
      flex: 1 1 0px;
      text-align: center;
      border: 4px solid white;
  }
  article {
            width: 100%;
            display: flex;
            align-items: center;
            text-align: center;
            float: middle;
            justify-content: center;
            padding-top: 2%;
            padding-bottom: 15%;
        }
        #inputSearch{
          align-self: right;
        }
        /* The "responsive" class is added to the topnav with JavaScript when the user clicks on the icon. This class makes the topnav look good on small screens (display the links vertically instead of horizontally) */

        </style>
</head>
    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-sm-inline-block">
            <a href="../index.php" class="nav-link">البداية</a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown al">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments" id="cc"></i>
              <span class="badge badge-danger navbar-badge" id="countAlert"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

          <div class="dropdown-divider"></div>
                                          </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item">
              <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="../index.php" class="brand-link">
          <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
          <span class="brand-text font-weight-light">PIONEER</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a  id="userName" href="#" class="d-block"></a>
              <a href="#" id="userStatus"></a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                   <li class="nav-item">
                     <a href="../index.php" class="nav-link">
                       <i class="nav-icon fas fa-tachometer-alt"></i>
                       <p>
                         لوحة التحكم
                       </p>
                     </a>
                   </li>


              <li class="nav-item">
                <a href="../employees/profile.php" class="nav-link">
                  <i class="nav-icon fas fa-id-card-alt"></i>
                  <p>
                    البروفايل
                  </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="../employees/requests.php" class="nav-link">
                  <i class="nav-icon fas fa-list-ol"></i>
                  <p>
                    الطلبات
                  </p>
                </a>
              </li>



              <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    أقسام الإدارة المتوسطة
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../midEmployees/projects.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم المشاريع</p>
                    </a>
                  </li>

                </ul>

              </li>



              <li class="nav-item has-treeview opened menu-open" id="admin-section" style="display:none;">
                <a href="#" class="nav-link active">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                     أقسام الإدارة العليا
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/employees.php" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم الموارد البشرية</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/others.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم العملاء والمقاولين</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/AdminRequests.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم خصائص المعاملات</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">

                <li class="nav-item">
    <a href="../sms/form.php" class="nav-link">
    <i class="nav-icon fas fa-sms"></i>
    <p>
    SMS
    </p>
    </a>
    </li>
    </ul>

              </li>

              <li class="nav-item">
                <a href="../employees/news.php" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
            آخر الأخبار
            <span class="badge badge-info right" id="blogsCounting"></span>
            </p>
            </a>
            </li>

              <li class="nav-item">
     <a id="logout"  class="nav-link">
       <i class="nav-icon  ion-log-out"></i>
       <p>خروج</p>
     </a>
     </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>

            <!-- end sidebars aside-->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 10px;">
                                <div class="card card-primary card-tabs">
                                    <div class="card-header p-0 pt-1">
                                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                            <li class="nav-item"> <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">الصفحة الرئيسية</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">التقييم</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">المستخدمين والصلاحيات</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">طلبات الإجازة</a> </li>

                                        </ul>
                                    </div>
                                    <br>
                                    <div class="card-body">

                                    <!-- /.card secondary-->
                                    <div class="tab-content" id="custom-tabs-one-tabContent">
                                        <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">

                                          <div class="row">
                                            <div class="col-12">

                                            <input type="text" id="inputSearch2" name="input2">
                                            <label for="input2"> :بحث </label>
<br>
<br>
</div>
                                                                          <!-- to take all space width of row-->
                                                                      <div class="col-12">
                                                                          <!-- card-->
                                                                          <div class="card">
                                                                              <!-- card header-->
                                                                              <div class="card-header">
                                                                                  <h3 class="card-title">السيرة الذاتية للموظفين</h3> </div>
                                                                              <!-- /.card-header -->
                                                                              <!-- card body-->
                                                                              <div class="card-body tableHolder" style="overflow-x:auto;">
                                                                                  <table id="employeesTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                      <thead>
                                                                                          <tr>
                                                                                            <th><br>#
                                                                                            <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                                                  <th width="10%">رقم البطاقة</th>
                                                                                              <th width="35%">الاسم</th>
                                                                                              <th width="35%">الجنسية</th>
                                                                                              <th width="35%">المهارات</th>
                                                                                              <th width="35%">الخبرة</th>
                                                                                              <th width="50%">آخر مؤهل</th>
                                                                                              <th width="35%">المستندات</th>
                                                                                              <th width="35%">تاريخ الانضمام</th>
                                                                                              <th width="40%">الحالة</th>
                                                                                              <th class="no-sort"></th>
                                                                                          </tr>
                                                                                      </thead>
                                                                                      <tbody id="employeestbody">

                                                                                      </tbody>
                                                                                  </table>
                                                                                  <br />
                                                                                  <div class="card card-secondary">
                                                                                      <div class="card-body">
                                                                                          <label>الصفوف المحددة</label>
                                                                                          <div>
                                                                                              <button type="button" name="deleteSelect" id="deleteSelect" class="btn btn-danger" style="margin-bottom: 10px; height: 35px;">حذف المحددين</button>

                                                                                          </div>
                                                                                      </div>
                                                                                  </div>
                                                                                  <!-- /.card-secondary -->
                                                                              </div>
                                                                              <!-- /.card-body -->
                                                                          </div>
                                                                          <!-- /.card -->
                                                                      </div>
                                                                      <!-- /.col-12 -->
<!-- to take all space width of row-->
  <!-- to take all space width of row-->
                                                                      <div class="col-12">
                                                                          <!-- card-->
                                                                          <div class="card">
                                                                              <!-- card header-->
                                                                              <div class="card-header">
                                                                                  <h3 class="card-title">المعلومات الوظيفية</h3> </div>
                                                                              <!-- /.card-header -->
                                                                              <!-- card body-->
                                                                              <div class="card-body" style="overflow-x:auto;">
                                                                                  <br>
                                                                                  <table id="ejobinfoTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                      <thead>
                                                                                          <tr>
                                                                                              <th class="no-sort"># </th>
                                                                                              <th width="10%">رقم البطاقة</th>
                                                                                              <th width="35%">المسمى الوظيفي</th>
                                                                                              <th width="50%">الراتب</th>
                                                                                              <th width="35%">المزايا الوظيفية</th>
                                                                                              <th width="40%">اسم القسم</th>
                                                                                              <th width="35%">تاريخ بداية العمل</th>
                                                                                              <th width="35%">تاريخ انتهاء الاقامة</th>
                                                                                              <th width="35%">تاريخ  الإجازة المستحقة القادمة</th>
                                                                                              <th class="no-sort"></th>
                                                                                          </tr>
                                                                                      </thead>
                                                                                      <tbody id="ejobinfoTbody">

                                                                                      </tbody>
                                                                                  </table>


                                                                                  <!-- /.card-secondary -->
                                                                              </div>
                                                                              <!-- /.card-body -->
                                                                          </div>
                                                                          <!-- /.card -->
                                                                      </div>
                                                                      <!-- /.col-12 -->


                                                                      <div class="col-12">
                                                                          <!-- card-->
                                                                          <div class="card">
                                                                              <!-- card header-->
                                                                              <div class="card-header">
                                                                                  <h3 class="card-title">بيانات التواصل</h3> </div>
                                                                              <!-- /.card-header -->
                                                                              <!-- card body-->
                                                                              <div class="card-body" style="overflow-x:auto;">
                                                                                  <br>
                                                                                  <table id="contactsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                      <thead>
                                                                                          <tr>
                                                                                              <th class="no-sort"># </th>
                                                                                              <th width="10%">الرقم</th>
                                                                                              <th width="35%">الاسم</th>
                                                                                              <th width="50%">الجوال1</th>
                                                                                              <th width="35%">الجوال2</th>
                                                                                              <th width="40%">رقم المكتب</th>
                                                                                              <th width="35%">الايميل</th>
                                                                                              <th width="35%">ملاحظات</th>
                                                                                              <th class="no-sort"></th>
                                                                                          </tr>
                                                                                      </thead>
                                                                                      <tbody id="contactstbody">

                                                                                      </tbody>
                                                                                  </table>

                                                                                  <!-- /.card-secondary -->
                                                                              </div>
                                                                              <!-- /.card-body -->
                                                                          </div>
                                                                          <!-- /.card -->
                                                                      </div>
                                                                      <!-- /.col-12 -->
                                                                    </div>
</div>

                                            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                                <div class="row">
                                                    <div class="col-12">
                                                      <div class="card">
                                                      <div class="card-header">
                                                                <h3 class="card-title">تقدم الموظفين</h3> </div>
                                                            <!-- /.card-header -->
                                                            <div class="card-body" style="overflow-x:auto;">
                                                                <table id="progressTable" class="table table-striped projects">
                                                                    <thead>
                                                                        <tr>
                                                                          <th><br>#
                                                                          <input class="checkall" type="checkbox" name="checkall"> </th>

                                                                            <th>الاسم</th>
                                                                            <th>عدد المشاريع التي تمت قيادتها</th>
                                                                            <th> عدد المهام المسجلة</th>
                                                                            <th>عدد الطلبات المعالجة</th>
                                                                            <th>نقاط المكافآت</th>
                                                                            <th>نقاط الخصم</th>
                                                                            <!-- assessment quiz -->
                                                                            <th>تقديرات</th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="progressTbody">

                                                                    </tbody>
                                                                </table>
                                                                <br />
                                                                <div class="card card-secondary">
                                                                    <div class="card-body">
                                                                        <label>Selected Records</label>
                                                                        <div>
                                                                            <button type="button" name="payEmp" id="payEmp" class="btn btn-info" style="margin-bottom: 10px; height: 35px;">دفع الرواتب</button>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- /.card-secondary -->
                                                            </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>
                                                <!-- /.col -->
                                                <!-- Main content -->
                                                <div class="col-12">
                                                      <div class="card">
                                                      <div class="card-header">
                                                                <h3 class="card-title">سجلات البونس</h3> </div>
                                                            <!-- /.card-header -->
                                                            <div class="card-body" style="overflow-x:auto;">
                                                                <table id="bonusTable" class="table table-striped bonus">
                                                                    <thead>
                                                                        <tr>
                                                                          <th><br>#</th>

                                                                            <th>الرقم</th>
                                                                            <th>المبلغ المستحق</th>
                                                                            <th>السبب</th>
                                                                            <th>تاريخ الاستحقاق</th>
                                                                            <th>نقاط المكافآت</th>

                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="bonustbody">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>

                                                <!-- /.col -->
                                                <div class="col-12">
                                                      <div class="card">
                                                      <div class="card-header">
                                                                <h3 class="card-title"> سجلات الخصم</h3> </div>
                                                            <!-- /.card-header -->
                                                            <div class="card-body" style="overflow-x:auto;">
                                                                <table id="deductTable" class="table table-striped projects">
                                                                    <thead>
                                                                        <tr>
                                                                          <th><br>#</th>

                                                                            <th>الرقم</th>
                                                                            <th>المبلغ المخصوم</th>
                                                                            <th>السبب</th>
                                                                            <th>تاريخ الاستحقاق</th>
                                                                            <th>نقاط الخصم</th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="deductTbody">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>

                                                <!-- /.col -->
                                                <div class="col-12">
                                                      <div class="card">
                                                      <div class="card-header">
                                                                <h3 class="card-title"> سجلات الرواتب</h3> </div>
                                                            <!-- /.card-header -->
                                                            <div class="card-body" style="overflow-x:auto;">
                                                                <table id="epaymentsTable" class="table table-striped projects">
                                                                    <thead>
                                                                        <tr>
                                                                          <th><br>#</th>
                                                                            <th>الرقم</th>
                                                                            <th>المبلغ</th>
                                                                            <th>نوع الدفع</th>
                                                                            <th>تاريخ السداد</th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="epaymentsTbody">

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>

</div>
</div>
                                                <!-- /.content -->

                                            <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">

                                              <div class="row">
                                                <div class="col-12">

                                                <input type="text" id="inputSearch" name="input"/>
                                                <label for="input"> :بحث </label>

                                                <br>
                                                <br>
                                              </div>
                                                  <div class="col-12">
                                                              <div class="card">
                                                              <div class="card-header">
                                                                        <h3 class="card-title">معلومات المستخدم للموظفين </h3> </div>
                                                                    <!-- /.card-header -->
                                                                    <div class="card-body" style="overflow-x:auto;">
                                                              <table id="usersTable" class="usersTable table table-striped" >
                                                                  <thead>
                                                                      <tr>
                                                                        <th><br>#</th>
                                                                          <th>رقم المستخدم</th>
                                                                          <th>اسم المستخدم</th>
                                                                          <th>صورة المستخدم</th>
                                                                          <th></th>
                                                                      </tr>
                                                                  </thead>
                                                                  <tbody id="userstbody">


                                                                  </tbody>
                                                              </table>
                                                          </div>
                                                          <!-- /.card-body -->
                                                      </div>
                                                      <!-- /.card -->
                                                  </div>  <!-- /.col12 -->
                                              </div> <!-- /.row -->
                                                <div class="row">
                                                    <div class="col-12">
                                                                <div class="card">
                                                                <div class="card-header">
                                                                          <h3 class="card-title">صلاحيات الموظفين </h3> </div>
                                                                      <!-- /.card-header -->
                                                                      <div class="card-body" style="overflow-x:auto;">
                                                                <table id="employeesPTable" class="usersTable table table-striped" >
                                                                    <thead>
                                                                        <tr>
                                                                          <th><br>#</th>
                                                                            <th>رقم المستخدم</th>
                                                                            <th>اسم المستخدم</th>
                                                                            <th>الصلاحيات</th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="employeesPtbody">


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>  <!-- /.col12 -->
                                                        <div class="col-12">
                                                                    <div class="card">
                                                                    <div class="card-header">
                                                                              <h3 class="card-title">صلاحيات الموظفين </h3> </div>
                                                                          <!-- /.card-header -->
                                                                          <div class="card-body" style="overflow-x:auto;">
                                                                    <table id="permissionTable" class="usersTable table table-striped" >
                                                                        <thead>
                                                                            <tr>
                                                                                <th>مستوى الصلاحية</th>
                                                                                <th>نوع الصلاحية</th>
                                                                                <th></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="permissionTbody">


                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!-- /.card-body -->
                                                            </div>
                                                            <!-- /.card -->
                                                        </div>  <!-- /.col12 -->

                                                    <div class="col-12">
                                                                <div class="card">
                                                                <div class="card-header">
                                                                          <h3 class="card-title">الجدول الزمني للمستخدم</h3> </div>
                                                                      <!-- /.card-header -->
                                                                      <div class="card-body" style="overflow-x:auto;">
                                                                <table id="employeesLogsTimeline" class="usersTable table table-striped" >
                                                                    <thead>
                                                                        <tr>
                                                                          <th><br>#</th>
                                                                            <th>الرقم</th>
                                                                            <th> اسم النشاط</th>
                                                                            <th>التاريخ</th>
                                                                            <th>المستندات</th>
                                                                             <th> </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="employeesLogsTimelineTbody">


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>  <!-- /.col12 -->

                                                    <!-- to take all space width of row-->


                                                </div> <!-- /.row -->

                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
<div class="row">


    <div class="col-12">
                                                      <!-- card-->
                                                      <div class="card">
                                                          <!-- card header-->
                                                          <div class="card-header">
                                                              <h3 class="card-title">طلبات الإجازات</h3> </div>
                                                          <!-- /.card-header -->
                                                          <!-- card body-->
                                                          <div class="card-body tableHolder" style="overflow-x:auto;">
                                                              <table id="vacationsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                  <thead>
                                                                      <tr>
                                                                        <th><br>#
                                                                        <input class="checkall" type="checkbox" name="checkall"> </th>
                                                                          <th width="35%">الاسم</th>
                                                                          <th width="35%">صاحب الطلب</th>
                                                                          <th width="35%">تاريخ الطلب</th>
                                                                          <th width="50%">ملاحظات</th>
                                                                          <th width="35%">المستندات</th>
                                                                          <th width="40%">الحالة</th>
                                                                          <th class="no-sort"></th>
                                                                      </tr>
                                                                  </thead>
                                                                  <tbody id="vacationstbody">

                                                                  </tbody>
                                                              </table>

                                                              <!-- /.card-secondary -->
                                                          </div>
                                                          <!-- /.card-body -->
                                                      </div>
                                                      <!-- /.card -->
                                                  </div>
                                                  <!-- /.col-12 -->



</div>



</div>
                                            <!-- /.content-wrapper -->
                                            <!-- Control Sidebar -->
                                            <aside class="control-sidebar control-sidebar-dark">
                                                <!-- Control sidebar content goes here -->
                                            </aside>
                                            <!-- /.control-sidebar -->
                                        </div>
                                        <!-- ./wrapper -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
                </section>
            </div>
            <div class="custom modal fade" id="profileModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-body">

             <div class="card bg-light">
               <div class="card-header text-muted border-bottom-0" id="profileHeader">
               </div>
               <div class="card-body pt-0" id="myprofileContent">
                 <!-- <div class="row" id="myprofileContent">

                 </div> -->
               </div>
               <div class="card-footer" id="profilefooter">

               </div>
             </div>
           </div>

           </div>
           </div>
           </div>
           <div class="custom modal fade" id="bonusDeductModal">
               <div class="modal-dialog">
                   <form id="bdForm" method="post">
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                               <h4 class="modal-title" id="myModalLabel">Add Record</h4> </div>
                           <!-- /.modal-header -->
                           <div class="modal-body">
                               <div class="form-group" id="ptsDiv">
                                   <label for="pts" class="control-label">النقاط</label>
                                   <input type="text" id="pts" name="pts" class="form-control" placeholder="عدد النقاط" required> </div>
                                   <div class="form-group">
                                       <label for="reason" class="control-label">السبب</label>


                                       <input type="text" id="reason" name="reason" class="form-control" placeholder="السبب" required>
                                       <div class="form-group">
                                           <input type="hidden" id="ptsid" name="ptsid" class="form-control" placeholder=""> </div>
                                     </div>

                                           <div class="form-group">
                                             <label for="amountPayment" class="control-label">المبلغ</label>
                                               <input type="text" id="amountPayment" name="amountPayment" class="form-control" placeholder="00.00" value="00.00"> </div>
                                               <div id="selectFileDiv">

                                           <form method='post' action='' enctype="multipart/form-data" id="selectFileForm"> Select file :
                                               <input type='file' name='userfile[]' id='userfile2' class='form-control' multiple>
                                               <br>
                                               <input type='button' class='btn btn-info' value='Upload' id='btn_upload_cer'>
                                             </form>
                                           </div>




                                               <div class="form-group" id="documentsm" name="documentsm" > </div>
                           <!-- /.modal-body -->
                           <div class="modal-footer">

                               <!-- the value taken id value-->
                               <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                               <input type='hidden' name="actionp" id="actionp" class='form-control' value=''>
                               <input type="submit" name="saveb" id="saveb" class="btn btn-info" value="Save" /> </div>
                       </div>
                     </div>
                       <!-- /.modal-content -->
                   </form>
               </div>
               <!-- /.modal-dialog -->
           </div>
           <div class="custom modal fade" id="declineVModal">
               <div class="modal-dialog">
                   <form id="declineVForm" method="post">
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                               <h4 class="modal-title">رفض الإجازة</h4> </div>
                           <!-- /.modal-header -->
                           <div class="modal-body">
                                   <div class="form-group">
                                       <label for="reason" class="control-label">السبب</label>
                                       <input type="text" name="reason" class="form-control" placeholder="السبب" required>
                                     </div>
                                   </div>
                           <!-- /.modal-body -->
                           <div class="modal-footer">

                               <!-- the value taken id value-->
                               <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                               <input type="submit" name="saveb" class="btn btn-info" value="حفظ" />
                               <input type="hidden" name="idV" class="form-control" id="idV" /> </div>

                       </div>
                     </div>
                       <!-- /.modal-content -->
                   </form>
               </div>
               <!-- /.modal-dialog -->
           </div>
            <!-- Modal window pop up when click update or add records -->
            <div class="custom modal fade" id="recordModal">
                <div class="modal-dialog">
                    <form id="recordForm" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">تحرير السجل</h4> </div>
                            <!-- /.modal-header -->
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="id" class="control-label">الرقم</label>
                                    <input type="text" id="id" name="id" class="form-control" placeholder="id" required> </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">الاسم</label>
                                    <input type="text" id="name" name="name" class="form-control" placeholder="name" required> </div>
                                <div class="form-group">
                                    <label for="nationality" class="control-label">الجنسية</label>
                                    <select name="nationality" id="nationality" class="custom-select form-control" style="width: 100%;">
                                      <option value="سعودي">سعودي</option>
                                      <option value="هندي">هندي</option>
                                      <option value="مصري">مصري</option>
                                      <option value="سوري">سوري</option>
                                      <option value="إماراتي">إماراتي</option>
                                      <option value="كويتي">كويتي</option>
                                      <option value="بحريني">بحريني</option>
                                      <option value="قطري">قطري</option>
                                      <option value="عماني">عماني</option>
                                      <option value="">فلسطيني</option>
                                      <option value="">باكستاني</option>
                                      <option value="">أردني</option>
                                      <option value="لبناني">لبناني</option>
                                      <option value="">ليبي</option>
                                      <option value="">مغربي</option>
                                      <option value="">سوداني</option>
                                      <option value="">جزائري</option>
                                      <option value="يمني">يمني</option>
                                      <option value="عراقي">عراقي</option>
                                      <option value="تركي">تركي</option>
                                      <option value="تونسي">تونسي</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="skills" class="control-label">المهارات</label>
                                    <input type="text" id="skills" name="skills" class="form-control" placeholder="skills" required> </div>
                                <div class="form-group">
                                    <label for="experience" class="control-label">الخبرة</label>
                                    <select name="experience" id="experience" class="custom-select form-control" style="width: 100%;">
                                      <option value="من 2إلى 4 سنوات">من 2 إلى 4 سنوات</option>
                                      <option value="من 4 إلى 8 سنوات">من 4 إلى 8 سنوات</option>
                                      	<option value="أكثر من 8 سنوات">أكثر من 8 سنوات</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="lastCer" class="control-label"> آخر مؤهل علمي</label>

                                    <select name="lastCer" id="lastCer" class="custom-select form-control" style="width: 100%;" re>
                                      <option value="دكتوراة">دكتوراة</option>
                                      <option value="ماجستير">ماجستير</option>
                                      <option value="بكالوريوس">بكالوريوس</option>
                                      <option value="ثانوي">ثانوي</option>
                                      <option value="دبلوم">دبلوم</option>
                                    </select>                                <!-- Date picker -->
                                  </div>
                                <div class="form-group">
                                    <label for="joinDate" class="control-label">تاريخ الانضمام</label>

                                    <input type="text" class="form-control" id="joinDate" name="joinDate"> </div>
                                <!-- /.form group -->
                                <!-- /.End Date Picker -->
                                <div class="form-group">
                                    <label for="status" class="control-label">الحالة</label>
                                    <select name="status" id="status" class="custom-select form-control" style="width: 100%;">
                                        <option value="موظف">موظف</option>
                                        <option value="متقاعد">متقاعد</option>
                                    </select>
                                </div>
                                <!-- browse multible files -->
                                <div class="form-group">
                                    <label for="documents" class="control-label">المستندات</label>
                                    <div id="documents" name="documents" class="form-control"> </div>
                               </div>

                                <form method='post' action='' enctype="multipart/form-data"> اختر ملف :
                                    <input type='file' name='userfile[]' id='userfile' class='form-control' multiple>
                                    <br>
                                    <input type='button' class='btn btn-info' value='Upload' id='btn_upload'>
                                </form>
                    <!-- Preview-->
                                <div id="documentsu"> </div>



                            <!-- /.modal-body -->
                            <div class="modal-footer">
                                <input type="hidden" name="idt" id="idt" />
                                <!-- the value taken id value-->
                                <input type="hidden" name="action" id="action" value="" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                                <input type="submit" name="save" id="save" class="btn btn-info" value="حفظ" /> </div>
                        </div>
                        <!-- /.modal-content -->
                    </form>
                </div>
                <!-- /.modal-dialog -->
            </div>


        </div>
        <!-- UnifedModals -->
        <div class="modal custom fade" id="blogModal">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">

                  <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
                   <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button> -->

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                   </div>
                <div class="modal-body">

         <div class="card bg-light">
             <!-- Grid row -->
             <div class="row">

               <!-- Grid column -->
               <div class="col-12">

                 <!-- Exaple 1 -->
                 <div class="card example-1 scrollbar-ripe-malinka">
                   <div class="card-body"  id="MainDivPostsm">

                   </div>
                 </div>
                 <!-- Exaple 1 -->

               </div>
               <!-- Grid column -->



             </div>
             <!-- Grid row -->

           </div>
         </div>
       </div>

       </div>
       </div>
       <!-- ./blog Modal -->

       <div class="custom modal fade" id="chatModal">
         <div class="modal-dialog">
           <div class="modal-content">
                 <div class="modal-header">

                   <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
                    <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                     <i class="fas fa-times"></i>
                   </button> -->

                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    </div>

             <div class="modal-body">
         <!-- /.card-header -->
         <div class="card direct-chat direct-chat-primary">
         <div class="card-body">
           <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
       <div class="image">
       <img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
       </div>
       <div class="info">
       <a href="#" class="d-block"  id="receivernamem"></a>
       <a href="#" id="statusr2"></a>
       </div>
       </div>
       <hr>
           <!-- Conversations are loaded here -->
           <div class="direct-chat-messages" id="direct-chat-messages2">


           </div>
           <!--/.direct-chat-messages-->

           <!-- Contacts are loaded here -->
           <div class="direct-chat-contacts" >
             <ul class="contacts-list" id="direct-chat-contacts">
               <!-- End Contact Item -->
             </ul>
             <!-- /.contacts-list -->
           </div>
           <!-- /.direct-chat-pane -->
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
           <form action="#" method="post" id="myForm2">
             <div class="input-group">
               <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
               <input type="hidden" name="action" value="sendChat" class="form-control">
               <span class="input-group-append">
                 <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
               </span>
             </div>
           </form>
         </div>
         <!-- /.card-footer-->
       </div>
     </div>
    </div>

       </div>
     </div>
       <!--/.direct-chat modal -->

    </body>

    </html>
    <script>

</script>
    <!-- jQuery -->
    <script src="../plugins/jquery/jquery.min.js"></script>
    <!-- MyOwn script -->
    <script src="../dist/js/employees.js"></script>
    <!-- MyOwn script -->
    <script src="../dist/js/unified.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- script for tables to have entries and search-->
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- Bootstrap Switch -->
    <script src="../plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <!-- Datepicker -->
    <script src="../plugins/gijgo/js/gijgo.min.js"></script>
    <!-- Select2 -->
    <!-- InputMask -->
    <script src="../plugins/moment/moment.min.js"></script>
    <script src="../plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- date-range-picker -->
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="../dist/js/modalStyle.js"></script>
    <!-- SweetAlert2 and Toast -->
    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"></script> -->
       <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
       <script  src="../dist/js/requestsActions.js"></script>
