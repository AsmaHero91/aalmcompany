<?php
session_start();
if ($_SESSION['levelid']) {
} else {
    header("Location: ../login.html");
}

    ?>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>طلبات المستخدم</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Select2 for select multible -->
        <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="../dist/css/modalStyle.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- Google Font: Source Sans Pro -->
        <!-- DataTables styles with filters -->
        <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
        <!-- SweetAlert2 and Toast -->
        <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <link rel="stylesheet" href="../dist/css/responsiveTable.css">
        <link rel="stylesheet" href="../dist/css/blogModal.css">

           <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

</head>
    <style media="screen">
        #description {
            background: url('../dist/img/ynxjD.png') repeat-y;
            font: normal 14px verdana;
            line-height: 25px;
            padding: 2px 10px;
            border: solid 1px #ddd;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
        }



        .documentdeletespan {
            display: inline-block;
            margin-right: 10px;
        }

        #documentsu {
            display: flex;
            column-gap: 25px;
            margin-top: 0em;
        }

        #documentsu>div {
            flex: 1 1 0px;
            text-align: center;
            border: 4px solid white;
        }

        article {
            width: 100%;
            display: flex;
            align-items: center;
            text-align: center;
            float: middle;
            justify-content: center;
            padding-top: 2%;
            padding-bottom: 15%;
        }

        checkbox {
            width: 48px;
            height: 48px;
        }
        .responsive {
          display: flex;
          flex-direction: column;
      width: 70%;
      padding: 7.5;

      }
        @media screen and (max-width: 600px) {
          .responsive {
            display: flex;
            flex-direction: column;
        width: 100%;
        padding: 7.5;

        }

}
    </style>

    <body class="hold-transition sidebar-mini layout-fixed">
      <div class="wrapper">

      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-sm-inline-block">
            <a href="../index.php" class="nav-link">البداية</a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown al">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments" id="cc"></i>
              <span class="badge badge-danger navbar-badge" id="countAlert"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

          <div class="dropdown-divider"></div>
                                          </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item dropdown al">
              <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
                  <div class="dropdown-divider"></div>
              </div>
          </li>
          <li class="nav-item">
              <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="../index.php" class="brand-link">
          <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
          <span class="brand-text font-weight-light">PIONEER</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a  id="userName" href="#" class="d-block"></a>
              <a href="#" id="userStatus"></a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                   <li class="nav-item">
                     <a href="../index.php" class="nav-link">
                       <i class="nav-icon fas fa-tachometer-alt"></i>
                       <p>
                         لوحة التحكم
                       </p>
                     </a>
                   </li>
              <li class="nav-item">
                <a href="./profile.php" class="nav-link">
                  <i class="nav-icon fas fa-id-card-alt"></i>
                  <p>
                    البروفايل
                  </p>
                </a>
              </li>

              <li class="nav-item">
                <a href="./requests.php" class="nav-link active">
                  <i class="nav-icon fas fa-list-ol"></i>
                  <p>
                    الطلبات
                  </p>
                </a>
              </li>



              <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    أقسام الإدارة المتوسطة
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../midEmployees/projects.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم المشاريع</p>
                    </a>
                  </li>

                </ul>

              </li>



              <li class="nav-item has-treeview" id="admin-section" style="display:none;">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                     أقسام الإدارة العليا
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/employees.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم الموارد البشرية</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/others.php" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p> قسم العملاء والمقاولين</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="../admin/AdminRequests.php" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p>قسم خصائص المعاملات</p>
                    </a>
                  </li>

                </ul>
                <ul class="nav nav-treeview">

                <li class="nav-item">
    <a href="../sms/form.php" class="nav-link">
    <i class="nav-icon fas fa-sms"></i>
    <p>
    SMS
    </p>
    </a>
    </li>
    </ul>

              </li>
              <li class="nav-item">
            <a href="../employees/news.php" class="nav-link">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
            آخر الأخبار
            <span class="badge badge-info right" id="blogsCounting"></span>
            </p>
            </a>
            </li>


              <li class="nav-item">
     <a id="logout" class="nav-link">
       <i class="nav-icon  ion-log-out"></i>
       <p>خروج</p>
     </a>
     </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                              <h1>الطلبات</h1> </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item active">طلبات المستخدم</li>
                                    <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <!-- Profile Image -->
                                <div class="card card-primary card-outline">
                                    <div class="card-body box-profile">
                                        <div class="text-center"> <img id="imageProfile" class="profile-user-img img-fluid img-circle" src="" alt="User profile picture"> </div>
                                        <h3 class="profile-username text-center"></h3>
                                        <p class="text-muted text-center"><span id="jobTitle-department"></span></p>
                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item"> <b>الطلبات الجديدة</b>
                                                <a class="float-right" id="new">
                                                      </a>
                                            </li>
                                            <li class="list-group-item"> <b>الطلبات المعالجة</b>
                                                <a class="float-right"  id="handeled">

                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                            <div class="responsive">
                                <div class="card card-primary card-tabs">
                                    <div class="card-header p-0 pt-1">
                                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                            <li class="nav-item"> <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">البداية</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">السجل التاريخي للمعاملة</a> </li>
                                            <li class="nav-item"> <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">الخط الزمني للمعاملة</a> </li>
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-one-tabContent">
                                            <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                                <div class="card-header" style="background: white;">
                                                    <button type="button" name="add" id="addRecord" class="btn btn-success" style="margin-right: 10px; margin-left: 10px;">+ إضافة طلب</button>
                                                </div>
                                                <!-- Main content -->
                                                <section class="content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="card card-outline card-info">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">
                                                                        <span class="badge bg-danger">تحتاج إلى تصرف</span>
                                                                    </h3>
                                                                    <!-- tools box -->
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fas fa-minus"></i></button>
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove"> <i class="fas fa-times"></i></button>
                                                                    </div>
                                                                    <!-- /. tools -->
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <div id="needsYourActionTableDiv" class="card-body pad">
                                                                    <div class="mb-3">
                                                                        <!-- put table here -->
                                                                        <!-- table-->
                                                                        <div style="overflow-x:auto;">
                                                                            <table id="needsYourActionTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="no-sort">#</th>
                                                                                        <th width="5%">الرقم المرجعي</th>
                                                                                        <th width="20%">النوع</th>
                                                                                        <th width="20%">الاسم</th>
                                                                                        <th width="50%">الأمر المطلوب</th>
                                                                                        <th width="35%">المستندات</th>
                                                                                        <th width="45%">تاريخ الإنشاء</th>
                                                                                        <th width="35%">التاريخ المستحق</th>
                                                                                        <th width="35%">ملاحظات</th>
                                                                                        <th width="35%">يرسل إلى</th>
                                                                                        <th class="no-sort"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="needsYourActionTbody">

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <!-- /.table 1 content -->
                                                <br>
                                                <!-- Main content -->
                                                <section class="content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="card card-outline card-info collapsed-card">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">
                                                                        <span class="badge bg-warning">قيد المعالجة</span>
                                                                    </h3>
                                                                    <!-- tools box -->
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fas fa-minus"></i></button>
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove"> <i class="fas fa-times"></i></button>
                                                                    </div>
                                                                    <!-- /. tools -->
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <div id="inProgressTableDiv" class="card-body pad">
                                                                    <div class="mb-3">
                                                                        <!-- put table here -->
                                                                        <!-- table-->
                                                                        <div style="overflow-x:auto;">
                                                                            <table id="inProgressTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="no-sort">#</th>
                                                                                        <th width="5%">الرقم المرجعي</th>
                                                                                        <th width="10%">النوع</th>
                                                                                        <th width="20%">الاسم</th>
                                                                                        <th width="35%">المستندات</th>
                                                                                        <th width="45%">تاريخ الانشاء</th>
                                                                                        <th width="45%">التاريخ المستحق</th>
                                                                                        <th width="35%">الحالة</th>
                                                                                        <th></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="inProgressTbody">

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <!-- /.table 2 content -->
                                                <br>
                                                <!-- Main content -->
                                                <section class="content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="card card-outline card-info collapsed-card">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">
                                                                        <span class="badge bg-success">مكتملة</span>
                                                                    </h3>
                                                                    <!-- tools box -->
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fas fa-minus"></i></button>
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove"> <i class="fas fa-times"></i></button>
                                                                    </div>
                                                                    <!-- /. tools -->
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <div id="completedTableDiv" class="card-body pad">
                                                                    <div class="mb-3">
                                                                        <!-- put table here -->
                                                                        <!-- table-->
                                                                        <div style="overflow-x:auto;">
                                                                            <table id="completedTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="no-sort">#</th>
                                                                                        <th width="5%">الرقم المرجعي</th>
                                                                                        <th width="10%">النوع</th>
                                                                                        <th width="20%">الاسم</th>
                                                                                        <th width="35%">المستندات</th>
                                                                                        <th width="45%">تاريخ الانشاء</th>
                                                                                        <th width="45%">التاريخ المستحق</th>
                                                                                        <th width="35%">الحالة</th>
                                                                                        <th></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="completedTbody">

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <!-- /.table 2 content -->
                                                <br>
                                                <section class="content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="card card-outline card-info collapsed-card">
                                                                <div class="card-header">
                                                                    <h3 class="card-title">
                                                                        <span class="badge bg-danger">مرفوضة</span>
                                                                    </h3>
                                                                    <!-- tools box -->
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> <i class="fas fa-minus"></i></button>
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove"> <i class="fas fa-times"></i></button>
                                                                    </div>
                                                                    <!-- /. tools -->
                                                                </div>
                                                                <!-- /.card-header -->
                                                                <div id="declinedTableDiv" class="card-body pad">
                                                                    <div class="mb-3">
                                                                        <!-- put table here -->
                                                                        <!-- table-->
                                                                        <div style="overflow-x:auto;">
                                                                            <table id="declinedTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="no-sort">#</th>
                                                                                        <th width="5%">الرقم المرجعي</th>
                                                                                        <th width="10%">النوع</th>
                                                                                        <th width="20%">الاسم</th>
                                                                                        <th width="35%">المستندات</th>
                                                                                        <th width="45%">تاريخ الانشاء</th>
                                                                                        <th width="45%">التاريخ المستحق</th>
                                                                                        <th width="35%">الحالة</th>
                                                                                        <th></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="declinedTbody">

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                            <!--
    end of  all tables requests section -->
                                            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                                <!-- table-->
                                                <!-- <div  class="mb-3"> -->
                                                <div style="overflow-x:auto;" id="historylogsTableDiv">
                                                    <table id="historylogsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th width="5%">رقم المرجع</th>
                                                                <th width="10%">الاسم</th>
                                                                <th width="20%">التصرف المراد</th>
                                                                <th width="35%">المستندات</th>
                                                                <th width="45%">تاريخ الإنشاء</th>
                                                                <th width="35%">التاريخ المستحق</th>
                                                                <th width="35%">الملاحظات</th>
                                                                <th width="35%">مرسل إلى</th>
                                                                <th width="35%">الحالة</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="historytb"> </tbody>
                                                    </table>
                                                </div>
                                                <div id="secondcontent-custom-tabs-one-profile"> </div>
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                                                <div id="content-custom-tabs-one-messages"> </div>
                                                <!-- /. end timleline section -->
                                            </div>

                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
                <!-- /.control-sidebar -->
                <!-- ./wrapper -->
                <!-- here should end all of cardbody -->
                <!--*************************************************************-->
                <!-- ************************************************8 -->
                <div class="custom modal fade" id="recordModal">
                    <div class="modal-dialog">
                        <form id="recordForm" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">تحرير السجل</h4> </div>
                                <!-- /.modal-header -->
                                <div class="modal-body">
                                    <div class="form-group" id="selectAssignedtoDiv">
                                        <label for="select2Emp" class="control-label">يرسل إلى: <small>(مطلوب)</small></label>
                                        <select class="select2Emp" multiple="multiple" data-placeholder="اختر الموظفين" style="width: 100%;"> </select>
                                    </div>
                                    <div class="form-group" id="actionreqInputDiv">
                                        <label for="actionRequiredm" class="control-label">التصرف المطلوب: <small>(مطلوب)</small></label>
                                        <input type="text" name="actionRequiredm" id="actionRequiredm" value=""> </div>
                                    <div class="form-group" id="durationInputDiv">
                                        <label for="duration" class="control-label">عدد الأيام: <small>(مطلوب)</small></label>
                                        <input type="text" name="duration" id="duration" value="" data-inputmask='"mask": "99"' data-mask required> </div>
                                    <div class="form-group" id="returncheckboxDiv">
                                        <label for="sourceEmpck" class="control-label">إرجاع إلى صاحب الطلب<small>(اختياري)</small></label>
                                        <input type="checkbox" class="form-control" name="sourceEmpck" id="sourceEmpck" value="" style="height: 30px; width: 30px;"> </div>
                                    <div class="form-group">
                                                  <input type="hidden" id="reqid" name="reqid" class="form-control" readonly="readonly" value="">

                                    </div>
                                    <div class="form-group" id="selectRequestDiv">
                                        <label for="selectRequest" class="control-label">أنواع الطلبات: <small>(مطلوب)</small></label>
                                        <select class="form-control select2" name="selectRequest" id="selectRequest" style="width: 100%;" required>
                                            <option value="">من فضلك اختر طلبك</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="control-label">الوصف :<small>(اختياري)</small></label>
                                        <textarea name="description" id="description" class="form-control" placeholder="اكتب الوصف هنا"> </textarea>
                                    </div>

                                    <div id="selectFileDiv">

                                <form method='post' action='' enctype="multipart/form-data" id="selectFileForm"> اختار ملف :
                                    <input type='file' name='userfile[]' id='userfile' class='form-control' multiple>
                                    <input type='button' class='btn btn-info' value='Upload' id='btn_upload'>
                                  </form>
                                </div>

                                  <div class="form-group" id="documentsm" name="documentsm" > </div>

                                    <div class="form-group" id="checkboxDiv">
                                        <label for="completeck" class="control-label">اكمال<small>(اختياري)</small></label>
                                        <input type="checkbox" class="form-control" name="completeck" id="completeck" value="" style="height: 30px; width: 30px;"> </div>
                                </div>
                                <!-- /.modal-body -->
                                <div class="modal-footer">
                                    <!-- the value taken id value to update later-->
                                    <input type="hidden" name="actionm" id="actionm" value="" />
                                    <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                                    <input type="submit" name="save" id="save" class="btn btn-info" value="Save" /> </div>
                            </div>
                            <!-- /.modal-content -->
                        </form>
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>

            <!-- UnifedModals -->
            <div class="custom modal fade" id="blogModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">

                      <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
                       <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                      </button> -->

                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                       </div>
                    <div class="modal-body">

             <div class="card bg-light">
                 <!-- Grid row -->
                 <div class="row">

                   <!-- Grid column -->
                   <div class="col-12">

                     <!-- Exaple 1 -->
                     <div class="card example-1 scrollbar-ripe-malinka">
                       <div class="card-body"  id="MainDivPostsm">

                       </div>
                     </div>
                     <!-- Exaple 1 -->

                   </div>
                   <!-- Grid column -->



                 </div>
                 <!-- Grid row -->

               </div>
             </div>
            </div>

            </div>
            </div>
            <!-- ./blog Modal -->

            <div class="custom modal fade" id="chatModal">
             <div class="modal-dialog">
               <div class="modal-content">
                     <div class="modal-header">

                       <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
                        <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                         <i class="fas fa-times"></i>
                       </button> -->

                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                        </div>

                 <div class="modal-body">
             <!-- /.card-header -->
             <div class="card direct-chat direct-chat-primary">
             <div class="card-body">
               <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
            <div class="image">
            <img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
            <a href="#" class="d-block"  id="receivernamem"></a>
            <a href="#" id="statusr2"></a>
            </div>
            </div>
            <hr>
               <!-- Conversations are loaded here -->
               <div class="direct-chat-messages" id="direct-chat-messages2">


               </div>
               <!--/.direct-chat-messages-->

               <!-- Contacts are loaded here -->
               <div class="direct-chat-contacts" >
                 <ul class="contacts-list" id="direct-chat-contacts">
                   <!-- End Contact Item -->
                 </ul>
                 <!-- /.contacts-list -->
               </div>
               <!-- /.direct-chat-pane -->
             </div>
             <!-- /.card-body -->
             <div class="card-footer">
               <form action="#" method="post" id="myForm2">
                 <div class="input-group">
                   <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
                   <input type="hidden" name="action" value="sendChat" class="form-control">
                   <span class="input-group-append">
                     <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
                   </span>
                 </div>
               </form>
             </div>
             <!-- /.card-footer-->
            </div>
            </div>
            </div>

            </div>
            </div>
            <!--/.direct-chat modal -->


    </body>

    </html>
    <!-- Select2 -->
    <!-- jQuery -->
    <script src="../plugins/jquery/jquery.min.js"></script>
    <script src="../dist/js/modalStyle.js"></script>
    <script src="../dist/js/unified.js"></script>


    <!-- Bootstrap 4 -->
    <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>

    <!-- AdminLTE for demo purposes -->
    <!-- Select2 -->
    <script src="../plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="../plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>

    <script src="../plugins/char-counter/jquery.charcounter.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 and Toast -->
    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
   <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
   <script  src='../dist/js/pdfmake-master/build/pdfmake.min.js'></script>
   <script  src='../dist/js/pdfmake-master/build/vfs_fonts.js'></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
   <script  src="//cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
   <script  src="../dist/js/requestsActions.js"></script>
   <script  src="../dist/js/upload.js"></script>

    <script>
    showCompletedRequests();
showDeclinedRequests();
showNeedsYourAction();
showprogresstable();
  loadDatas();
  function loadDatas(){
    var action = 'loadSessionData';
    $.ajax({
      url: '../dist/php/sessionData.php',
      type: 'POST',
      data: {
          action: action
      }
      , dataType:"json"
      , success: function(data) {
    //to conver it to Json after contactit as string  to be used together
    $('#imageProfile')[0].src = '../dist/img/'+ ''+currentUserPic+'';

    }
     });
  }
  function prependZero(number) {
    number++;
      if (number <= 9) return "000" + number;
      else if (number <= 99) return "00" + number;
      else if (number <= 999) return "0" + number;
      else return number;
  }
  function extractNumberFromText(string){
    var matches = string.match(/(\d+)/);
    if (matches){
      return matches[0];
    }
  }
        var count = 0;
        var text = '';
        var arrayhtmlContent = [];
        var dataArray = [];
        var dataArray2 = [];

        // declinedTableDiv
        let firstClickedStrt = false;
        let firstClickedttab = false;
        let selectedEmployees;
        let isClickedstart = false;
        let testid = [];
        let testReqtype = [];
        var myhilitedTable = [];
        var myhilitedRow = [];

        // $('tr[role="row"]:has(button[name="actionBSettings"] , )').click(function (event) {
            $(document).on('click', "tr:not(':has(th),:has(.dataTables_empty)')", function (event) {

          //make sure the user can choose only one record
            var clickedtr = this;
            if (clickedtr.closest('table').getAttribute("id") == 'historylogsTable'){
              return;

            }
            if (!isClickedstart) {
                isClickedstart = true;
            }
            if (testid.length == 1 && testReqtype.length == 1 && !clickedtr.classList.contains('selected')) {
                return;
            }
            //deal with first tr clicked
            if (clickedtr.classList.contains('selected')) {
                clickedtr.style.backgroundColor = '#fff'
                clickedtr.classList.remove('selected');
                selectedtdreqid = '';
                testid = [];
                testReqtype = [];
                arrayhtmlContent = [];
                myHistoryHtml = [];
                dataArray = [];
                dataArray2 = [];
                myhilitedTable = [];
                myhilitedRow = [];
                localStorage.setItem('myhilitedTable', []);
                localStorage.setItem('myhilitedRow', []);
                localStorage.setItem('myselectionid', []);
                localStorage.setItem('myselectiontype', []);
            }
            else {
                clickedtr.style.backgroundColor = '#FFFF00'; //to hilight selected
                clickedtrid = clickedtr.getAttribute("id");
                //clickedtrid represents table that related to clicked part
                var tableeid = clickedtr.closest('table').getAttribute("id");
                var table = document.getElementById(tableeid);
                selectedtdreqid = table.rows[clickedtrid].cells[1].innerText;
                selectedtdReqtype = table.rows[clickedtrid].cells[2].getAttribute("data-id1");

                myhilitedTable.push(tableeid);
                myhilitedRow.push(clickedtrid);

                //console.log(document.getElementById(''+tableeid+'').rows[clickedtrid]);
                testReqtype.push(selectedtdReqtype);
                testid.push(selectedtdreqid);

                  localStorage.setItem('myhilitedTable', myhilitedTable[0]);
                  localStorage.setItem('myhilitedRow', myhilitedRow[0]);
                  localStorage.setItem('myselectionid', testid[0]);
                  localStorage.setItem('myselectiontype', testReqtype[0]);

                clickedtr.classList.toggle('selected');
                firstClickedStrt = false;
                firstClickedttab = false;
                return;
            }
        });
        var myhtmlella;
        var myhtmlella2;
        var table;
        var myHistoryHtml = [];

        function handleJSONObject(jsonarrays,tableName,t) {

            var count = 0;
            var m = 0;
            var j = 0;
            jsonarrays['data1'].forEach(row => {
                count = count + 1;
                // ["a","b","c","d"]
                var arrayd = [];
                var arrayhtml = [];
                var value = row.documents
                var valueString = value.toString();
                var dlink = '../../upload/requests/'
                strx = valueString.split(',');
                arrayd = arrayd.concat(strx);
                //formattedDocuments.split(',');
                for (element of arrayd) {
                    arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + element.trim() + '</a>')
                }
                var arraydString = arrayhtml;

                    //formattedDocuments.split(',');
                var row = t.row.add(['<td class="#">' + count + ' </td>'
                    , '<td class="ref">' + row.reqid + '</td>'
                    , '<td class="namem">' + row.employeeName + '</td>'
                    , '<td class="documentsm">' + arraydString + '</td>'
                    , '<td class="actiondid">' + row.actionRequired + '</td>'
                    , '<td class="dateCreated">' + row.dateCreated + '</td>'
                    , '<td class="deadline">' + row.deadline + '</td>'
                    , '<td class="notes">' + row.notes + '</td>'
                    , '<td class="assignedTom">' + jsonarrays['data2'][j] + '</td>', //for assignedto
                    '<td class="status">' + row.status + '</td>'
                    , '<td></td>'
                , ]).draw().node();
                //$(row).find('td:eq(8)').addClass('assignedTom'); //to add class or mainpulate specific cell
                j++;
                return row;
            });
        }

        function handleJSONObjectTimeline(jsonarrays) {
            var count = 0;
            var j = 0;
            jsonarrays['data1'].forEach(row => {
                count = count + 1;
                if (j == 0) {
                    text = '<div class="time-label"><span class="bg-danger"><i class="far fa-clock"> البداية</i></span></div><div><i class="fas fa-clock bg-warning"></i><div class="timeline-item" style="width: 50%"> <span class="time"><i class="far fa-clock"></i> المتوقع: <span>' + row.durationInDays + '</span> أيام</span><h3 class="timeline-header">' + row.actionStatement + ' <a href="#profileModal" id="assignedTopr">' + jsonarrays['data2'][j] + '</a></h3></div></div>';
                    arrayhtmlContent.push(text);

                }
                if (j > 0 && j < jsonarrays['data1'].length ) {
                    text = '<div><i class="fas fa-clock bg-warning"></i><div class="timeline-item" style="width: 50%"> <span class="time"><i class="far fa-clock"></i> المتوقع: <span>' + row.durationInDays + '</span> أيام</span><h3 class="timeline-header">' + row.actionStatement + ' <a href="#profileModal" id="assignedTopr">' + jsonarrays['data2'][j] + '</a></h3></div></div>';
                    arrayhtmlContent.push(text);

                }
                if (count == jsonarrays['data1'].length) {
                    text = '<div><i class="far fa-flag bg-green"></i></div>';
                    arrayhtmlContent.push(text);
                }
                j++;
                //arrayhtmlContent.push(text);
                text = '';
                return arrayhtmlContent;
            });
        }

        var myjson = [];
function fetchSelect(){
  var action = "fetchSelectRequests";
  return $.ajax({
  url: '../dist/php/fetchSelect.php'
  , type: 'POST'
  , data: {
  action: action
  }
  , success: function (data) {

  if (data != -1){
  var myObj = JSON.parse(data);
  var m = 0;
  myObj.forEach(row => {
  myjson[m] = {id:row.id,text:row.name}; //convert numbers to ["1077","1022"]
  m++;
  });
  //  var jsonarrays = data;
  }

}

  });
}
let result;

function handlefetchSelect(){
result = myjson;
return myjson;
}
function defineSelect2(){
    // the code here will be executed when all four ajax requests resolve.
    // a1, a2, a3 and a4 are lists of length 3 containing the response text,
    // status, and jqXHR object for each of the four ajax calls respectively.
    // wait until the promise returns us a value
    $('.select2Emp').select2({
      data:handlefetchSelect(),
    multiple: true,
    });


    $('.select2Emp').change(function () {
      var selections = $('.select2Emp').val();
      selectedEmployees = selections;
    });


}

        function initializeArrays(){
          addRecords = [];
          addRecordsRowsNo = [];
          editedRecords = [];
        }
        $(document).ready(function () {
          $.when(fetchSelect()).done(function(a1){
            $('.select2').select2()
            defineSelect2();
});
            //    $('.select2').select2()
            var myactiveTab = localStorage.getItem('currentActiveTab');



            if (myactiveTab == 'custom-tabs-one-messages-tab') {
                $('a#custom-tabs-one-messages-tab').click();
                var myhilitedTable = localStorage.getItem('myhilitedTable');
                var myhilitedRow = localStorage.getItem('myhilitedRow');
                if (typeof myhilitedRow != "undefined" && myhilitedRow != null && myhilitedRow.length != null && myhilitedRow.length > 0) {
                    document.getElementById("" + myhilitedTable + "").rows[myhilitedRow].style.backgroundColor = '#FFFF00';
                    //    myhilightedSection
                }
            }
            if (myactiveTab == 'custom-tabs-one-profile-tab') {
                $('a#custom-tabs-one-profile-tab').click();
                var myhilitedTable = localStorage.getItem('myhilitedTable');
                var myhilitedRow = localStorage.getItem('myhilitedRow');
                if (typeof myhilitedRow != "undefined" && myhilitedRow != null && myhilitedRow.length != null && myhilitedRow.length > 0) {
                    document.getElementById("" + myhilitedTable + "").rows[myhilitedRow].style.backgroundColor = '#FFFF00';
                    //    myhilightedSection
                }
            }
            if (myactiveTab == 'custom-tabs-one-home-tab') {
                $('a#custom-tabs-one-home-tab').click();
                var myhilitedTable = localStorage.getItem('myhilitedTable');
                var myhilitedRow = localStorage.getItem('myhilitedRow');
                if (typeof myhilitedRow != "undefined" && myhilitedRow != null && myhilitedRow.length != null && myhilitedRow.length > 0) {
                    document.getElementById("" + myhilitedTable + "").rows[myhilitedRow].style.backgroundColor = '#FFFF00';
                    //    myhilightedSection
                }
            }
            if (myactiveTab == 'custom-tabs-one-settings-tab') {
                $('a#custom-tabs-one-settings-tab').click();
                $('a#custom-content1-below-settings-tab').click();
                var myhilitedTable = localStorage.getItem('myhilitedTable');
                var myhilitedRow = localStorage.getItem('myhilitedRow');
                if (typeof myhilitedRow != "undefined" && myhilitedRow != null && myhilitedRow.length != null && myhilitedRow.length > 0) {
                    document.getElementById("" + myhilitedTable + "").rows[myhilitedRow].style.backgroundColor = '#FFFF00';
                    //    myhilightedSection
                }
            }
            $('[data-mask]').inputmask() //to run input mask


        });
  $('a#custom-tabs-one-profile-tab').click(function () {
            let myselectionid = localStorage.getItem('myselectionid');
            //if (myselectionid != "undefined" ){
            //let htmltoAppend = document.getElementById("historytb");
            var t =  defineTable('#historylogsTable');
            t.clear().draw();
            document.getElementById("historytb").innerHTML = '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">لا يوجد معاملات مختارة </td></tr>';
            //  htmltoAppend.innerHTML = '';
            if (typeof myselectionid != "undefined" && myselectionid != null && myselectionid.length != null && myselectionid.length > 0) {
                var element = myselectionid;
                var action = 'passSelectedreqid';
                $.ajax({
                    url: '../dist/php/ajax_action.php'
                    , type: 'POST'
                    , data: {
                        element: element
                        , action: action
                    , }
                    , dataType: "json"
                    , success: function (data) {
                      var t =  defineTable('#historylogsTable');
                      t.clear().draw();
                        //  alert(JSON. stringify(data) );
                        var jsonarrays = data;
                        handleJSONObject(jsonarrays,"historylogsTable",t);
                    }
                });
            }

        });



        $('a#custom-tabs-one-messages-tab').click(function () {
            var myselectiontype = localStorage.getItem('myselectiontype');
            if (typeof myselectiontype != "undefined" && myselectiontype != null && myselectiontype.length != null && myselectiontype.length > 0) {
                var element = myselectiontype;
                var action = 'passSelectedreqtypeProcess';

                $.ajax({
                    url: '../dist/php/ajax_action.php'
                    , type: 'POST'
                    , data: {
                        element: element
                        , action: action
                    , }
                    , dataType: "json"
                    , success: function (data) {
                        var jsonarrays = data;
                        handleJSONObjectTimeline(jsonarrays);
                        document.getElementById("content-custom-tabs-one-messages").innerHTML = '<br><div class="timeline timeline-inverse" id="timeline"></div>';
                        let htmltoAppend = document.getElementById("timeline");
                        htmltoAppend.innerHTML = arrayhtmlContent.join('');
                        arrayhtmlContent = [];
                        //  callempnamesTimeline(element, action)
                    }
                });
            }
            else {
                document.getElementById("content-custom-tabs-one-messages").innerHTML = '<p>لا يوجد معاملات مختارة</p>';
            }
        });




              $(document).ready(function () {


        var selectionsEmployees;
        //for adding request
        $('#addRecord').click(function () {
            $('#recordModal').modal('show');
            $('#recordForm')[0].reset();
          //  fillSelectRequest();
            $('.modal-title').html("<i></i> إرسال طلب");
            $('#actionm').val('addARequest'); //from modal footer to send
            $('#save').val('إرسال');
            document.getElementById("selectRequestDiv").style.display = 'block';
            document.getElementById("selectRequest").setAttribute("required", "");
            document.getElementById("checkboxDiv").style.display = 'none';
            document.getElementById("actionRequiredm").removeAttribute("required");
            document.getElementById("duration").removeAttribute("required");
            document.getElementById("checkboxDiv").style.display = 'none';
            document.getElementById("selectAssignedtoDiv").style.display = 'none';
            document.getElementById("actionreqInputDiv").style.display = 'none';
            document.getElementById("returncheckboxDiv").style.display = 'none';
            document.getElementById("durationInputDiv").style.display = 'none';
        });
        var idg;
        var reqidg;
        var selectRequestg;
        var assignedToToInsert;
          $(document).on('click', '.ccaction', function () {
        // $('.ccaction').click(function () {
            $('#recordModal').modal('show');
        //    fillSelectRequest();
            $('#recordForm')[0].reset();
            $('#actionm').val('AddCustomActions'); //from modal footer to send
            $('.modal-title').html("<i></i> إضافة نشاط مخصص");
            $('#save').val('إرسال');
            document.getElementById("selectRequestDiv").style.display = 'block';
           idg = $(this).data("id1");
            reqidg = $(this).data("id2");
            selectRequestg = $(this).data("id3");
            assignedToToInsert = $(this).data("id5");
            document.getElementById("selectRequestDiv").style.display = 'none';
            document.getElementById("selectRequest").removeAttribute("required");
            document.getElementById("actionRequiredm").setAttribute("required", "");
            document.getElementById("duration").setAttribute("required", "");
            document.getElementById("checkboxDiv").style.display = 'block';
            document.getElementById("selectAssignedtoDiv").style.display = 'block';
            document.getElementById("actionreqInputDiv").style.display = 'block';
            document.getElementById("durationInputDiv").style.display = 'block';
            document.getElementById("returncheckboxDiv").style.display = 'block';
            document.getElementById("selectFileDiv").style.display = 'block';
            document.getElementById("selectRequestDiv").style.display = 'none';
        });

        $(document).on('click', '.accept', function (event) {
          event.preventDefault();
            $('#recordModal').modal('show');
            $('#recordForm')[0].reset();
            $('.modal-title').html("<i></i> قبول الطلب");
            $('#actionm').val('AcceptRequest'); //from modal footer to send
            $('#save').val('قبول');
            idg = $(this).data("id1");
            reqidg = $(this).data("id2");
            selectRequestg = $(this).data("id3");
            assignedToToInsert = $(this).data("id5");
            document.getElementById("selectRequestDiv").style.display = 'none';
            document.getElementById("selectRequest").removeAttribute("required");
            document.getElementById("actionRequiredm").removeAttribute("required");
            document.getElementById("duration").removeAttribute("required");
            document.getElementById("checkboxDiv").style.display = 'block';
            document.getElementById("selectAssignedtoDiv").style.display = 'none';
            document.getElementById("actionreqInputDiv").style.display = 'none';
            document.getElementById("returncheckboxDiv").style.display = 'none';
            document.getElementById("durationInputDiv").style.display = 'none';
            document.getElementById("selectFileDiv").style.display = 'block';
        });
        $(document).on('click', '.decline', function (event) {
          event.preventDefault();
            $('#recordModal').modal('show');
            $('#recordForm')[0].reset();
            $('.modal-title').html("<i></i> رفض الطلب");
            $('#actionm').val('DeclineRequest'); //from modal footer to send
            $('#save').val('رفض');
            document.getElementById("selectRequest").removeAttribute("required");
            document.getElementById("actionRequiredm").removeAttribute("required");
            document.getElementById("duration").removeAttribute("required");
            document.getElementById("checkboxDiv").style.display = 'none';
            document.getElementById("selectAssignedtoDiv").style.display = 'none';
            document.getElementById("actionreqInputDiv").style.display = 'none';
            document.getElementById("durationInputDiv").style.display = 'none';
            document.getElementById("returncheckboxDiv").style.display = 'none';
            document.getElementById("selectRequestDiv").style.display = 'none';
            document.getElementById("selectFileDiv").style.display = 'none';
            idg = $(this).data("id1");
            reqidg = $(this).data("id2");
            selectRequestg = $(this).data("id3");
            assignedToToInsert = $(this).data("id5");
        });
        $("#recordModal").on('submit', '#recordForm', function (event) {
            event.preventDefault();
            localStorage.setItem('myhilitedTable', []);
            localStorage.setItem('myhilitedRow', []);
            $('#save').attr('disabled', false);
            var modalTitle = document.getElementById("actionm").value;
            var uploadedFiles = document.querySelectorAll("button.documentdelete");
            var allFilesNames = [];
            uploadedFiles.forEach((item) => {
            	var el = item.getAttribute('data-filename');
            allFilesNames.push(el);
            });
            //this the uploaded files before
            if (modalTitle === 'addARequest') {
              var fd = new FormData();

              fd.append('allFilesNames', allFilesNames);

              var formData = jQuery(this).serializeArray();
jQuery.each(formData, function(i, fdm) {
  fd.append('' + fdm.name + '', fdm.value);
});
                $.ajax({
                  url: '../dist/php/ajax_action.php',
                  type: 'post',
                  data: fd,
                  contentType: false,
                  processData: false,
                  success: function(data) {
                        if (data == 0) {
                            $('#recordModal').modal('hide');
                            $('#recordForm')[0].reset();
                            $('#save').attr('disabled', false);
                            maessageResponse = 'الطلب تم إرساله بشكل صحيح';
                            Swal.fire({
                                position: 'center'
                                , type: 'success'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 5000
                            })
                            window.location.reload();
                        }
                      else if (data == -2) {
                            $('#recordModal').modal('hide');
                            $('#recordForm')[0].reset();
                            $('#save').attr('disabled', false);
                            maessageResponse = 'لم يتم إضافة هذا الطلب بعد تواصل مع المدير لإنشائه';
                            Swal.fire({
                                position: 'center'
                                , type: 'error'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 3000
                            })
                        }
                        else if(data == -1) {
                            maessageResponse = ' يتم طلبك بشكل صحيح تأكد من البيانات المدخله ثم حاول مرة أخرى';
                            Swal.fire({
                                position: 'center'
                                , type: 'error'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 3000
                            })
                        }
                        else{
                          maessageResponse = data;
                          Swal.fire({
                              position: 'center'
                              , type: 'error'
                              , title: maessageResponse
                              , showConfirmButton: true
                          })
                        }
                    }

                });
            }
            if (modalTitle === 'AcceptRequest') {
                var checkedValue;
                var action = 'AcceptRequest';
                var description = document.getElementById("description").value;
                //to make input hidden and remove attr of required
                var atLeastOneIsChecked = $('input[name="completeck"]:checked').length;
                if (atLeastOneIsChecked > 0) {
                    checkedValue = '1';
                }
                else {
                    checkedValue = '';
                }

                var fd = new FormData();
                fd.append('allFilesNames', allFilesNames);

                fd.append('description', description);
                fd.append('idg', idg);
                fd.append('reqidg', reqidg);
                fd.append('selectRequestg', selectRequestg);
                fd.append('action', action);
                fd.append('checkedValue', checkedValue);
                fd.append('assignedToToInsert', assignedToToInsert);

                $.ajax({
                  url: '../dist/php/ajax_action.php',
                  type: 'post',
                  data: fd
                  ,contentType: false,
                  processData: false,
                  success: function(data) {

                        if (data == 0) {
                            $('#recordModal').modal('hide');
                            $('#recordForm')[0].reset();
                            $('#save').attr('disabled', false);
                            maessageResponse = 'الطلب تم إرساله بشكل صحيح';
                            Swal.fire({
                                position: 'center'
                                , type: 'success'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 3000
                            })
                            window.location.reload();
                        }
                        else if (data == -1) {
                            maessageResponse = 'لم يتم طلبك بشكل صحيح تأكد من البيانات المدخله ثم حاول مرة أخرى';
                            Swal.fire({
                                position: 'center'
                                , type: 'error'
                                , title: maessageResponse
                                , showConfirmButton: false
                                , timer: 3000
                            })
                        } else{
                          maessageResponse = data;
                          Swal.fire({
                              position: 'center'
                              , type: 'error'
                              , title: maessageResponse
                              , showConfirmButton: true
                          })

                        }
                    }
                });
            }
            if (modalTitle === 'DeclineRequest') {
                var action = 'DeclineRequest';
                var description = document.getElementById("description").value;

                var fd = new FormData();
                fd.append('allFilesNames', allFilesNames);

                fd.append('idg', idg);
                fd.append('reqidg', reqidg);
                fd.append('selectRequestg', selectRequestg);
                fd.append('action', action);
                fd.append('assignedToToInsert', assignedToToInsert);
                fd.append('description', description);

                $.ajax({
                  url: '../dist/php/ajax_action.php',
                  type: 'post',
                  data: fd
                  ,contentType: false,
                  processData: false,
                  success: function(data) {
                      if (data == 0) {
                              $('#recordModal').modal('hide');
                              $('#recordForm')[0].reset();
                              $('#save').attr('disabled', false);
                              maessageResponse = 'الطلب تم إرساله بشكل صحيح';
                              Swal.fire({
                                  position: 'center'
                                  , type: 'success'
                                  , title: maessageResponse
                                  , showConfirmButton: false
                                  , timer: 3000
                              })
                              window.location.reload();
                          }
                          else if (data == -1) {
                              maessageResponse = 'لم يتم طلبك بشكل صحيح تأكد من البيانات المدخله ثم حاول مرة أخرى';
                              Swal.fire({
                                  position: 'center'
                                  , type: 'error'
                                  , title: maessageResponse
                                  , showConfirmButton: false
                                  , timer: 3000
                              })
                          } else{
                            maessageResponse = data;
                            Swal.fire({
                                position: 'center'
                                , type: 'error'
                                , title: maessageResponse
                                , showConfirmButton: true
                            })

                          }
                        }
                });
            }
            if (modalTitle === 'AddCustomActions') {
                var actionRequiredm = document.getElementById("actionRequiredm").value;
                var duration = document.getElementById("duration").value;
                var description = document.getElementById("description").value;
                var action = 'AddCustomActions';
                var atLeastOneIsChecked = $('input[name="sourceEmpck"]:checked').length;
                if (atLeastOneIsChecked > 0) {
                    if (typeof selectedEmployees != "undefined" && selectedEmployees != null && selectedEmployees.length != null && selectedEmployees.length > 0) {
                        selectionsEmployees = selectedEmployees.join(',') + ',' + assignedToToInsert;
                    }
                    else {
                        selectionsEmployees = assignedToToInsert;
                    }
                }
                else {
                    selectionsEmployees = selectedEmployees.join(',');
                }
                var completeIsChecked = $('input[name="completeck"]:checked').length;
                if (completeIsChecked > 0) {
                    checkedValue = '1';
                }
                else {
                    checkedValue = '';
                }


                             var fd = new FormData();
                             fd.append('allFilesNames', allFilesNames);

                             fd.append('description', description);
                             fd.append('idg', idg);
                             fd.append('reqidg', reqidg);
                             fd.append('selectRequestg', selectRequestg);
                             fd.append('action', action);
                             fd.append('checkedValue', checkedValue);
                             fd.append('selectionsEmployees', selectionsEmployees);
                             fd.append('duration', duration);
                             fd.append('actionRequiredm', actionRequiredm);

                $.ajax({
                  url: '../dist/php/ajax_action.php',
                  type: 'post',
                  data: fd
                  ,contentType: false,
                  processData: false,
                  success: function(data) {
                        if (data == 0) {
                                $('#recordModal').modal('hide');
                                $('#recordForm')[0].reset();
                                $('#save').attr('disabled', false);
                                maessageResponse = 'الطلب تم إرساله بشكل صحيح';
                                Swal.fire({
                                    position: 'center'
                                    , type: 'success'
                                    , title: maessageResponse
                                    , showConfirmButton: false
                                    , timer: 3000
                                })
                                window.location.reload();
                            }
                            else if (data == -1) {
                                maessageResponse = 'لم يتم طلبك بشكل صحيح تأكد من البيانات المدخله ثم حاول مرة أخرى';
                                Swal.fire({
                                    position: 'center'
                                    , type: 'error'
                                    , title: maessageResponse
                                    , showConfirmButton: false
                                    , timer: 3000
                                })
                            } else{
                              maessageResponse = data;
                              Swal.fire({
                                  position: 'center'
                                  , type: 'error'
                                  , title: maessageResponse
                                  , showConfirmButton: true
                              })

                            }
                    }
                });
            }
        });
        // on load of the page: switch to the currently selected tab
        // on load of the page: switch to the currently selected tab

        $("a#custom-tabs-one-messages-tab").on("shown.bs.tab", function (e) {
            localStorage.setItem('currentActiveTab', 'custom-tabs-one-messages-tab');
            $('a#custom-tabs-one-messages-tab').click();

        });
        $("a#custom-tabs-one-profile-tab").on("shown.bs.tab", function (e) {
            localStorage.setItem('currentActiveTab', 'custom-tabs-one-profile-tab');
        });
        $("a#custom-tabs-one-home-tab").on("shown.bs.tab", function (e) {
            localStorage.setItem('currentActiveTab', 'custom-tabs-one-home-tab');
        });


        $('#completeck').change(function () {
            if (this.checked) {
                document.getElementById("actionRequiredm").removeAttribute("required");
                document.getElementById("duration").removeAttribute("required");
                document.getElementById("actionreqInputDiv").style.display = 'none';
                document.getElementById("durationInputDiv").style.display = 'none';
            }
            else {
                if (document.getElementById("actionm").value == 'AddCustomActions') {
                    document.getElementById("actionRequiredm").setAttribute("required", "");
                    document.getElementById("duration").setAttribute("required", "");
                    document.getElementById("actionreqInputDiv").style.display = 'block';
                    document.getElementById("durationInputDiv").style.display = 'block';
                }
            }
        });
        $(document).on('click', '.update', function () {
            var idt = $(this).data("id1");
            var action = 'getRecordRequest'; //it will call the record with selected editbutton id
            $.ajax({
                url: '../dist/php/ajax_action.php'
                , method: "POST"
                , data: {
                    idt: idt
                    , action: action
                }
                , dataType: "json"
                , success: function (data) { //the data return in json format from db to be used in the modal.
                    $('#recordModal').modal('show');
                    $('#description').val(data.notes);
                    $('.modal-title').html("<i></i> رؤية التفاصيل");
                    document.getElementById("save").style.display = 'none';
                    document.getElementById("selectRequest").removeAttribute("required");
                    document.getElementById("actionRequiredm").removeAttribute("required");
                    document.getElementById("duration").removeAttribute("required");
                    document.getElementById("checkboxDiv").style.display = 'none';
                    document.getElementById("selectAssignedtoDiv").style.display = 'none';
                    document.getElementById("actionreqInputDiv").style.display = 'none';
                    document.getElementById("durationInputDiv").style.display = 'none';
                    document.getElementById("returncheckboxDiv").style.display = 'none';
                    document.getElementById("selectRequestDiv").style.display = 'none';
                    document.getElementById("selectFileDiv").style.display = 'none';
                }
            });
        });

        function getUnique(array) {
            var uniqueArray = [];
            // Loop through array values
            for (i = 0; i < array.length; i++) {
                if (uniqueArray.indexOf(array[i]) === -1) {
                    uniqueArray.push(array[i]);
                }
            }
            return uniqueArray;
        }

        $('.checkall').change(function () {
            $('.checkboxtd').each(function () {
                if ($('.checkall').is(':checked')) {
                    $(this).attr("disabled", false);
                    $(this).prop("checked", true);
                }
                else {
                    $(this).prop("checked", false);
                }
            });
        });


              });

              $('#btn_upload').click(function() {
                uploadFiles('userfile','#documentsm','../../upload/requests/');

              });
              $(document).on('click', 'button.documentdelete', function(event) {
                var mine = event.target;
                var formID = $(mine).closest('form')[0].id;
                var link;

              link = '../../upload/requests/';

              var fileToDelete = $(mine).data('filename');
            deleteFile(link,fileToDelete,mine);

              });
function fillSelectRequest(){
  var action = 'fillSelectRequest';
  $.ajax({
    url: '../dist/php/requests.php',
    type: 'POST',
    data: {
        action: action,
    }
    , dataType:"json"
    , success: function(data) {
var i = 0 ;
var text;

data.forEach(row => {
if(i == 0){
  text = '<option value="'+row.requestTCode+'">'+row.name+'</option>';
}else{
  text += '<option value="'+row.requestTCode+'">'+row.name+'</option>';

}
i = i + 1;
      });
  //to conver it to Json after contactit as string  to be used together
  $('#selectRequest').html(text);
  }
});

}
function countingRequestsHandeled(){
  var action = 'countingRequestsHandeled';
  $.ajax({
    url: '../dist/php/requests.php',
    type: 'POST',
    data: {
        action: action,
    }
    , dataType:"json"
    , success: function(data) {

$('a#new').html(data['new']);
$('a#handeled').html(data['handeled']);


  }
});

}
fillSelectRequest();
countingRequestsHandeled();
    </script>
