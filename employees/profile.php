<?php
session_start();
if ($_SESSION['levelid']) {
} else {
    header("Location: ../login.html");
}
    ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>بروفايل المستخدم</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

      <!-- summernote -->
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- SweetAlert2 and Toast -->
  <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- SweetAlert2 and Toast -->
  <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
  <link rel="stylesheet" href="../dist/css/responsiveTable.css">

</head>
<style media="screen">
.blogImage { /* here the the images to make it responsive use flex and wrap */
  display: block;
  margin: 0;
  max-width: 100%;
  min-height: 248px;
  min-width: 100%;
  margin-top: 25px;
}
.page-link-prevN {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #007bff;
    background-color: #fff;
    border: 1px solid #dee2e6;
}
.contactCard{
  display: flex!important;
  flex: 0 0 100%;
  max-width: 100;
  position: relative;
width: 100%;
padding-right: 7.5px;
padding-left: 7.5px;
}

</style>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="../index.php" class="nav-link">البداية</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown al">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments" id="cc"></i>
          <span class="badge badge-danger navbar-badge" id="countAlert"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

      <div class="dropdown-divider"></div>
                                      </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown al">
          <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
              <div class="dropdown-divider"></div>
          </div>
      </li>
      <li class="nav-item dropdown al">
          <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
              <div class="dropdown-divider"></div>
          </div>
      </li>
      <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../index.php" class="brand-link">
      <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">PIONEER</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a  id="userName" href="#" class="d-block"></a>
          <a href="#" id="userStatus"></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                 <a href="../index.php" class="nav-link">
                   <i class="nav-icon fas fa-tachometer-alt"></i>
                   <p>
                     لوحة التحكم
                   </p>
                 </a>
               </li>
          <li class="nav-item">
            <a href="./profile.php" class="nav-link active">
              <i class="nav-icon fas fa-id-card-alt"></i>
              <p>
                البروفايل
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="./requests.php" class="nav-link">
              <i class="nav-icon fas fa-list-ol"></i>
              <p>
                الطلبات
              </p>
            </a>
          </li>



          <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                أقسام الإدارة المتوسطة
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../midEmployees/projects.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>قسم المشاريع</p>
                </a>
              </li>

            </ul>

          </li>



          <li class="nav-item has-treeview" id="admin-section" style="display:none;">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                 أقسام الإدارة العليا
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../admin/employees.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p> قسم الموارد البشرية</p>
                </a>
              </li>

            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../admin/others.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p> قسم العملاء والمقاولين</p>
                </a>
              </li>

            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../admin/AdminRequests.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>قسم خصائص المعاملات</p>
                </a>
              </li>

            </ul>
            <ul class="nav nav-treeview">

            <li class="nav-item">
<a href="../sms/form.php" class="nav-link">
<i class="nav-icon fas fa-sms"></i>
<p>
SMS
</p>
</a>
</li>
</ul>

          </li>
          <li class="nav-item">
        <a href="../employees/news.php" class="nav-link">
        <i class="nav-icon fas fa-newspaper"></i>
        <p>
        آخر الأخبار
        <span class="badge badge-info right" id="blogsCounting"></span>
        </p>
        </a>
        </li>


          <li class="nav-item">
 <a id="logout" class="nav-link">
   <i class="nav-icon  ion-log-out"></i>
   <p>خروج</p>
 </a>
 </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

</div>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>الملف الشخصي</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../index.php">البداية</a></li>
              <li class="breadcrumb-item active">الملف الشخصي</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile" id="profileInfor">


              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!--  Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">ماذا عني؟</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body" id="aboutMe">


              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">المقالات الشخصية</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">الخط الزمني</a></li>
                    <li class="contacts-section nav-item"><a class="nav-link" href="#contacts" data-toggle="tab">جهات الاتصال</a></li>

                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
<div style="display:none;" id="blogTitleSendDiv">
                                            <label for="title">العنوان:</lablel>
                                              <input name"title" type="text"  id="blogTitleSend">
                                            </div>
                                              <br>   <br>

                      <div class="post" id="writing">
              <!-- Main content -->

 <div class="card card-outline card-info">

            <div class="card-header">

              <h3 class="card-title">
                 اكتب مقالك
                <small>ببساطة وسرعة</small>
              </h3>
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <form id="blogForm" method="post">

              <div class="mb-3">
                <textarea class="textarea form-control" placeholder="Place some text here" name="blogArea" id="blogArea"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
              <input type="hidden" name="action" value="sendBlog" class="form-control"/>
              <button type="submit" class="btn btn-primary" id="sendblog">إرسال</button>
            </div>
          </form>

          </div>


                      </div>
                      <div  id="MainDivPosts">

                      </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse" id="timelinediv">
                      <!-- timeline time label -->


                    </div>
                  </div>
                  <!-- /.tab-pane -->


                  <!-- /.tab-pane -->
                                      <div class="contacts-section tab-pane" id="contacts">
                                        <div class="input-group input-group-sm">
                                      <input  type="search" placeholder="بحث" aria-label="Search" id="search-weeazer" onfocus="this.value=''" value="  ">
                                      <div class="input-group-append">
                                        <button class="btn btn-navbar" type="submit">
                                          <i class="fas fa-search"></i>
                                        </button>
                                      </div>
                                    </div>
                                    <br>

      <!-- Default box -->
      <div class="card card-solid">

        <div class="card-body pb-0">


          <div class="row d-flex align-items-stretch" id="cardsToSearch">

        </div>
        <!-- /.card-body -->

        <div class="pagination card-footer" >
          <nav aria-label="Contacts Page Navigation" id="pagination">
            <ul class="pagination justify-content-center m-0" >
              <!-- <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">4</a></li>
              <li class="page-item"><a class="page-link" href="#">5</a></li>
              <li class="page-item"><a class="page-link" href="#">6</a></li>
              <li class="page-item"><a class="page-link" href="#">7</a></li>
              <li class="page-item"><a class="page-link" href="#">8</a></li> -->
          </ul>
          </nav>
        </div>
        <!-- /.card-footer -->
      </div>  <!-- /.tab-pane -->

      </div>


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
<div class="float-right d-none d-sm-block">
<b>Version</b> 3.0.0
</div>
<strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
reserved.
</footer>
<div class="wrapper">
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
<!-- Control sidebar content goes here -->
</aside>
</div>

<!-- /.control-sidebar -->
<!-- ./wrapper -->
<!-- here should end all of cardbody -->
                  <!-- ************************************************8 -->
<!-- UnifedModals -->
<div class="custom modal fade" id="blogModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
           <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
            <i class="fas fa-times"></i>
          </button> -->

          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                           </div>
        <div class="modal-body">

 <div class="card bg-light">
     <!-- Grid row -->
     <div class="row">

       <!-- Grid column -->
       <div class="col-12">

         <!-- Exaple 1 -->
         <div class="card example-1 scrollbar-ripe-malinka">
           <div class="card-body"  id="MainDivPostsm">

           </div>
         </div>
         <!-- Exaple 1 -->

       </div>
       <!-- Grid column -->



     </div>
     <!-- Grid row -->

   </div>
 </div>
</div>

</div>
</div>
<!-- ./blog Modal -->

<div class="custom modal fade" id="chatModal">
 <div class="modal-dialog">
   <div class="modal-content">
         <div class="modal-header">

           <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
            <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
             <i class="fas fa-times"></i>
           </button> -->

           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                            </div>

     <div class="modal-body">
 <!-- /.card-header -->
 <div class="card direct-chat direct-chat-primary">
 <div class="card-body">
   <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
<div class="image">
<img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
</div>
<div class="info">
<a href="#" class="d-block"  id="receivernamem"></a>
<a href="#" id="statusr2"></a>
</div>
</div>
<hr>
   <!-- Conversations are loaded here -->
   <div class="direct-chat-messages" id="direct-chat-messages2">


   </div>
   <!--/.direct-chat-messages-->

   <!-- Contacts are loaded here -->
   <div class="direct-chat-contacts" >
     <ul class="contacts-list" id="direct-chat-contacts">
       <!-- End Contact Item -->
     </ul>
     <!-- /.contacts-list -->
   </div>
   <!-- /.direct-chat-pane -->
 </div>
 <!-- /.card-body -->
 <div class="card-footer">
   <form action="#" method="post" id="myForm2">
     <div class="input-group">
       <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
       <input type="hidden" name="action" value="sendChat" class="form-control">
       <span class="input-group-append">
         <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
       </span>
     </div>
   </form>
 </div>
 <!-- /.card-footer-->
</div>
</div>
</div>

</div>
</div>
<!--/.direct-chat modal -->



</body>
</html>

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<script src="../dist/js/blogs.js"></script>
<script src="../dist/js/unified.js"></script>

<script>
//$('#cardsToSearch').dataTables();
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
function loadContactCards(){

  var action = 'displayContactsCards';
  $.ajax({
      url: '../dist/php/contactsCards.php'
      , type: 'POST'
      , data: {
          action: action
      }
      , dataType: "json"
      , success: function (data) {
        var jsonarrays = data;
          jsonarrays.forEach(rowd => {
            var divCard = document.createElement('div');
            var htmlToAppend = document.getElementById("cardsToSearch");
divCard.className ="contactCard";
        divCard.innerHTML = '<div class="card bg-light"><div class="card-header text-muted border-bottom-0 jobTitleAndDepartment">'+
        rowd.jobTitle + ' - ' + rowd.departmentName + '</div><div class="card-body pt-0"><div class="row"><div class="col-7"><h2 class="lead"><b>'+
        rowd.username + '</b></h2><p class="text-muted text-sm about"><b></b>'+
        rowd.userNote+'</p><ul class="ml-4 mb-0 fa-ul text-muted"><li class="small phoneNum"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span>'+
        rowd.officeNo +'-'+rowd.mobile1+'-'+ rowd.mobile2 + '.'+'</li></ul></div><div class="col-5 text-center">'+
'<img src="../dist/img/'+ rowd.userPic +'" alt="user image" class="img-circle img-fluid" width="128" height="128px"></div></div></div>'+
        '<div class="card-footer"><div class="text-right"><button class="btn btn-sm bg-teal btn_chat" data-id1="' + btoa(rowd.employeeid) + '"><i class="fas fa-comments"></i> شات</button>'+
        '<a href="?uid='+ btoa(rowd.employeeid)+'" class="btn btn-sm btn-primary"><i class="fas fa-user"></i> رؤية البروفايل</a>'+
         '</div></div></div>';
        htmlToAppend.append(divCard);




      });

    //  paging(pageNu);

$("#cardsToSearch").pagify(9, ".contactCard");

      }
  });
}
loadContactCards();

$searchBox = $('#search-weeazer');
$searchBox.on('focus', function() {
$searchBox.trigger('input');

});
$searchBox.on('focusout', function() {
  $("#cardsToSearch").pagify(9, ".contactCard");


});
$searchBox.on('input', function() {


  $cardsToSearchDiv = $('#cardsToSearch div');

  var scope = this;
  if (!scope.value || scope.value == '') {
    $cardsToSearchDiv.show();
    return;
  }

  $cardsToSearchDiv.each(function(i, div) {
    var $div = $(div).parents('.contactCard div');
    var jobtitle = $div.find('.jobTitleAndDepartment').text();
    var name =  $div.find('.lead').text();
    var about =  $div.find('.about').text();
    var phoneNum =  $div.find('.phoneNum').text();


    var valuesToSearch = jobtitle+name+about+phoneNum;

    $div.toggle(valuesToSearch.toLowerCase().indexOf(scope.value.toLowerCase()) > -1);

  })

  });

  </script>

    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>

<script>

(function($) {
var pagify = {
  items: {},
  container: null,
  totalPages: 1,
  perPage: 3,
  currentPage: 0,
  createNavigation: function() {
    this.totalPages = Math.ceil(this.items.length / this.perPage);

    $('.pagination', this.container.parent()).remove();

    var pagination = $('<ul class="pagination justify-content-center m-0" ></ul>').append('<li class="page-item"><a class="nav prev disabled page-link-prevN" data-next="false"><</a></li>');


    for (var i = 0; i < this.totalPages; i++) {

      var pageElClass = "page-link active";
      if (!i)
        pageElClass = "page-link";
      var pageEl = '<li class="page-item"><a class="' + pageElClass + '" data-page="' + (
      i + 1) + '">' + (
      i + 1) + "</a></li>";

    pagination.append(pageEl);
    }
    pagination.append('<li class="page-item"><a class="nav next page-link-prevN" data-next="true">></a></li>');

    this.container.after(pagination);

    var that = this;
    $("body").off("click", ".nav");
    this.navigator = $("body").on("click", ".nav", function() {
      var el = $(this);
      that.navigate(el.data("next"));
    });

    $("body").off("click", ".page-link");
    this.pageNavigator = $("body").on("click", ".page-link", function() {
      var el = $(this);
      that.goToPage(el.data("page"));
    });
  },
  navigate: function(next) {
    // default perPage to 5
    if (isNaN(next) || next === undefined) {
      next = true;
    }
    $(".pagination .nav").removeClass("disabled");
    if (next) {
      this.currentPage++;
      if (this.currentPage > (this.totalPages - 1))
        this.currentPage = (this.totalPages - 1);
      if (this.currentPage == (this.totalPages - 1))
        $(".pagination .nav.next").addClass("disabled");
      }
    else {
      this.currentPage--;
      if (this.currentPage < 0)
        this.currentPage = 0;
      if (this.currentPage == 0)
        $(".pagination .nav.prev").addClass("disabled");
      }

    this.showItems();
  },
  updateNavigation: function() {

    var pages = $(".pagination .page-link");
    pages.removeClass("active");
    $('.pagination .page-link[data-page="' + (
    this.currentPage + 1) + '"]').addClass("active");
  },
  goToPage: function(page) {

    this.currentPage = page - 1;

    $(".pagination .nav").removeClass("disabled");
    if (this.currentPage == (this.totalPages - 1))
      $(".pagination .nav.next").addClass("disabled");

    if (this.currentPage == 0)
      $(".pagination .nav.prev").addClass("disabled");
    this.showItems();
  },
  showItems: function() {
    this.items.hide();
    var base = this.perPage * this.currentPage;
    this.items.slice(base, base + this.perPage).show();

    this.updateNavigation();
  },
  init: function(container, items, perPage) {
    this.container = container;
    this.currentPage = 0;
    this.totalPages = 1;
    this.perPage = perPage;
    this.items = items;
    this.createNavigation();
    this.showItems();
  }
};

// stuff it all into a jQuery method!
$.fn.pagify = function(perPage, itemSelector) {
  var el = $(this);
  var items = $(itemSelector, el);

  // default perPage to 5
  if (isNaN(perPage) || perPage === undefined) {
    perPage = 3;
  }

  // don't fire if fewer items than perPage
  if (items.length <= perPage) {

    perPage = items.length;
  //  return true;
  }

  pagify.init(el, items, perPage);
};
})(jQuery);



</script>
