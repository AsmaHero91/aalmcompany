<?php
session_start();
if ($_SESSION['levelid']) {
} else {
    header("Location: ../login.html");
}
    ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>آخر الأخبار</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

      <!-- summernote -->
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- SweetAlert2 and Toast -->
  <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <link rel="stylesheet" href="../dist/css/responsiveTable.css">
  <!-- SweetAlert2 and Toast -->
  <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
</head>
<style media="screen">
.blogImage { /* here the the images to make it responsive use flex and wrap */
  display: block;
  margin: 0;
  max-width: 100%;
  min-height: 248px;
  min-width: 100%;
  margin-top: 25px;
}
.page-link-prevN {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #007bff;
    background-color: #fff;
    border: 1px solid #dee2e6;
}
.contactCard{
  display: flex!important;
  flex: 0 0 100%;
  max-width: 100;
  position: relative;
width: 100%;
padding-right: 7.5px;
padding-left: 7.5px;
}

</style>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="../index.php" class="nav-link">البداية</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown al">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments" id="cc"></i>
          <span class="badge badge-danger navbar-badge" id="countAlert"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

      <div class="dropdown-divider"></div>
                                      </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown al">
          <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
              <div class="dropdown-divider"></div>
          </div>
      </li>
      <li class="nav-item dropdown al">
          <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
              <div class="dropdown-divider"></div>
          </div>
      </li>
      <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../index.php" class="brand-link">
      <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">PIONEER</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a  id="userName" href="#" class="d-block"></a>
          <a href="#" id="userStatus"></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

               <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item">
                      <a href="../index.php" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                          لوحة التحكم
                        </p>
                      </a>
                    </li>
               <li class="nav-item">
                 <a href="./profile.php" class="nav-link">
                   <i class="nav-icon fas fa-id-card-alt"></i>
                   <p>
                     البروفايل
                   </p>
                 </a>
               </li>

               <li class="nav-item">
                 <a href="./requests.php" class="nav-link">
                   <i class="nav-icon fas fa-list-ol"></i>
                   <p>
                     الطلبات
                   </p>
                 </a>
               </li>



               <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
                 <a href="#" class="nav-link">
                   <i class="nav-icon fas fa-edit"></i>
                   <p>
                     أقسام الإدارة المتوسطة
                     <i class="fas fa-angle-left right"></i>
                   </p>
                 </a>
                 <ul class="nav nav-treeview">
                   <li class="nav-item">
                     <a href="../midEmployees/projects.php" class="nav-link">
                       <i class="far fa-circle nav-icon"></i>
                       <p>قسم المشاريع</p>
                     </a>
                   </li>

                 </ul>

               </li>



               <li class="nav-item has-treeview" id="admin-section" style="display:none;">
                 <a href="#" class="nav-link">
                   <i class="nav-icon fas fa-edit"></i>
                   <p>
                      أقسام الإدارة العليا
                     <i class="fas fa-angle-left right"></i>
                   </p>
                 </a>
                 <ul class="nav nav-treeview">
                   <li class="nav-item">
                     <a href="../admin/employees.php" class="nav-link">
                       <i class="far fa-circle nav-icon"></i>
                       <p> قسم الموارد البشرية</p>
                     </a>
                   </li>

                 </ul>
                 <ul class="nav nav-treeview">
                   <li class="nav-item">
                     <a href="../admin/others.php" class="nav-link">
                       <i class="far fa-circle nav-icon"></i>
                       <p> قسم العملاء والمقاولين</p>
                     </a>
                   </li>

                 </ul>
                 <ul class="nav nav-treeview">
                   <li class="nav-item">
                     <a href="../admin/AdminRequests.php" class="nav-link active">
                       <i class="far fa-circle nav-icon"></i>
                       <p>قسم خصائص المعاملات</p>
                     </a>
                   </li>

                 </ul>
                 <ul class="nav nav-treeview">

                 <li class="nav-item">
     <a href="../sms/form.php" class="nav-link">
     <i class="nav-icon fas fa-sms"></i>
     <p>
     SMS
     </p>
     </a>
     </li>
     </ul>

               </li>
               <li class="nav-item">
             <a href="../employees/news.php" class="nav-link active">
             <i class="nav-icon fas fa-newspaper"></i>
             <p>
             آخر الأخبار
             <span class="badge badge-info right" id="blogsCounting"></span>
             </p>
             </a>
             </li>

          <li class="nav-item">
 <a id="logout" class="nav-link">
   <i class="nav-icon  ion-log-out"></i>
   <p>خروج</p>
 </a>
 </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <br>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
<h3 class="profile-username text-center">  آخر الأخبار   <i class="nav-icon fas fa-newspaper"></i></h3>
</div>
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">

                      <div class="post" id="writing">
              <!-- Main content -->

                      </div>
                      <div  id="MainDivPosts">

                      </div>

                  </div>
                  <!-- /.tab-pane -->

      <!-- Default box -->



</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- /.content-wrapper -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <strong>Copyright &copy; 2020 PIONEER</strong>
  All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 3.0.0
  </div>
</footer>

<div class="wrapper">
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
<!-- Control sidebar content goes here -->
</aside>
</div>

<!-- /.control-sidebar -->
<!-- ./wrapper -->
<!-- here should end all of cardbody -->
                  <!-- ************************************************8 -->
<!-- UnifedModals -->
<!-- UnifedModals -->
<div class="modal custom fade" id="blogModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
           <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
            <i class="fas fa-times"></i>
          </button> -->

          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                           </div>
        <div class="modal-body">

 <div class="card bg-light">
     <!-- Grid row -->
     <div class="row">

       <!-- Grid column -->
       <div class="col-12">

         <!-- Exaple 1 -->
         <div class="card example-1 scrollbar-ripe-malinka">
           <div class="card-body"  id="MainDivPostsm">

           </div>
         </div>
         <!-- Exaple 1 -->

       </div>
       <!-- Grid column -->



     </div>
     <!-- Grid row -->

   </div>
 </div>
</div>

</div>
</div>
<!-- ./blog Modal -->

<div class="custom modal fade" id="chatModal">
 <div class="modal-dialog">
   <div class="modal-content">
         <div class="modal-header">

           <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
            <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
             <i class="fas fa-times"></i>
           </button> -->

           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                            </div>

     <div class="modal-body">
 <!-- /.card-header -->
 <div class="card direct-chat direct-chat-primary">
 <div class="card-body">
   <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
<div class="image">
<img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
</div>
<div class="info">
<a href="#" class="d-block"  id="receivernamem"></a>
<a href="#" id="statusr2"></a>
</div>
</div>
<hr>
   <!-- Conversations are loaded here -->
   <div class="direct-chat-messages" id="direct-chat-messages2">


   </div>
   <!--/.direct-chat-messages-->

   <!-- Contacts are loaded here -->
   <div class="direct-chat-contacts" >
     <ul class="contacts-list" id="direct-chat-contacts">
       <!-- End Contact Item -->
     </ul>
     <!-- /.contacts-list -->
   </div>
   <!-- /.direct-chat-pane -->
 </div>
 <!-- /.card-body -->
 <div class="card-footer">
   <form action="#" method="post" id="myForm2">
     <div class="input-group">
       <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
       <input type="hidden" name="action" value="sendChat" class="form-control">
       <span class="input-group-append">
         <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
       </span>
     </div>
   </form>
 </div>
 <!-- /.card-footer-->
</div>
</div>
</div>

</div>
</div>
<!--/.direct-chat modal -->
<!-- /.Unified Modals -->

</body>
</html>

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Summernote -->
<script src="../dist/js/blogs2.js"></script>
<script src="../dist/js/unified.js"></script>
