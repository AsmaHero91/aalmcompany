<?php
session_start();
if ($_SESSION['levelid']) {
    //successfully login
} else {
    header("Location: login.html");
}

?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>لوحة البيانات</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->

  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- SweetAlert2 and Toast -->
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

  <link rel="stylesheet" href="dist/css/modalStyle.css">
  <!-- DataTables styles with filters -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="dist/css/blogModal.css">
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> -->
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
  <link href="dist/css/loadingStyle.css" rel="stylesheet">

  <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
  <script  defer src='dist/js/pdfmake-master/build/pdfmake.min.js'></script>
            <script defer  src='dist/js/pdfmake-master/build/vfs_fonts.js'></script>

</head>
<style media="screen">

@media screen and (max-width: 600px) {
  /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr:contains('row') {
      display: block;
    }

    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr {
      position: absolute;
      top: -9999px;
      left: -9999px;
    }

    tr { border: 1px solid #ccc; }

    td {
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee;
      position: relative;
      padding-left: 50%;
    }

    td:contains('row'):before {
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      /* Label the data */
		content: attr(data-column);
    }
  }
</style>

<body class="hold-transition sidebar-mini layout-fixed">

  <div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="index.php" class="nav-link">البداية</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown al">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments" id="cc"></i>
          <span class="badge badge-danger navbar-badge" id="countAlert"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

      <div class="dropdown-divider"></div>
                                      </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown al">
          <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
              <div class="dropdown-divider"></div>
          </div>
      </li>
      <li class="nav-item dropdown al">
          <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
              <div class="dropdown-divider"></div>
          </div>
      </li>
      <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">PIONEER</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a  id="userName" href="#" class="d-block"></a>
          <a href="#" id="userStatus"></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="index.php" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                لوحة التحكم
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="employees/profile.php" class="nav-link">
              <i class="nav-icon fas fa-id-card-alt"></i>
              <p>
                البروفايل
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="employees/requests.php" class="nav-link">
              <i class="nav-icon fas fa-list-ol"></i>
              <p>
                الطلبات
              </p>
            </a>
          </li>



          <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                أقسام الإدارة المتوسطة
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="midEmployees/projects.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>قسم المشاريع</p>
                </a>
              </li>

            </ul>

          </li>



          <li class="nav-item has-treeview" id="admin-section" style="display:none;">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                 أقسام الإدارة العليا
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin/employees.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p> قسم الموارد البشرية</p>
                </a>
              </li>

            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin/others.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p> قسم العملاء والمقاولين</p>
                </a>
              </li>

            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin/AdminRequests.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>قسم خصائص المعاملات</p>
                </a>
              </li>

            </ul>
            <ul class="nav nav-treeview">

            <li class="nav-item">
<a href="sms/form.php" class="nav-link">
<i class="nav-icon fas fa-sms"></i>
<p>
SMS
</p>
</a>
</li>
</ul>

          </li>

                     <li class="nav-item">
            <a href="employees/news.php" class="nav-link">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                آخر الأخبار
                <span class="badge badge-info right" id="blogsCounting"></span>
              </p>
            </a>
          </li>




          <li class="nav-item">
 <a id="logout"  class="nav-link">
   <i class="nav-icon  ion-log-out"></i>
   <p>خروج</p>
 </a>
 </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">لوحة التحكم</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<!-- End of unuified Content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3 id="allr"></h3>

                <p>مجموع الطلبات المعالجة</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="./employees/requests.php" class="small-box-footer">اتجه إلى قسم الطلبات <i class="fas fa-arrow-circle-right"></i></a>

            </div>
          </div>
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><span id="precentage"></span>%</h3>
                <p>
                <!-- <span class="fa fa-star checkedstar"></span>
  <span class="fa fa-star checkedstar"></span>
  <span class="fa fa-star checkedstar"></span>
  <span class="fa fa-star"></span>
  <span class="fa fa-star"></span> -->
 <span id="new"> </span><span id="handeled"></span>

</p>
              </div>
              <div class="icon">
                <i class="ion ion-clipboard"></i>
              </div>
              <a href="./employees/requests.php" class="small-box-footer">نسبة الانجاز اليومي <i class="fas fa-arrow-circle-right"></i></a>

            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3 id="innerappraisals"></h3>

                <p>التقديرات</p>
              </div>
              <div class="icon">
                <i class="ion ion-happy"></i>
              </div>
              <a href="#" class="logs small-box-footer"  id="appraisals">مزيد من المعلومات <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-2 col-6">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3 id="innersalary"></h3>

              <p>سجلات الرواتب</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="salarylogs small-box-footer"  id="salary">مزيد من المعلومات<i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3  id="innerbonus"></h3>

                <p>سجلات البونس</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="logs small-box-footer"  id="bonus">مزيد من المعلومات <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

          <!-- ./col -->
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3 id="innerdeduct"></h3>

                <p>سجلات الخصم</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="logs small-box-footer" id="deduct">مزيد من المعلومات <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">

                  الإجازات
                </h3>

              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                <div class="col-lg-4 col-4">
                  <div class="small-box bg-warning">
                    <div class="inner">


                      <h3>  <p>إجازات مرضية</p></h3>
                      <p id="sickVn"></p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-first-aid"></i>
                    </div>
                    <a href="#" class="small-box-footer" id="sickV">اطلب المزيد <i class="fas fa-plus"></i></a>

                  </div>
                </div>
                  <div class="col-lg-4 col-4">
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>  <p>إجازات سنوية</p></h3>


                    <p id="yearVn"></p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-umbrella-beach"></i>
                    </div>
                    <a href="#" class="small-box-footer" id="yearV">اطلب المزيد <i class="fas fa-plus"></i></a>

                  </div>
                    </div>
                    <div class="col-lg-4 col-4">
                      <div class="small-box bg-success">
                        <div class="inner">
                        <h3>  <p>طلبات الأجازة</p></h3>
                        <p id="vacationN"></p>

                        </div>
                        <div class="icon">
                          <i class="fas fa-umbrella-beach"></i>
                        </div>
<a href="#" class="small-box-footer" id="vacationsInfo">رؤية الطلبات <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>

                    <div class="col-lg-6 col-6">
                      <div class="small-box bg-info">
                        <div class="inner">

<p>تاريخ استحقاق الاجازة القادمة</p>
                          <p id="vacationNext"></p>
                        </div>

                      </div>
                    </div>


<div class="col-lg-6 col-6">
  <div class="small-box bg-info">
    <div class="inner">

<p>تاريخ بدء العمل</p>
      <p id="joinDate"></p>
    </div>

  </div>
</div>

</div>
              </div><!-- /.card-body -->

            </div>
            <!-- /.card -->

            <!-- DIRECT CHAT -->
            <div class="card direct-chat direct-chat-primary">
              <div class="card-header">
                <h3 class="card-title">محادثة مباشرة</h3>

                <div class="card-tools">
                  <span data-toggle="tooltip" title="3 New Messages" id="countUnreadMessages"class="badge badge-primary"></span>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" id="viewMessagesSubjectsList" class="btn btn-tool" data-toggle="tooltip" title="Contacts"
                          data-widget="chat-pane-toggle">
                    <i class="fas fa-comments"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
          <div class="image">
            <img id="receiverpic" src="" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info" id="receiverInfo">
            <a href="#" class="d-block"  id="receivername"></a>
            <a href="#" id="statusr"></a>

          </div>
        </div>
        <hr>
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages" id="direct-chat-messages">


                </div>
                <!--/.direct-chat-messages-->

                <!-- Contacts are loaded here -->
                <div class="direct-chat-contacts" >
                  <ul class="contacts-list" id="direct-chat-contacts">
                    <!-- End Contact Item -->
<li>لم تبدأ بعد أي محادثة</li>
                  </ul>
                  <!-- /.contacts-list -->
                </div>
                <!-- /.direct-chat-pane -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <form action="#" method="post" id="myForm">
                  <div class="input-group">
                    <input type="text" name="message" id="chatMessage" placeholder="Type Message ..." class="form-control" >
                    <input type="hidden" name="action" value="sendChat" class="form-control">
                    <span class="input-group-append">
                      <button type="button" class="btn btn-primary" id="sendChat">إرسال</button>
                    </span>
                  </div>
                </form>
              </div>
              <!-- /.card-footer-->
            </div>
            <!--/.direct-chat -->

            <!-- TO DO List -->

            <!-- /.card -->
          </section>

          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  قائمة مهام المشاريع
                </h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body" id="todo-list-Div">
                <ul class="todo-list" data-widget="todo-list" id="todolistP">

                </ul>
                <div id="pagination">

                </div>

              </div>
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  قائمة المشاريع المسجلة
                </h3>

              </div>
              <div class="card-body" id="project-list-Div">
                <ul class="todo-list" data-widget="todo-list" id="projectsList">


                </ul>
<div id="pagination2">

</div>
              </div>

            <div class="card bg-gradient-success">
              <div class="card-header border-0">

                <h3 class="card-title">
                  <i class="far fa-calendar-alt"></i>
                  التقويم
                </h3>
                <!-- tools card -->
                <div class="card-tools">
                  <!-- button with a dropdown -->

                  <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /. tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body pt-0">
                <!--The calendar -->
                <div id="calendar" style="width: 100%"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-card mr-1"></i>
تاريخ إنتهاء الهوية الوطنية                </h3>
              </div>
                <div class="card-body">


          <p id="endCard"></p>

        </div>
      </div>
          </section>



  </div>
            <!-- Calendar -->

          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

    <!-- /.content -->
    <!-- hidden tables -->
    <div class="hiddenTable" id="vacationsTableDiv" style="display: none;">
      <div class="card-body" style="overflow-x:auto;">

      <table id="vacationsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
              <tr>
                <th>#</th>
                  <th width="35%">الملاحظة</th>
                  <th width="35%">نوع الطلب</th>
                  <th width="35%">حالة الطلب</th>
                  <th class="no-sort"></th>
              </tr>
          </thead>
          <tbody id="vacationsTbody">

          </tbody>
      </table>

</div>
</div>
<div class="hiddenTable" id="logsTableDiv" style="display: none;">
  <div class="card-body" style="overflow-x:auto;">

<table id="logsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th width="35%">النقاط</th>
            <th width="35%">السبب</th>
            <th width="35%">التاريخ</th>
            <th width="35%">المبلغ</th>
            <th class="no-sort"></th>
        </tr>
    </thead>
    <tbody id="logsTbody">

    </tbody>
</table>
</div>
</div>
<div class="hiddenTable" id="salaryTableDiv" style="display: none;">
  <div class="card-body" style="overflow-x:auto;">

<table id="salaryTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
          <th>#</th>
            <th width="35%">الراتب</th>
            <th width="35%">تاريخ الإيداع</th>
            <th class="no-sort"></th>
        </tr>
    </thead>
    <tbody id="salaryTbody">

    </tbody>
</table>
</div>
</div>
    <!-- ./hidden tables -->

    <!-- DashboardModals -->


    <div class="modal custom fade" id="tablesModal">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="tablesLabel"></h4> </div>

            <div class="modal-body" id="tablesBody">

</div>

            <div class="modal-footer">

                <!-- the value taken id value-->
                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                 </div>


</div> <!--modal content-->
  </div>


</div>
    <div class="modal custom fade" id="rVacationsModal">
        <div class="modal-dialog"  style="overflow-y:auto;">
            <form id="rvForm" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="rVacationsLabel"></h4> </div>
                    <!-- /.modal-header -->
                    <div class="modal-body" >
                              <div class="form-group">
                                <label for="note" class="control-label">ملاحظة</label>
                                <input type="text" name="note" id="noteV" class="form-control" placeholder="ملاحظة">
                            </div>


                            <div id="selectFileDiv">
                        <form method='post' action='' enctype="multipart/form-data" id="selectFileForm"> اختار ملف :
                            <input type='file' name='userfile[]' id='userfile' class='form-control' multiple>
                            <br>
                            <input type='button' class='btn btn-info' value='Upload' id='btn_upload'>
                          </form>
                        </div>

                          <div id="documentsm"> </div>

                    <!-- /.modal-body -->
                    <div class="modal-footer">

                        <!-- the value taken id value-->

                        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                        <input type="submit" name="saveb" id="saveb" class="btn btn-info" value="إرسال" /> </div>
                </div>
              </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>






    <!--./END DashboardModals -->

    <!-- UnifedModals -->
    <!-- UnifedModals -->
    <div class="modal custom fade" id="blogModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">

              <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
               <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                <i class="fas fa-times"></i>
              </button> -->

              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                               </div>
            <div class="modal-body">

     <div class="card bg-light">
         <!-- Grid row -->
         <div class="row">

           <!-- Grid column -->
           <div class="col-12">

             <!-- Exaple 1 -->
             <div class="card example-1 scrollbar-ripe-malinka">
               <div class="card-body"  id="MainDivPostsm">

               </div>
             </div>
             <!-- Exaple 1 -->

           </div>
           <!-- Grid column -->



         </div>
         <!-- Grid row -->

       </div>
     </div>
    </div>

    </div>
    </div>
    <!-- ./blog Modal -->

    <div class="custom modal fade" id="chatModal">
     <div class="modal-dialog">
       <div class="modal-content">
             <div class="modal-header">

               <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
                <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button> -->

               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                </div>

         <div class="modal-body">
     <!-- /.card-header -->
     <div class="card direct-chat direct-chat-primary">
     <div class="card-body">
       <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
    <div class="image">
    <img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
    <a href="#" class="d-block"  id="receivernamem"></a>
    <a href="#" id="statusr2"></a>
    </div>
    </div>
    <hr>
       <!-- Conversations are loaded here -->
       <div class="direct-chat-messages" id="direct-chat-messages2">


       </div>
       <!--/.direct-chat-messages-->

       <!-- Contacts are loaded here -->
       <div class="direct-chat-contacts" >
         <ul class="contacts-list" id="direct-chat-contacts">
           <!-- End Contact Item -->
         </ul>
         <!-- /.contacts-list -->
       </div>
       <!-- /.direct-chat-pane -->
     </div>
     <!-- /.card-body -->
     <div class="card-footer">
       <form action="#" method="post" id="myForm2">
         <div class="input-group">
           <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
           <input type="hidden" name="action" value="sendChat" class="form-control">
           <span class="input-group-append">
             <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
           </span>
         </div>
       </form>
     </div>
     <!-- /.card-footer-->
    </div>
    </div>
    </div>

    </div>
    </div>
    <!--/.direct-chat modal -->
    <!-- /.Unified Modals -->

  </div>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 PIONEER</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0
    </div>
  </footer>


<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="dist/js/unifiedD.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>

<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/mydashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- SweetAlert2 and Toast -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- CALENDER -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/fullcalendar/main.min.js"></script>
<script src="plugins/fullcalendar-daygrid/main.min.js"></script>
<script src="plugins/fullcalendar-timegrid/main.min.js"></script>
<script src="plugins/fullcalendar-interaction/main.min.js"></script>
<script src="plugins/fullcalendar-bootstrap/main.min.js"></script>
<script src="dist/js/dashboardD.js"></script>
<!-- <script src="dist/js/chats.js"></script> -->
<script src="dist/js/paginationli.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="dist/js/modalStyle.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="dist/js/requestsActions.js"></script>
<script src="dist/js/uploadDashboard.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script   src="//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script   src="//cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script   src="//cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>

</script>
</body>

</html>
