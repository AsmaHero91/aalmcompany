<?php
session_start();
if ($_SESSION['levelid']) {
    //successfully login
} else {
    header("Location: ../login.html");
}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

               <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
        <!-- Font for add/edit/delete-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" />
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- Tempusdominus Bbootstrap 4 -->
        <!-- <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"> -->
        <!-- Select2 for select multible -->
        <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
        <!-- Bootstrap4 Duallistbox for adding to groups as an example -->
        <link rel="stylesheet" href="../plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">

        <!-- SweetAlert2 and Toast -->
        <link rel="stylesheet" href="../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
        <!-- SweetAlert2 and Toast -->
        <link rel="stylesheet" href="../dist/css/responsiveTable.css">
        <link rel="stylesheet" href="../dist/css/blogModal.css">
        <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.css">

         <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">


      <script type="text/javascript">
         <!--
         function popup(url)
         {
          var width  = 600;
          var height = 350;
          var left   = (screen.width  - width)/2;
          var top    = (screen.height - height)/2;
          var params = 'width='+width+', height='+height;
          params += ', top='+top+', left='+left;
          params += ', directories=no';
          params += ', location=no';
          params += ', menubar=no';
          params += ', resizable=no';
          params += ', scrollbars=no';
          params += ', status=no';
          params += ', toolbar=no';
          newwin=window.open(url,'windowname5', params);
          if (window.focus) {newwin.focus()}
          return false;
         }
         // -->
      </script>

  </head>
       <body class="hold-transition sidebar-mini layout-fixed">
         <div class="wrapper">

         <!-- Navbar -->
         <nav class="main-header navbar navbar-expand navbar-white navbar-light">
           <!-- Left navbar links -->
           <ul class="navbar-nav">
             <li class="nav-item">
               <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
             </li>
             <li class="nav-item d-sm-inline-block">
               <a href="../index.php" class="nav-link">البداية</a>
             </li>
           </ul>

           <!-- Right navbar links -->
           <ul class="navbar-nav ml-auto">
             <!-- Messages Dropdown Menu -->
             <li class="nav-item dropdown al">
               <a class="nav-link" data-toggle="dropdown" href="#">
                 <i class="far fa-comments" id="cc"></i>
                 <span class="badge badge-danger navbar-badge" id="countAlert"></span>
               </a>
               <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="displayAlertUnreadmessages">

             <div class="dropdown-divider"></div>
                                             </div>
             </li>
             <!-- Notifications Dropdown Menu -->
             <li class="nav-item dropdown al">
                 <a class="nav-link" data-toggle="dropdown" href="#"> <i class="far fa-bell"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg1"></span></span>
                 </a>
                 <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu2"> <span class="dropdown-item dropdown-header"><span class="countbdg2"></span> Notifications</span>
                     <div class="dropdown-divider"></div>
                 </div>
             </li>
             <li class="nav-item dropdown al">
                 <a class="nav-link" data-toggle="dropdown" href="#"> <i class="fas fa-tasks"></i> <span class="badge badge-warning navbar-badge"><span class="countbdg3"></span></span>
                 </a>
                 <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="dropdown-menu3"> <span class="dropdown-item dropdown-header"><span class="countbdg4"></span> Notifications</span>
                     <div class="dropdown-divider"></div>
                 </div>
             </li>
             <li class="nav-item">
                 <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"> <i class="fas fa-th-large"></i> </a>
             </li>
           </ul>
         </nav>
         <!-- /.navbar -->

         <!-- Main Sidebar Container -->
         <aside class="main-sidebar sidebar-dark-primary elevation-4">
           <!-- Brand Logo -->
           <a href="../index.php" class="brand-link">
             <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                  style="opacity: .8">
             <span class="brand-text font-weight-light">PIONEER</span>
           </a>

           <!-- Sidebar -->
           <div class="sidebar">
             <!-- Sidebar user panel (optional) -->
             <div class="user-panel mt-3 pb-3 mb-3 d-flex">
               <div class="image">
                 <img id="userPic" src="" class="img-circle elevation-2" alt="User Image">
               </div>
               <div class="info">
                 <a  id="userName" href="#" class="d-block"></a>
                 <a href="#" id="userStatus"></a>
               </div>
             </div>

             <!-- Sidebar Menu -->
             <nav class="mt-2">
               <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                 <!-- Add icons to the links using the .nav-icon class
                      with font-awesome or any other icon font library -->
                      <li class="nav-item">
                        <a href="../index.php" class="nav-link">
                          <i class="nav-icon fas fa-tachometer-alt"></i>
                          <p>
                            لوحة التحكم
                          </p>
                        </a>
                      </li>


                 <li class="nav-item">
                   <a href="../employees/profile.php" class="nav-link">
                     <i class="nav-icon fas fa-id-card-alt"></i>
                     <p>
                       البروفايل
                     </p>
                   </a>
                 </li>

                 <li class="nav-item">
                   <a href="../employees/requests.php" class="nav-link">
                     <i class="nav-icon fas fa-list-ol"></i>
                     <p>
                       الطلبات
                     </p>
                   </a>
                 </li>



                 <li class="nav-item has-treeview" id="midadmin-section" style="display:none;">
                   <a href="#" class="nav-link">
                     <i class="nav-icon fas fa-edit"></i>
                     <p>
                       أقسام الإدارة المتوسطة
                       <i class="fas fa-angle-left right"></i>
                     </p>
                   </a>
                   <ul class="nav nav-treeview">
                     <li class="nav-item">
                       <a href="../midEmployees/projects.php" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>قسم المشاريع</p>
                       </a>
                     </li>

                   </ul>

                 </li>



                 <li class="nav-item has-treeview opened menu-open" id="admin-section" style="display:none;">
                   <a href="#" class="nav-link active">
                     <i class="nav-icon fas fa-edit"></i>
                     <p>
                        أقسام الإدارة العليا
                       <i class="fas fa-angle-left right"></i>
                     </p>
                   </a>
                   <ul class="nav nav-treeview">
                     <li class="nav-item">
                       <a href="../admin/employees.php" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p> قسم الموارد البشرية</p>
                       </a>
                     </li>

                   </ul>
                   <ul class="nav nav-treeview">
                     <li class="nav-item">
                       <a href="../admin/others.php" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p> قسم العملاء والمقاولين</p>
                       </a>
                     </li>

                   </ul>
                   <ul class="nav nav-treeview">
                     <li class="nav-item">
                       <a href="../admin/AdminRequests.php" class="nav-link">
                         <i class="far fa-circle nav-icon"></i>
                         <p>قسم خصائص المعاملات</p>
                       </a>
                     </li>

                   </ul>
                   <ul class="nav nav-treeview">

                   <li class="nav-item">
       <a href="form.php" class="nav-link active">
       <i class="nav-icon fas fa-sms"></i>
       <p>
       SMS
       </p>
       </a>
       </li>
       </ul>

                 </li>
                 <li class="nav-item">
               <a href="../employees/news.php" class="nav-link">
               <i class="nav-icon fas fa-newspaper"></i>
               <p>
               آخر الأخبار
               <span class="badge badge-info right" id="blogsCounting"></span>
               </p>
               </a>
               </li>
                 <li class="nav-item">
        <a id="logout" class="nav-link">
          <i class="nav-icon  ion-log-out"></i>
          <p>خروج</p>
        </a>
        </li>
               </ul>
             </nav>
             <!-- /.sidebar-menu -->
           </div>
           <!-- /.sidebar -->
         </aside>

       <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid" style="padding-top: 20px;">
          <div class="card">
              <div class="card-header">
              <h4> نموذج إرسال الرسائل النصية</h4>

              </div>
              <div class="card-body" style="overflow-x:auto;">


         <form action="" method="POST" id="smsForm">
            <table class="table table-striped table-bordered"  border="0" cellspacing="0" cellpadding="2" style="width: 100%;">
               <tr>
                  <td><strong>فحص اسم المستخدم وكلمة المرور</strong></td>
                  <td><input class="btn btn-primary" type="submit" name="check" value="فحص" /></td>
               </tr>
               <tr>
                   <td><strong>الرصيد</strong></td>
                  <td><input type="text" name="Balance" size="20" disabled="disabled" value="<?php
                     echo $Credits;
                     ?>"></td>
               </tr>
               <tr>
                   <td><strong>اسم المرسل</strong></td>
                  <td>
                     <select size=1 name=Originator>
                        <option selected value="1">اختار اسم المرسل</option>
                        <?php
                           for ($i = 0; $i < count($SenderName); $i++) {
                               echo '<option value="' . $SenderName[$i] . '">' . $SenderName[$i] . '</option>';
                           }
                           ?>
                     </select>
                     <a href="javascript: void(0)" onclick="popup('addsender.php')"> إضافة اسم</a>
                  </td>
               </tr>
                <tr>
                 <td><strong> إرسال إلى المجموعات أو</strong></td>
                  <td>
                            <div class="card card-default">
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label>:أسماء المجموعات</label>
                  <select class="duallistbox" multiple="multiple">
                    <!-- depend on the groupid number selected to be used for query -->

                    <option value="customers"> العملاء</option>
                      <option value="employees"> الموظفين</option>
                      <option value="contractors"> المقاولين</option>


                  </select>
                </div>


<div class="form-group">

  <label>قوالب الرسائل النصية</label>

                  <select class="form-control select2" name="selecto" id="selecto" style="width: 100%;">



                  </select>


                </div>

                  <div class="form-group">
                    <button type="submit" id="submit" class="btn btn-primary">إدراج</button>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
        </div>

        <!-- /.card -->
                    </td>
                </tr>
               <tr>
                  <td><strong>أرقام الجوال</strong></td>
                  <td><textarea name="Mobile" id="mobileta" cols="60" rows="5"></textarea><br><font size="2" color="#FF0000"> Ex. 96655xxxxxxx,96655xxxxxxx</font>
                   </td>

               </tr>
               <tr>
                  <td><strong>الرسالة النصية</strong></td>
                  <td>

	<textarea id='Text' name='Text' class='charcounter-control form-control' maxlength='70' warnlength='60' placeholder="Type in your message" cols="60" rows="5"></textarea>
                   </td>
               </tr>
                               <tr>
                  <td><button id="addTemplate" class="btn btn-info">أضف قالب </button></td>
                  <td><input type="submit" name="Go" value="إرسال الرسالة" class="btn btn-info" /></td>
               </tr>
         </table>
         </form>

            </div>
            </div><!--/.end of card body-->
              </div>


           </section>
           <section class="content">
                 <div class="card" style="padding-top: 20px;">
                     <div class="card-header">
                     <h4>قوالب الرسائل النصية</h4>

                     </div>
                     <div class="card-body tableHolder" style="overflow-x:auto;">

                     <table id="smsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                         <thead>
                             <tr>
                               <th>#</th>
                                 <th width="35%">العنوان</th>
                                 <th width="35%">القالب</th>
                                 <th class="no-sort"></th>
                             </tr>
                         </thead>
                         <tbody id="smsTbody">

                         </tbody>
                     </table>
              </div>

            </div>
        </section>

        </div>
</div>
           <!--****this code for displaying label counter in realtime with its jquery script below****-->
           <span id="charcounter-label" style="position: absolute; z-index: 2000; vertical-align: middle; text-align: center; padding: 3px; border-radius: 2.5px; font-family: Tahoma; font-size: 10px; color: rgb(255, 255, 255); font-weight: bold; top: 472.2px; left: 480.05px; display: none;" class="charcounter-danger">70 / 70</span><iframe id="google_osd_static_frame_1927758190081" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;"></iframe><ins class="adsbygoogle adsbygoogle-noablate" style="display: none !important;" data-adsbygoogle-status="done"><ins id="aswift_1_expand" style="display:inline-table;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><ins id="aswift_1_anchor" style="display:block;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><iframe frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_1" name="aswift_1" style="left:0;position:absolute;top:0;border:0px;width:0px;height:0px;"></iframe></ins></ins></ins>
           <div class="modal custom fade" id="smsModal">
               <div class="modal-dialog"  style="overflow-y:auto;">
                   <form id="smsFormt" method="post">
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                               <h4 class="modal-title" id="smsLabel">قوالب الرسائل النصية</h4> </div>
                           <!-- /.modal-header -->
                           <div class="modal-body" >

                             <div class="form-group">
                               <label for="title" class="control-label">عنوان القالب</label>
                             <input type="text" name="title" size="20" placeholder="title" class="form-control">
                           </div>
                                     <div class="form-group">
                                       <label for="smstemplates" class="control-label">قالب الرسالةا لنصية</label>
                                       <td><textarea name="smstemplates" id="smstemplates" cols="60" rows="5" class="form-control"></textarea>

                                   </div>

                               <!-- the value taken id value-->

                               <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                               <input type="submit" name="saveb" id="saveb" class="btn btn-info" value="إرسال" />
                               <input type="hidden" name="action" value="addSmsTemplate" class="form-control" />

                              </div>
                       </div>
                     </div>
                       <!-- /.modal-content -->
                   </form>
               </div>
               <!-- /.modal-dialog -->
           </div>
      <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 PIONEER</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0
    </div>
  </footer>
<div class="wrapper">
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
    </div>
    <!-- UnifedModals -->
    <div class="modal custom fade" id="blogModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">

              <h4 class="modal-title" id="myModalLabel">عرض المقالة</h4>
               <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                <i class="fas fa-times"></i>
              </button> -->

              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                               </div>
            <div class="modal-body">

     <div class="card bg-light">
         <!-- Grid row -->
         <div class="row">

           <!-- Grid column -->
           <div class="col-12">

             <!-- Exaple 1 -->
             <div class="card example-1 scrollbar-ripe-malinka">
               <div class="card-body"  id="MainDivPostsm">

               </div>
             </div>
             <!-- Exaple 1 -->

           </div>
           <!-- Grid column -->



         </div>
         <!-- Grid row -->

       </div>
     </div>
    </div>

    </div>
    </div>
    <!-- ./blog Modal -->

    <div class="custom modal fade" id="chatModal">
     <div class="modal-dialog">
       <div class="modal-content">
             <div class="modal-header">

               <h4 class="modal-title" id="myModalLabel">محادثة مباشرة</h4>
                <!-- <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button> -->

               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                </div>

         <div class="modal-body">
     <!-- /.card-header -->
     <div class="card direct-chat direct-chat-primary">
     <div class="card-body">
       <div class="user-panel mt-2 pb-2 mb-2 d-flex" >
    <div class="image">
    <img id="receiverpicm" src="" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
    <a href="#" class="d-block"  id="receivernamem"></a>
    <a href="#" id="statusr2"></a>
    </div>
    </div>
    <hr>
       <!-- Conversations are loaded here -->
       <div class="direct-chat-messages" id="direct-chat-messages2">


       </div>
       <!--/.direct-chat-messages-->

       <!-- Contacts are loaded here -->
       <div class="direct-chat-contacts" >
         <ul class="contacts-list" id="direct-chat-contacts">
           <!-- End Contact Item -->
         </ul>
         <!-- /.contacts-list -->
       </div>
       <!-- /.direct-chat-pane -->
     </div>
     <!-- /.card-body -->
     <div class="card-footer">
       <form action="#" method="post" id="myForm2">
         <div class="input-group">
           <input type="text" name="message" id="chatMessage2" placeholder="Type Message ..." class="form-control">
           <input type="hidden" name="action" value="sendChat" class="form-control">
           <span class="input-group-append">
             <button type="button" class="btn btn-primary" id="sendChat2">إرسال</button>
           </span>
         </div>
       </form>
     </div>
     <!-- /.card-footer-->
    </div>
    </div>
    </div>

    </div>
    </div>
    <!--/.direct-chat modal -->
    <!-- /.Unified Modals -->

   </body>

</html>
 <!-- jQuery -->
    <script src="../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- script for tables to have entries and search-->
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

    <!-- Datepicker -->
    <script src="../plugins/gijgo/js/gijgo.min.js"></script>
    <!-- Select2 -->
    <script src="../plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <!-- InputMask -->
    <script src="../plugins/moment/moment.min.js"></script>
    <script src="../plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
   <script src="../plugins/char-counter/jquery.charcounter.js"></script>
   <script src="../dist/js/unified.js"></script>
   <script src="../dist/js/sms.js"></script>
   <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
  <script  src='../dist/js/pdfmake-master/build/pdfmake.min.js'></script>
  <script  src='../dist/js/pdfmake-master/build/vfs_fonts.js'></script>
  <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>.
