<?php

   $recordsTable = "employees";
   include "connect.php";


function closeSession()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    session_unset();
    session_destroy();
    //echo"you have successfully logout. Click here to <a herf='login.html'>login again</a>";
}

if (!empty($_POST['action']) && $_POST['action'] == 'validateSession') {
    validateSession();
}
if (!empty($_POST['action']) && $_POST['action'] == 'logout') {
    closeSession();
}
if (!empty($_POST['action']) && $_POST['action'] == 'checklevelid') {
    if ($_POST["usergcode"]) {
        $table = "usergpermission";
        $sqlQuery = "
				SELECT * FROM ".$table."
				WHERE UserGCategory = '".$_POST["usergcode"]."'";
        $result = mysqli_query($connect, $sqlQuery);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        echo json_encode($row);
    } else {
        $error = -1;//eno usercode
        echo $error;
    }
}


if (!empty($_POST['action']) && $_POST['action'] == 'addRecordReg') {
    if ($_POST["idcard"]) {
        $todayDate = date("Y-m-d");
        $myDocuments =  $_POST['allFilesNames'];
        $myFilesData =  $myDocuments;

        $table = 'employees';
        $table2 = 'users';
        $table3 = 'contacts';
        $table4 = 'ejobinfo';
        $table5 = 'employeeslog';
        $table6 = 'eprogress';
        $table7 = 'chats';

        $query = "SELECT `employeeid` FROM ".$table2." WHERE employeeid = '".mysqli_real_escape_string($connect, $_POST['idcard'])."'";
        $result = mysqli_query($connect, $query);

        if (mysqli_num_rows($result) > 0) {
            echo -1; //idcard already registered
        } else {
            //here to insertimage profile
               if (strpos($_POST['pictureProfile'], 'data:image') === false) { //to check if  contains this character

                 $imagenNameToInsert = "default-avatar.jpg";
               } else {
                   // $filename = $_POST['profileImage'];

                   // $imagenNameToInsert = $filename;
                   $imagenNameToInsert = "default-avatar.jpg";
               }
            $table8 = 'session';
            ////////end of imagefile name
            $mysqli = $connect;
            ////////end of imagefile name
            $mysqli -> query("INSERT INTO ".$table." (id, name, nationality, skills, experience, lastCer, joinDate, status, documents) VALUES ('".$_POST["idcard"]."','".$_POST["name"]."','".$_POST["nationality"]."', '".$_POST["skills"]."', '".$_POST["experience"]."', '".$_POST["lastCer"]."', '".$todayDate."','موظف','".$myFilesData."')");
            $mysqli -> query("INSERT INTO ".$table2." (employeeid, levelid, userPic, userNote, userPassword, useremail, username) VALUES ('".$_POST["idcard"]."','".$_POST["levelid"]."', '".$imagenNameToInsert."', '".$_POST["profileNote"]."', '".$_POST["password"]."' , '".$_POST["email"]."', '".$_POST["name"]."')");
            $mysqli -> query("INSERT INTO ".$table3." (id, name, mobile1, mobile2, officeNo, email, notes, groupCat,personToCall) VALUES ('".$_POST["idcard"]."','".$_POST["name"]."', '".$_POST["mobileN1"]."', '".$_POST["mobileN2"]."', '', '".$_POST["email"]."' ,'', '1','".$_POST["personToCall"]."')");
            $mysqli -> query("INSERT INTO ".$table4." (empid, jobTitle, salary, benefits, departmentName, datebegin, idEnd , vacationDate) VALUES ('".$_POST["idcard"]."', '', '0.00', '' ,'', '".$todayDate."', '".$todayDate."', '".$todayDate."')");
            $mysqli -> query("INSERT INTO ".$table5." (id, empid, activityName, date, documents) VALUES (NULL,'".$_POST["idcard"]."', 'تم الانضمام للنظام', '".$todayDate."','')");
            $conversation_id = $_POST["idcard"].'-'.$_POST["idcard"];
            $mysqli -> query("INSERT INTO ".$table6." (id, noOfProjectsLead, tasksAssigned,tasksHandled, honorspts, deductpts, Appraisals) VALUES ('".$_POST["idcard"]."', '0', '0','0' , '0' , '0' , '0')");
            $message = 'استمتع بتجربتك الأولى بالنظام !';
            $datetime = date("Y-m-d H:i:s"); //dateCreated

            $mysqli -> query("INSERT INTO ".$table7." (id, conversation_id, toUserId, fromUserId, chatMessage, timestamp, readen) VALUES (NULL,'".base64_encode($conversation_id)."', '".$_POST["idcard"]."', '".$_POST["idcard"]."','".$message."','$datetime','0')");
            $mysqli -> query("INSERT INTO ".$table8." (sessionid, empid, activityName, status, timeActivity) VALUES (NULL,'".$_POST["idcard"]."', 'register', 'offline','".$datetime."')");

            if ($mysqli -> commit()) {
                echo 0;
            } else {
                echo -2; //not succcess
            }
        }
    } else {
        $error = -3;//eno usercode
        echo $error;
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'login') {
    if (array_key_exists('email', $_POST) or array_key_exists('password', $_POST)) {
        if ($_POST['email'] == '') {
            echo -1;//"<p>Email address is required.</p>";
        } elseif ($_POST['password'] == '') {
            echo -2; //"<p>Password is required.</p>";
        } else {
            $table = 'users';
            $table2 = 'employees';
            $query = "SELECT `levelid`,`employeeid`,`userPic` ,`username` FROM ".$table." WHERE useremail = '".mysqli_real_escape_string($connect, $_POST['email'])."' AND userPassword = '".mysqli_real_escape_string($connect, $_POST['password'])."'";

            $result = mysqli_query($connect, $query);


            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result, MYSQLI_NUM);
                $recordsTable = 'session';

                session_start();
                $_SESSION['levelid'] = $row[0];
                $_SESSION['employeeid'] = $row[1];
                $_SESSION['userPic'] = $row[2];
                $_SESSION['username'] = $row[3];
                $datetime = date("Y-m-d H:i:s"); //dateCreated
                $insertquery = "INSERT INTO ".$recordsTable." (sessionid, empid, activityName, status, timeActivity)
                  VALUES (NULL,'".$_SESSION['employeeid']."','login','online','".$datetime."')";
                mysqli_query($connect, $insertquery);
                // echo $_SESSION['employeeid'];
                // echo   $_SESSION['levelid'];
                echo 0;
            } else {
                echo -3; //                    echo "<p>There was a problem signing you in- please try again later.</p>";
            }
        }
    }
}

if (!empty($_POST['action']) && $_POST['action'] == 'getRecordRequest') {
    $table = "requesthistorylog";
    if ($_POST["idt"]) {
        $sqlQuery = "
				SELECT * FROM ".$table."
				WHERE id = '".mysqli_real_escape_string($connect, $_POST["idt"])."'";
        $result = mysqli_query($connect, $sqlQuery);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        echo json_encode($row);
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'getRecord') {
    if ($_POST["idt"]) {
        $sqlQuery = "
				SELECT * FROM ".$recordsTable."
				WHERE id = '".$_POST["idt"]."'";
        $result = mysqli_query($connect, $sqlQuery);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        echo json_encode($row);
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'updateRecord') {
    if ($_POST["idt"]) {
        // these data from formdata
        $updateQuery = "UPDATE ".$recordsTable."
			SET id = '".$_POST["id"]."', name = '".$_POST["name"]."' , nationality = '".$_POST["nationality"]."', skills = '".$_POST["skills"]."' , experience = '".$_POST["experience"]."', lastCer = '".$_POST["lastCer"]."', joinDate = '".$_POST["joinDate"]."', status = '".$_POST["status"]."', documents = '".$_POST["documents"]."'
      WHERE id ='".$_POST["idt"]."'";
        $result = mysqli_query($connect, $updateQuery);

        echo 'Data Updated';
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'deleteRecord') {
    if ($_POST["idt"]) {
        if ($_POST["idocuments"]) {
            $documentstToDelete = $_POST["idocuments"];
            $documentsList = explode(",", $documentstToDelete);
            foreach ($documentsList as &$value) {
                $locationToDelete = 'upload/hr/'.str_replace(' ', '', $value);// to remove white space before set a link
                unlink($locationToDelete);
            }
            $sqlDelete = "DELETE FROM ".$recordsTable."
				WHERE id = '".$_POST["idt"]."'";
            $result =  mysqli_query($connect, $sqlDelete);

            echo 'Data Deleted';
        }
    }
}
@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');
