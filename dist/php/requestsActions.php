<?php
include "connect.php";

//where assignedTo contains *ssession empid*
if (!empty($_POST['action']) && $_POST['action'] == 'completedRequests') {
    include "session.php";
    $currentuserId = $_SESSION['employeeid'];
    ///lists of  converstaions latest to recents
    $listQuery = "SELECT h.* , CASE WHEN t.requestTCode=h.requestTCode
           THEN t.name
         END as requestName , CASE  WHEN e.employeeid=h.empid
                       THEN e.username
                     END as requester
FROM requesthistorylog as h
JOIN requesttype as t ON t.requestTCode=h.requestTCode
LEFT JOIN users as e ON e.employeeid=h.empid
WHERE status = 'completed' AND ('$currentuserId' IN (assignedTo) OR empid = '$currentuserId') AND isActionTaken = '1'
ORDER BY dateCreated DESC";
    $resultlistQuery = mysqli_query($connect, $listQuery);
    $rows = mysqli_fetch_all($resultlistQuery, MYSQLI_ASSOC);

    if (count($rows) > 0) {
        echo json_encode($rows);
    } else {
        echo json_encode(0);
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'declinedRequests') {
    include "session.php";
    $currentuserId = $_SESSION['employeeid'];
    ///lists of  converstaions latest to recents
    $listQuery = "SELECT h.* , CASE WHEN t.requestTCode=h.requestTCode
           THEN t.name
         END as requestName , CASE  WHEN e.employeeid=h.empid
                       THEN e.username
                     END as requester
FROM requesthistorylog as h
JOIN requesttype as t ON t.requestTCode=h.requestTCode
LEFT JOIN users as e ON e.employeeid=h.empid
WHERE status = 'Declined' AND ('$currentuserId' IN (assignedTo) OR empid = '$currentuserId') AND isActionTaken = '1'
ORDER BY dateCreated DESC";
    $resultlistQuery = mysqli_query($connect, $listQuery);
    $rows = mysqli_fetch_all($resultlistQuery, MYSQLI_ASSOC);

    if (count($rows) > 0) {
        echo json_encode($rows);
    } else {
        echo json_encode(0);
    }
}


if (!empty($_POST['action']) && $_POST['action'] == 'needsYourAction') {
    include "session.php";
    include "db.php";
    $currentuserId = $_SESSION['employeeid'];
    $currentuserIds = strval($currentuserId);
    ///lists of  converstaions latest to recents
    $listQuery = "SELECT h.* , CASE WHEN t.requestTCode=h.requestTCode
           THEN t.name
         END as requestName , CASE  WHEN e.employeeid=h.empid
                       THEN e.username
                     END as requester
FROM requesthistorylog as h
JOIN requesttype as t ON t.requestTCode=h.requestTCode
LEFT JOIN users as e ON e.employeeid=h.empid
WHERE status = 'in progress' AND assignedTo LIKE '%$currentuserIds%' AND isActionTaken = '0'
ORDER BY dateCreated DESC";
    $resultlistQuery = mysqli_query($connect, $listQuery);
    $rows = mysqli_fetch_all($resultlistQuery, MYSQLI_ASSOC);
    $commanamesAfterhandeled = array();
    $arrayNames = array();

    foreach ($rows as $row) {
        $idval = $row['assignedTo'];
        $commanamesAfterhandeled = array();
        if (strpos($idval, ',') != false) { //to
            $commanames = explode(",", $idval);
            ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
            foreach ($commanames as $singleValue) {
                $query1=$conn->prepare("select name from employees where id = '$singleValue'");
                $query1->setFetchMode(PDO::FETCH_OBJ);
                $query1->execute();
                while ($row1=$query1->fetch()) {
                    array_push($commanamesAfterhandeled, $row1->name);
                }
            }
            $str2 = implode(",", $commanamesAfterhandeled);
            ///////////////////////////////////////////////////////
            array_push($arrayNames, $str2); //the value taken from the function above
        } else {
            $query1=$conn->prepare("select name from employees where id = '$idval'");
            $query1->setFetchMode(PDO::FETCH_OBJ);
            $query1->execute();
            while ($row1=$query1->fetch()) {
                array_push($arrayNames, $row1->name);
            }
        }
    }

    $json['rows'] = $rows;
    $json['empNames'] = $arrayNames;


    $myObj = (object)[];
    $myObj->rows = $rows;
    $myObj->empNames = $arrayNames;
    $data = json_encode($myObj);


    if (count($rows) > 0) {
        echo $data;
    } else {
        echo json_encode(0);
    }
}

if (!empty($_POST['action']) && $_POST['action'] == 'showInProgressTable') {
    include "session.php";
    $currentuserId = $_SESSION['employeeid'];
    $currentuserIds = strval($currentuserId);
    ///lists of  converstaions latest to recents
    $listQuery = "SELECT h.* , CASE WHEN t.requestTCode=h.requestTCode
           THEN t.name
         END as requestName , CASE  WHEN e.employeeid=h.empid
                       THEN e.username
                     END as requester
FROM requesthistorylog as h
JOIN requesttype as t ON t.requestTCode=h.requestTCode
LEFT JOIN users as e ON e.employeeid=h.empid
WHERE status = 'in progress' AND (assignedTo LIKE '%$currentuserIds%' OR empid = '$currentuserId') AND isActionTaken = '0'
ORDER BY dateCreated DESC";

    $resultlistQuery = mysqli_query($connect, $listQuery);
    $rows = mysqli_fetch_all($resultlistQuery, MYSQLI_ASSOC);

    if (count($rows) > 0) {
        echo json_encode($rows);
    } else {
        echo json_encode(0);
    }
}
