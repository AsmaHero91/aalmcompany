 <?php
 include "connect.php";
ini_set('mysql.connect_timeout', 300);
ini_set('default_socket_timeout', 300);

if (mysqli_connect_error()) {
    die("There was an error connecting to the database");
}
function recordSession($connect)
{
    include "session.php";
    $recordsTable = "session";
    $datetime = date("Y-m-d H:i:s"); //dateCreated
    $insertquery = "INSERT INTO ".$recordsTable." (sessionid, empid, activityName, status, timeActivity)
VALUES (NULL,'".$_SESSION['employeeid']."','read','online','".$datetime."')";
    mysqli_query($connect, $insertquery);
}

if (!empty($_POST['action']) && $_POST['action'] == 'deleteBlog') {
    $bid = $_POST['bID'];
    $query = "DELETE FROM blogs WHERE blogid = '$bid'";
    $result = mysqli_query($connect, $query);
    if ($result) {
        echo json_encode(0);
    } else {
        echo json_encode(-1);
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'viewMyBlogs') {
    include "session.php";
    if (isset($_POST['uid'])) {
        $currentUserId = base64_decode($_POST['uid']);
    } else {
        $currentUserId = $_SESSION['employeeid'];
    }

    $listMessagesQuery = "SELECT b.*, CASE WHEN u1.employeeid = b.authorid
           THEN u1.username
         END as participantName , CASE WHEN u1.employeeid = b.authorid
                       THEN u1.userPic
                     END as partcipantPic
FROM blogs as b
JOIN users as u1 ON u1.employeeid = b.authorid
WHERE authorid = '$currentUserId'
ORDER BY b.blogid DESC";



    $resultlistMessagesQuery = mysqli_query($connect, $listMessagesQuery);

    $rows['blogs'] = mysqli_fetch_all($resultlistMessagesQuery, MYSQLI_ASSOC);
    recordSession($connect);
    echo json_encode($rows);
}
if (!empty($_POST['action']) && $_POST['action'] == 'viewAllBlogs') {
    $listMessagesQuery = "SELECT b.*, CASE WHEN u1.employeeid = b.authorid
           THEN u1.username
         END as participantName , CASE WHEN u1.employeeid = b.authorid
                       THEN u1.userPic
                     END as partcipantPic
FROM blogs as b
JOIN users as u1 ON u1.employeeid = b.authorid
ORDER BY blogid DESC";
    $resultlistMessagesQuery = mysqli_query($connect, $listMessagesQuery);

    $rows['blogs'] = mysqli_fetch_all($resultlistMessagesQuery, MYSQLI_ASSOC);
    recordSession($connect);
    echo json_encode($rows);
}
if (!empty($_GET['action']) && $_GET['action'] == 'displayComments') {
    include "session.php";
    $val = 0;
    $element = $_GET['element'];

    $listMessagesQuery = "SELECT c.*, CASE WHEN u1.employeeid = c.authorid
           THEN u1.username
         END as participantName , CASE WHEN u1.employeeid = c.authorid
                       THEN u1.userPic
                     END as partcipantPic
FROM comments as c
JOIN users as u1 ON u1.employeeid = c.authorid
WHERE c.blogid = $element
ORDER BY c.dateCreated DESC";

    //  echo $listMessagesQuery;
    $resultlistMessagesQuery = mysqli_query($connect, $listMessagesQuery);
    $rows = mysqli_fetch_all($resultlistMessagesQuery, MYSQLI_ASSOC);

    echo json_encode($rows);
}

if (!empty($_POST['action']) && $_POST['action'] == 'sendBlog') {
    include "session.php";
    $recordsTable = "blogs";
    $recordsTable2 = "blogshistory";
    $datetime = date("Y-m-d H:i:s");
    $currentuserId = $_SESSION["employeeid"];
    $currentusername = $_SESSION["username"];
    $message = $_POST["blogArea"];
    $selectQuery = "SELECT MAX(blogid) FROM ".$recordsTable."";
    $result = mysqli_query($connect, $selectQuery);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    $blogidtoInsert = intval($row[0]) + 1;
    $title = $_POST['title'];
    $insertQuery = "INSERT INTO ".$recordsTable." (blogid, title, authorid, dateCreated, message, commentsCounter)
    VALUES ('$blogidtoInsert', '$title', '$currentuserId', '".$datetime."','".$message."', '0')";
    $insertQuery2 = "INSERT INTO ".$recordsTable2." (id, blogid, empid, activityType, dateCreated)
    VALUES (NULL,'$blogidtoInsert', '$currentuserId', 'created', '".$datetime."')";
    $result = mysqli_query($connect, $insertQuery);
    $result2 = mysqli_query($connect, $insertQuery2);
    if ($result && $result2) {
        echo 0;
    } else {
        echo -1;
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'sendComment') {
    include "session.php";
    $recordsTable = "comments";
    $recordsTable2 = "blogs";
    $datetime = date("Y-m-d H:i:s");
    $currentuserId = $_SESSION["employeeid"];
    $message = $_POST["comment"];
    $blogidtoInsert = $_POST['bID'];

    $selectQuery = "SELECT commentsCounter FROM ".$recordsTable2." WHERE blogid = '$blogidtoInsert'";
    $result = mysqli_query($connect, $selectQuery);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    $counterToInsert = intval($row[0]) + 1;

    $insertQuery = "INSERT INTO ".$recordsTable." (id, blogid, comment, dateCreated, authorid)
  VALUES (NULL,'$blogidtoInsert','".$message."', '".$datetime."','$currentuserId')";

    $updateQuery = "UPDATE ".$recordsTable2." SET commentsCounter='$counterToInsert' WHERE blogid = '$blogidtoInsert'"; // to take the first record of request process to insert it

    $result1 = mysqli_query($connect, $insertQuery);
    $result2 = mysqli_query($connect, $updateQuery);
    if ($result1 && $result2) {
        echo 0;
    } else {
        echo -1;
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'updateReadenBlogList') {
    include "session.php";
    $currentuserId = $_SESSION['employeeid'];
    $table = "blogs";
    // $sqlUpdateQuery = "UPDATE ".$table." SET readen=1 WHERE empid = '".mysqli_real_escape_string($connect, $currentuserId)."' AND blogid = '".mysqli_real_escape_string($connect, $currentConversation)."'"; // to take the first record of request process to insert it
    //
    // if (mysqli_query($connect, $sqlUpdateQuery)) {
    //     echo 0;
    // } else {
    //     echo -1;
    // }
}
