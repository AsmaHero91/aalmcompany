 <?php

include "connect.php";

  function closeSession()
  {
      if (!isset($_SESSION)) {
          session_start();

      }
      session_unset();
      session_destroy();
      //echo"you have successfully logout. Click here to <a herf='login.html'>login again</a>";
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'checkAuthority') {
      include "session.php";
      $level = intval($_SESSION['levelid']);
      if ($_SESSION['levelid'] == 1) {
          echo json_encode(1);
      }
      if ($_SESSION['levelid'] == 2) {
          echo json_encode(2);
      }

      if ($level == 3) {
          echo json_encode(3);
      }
      //echo json_encode(-1);
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'loadSessionData') {
      include "session.php";
      $currentUserId = $_SESSION['employeeid'];
      $currentUserName = $_SESSION['username'];
      $currentUserPic = $_SESSION['userPic'];

      $obj = (object) array('currentUserId' => $currentUserId,'currentUserName' => $currentUserName , 'currentUserPic' => $currentUserPic);
      echo json_encode($obj);
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'logout') {
      include "session.php";
      $recordsTable = "session";
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      $insertquery = "INSERT INTO ".$recordsTable." (sessionid, empid, activityName, status, timeActivity)
VALUES (NULL,'".$_SESSION['employeeid']."','logout','offline','".$datetime."')";
      if (mysqli_query($connect, $insertquery)) {
          echo 0;
      } else {
          echo -1;
      }
  }
