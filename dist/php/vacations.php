<?php
include "connect.php";

  if (!empty($_POST['action']) && $_POST['action'] == 'sendSickVacation') {
      include "session.php";

      $myDocuments =  $_POST['allFilesNames'];
      $table = 'vacationsrequests';
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      $val = 0;
      if (!empty($myDocuments)) {
          $myFilesData =  $myDocuments;


          $insertQuery = "INSERT INTO ".$table."(id,vName,empid,dateCreated,Notes,documents,status)
          VALUES(NULL,'إجازة مرضية','".$_SESSION['employeeid']."','$datetime','".$_POST['note']."','".$myFilesData."','$val')";
          // $result = mysqli_query($connect, $insertQuery);
          if (mysqli_query($connect, $insertQuery)) {
              echo 0;
              exit();
          } else {
              echo 'هناك مشكلة بالبيانات ارجو التأكد من سلامتها';
              exit();
          }
      } else {
          $insertQuery = "INSERT INTO ".$table."(id,vName,empid,dateCreated,Notes,documents,status)
        VALUES(NULL,'إجازة مرضية','".$_SESSION['employeeid']."','$datetime','".$_POST['note']."','','$val')";
          // $result = mysqli_query($connect, $insertQuery);
          if (mysqli_query($connect, $insertQuery)) {
              echo 0;
              exit();
          } else {
              echo 'هناك مشكلة بالبيانات ارجو التأكد من سلامتها';
              exit();
          }
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'sendYearVacation') {
      include "session.php";

      $myDocuments =  $_POST['allFilesNames'];
      $table = 'vacationsrequests';
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      $val = 0;
      if (!empty($myDocuments)) {
          $myFilesData =  $myDocuments;


          $insertQuery = "INSERT INTO ".$table."(id,vName,empid,dateCreated,Notes,documents,status)
          VALUES(NULL,'إجازة سنوية','".$_SESSION['employeeid']."','$datetime','".$_POST['note']."','".$myFilesData."','$val')";
          // $result = mysqli_query($connect, $insertQuery);
          if (mysqli_query($connect, $insertQuery)) {
              echo 0;
              exit();
          } else {
              echo 'هناك مشكلة بالبيانات ارجو التأكد من سلامتها';
              exit();
          }
      } else {
          $insertQuery = "INSERT INTO ".$table."(id,vName,empid,dateCreated,Notes,documents,status)
        VALUES(NULL,'إجازة سنوية','".$_SESSION['employeeid']."','$datetime','".$_POST['note']."','','$val')";
          // $result = mysqli_query($connect, $insertQuery);
          if (mysqli_query($connect, $insertQuery)) {
              echo 0;
              exit();
          } else {
              echo 'هناك مشكلة بالبيانات ارجو التأكد من سلامتها';
              exit();
          }
      }
  }

  if (!empty($_POST['action']) && $_POST['action'] == 'loadVacationsSummary') {
      include "session.php";
      $currentUserId = $_SESSION['employeeid'];
      $table = 'vacationsrequests';
      $selectQuery = "SELECT COUNT(vName) as sickVcounter FROM ".$table." WHERE empid = '$currentUserId' AND vName = 'إجازة مرضية' AND status = '0' GROUP BY vName";
      $selectQuery2 ="SELECT COUNT(id) as yearVcounter FROM ".$table." WHERE empid = '$currentUserId' AND vName = 'إجازة سنوية' AND status = '0' GROUP BY vName";
      $selectQuery3 = "SELECT COUNT(id) as allVcounter FROM ".$table." WHERE empid = '$currentUserId' GROUP BY empid";
      $result1 = mysqli_query($connect, $selectQuery);
      $result2 = mysqli_query($connect, $selectQuery2);
      $result3 = mysqli_query($connect, $selectQuery3);
      if ($result1 && $result2 && $result3) {
          $rows[0] = mysqli_fetch_array($result1, MYSQLI_NUM);
          $rows[1] = mysqli_fetch_array($result2, MYSQLI_NUM);

          $rows[2] = mysqli_fetch_array($result3, MYSQLI_NUM);

          echo json_encode($rows);
          exit();
      } else {
          echo json_encode('هناك مشكلة بالبيانات ارجو التأكد من سلامتها');
          exit();
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'loadVacationsData') {
      include "session.php";
      $currentUserId = $_SESSION['employeeid'];
      $table = 'vacationsrequests';
      $selectQuery = "SELECT v.* , CASE WHEN e.id=v.empid
    THEN e.name
    END AS employeeName
    FROM vacationsrequests as v
    JOIN employees as e ON e.id=v.empid
    WHERE empid = '$currentUserId'";
      if ($result = mysqli_query($connect, $selectQuery)) {
          $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
          echo json_encode($rows);
          exit();
      } else {
          echo json_encode('هناك مشكلة بالبيانات ارجو التأكد من سلامتها');
          exit();
      }
  }
