<?php
if (isset($_SESSION)) {
include "session.php";
}
$phpFileUploadErrors = array(
0 => 'The file has uploaded successfully with no errors',
1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
3 => 'The uploaded file was only partially uploaded',
4 => 'No file was uploaded',
6 => 'Missing a temporary folder',
7 => 'Failed to write file to disk.',
8 => 'A PHP extension stopped the file upload.',
);
$output = '';
$filesnames = array();
$filesnamesids = array();

if (isset($_POST['encodedIdcard'])) {
    $uniqueNumber = $_POST['encodedIdcard'].'+';
}
if (isset($_SESSION['employeeid'])) {
    $uniqueNumber = base64_encode($_SESSION['employeeid']).'+';
}
if (isset($_FILES['userfile'])) {
    $numberOfErrors = 0;

    $file_array = reArrayFiles($_FILES['userfile']);
    //pre_r($file_array);
    for ($i=0;$i<count($file_array);$i++) {
        if ($file_array[$i]['error']) {
            $output .= $file_array[$i]['name'].' - '.$phpFileUploadErrors[$file_array[$i]['error']];
        } else {
            $extensions = array("jpg","png","jpeg","gif","jfif","pdf","doc","docx","dwg","xls");
            $file_ext = explode('.', $file_array[$i]['name']);
            $file_ext = end($file_ext);

            if (!in_array($file_ext, $extensions)) {
                $output .="{$file_array[$i]['name']} - صيغ الملف غير صالحة!";
                $numberOfErrors = $numberOfErrors + 1;
            } else {
                move_uploaded_file($file_array[$i]['tmp_name'], $_POST['link'].$uniqueNumber.$file_array[$i]['name']);

                $output .= $file_array[$i]['name'].' - '.$phpFileUploadErrors[$file_array[$i]['error']];


                if ($phpFileUploadErrors[$file_array[$i]['error']] == 'The file has uploaded successfully with no errors') {
                    array_push($filesnames, $uniqueNumber.$file_array[$i]['name']);
                } else {
                    $numberOfErrors = $numberOfErrors + 1;
                }
            }
        }
    }
    $data = array(
         'output' => $output,
         'filesnames' => $filesnames,
         'numberOfErrors' => $numberOfErrors
         );

    echo json_encode($data);
    exit();
}



if (isset($_POST['fileToDelete']) && !empty($_POST['fileToDelete'])) {
    $filename = $_POST['fileToDelete'];
    $locationToDelete = $_POST['link'].$filename;

    if (unlink($locationToDelete)) {
        echo 'تم حذف الملف بنجاح';
        exit();
    } else {
        echo 'الملف '.$filename.'لم يتم حذفه فهو غير موجود مسبقاً';
        exit();
    }
}
function reArrayFiles($file_post)
{
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}
