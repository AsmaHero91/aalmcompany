<?php
include "connect.php";

  function recordSession($connect)
  {
      include "session.php";
      $recordsTable = "session";
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      if (isset($_SESSION['employeeid'])) {
          $insertquery = "INSERT INTO ".$recordsTable." (sessionid, empid, activityName, status, timeActivity)
    VALUES (NULL,'".$_SESSION['employeeid']."','read','online','".$datetime."')";
          mysqli_query($connect, $insertquery);
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'DeleteOneItem') {
      if ($_POST["deletedItem"]) {
          if ($_POST["tableNameData"] == "customersTable") {
              $recordsTable = "customers";
              $deleteQuery = "DELETE FROM ".$recordsTable."
        WHERE customerid ='".$_POST["deletedItem"]."'";
              if (mysqli_query($connect, $deleteQuery)) {
                  echo 0;
                  exit;
              } else {
                  echo -1;
              }
          }
          if ($_POST["tableNameData"] == "customersTable2") {
              $recordsTable = "customers";
              $deleteQuery = "DELETE FROM ".$recordsTable."
        WHERE customerid ='".$_POST["deletedItem"]."'";
              if (mysqli_query($connect, $deleteQuery)) {
                  echo 0;
                  exit;
              } else {
                  echo -1;
              }
          }
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'editRecords') {
      if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'customersTable') {
          $recordsTable = "customers";
          if (!empty($_POST['editedRecordsData'])) {
              $rowseditRecords = $_POST['editedRecordsData'];
              $editedItems = $_POST['editedItemsData'];
              // these data to be user for delete all items

              $updateQuery = "UPDATE ".$recordsTable." SET customerName = '".$rowseditRecords[0][2]."', Notes = '".$rowseditRecords[0][8]."' , workDestination = '".$rowseditRecords[0][3]."' , mobile1 = '".$rowseditRecords[0][4]."' , mobile2 = '".$rowseditRecords[0][5]."'  ,  email = '".$rowseditRecords[0][7]."'
        WHERE customerid ='".$editedItems[0]."'";
              if (mysqli_query($connect, $updateQuery)) {
                  echo json_encode(0);
              } else {
                  echo -2;
              }
          } else {
              echo -1;
          }
      }

      if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'customersTable2') {
          $recordsTable = "customers";
          if (!empty($_POST['editedRecordsData'])) {
              $rowseditRecords = $_POST['editedRecordsData'];
              $editedItems = $_POST['editedItemsData'];
              // these data to be user for delete all items

              $updateQuery = "UPDATE ".$recordsTable." SET customerName = '".$rowseditRecords[0][2]."', Notes = '".$rowseditRecords[0][8]."' , workDestination = '".$rowseditRecords[0][3]."' , mobile1 = '".$rowseditRecords[0][4]."' , mobile2 = '".$rowseditRecords[0][5]."'  ,  email = '".$rowseditRecords[0][7]."'
        WHERE customerid ='".$editedItems[0]."'";
              if (mysqli_query($connect, $updateQuery)) {
                  echo json_encode(0);
              } else {
                  echo -2;
              }
          } else {
              echo -1;
          }
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'deleteMultibleRecords') {
      if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'customersTable') {
          $recordsTable = "customers";
          if (!empty($_POST['deleteItemsData'])) {
              $deletedItems = $_POST['deleteItemsData'];
              // these data to be user for delete all items
              $j = 0;
              foreach ($deletedItems as $deletedItem) {
                  $deleteQuery = "DELETE FROM ".$recordsTable."
    WHERE customerid ='".$deletedItem."'";
                  if (mysqli_query($connect, $deleteQuery)) {
                      echo 0;
                  } else {
                      echo -1; //error with your data
                  }
                  $j++;
              }
          } else {
              echo -2;
          }
      }
      if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'customersTable2') {
          $recordsTable = "customers";
          if (!empty($_POST['deleteItemsData'])) {
              $deletedItems = $_POST['deleteItemsData'];
              // these data to be user for delete all items
              $j = 0;
              foreach ($deletedItems as $deletedItem) {
                  $deleteQuery = "DELETE FROM ".$recordsTable."
    WHERE customerid ='".$deletedItem."'";
                  if (mysqli_query($connect, $deleteQuery)) {
                      echo 0;
                  } else {
                      echo -1; //error with your data
                  }
                  $j++;
              }
          } else {
              echo -2;
          }
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'saveMultibleRecords') {
      if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == "customersTable") {
          $recordsTable = "customers";
$datetime = date("Y-m-d H:i:s"); //dateCreated
          if (!empty($_POST['addRecordsData'])) {
              $rowsaddRecords = $_POST['addRecordsData'];
              foreach ($rowsaddRecords[0] as $rowaddRecords) {
                  $insertQuery = "INSERT INTO ".$recordsTable." (customerid, customerName, customerType, Notes ,workDestination,dateJoined,mobile1,mobile2,email, groupCat)
                  VALUES ('".$rowaddRecords[1]."','".$rowaddRecords[2]."','','".$rowaddRecords[8]."','".$rowaddRecords[3]."','$datetime','".$rowaddRecords[4]."','".$rowaddRecords[5]."','".$rowaddRecords[7]."','1')";
                  if (mysqli_query($connect, $insertQuery)) {
                      echo 0;
                  } else {
                      echo -2; //error with your data
                  }
              }
          } else {
              echo -1;
          }
      }
  }

  if (!empty($_POST['action']) && $_POST['action'] == 'showCustomers') {
      $table = 'customers';
      //ORDER BY dateCreated DESC

      $sqlQuery = "SELECT * FROM ".$table." WHERE customerid LIKE 'cus%'"; // to take the first record of request process to insert it
      $result = mysqli_query($connect, $sqlQuery);
      $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
      if ($result) {
          recordSession($connect);
          echo json_encode($rows);
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'showContractors') {
      $table = 'customers';
      //ORDER BY dateCreated DESC

      $sqlQuery = "SELECT * FROM ".$table." WHERE customerid LIKE 'con%'"; // to take the first record of request process to insert it
      $result = mysqli_query($connect, $sqlQuery);
      $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
      if ($result) {
          recordSession($connect);
          echo json_encode($rows);
      }
  }
