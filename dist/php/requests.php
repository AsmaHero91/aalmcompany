<?php
include "connect.php";

  function recordSession($connect)
  {
      include "session.php";
      $recordsTable = "session";
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      $insertquery = "INSERT INTO ".$recordsTable." (sessionid, empid, activityName, status, timeActivity)
  VALUES (NULL,'".$_SESSION['employeeid']."','read','online','".$datetime."')";
      mysqli_query($connect, $insertquery);
  }

  if (!empty($_POST['action']) && $_POST['action'] == 'showRequestTypesTable') {
      $table = 'requesttype';
      //ORDER BY dateCreated DESC
$sqlQuery = "SELECT * FROM ".$table.""; // to take the first record of request process to insert it
$result = mysqli_query($connect, $sqlQuery);
      $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
      if ($result) {
          recordSession($connect);

          echo json_encode($rows);
      }
  }
if (!empty($_POST['action']) && $_POST['action'] == 'showAllRequests') {
    //ORDER BY dateCreated DESC
    $sqlQuery = "SELECT r.*, CASE WHEN r.requestTCode = ty.requestTCode
                 THEN ty.name
               END as requestTypeName, CASE WHEN r.empid = e.id
                                THEN e.name
                              END as requester
FROM requests as r
JOIN requesttype as ty ON ty.requestTCode=r.requestTCode
JOIN employees as e ON e.id=r.empid
ORDER BY r.dataCreated ASC";


    $result = mysqli_query($connect, $sqlQuery);

    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    recordSession($connect);

    echo json_encode($rows);
}
if (!empty($_POST['action']) && $_POST['action'] == 'DeleteOneItem') {
    if ($_POST["deletedItem"]) {
        if ($_POST["tableNameData"] == "requesttype") {
            $recordsTable = "requesttype";
            $deleteQuery = "DELETE FROM ".$recordsTable."
      WHERE requestTCode ='".$_POST["deletedItem"]."'";
            if (mysqli_query($connect, $deleteQuery)) {
                echo 0;
                exit;
            } else {
                echo -1;
            }
        }
        if ($_POST["tableNameData"] == "requestprocesses") {
            $recordsTable = "requestprocesses";
            $deleteQuery = "DELETE FROM ".$recordsTable."
      WHERE id ='".$_POST["deletedItem"]."'";
            if (mysqli_query($connect, $deleteQuery)) {
                echo 0;
            } else {
                echo -1;
            }
        }
        // these data to be user for delete all items
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'fillSelectRequest') {
    $recordsTable = "requesttype";

    $Query = "SELECT * FROM ".$recordsTable." ORDER BY requestTCode DESC";
    if ($result = mysqli_query($connect, $Query)) {
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
        echo json_encode($rows);
    } else {
        echo json_encode(-1); //error with your data
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'countingRequestsHandeled') {
    $recordsTable = "requesthistorylog";
    include "session.php";
    $Query1 = "SELECT * FROM ".$recordsTable." WHERE status='in progress' AND assignedTo LIKE '%".$_SESSION['employeeid']."%' AND isActionTaken = '0'";
    $Query2 = "SELECT * FROM ".$recordsTable." WHERE empid = ".$_SESSION['employeeid']." AND isActionTaken = '1'";
    $result1 = mysqli_query($connect, $Query1);
    $result2 = mysqli_query($connect, $Query2);
    if ($result1 && $result2) {
        $rows1 = mysqli_fetch_all($result1, MYSQLI_ASSOC);
        $rows2 = mysqli_fetch_all($result2, MYSQLI_ASSOC);
        $json['new'] = COUNT($rows1);
        $json['handeled'] = COUNT($rows2);
        echo json_encode($json);
    } else {
        echo json_encode(-1); //error with your data
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'deleteMultibleRecords') {
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'requesttype') {
        $recordsTable = "requesttype";
        if (!empty($_POST['deleteItemsData'])) {
            $deletedItems = $_POST['deleteItemsData'];
            // these data to be user for delete all items
            $j = 0;
            foreach ($deletedItems as $deletedItem) {
                $deleteQuery = "DELETE FROM ".$recordsTable."
    WHERE requestTCode ='".$deletedItem."'";
                if (mysqli_query($connect, $deleteQuery)) {
                    echo 0;
                } else {
                    echo -1; //error with your data
                }
                $j++;
            }
        } else {
            echo -2;
        }
    }

    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'requestprocesses') {
        $recordsTable = "requestprocesses";
        if (!empty($_POST['deleteItemsData'])) {
            $deletedItems = $_POST['deleteItemsData'];
            // these data to be user for delete all items
            $j = 0;
            foreach ($deletedItems as $deletedItem) {
                $deleteQuery = "DELETE FROM ".$recordsTable."
    WHERE id ='".$deletedItem."'";
                if (mysqli_query($connect, $deleteQuery)) {
                    echo 0;
                } else {
                    echo -1; //error with your data
                }
                $j++;
            }
        } else {
            echo -2;
        }
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'editRecords') {
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'requesttype') {
        $recordsTable = "requesttype";
        if (!empty($_POST['editedRecordsData'])) {
            $rowseditRecords = $_POST['editedRecordsData'];
            $editedItems = $_POST['editedItemsData'];
            // these data to be user for delete all items

            $updateQuery = "UPDATE ".$recordsTable."
      SET name = '".$rowseditRecords[0][2]."' , priority = '".$rowseditRecords[0][3]."'
      WHERE requestTCode ='".$editedItems[0]."'";
            if (mysqli_query($connect, $updateQuery)) {
                echo 0;
            } else {
                echo -2;
            }
        } else {
            echo -1;
        }
    }
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'requestprocesses') {
        $recordsTable = "requestprocesses";
        if (!empty($_POST['editedRecordsData'])) {
            $rowseditRecords = $_POST['editedRecordsData'];
            $editedItems = $_POST['editedItemsData'];
            // these data to be user for delete all items

            $updateQuery = "UPDATE ".$recordsTable."
  SET requestTCode = '".$rowseditRecords[0][1]."', poderNo = '".$rowseditRecords[0][2]."' , assignedTo = '".$rowseditRecords[0][3]."' , actionStatement = '".$rowseditRecords[0][4]."' , durationInDays = '".$rowseditRecords[0][5]."'
    WHERE id ='".$editedItems[0]."'";

            if (mysqli_query($connect, $updateQuery)) {
                echo 0;
            } else {
                echo -2;
            }
        } else {
            echo -1;
        }
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'saveMultibleRecords') {
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == "requesttype") {
        $recordsTable = "requesttype";

        if (!empty($_POST['addRecordsData'])) {
            $rowsaddRecords = $_POST['addRecordsData'];
            foreach ($rowsaddRecords[0] as $rowaddRecords) {
                $insertQuery = "INSERT INTO ".$recordsTable." (requestTCode, name, priority)
      VALUES ('".$rowaddRecords[1]."','".$rowaddRecords[2]."','".$rowaddRecords[3]."')";
                if (mysqli_query($connect, $insertQuery)) {
                    echo 0;
                } else {
                    echo -2; //error with your data
                }
            }
        } else {
            echo -1;
        }
    }
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == "requestprocesses") {
        $recordsTable = "requestprocesses";

        if (!empty($_POST['addRecordsData'])) {
            $rowsaddRecords = $_POST['addRecordsData'];
            foreach ($rowsaddRecords[0] as $rowaddRecords) {
                $insertQuery = "INSERT INTO ".$recordsTable." (id, requestTCode, poderNo , assignedTo , actionStatement,  durationInDays)
        VALUES (NULL,'".$rowaddRecords[1]."','".$rowaddRecords[2]."','".$rowaddRecords[3]."','".$rowaddRecords[4]."','".$rowaddRecords[5]."')";
                if (mysqli_query($connect, $insertQuery)) {
                    echo 0;
                } else {
                    echo -2; //error with your data
                }
            }
        } else {
            echo -1;
        }
    }
}

//
//   $table = 'requesttype';
// //ORDER BY dateCreated DESC
// $updateQuery = "SELECT * FROM ".$table." ORDER BY `requestTCode` ASC"; // to take the first record of request process to insert it
//
// echo $_POST['t'];
// }
// if(!empty($_POST['action']) && $_POST['action'] == 'Edit') {
//   if($_POST["values"]) {
//
// // these data from formdata
//   $updateQuery = "UPDATE ".$recordsTable."
// //     SET id = '".$_POST["id"]."', name = '".$_POST["name"]."' , nationality = '".$_POST["nationality"]."', skills = '".$_POST["skills"]."' , experience = '".$_POST["experience"]."', lastCer = '".$_POST["lastCer"]."', joinDate = '".$_POST["joinDate"]."', status = '".$_POST["status"]."', documents = '".$_POST["documents"]."'
//     WHERE id ='".$_POST["idt"]."'";
//     $result = mysqli_query($connect, $updateQuery);
//
//           echo 'Data Updated';
// if(mysqli_query($connect, $updateQuery)){
//
// }else{
//   mysqli_query($connect, $insertQuery)
// }
//   }
//   $table = 'requesttype';
// //ORDER BY dateCreated DESC
// $updateQuery = "SELECT * FROM ".$table." ORDER BY `requestTCode` ASC"; // to take the first record of request process to insert it
//
// echo $_POST['t'];
// }
