<?php
include "connect.php";

  if (!empty($_POST['action']) && $_POST['action'] == 'alertUnreadListMessages') {
      include "session.php";
      $currentuserId = $_SESSION['employeeid'];

      ///lists of  converstaions latest to recents
      $listMessagesalertQuery2 = "SELECT COUNT(blogid) as counter , h.* , CASE WHEN h.authorid=u1.employeeid
             THEN u1.username
           END as writerName , CASE WHEN h.authorid=u1.employeeid
                         THEN u1.userPic
                       END as writerPic
  FROM blogs as h
  JOIN users as u1 ON u1.employeeid=h.authorid
  WHERE authorid != '$currentuserId' AND '$currentuserId' NOT IN(SELECT empid from blogshistory WHERE blogid = h.blogid)
  GROUP BY blogid ORDER BY dateCreated DESC";
      $resultlistMessagesalertQuery2 = mysqli_query($connect, $listMessagesalertQuery2);
      $rows2 = mysqli_fetch_all($resultlistMessagesalertQuery2, MYSQLI_ASSOC);

      ////////////list of unreadBlogs
      ///lists of  converstaions latest to recents
      $listMessagesalertQuery = "SELECT COUNT(m.id) as counter , m.* , CASE WHEN u2.employeeid = $currentuserId
              THEN u1.username
              ELSE u2.username
            END as participantUsername , CASE WHEN u2.employeeid = $currentuserId
                          THEN u1.userPic
                          ELSE u2.userPic
                        END as participantPic
FROM chats as m
JOIN users as u1 ON m.fromUserId=u1.employeeid
JOIN users as u2 ON m.toUserId=u2.employeeid
WHERE m.toUserId = $currentuserId AND readen = 0
GROUP BY m.conversation_id  ORDER BY m.id DESC";
      $resultlistMessagesalertQuery = mysqli_query($connect, $listMessagesalertQuery);
      $rows = mysqli_fetch_all($resultlistMessagesalertQuery, MYSQLI_ASSOC);
      $jsonObject2['rowsBlogs'] = $rows2;
      $jsonObject2['rows'] = $rows;
      if (count($rows) > 0 || count($rows2) > 0) {
          echo json_encode($jsonObject2);
      } else {
          echo json_encode(-1);
      }
  }

  if (!empty($_POST['action']) && $_POST['action'] == 'ViewAndupdateReadenBlogs') {
      include "session.php";
      $currentuserId = $_SESSION['employeeid'];
      $currentblogId = intval($_POST['element']);
      $table = 'blogshistory';

      ///lists of  converstaions latest to recents
      $listMessagesalertQuery2 = "SELECT * FROM blogs WHERE blogid = '$currentblogId'";
      $resultlistMessagesalertQuery2 = mysqli_query($connect, $listMessagesalertQuery2);
      $row = mysqli_fetch_array($resultlistMessagesalertQuery2, MYSQLI_ASSOC);
      $datetime = date("Y-m-d H:i:s"); //dateCreated

      $insertQuery = "INSERT INTO ".$table." (id, blogid, empid, activityType, dateCreated)
      VALUES (NULL,'$currentblogId','$currentuserId', 'read','".$datetime."')";

      $result = mysqli_query($connect, $insertQuery);


      if (count($row) > 0) {
          echo json_encode($row);
      } else {
          echo json_encode(-1);
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'viewLatestMessages') {
      include "session.php";
      $currentuserId = $_SESSION['employeeid'];
      $val = 0;
      ///lists of  converstaions latest to recents
      $listMessagesQuery = "SELECT m.*, CASE WHEN u2.employeeid = $currentuserId
              THEN u1.username
              ELSE u2.username
            END as participantUsername , CASE WHEN u2.employeeid = $currentuserId
                          THEN u1.userPic
                          ELSE u2.userPic
                        END as participantPic , CASE WHEN m.readen = 0
                                      THEN 1
                                    END as counter
FROM chats as m
JOIN users as u1 ON m.fromUserId=u1.employeeid
JOIN users as u2 ON m.toUserId=u2.employeeid
WHERE m.id IN (
SELECT MAX(id)
FROM chats
WHERE (fromUserId = $currentuserId OR toUserId = $currentuserId)
GROUP BY conversation_id ) ORDER BY m.id DESC";
      $resultlistMessagesQuery = mysqli_query($connect, $listMessagesQuery);
      $rows = mysqli_fetch_all($resultlistMessagesQuery, MYSQLI_ASSOC);
      $jsonObject = json_encode($rows);
      echo $jsonObject;
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'sendChat') {
      include "session.php";
      $recordsTable = 'chats';
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      $currentuserId = $_SESSION['employeeid'];
      $currentusername = $_SESSION['username'];
      $currentConversation = base64_decode($_POST["conversation_id"]);
      $chatmessage = $_POST['message'];
      $formattedConversation = explode('-', base64_decode($currentConversation));
      if ($currentuserId == $formattedConversation[0]) {
          $receiverId = intval($formattedConversation[1]);
      } else {
          $receiverId = intval($formattedConversation[0]);
      }

      $insertQuery = "INSERT INTO ".$recordsTable." (id, conversation_id, toUserId, fromUserId, chatMessage, timestamp, readen)
    VALUES (NULL,'".$currentConversation."', '$receiverId', '$currentuserId', '".$chatmessage."','".$datetime."','0')";
      $result = mysqli_query($connect, $insertQuery);
      if ($result) {
          echo 0;
      } else {
          echo -1;
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'updateReadenMessagesOnCurrentChat') {
      include "session.php";
      $currentuserId = $_SESSION['employeeid'];
      $currentConversation = $_SESSION["conversation_id"];
      $table = "chats";
      $sqlUpdateQuery = "UPDATE ".$table." SET readen=1 WHERE toUserId = '".mysqli_real_escape_string($connect, $currentuserId)."' AND conversation_id = '".mysqli_real_escape_string($connect, $currentConversation)."'"; // to take the first record of request process to insert it

      if (mysqli_query($connect, $sqlUpdateQuery)) {
          echo 0;
      } else {
          echo -1;
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'updateReadenMessagesOnCurrentChate') {
      include "session.php";
      $currentuserId = $_SESSION['employeeid'];
      $currentConversation = $_POST["element"];
      $table = "chats";
      $sqlUpdateQuery = "UPDATE ".$table." SET readen=1 WHERE readen=0 AND toUserId = '".mysqli_real_escape_string($connect, $currentuserId)."' AND conversation_id = '".mysqli_real_escape_string($connect, $currentConversation)."'"; // to take the first record of request process to insert it

      if (mysqli_query($connect, $sqlUpdateQuery)) {
          echo 0;
      } else {
          echo -1;
      }
  }
if (!empty($_POST['action']) && $_POST['action'] == 'setLatestChatid') {
    include "session.php";
    include "db.php";
    $currentuserId = $_SESSION['employeeid'];

    /////all ready data to insert it later to json/////
    $table = "chats";
    $LASTmessageQuery=$conn->prepare("select conversation_id  from chats where fromUserId = ".$currentuserId." or toUserId = ".$currentuserId." order by id desc limit 0,1");
    $LASTmessageQuery->setFetchMode(PDO::FETCH_OBJ);
    $LASTmessageQuery->execute();
    while ($row = $LASTmessageQuery ->fetch()) {
        if (!isset($_SESSION['conversation_id'])) {
            $_SESSION['conversation_id'] = $row->conversation_id;
            echo json_encode($row->conversation_id);
        } else {
            echo json_encode($_SESSION['conversation_id']);
        }
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'setClickedChatid') {
    include "session.php";
    $currentConversation = $_POST['element'];
    $_SESSION['conversation_id'] = $currentConversation;
    echo json_encode($_SESSION['conversation_id']);
}
if (!empty($_POST['action']) && $_POST['action'] == 'viewClickedChat') {
    include "session.php";
    $currentuserId = $_SESSION['employeeid'];
    $currentReceiverID = base64_decode($_POST['element']);
    $cid = $currentuserId.'-'.$currentReceiverID;
    $currentConversation = base64_encode($currentuserId.'-'.$currentReceiverID);
    $currentConversation2 = base64_encode($currentReceiverID.'-'.$currentuserId);


    $viewMessagesQuery = "SELECT m.*, CASE WHEN u2.employeeid = $currentuserId
            THEN u1.username
            ELSE u2.username
          END as participantUsername , CASE WHEN u2.employeeid = $currentuserId
                        THEN u1.userPic
                        ELSE u2.userPic
                      END as participantPic , CASE WHEN m.readen = 0
                                    THEN 1
                                  END as counter
FROM chats as m
JOIN users as u1 ON m.fromUserId=u1.employeeid
JOIN users as u2 ON m.toUserId=u2.employeeid
WHERE (conversation_id = '$currentConversation' OR conversation_id = '$currentConversation2') ORDER BY m.id ASC";

    $resultviewMessagesQuery = mysqli_query($connect, $viewMessagesQuery);
    $rowcount = mysqli_num_rows($resultviewMessagesQuery);

    if ($rowcount > 0) {
        $rows = mysqli_fetch_all($resultviewMessagesQuery, MYSQLI_ASSOC);

        $sqlQuery = "SELECT MAX(s.sessionid) as max, s.*, CASE WHEN u1.employeeid = s.empid
              THEN u1.username
            END as receivername , CASE WHEN u1.employeeid = s.empid
                          THEN u1.userPic
                        END as receiverpic , CASE WHEN u1.employeeid = s.empid
                                      THEN u1.employeeid
                                    END as receiverid
      FROM session as s
      LEFT JOIN users as u1 ON u1.employeeid = s.empid
      WHERE s.sessionid IN (
      SELECT MAX(sessionid)
      FROM session
    WHERE empid = '$currentReceiverID')";

        $result = mysqli_query($connect, $sqlQuery);

        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        $jsonObject1['new'] = $row;
        $jsonObject1['rows'] = $rows;


        echo json_encode($jsonObject1);
    } else {
        $sqlQuery = "SELECT MAX(s.sessionid) as max, s.*, CASE WHEN u1.employeeid = s.empid
            THEN u1.username
          END as receivername , CASE WHEN u1.employeeid = s.empid
                        THEN u1.userPic
                      END as receiverpic , CASE WHEN u1.employeeid = s.empid
                                    THEN u1.employeeid
                                  END as receiverid
    FROM session as s
    LEFT JOIN users as u1 ON u1.employeeid = s.empid
    WHERE s.sessionid IN (
    SELECT MAX(sessionid)
    FROM session
  WHERE empid = '$currentReceiverID')";

        $result = mysqli_query($connect, $sqlQuery);

        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        $jsonObject1['new'] = $row;
        if ($result) {
            echo json_encode($jsonObject1);
        } else {
            echo json_encode(-1);
        }
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'viewClickedChatel') {
    include "session.php";
    $currentuserId = $_SESSION['employeeid'];
    $currentConversationel= $_POST['element'];
    $currentConversationformatted = explode('-', base64_decode($currentConversationel));

    if ($currentuserId == $currentConversationformatted[0]) {
        $currentReceiverID = $currentConversationformatted[1];
    } else {
        $currentReceiverID = $currentConversationformatted[0];
    }

    $currentConversation = base64_encode($currentConversationformatted[0].'-'.$currentConversationformatted[1]);
    $currentConversation2 = base64_encode($currentConversationformatted[1].'-'.$currentConversationformatted[0]);



    $viewMessagesQuery = "SELECT m.*, CASE WHEN u2.employeeid = $currentuserId
            THEN u1.username
            ELSE u2.username
          END as participantUsername , CASE WHEN u2.employeeid = $currentuserId
                        THEN u1.userPic
                        ELSE u2.userPic
                      END as participantPic , CASE WHEN m.readen = 0
                                    THEN 1
                                  END as counter
FROM chats as m
JOIN users as u1 ON m.fromUserId=u1.employeeid
JOIN users as u2 ON m.toUserId=u2.employeeid
WHERE (conversation_id = '$currentConversation' OR conversation_id = '$currentConversation2') ORDER BY m.id ASC";

    $resultviewMessagesQuery = mysqli_query($connect, $viewMessagesQuery);
    $rowcount = mysqli_num_rows($resultviewMessagesQuery);

    if ($rowcount > 0) {
        $rows = mysqli_fetch_all($resultviewMessagesQuery, MYSQLI_ASSOC);

        $sqlQuery = "SELECT MAX(s.sessionid) as max, s.*, CASE WHEN u1.employeeid = s.empid
              THEN u1.username
            END as receivername , CASE WHEN u1.employeeid = s.empid
                          THEN u1.userPic
                        END as receiverpic
      FROM session as s
      LEFT JOIN users as u1 ON u1.employeeid = s.empid
      WHERE s.sessionid IN (
      SELECT MAX(sessionid)
      FROM session
    WHERE empid = '$currentReceiverID')";

        $result = mysqli_query($connect, $sqlQuery);

        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        $jsonObject1['new'] = $row;
        $jsonObject1['rows'] = $rows;


        echo json_encode($jsonObject1);
    } else {
        $sqlQuery = "SELECT MAX(s.sessionid) as max, s.*, CASE WHEN u1.employeeid = $currentReceiverID
            THEN u1.username
          END as receivername , CASE WHEN u1.employeeid = $currentReceiverID
                        THEN u1.userPic
                      END as receiverpic
    FROM session as s
    JOIN users as u1 ON u1.employeeid=s.empid
    WHERE s.empid = '$currentReceiverID'
    GROUP BY empid";

        $result = mysqli_query($connect, $sqlQuery);

        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        $jsonObject1['new'] = $row;

        echo json_encode($jsonObject1);
    }
}
//note ******************** $_SESSION["conversation_id"] must FROM THE USER TO RECEIVER.
  if (!empty($_POST['action']) && $_POST['action'] == 'viewLatestChatOnLoad') {
      include "session.php";
      $currentuserId = $_SESSION['employeeid'];
      $currentConversation = $_SESSION['conversation_id'];
      $currentConversationformatted = explode('-', base64_decode($currentConversation));

      if ($currentuserId == $currentConversationformatted[0]) {
          $currentReceiverID = $currentConversationformatted[1];
      } else {
          $currentReceiverID = $currentConversationformatted[0];
      }
      $currentConversation = base64_encode($currentConversationformatted[0].'-'.$currentConversationformatted[1]);
      $currentConversation2 = base64_encode($currentConversationformatted[1].'-'.$currentConversationformatted[0]);



      $viewMessagesQuery = "SELECT m.*, CASE WHEN u2.employeeid = $currentuserId
              THEN u1.username
              ELSE u2.username
            END as participantUsername , CASE WHEN u2.employeeid = $currentuserId
                          THEN u1.userPic
                          ELSE u2.userPic
                        END as participantPic , CASE WHEN m.readen = 0
                                      THEN 1
                                    END as counter
  FROM chats as m
  JOIN users as u1 ON m.fromUserId=u1.employeeid
  JOIN users as u2 ON m.toUserId=u2.employeeid
  WHERE (conversation_id = '$currentConversation' OR conversation_id = '$currentConversation2') ORDER BY m.id ASC";


      $resultviewMessagesQuery = mysqli_query($connect, $viewMessagesQuery);
      $rowcount = mysqli_num_rows($resultviewMessagesQuery);

      if ($rowcount > 0) {
          $rows = mysqli_fetch_all($resultviewMessagesQuery, MYSQLI_ASSOC);

          $sqlQuery = "SELECT MAX(s.sessionid) as max, s.*, CASE WHEN u1.employeeid = s.empid
                THEN u1.username
              END as receivername , CASE WHEN u1.employeeid = s.empid
                            THEN u1.userPic
                          END as receiverpic
        FROM session as s
        LEFT JOIN users as u1 ON u1.employeeid = s.empid
        WHERE s.sessionid IN (
        SELECT MAX(sessionid)
        FROM session
      WHERE empid = '$currentReceiverID')";

          $result = mysqli_query($connect, $sqlQuery);

          $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

          $jsonObject1['new'] = $row;
          $jsonObject1['rows'] = $rows;


          echo json_encode($jsonObject1);
      } else {
          $sqlQuery = "SELECT MAX(s.sessionid) as max, s.*, CASE WHEN u1.employeeid = $currentReceiverID
              THEN u1.username
            END as receivername , CASE WHEN u1.employeeid = $currentReceiverID
                          THEN u1.userPic
                        END as receiverpic
      FROM session as s
      JOIN users as u1 ON u1.employeeid=s.empid
      WHERE s.empid = '$currentReceiverID'
      GROUP BY empid";

          $result = mysqli_query($connect, $sqlQuery);

          $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

          $jsonObject1['new'] = $row;

          echo json_encode($jsonObject1);
      }
  }
