<?php
include "connect.php";

  function recordSession($connect)
  {
      include "session.php";
      $recordsTable = "session";
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      if (isset($_SESSION['employeeid'])) {
          $insertquery = "INSERT INTO ".$recordsTable." (sessionid, empid, activityName, status, timeActivity)
    VALUES (NULL,'".$_SESSION['employeeid']."','read','online','".$datetime."')";
          mysqli_query($connect, $insertquery);
      }
  }


  if (!empty($_POST['action']) && $_POST['action'] == 'checkpermession') {
      include "session.php";
      $currentUserID = $_SESSION["employeeid"];
      $table = 'projects';
      $element = implode('', $_POST['element']);
      $query = "SELECT `teamemp` ,`leadby` FROM ".$table." WHERE projectCode = '".mysqli_real_escape_string($connect, $element)."'";
      $result = mysqli_query($connect, $query);
      $row = mysqli_fetch_array($result, MYSQLI_NUM);
      $selectedTeam = $row[0];//"1076609104,1076609102";
$selectedLead = $row[1];//"1076609104";
if (strpos($selectedLead, $currentUserID) !== false || strpos($selectedTeam, $currentUserID) !== false) { //to check if  contains this character
//check if the current user in selected team
 if (strpos($selectedLead, $currentUserID) !== false) { //to check if  contains this character
echo 0;
 } else {
     echo 1;
 }
} else {
    echo -1;
}
  }
if (!empty($_POST['action']) && $_POST['action'] == 'saveData') {
    include "session.php";
    $currentUserName = $_SESSION["username"];
    $table = 'gantt';
    $table2 = 'projects';
    $str = $_POST["json"];
    // Input:
  $datetime = date("Y-m-d H:i:s"); //dateCreated

  $insertQuery = "INSERT INTO ".$table." (id, projectCode, gantJson, employeename, dateCreated)
  VALUES (NULL,'".$_POST["element"]."', '".$str."' , '".$currentUserName."' , '".$datetime."')";//0 in progress / 1 completed / 2 declined

    $progress = intval($_POST['ProjectProgress']);
    $tasksAcheived = $_POST['MainTasksAcheived'];

    $updateQuery = "UPDATE ".$table2." SET progress = '$progress' , tasksAcheived = '".$tasksAcheived."' WHERE projectCode = '".$_POST["element"]."'";

    if (mysqli_query($connect, $insertQuery) && mysqli_query($connect, $updateQuery)) {
        echo 0;
    } else {
        echo -1;
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'sendProjectRequests') {
    include "session.php";
    $table = 'projectrequests';
    $positiveIndex = intval($_POST['positiveIndex']);

    //$myDataObjectResources = $_POST['myDataObjectTasksAss']['resources'];
    $myDocuments =  $_POST['allFilesNames'];
    if (!empty($myDocuments)) {
        $myFilesData =  $myDocuments;
    } else {
        $myFilesData = '';
    }
    $projectCode = $_POST['projectCode'];
    $taskNo = abs($_POST['id']); //abs to take positive value

    $taskName = $_POST['name'];
    $description = $_POST['description'];
    $duration = $_POST['duration'];
    $status = $_POST['status'];

    if ($_POST['status'] != "STATUS_SUSPENDED" && $_POST['status'] != "STATUS_ACTIVE" && $_POST['status'] != "STATUS_FAILED" && $_POST['status'] != "STATUS_DONE") {
        echo -4;
        exit();
    }
    if ($_POST['status'] == "STATUS_ACTIVE" || $_POST['status'] == "STATUS_FAILED" || $_POST['status'] == "STATUS_DONE") {
        $updateQuery = "UPDATE ".$table." SET status = '".$status."', documents = '".$myFilesData."' , description = '".$description."' WHERE taskNo = '".$taskNo."' AND  projectCode = '".$projectCode."'";
        $result = mysqli_query($connect, $updateQuery);
        if ($result) {
            echo 0;
        } else {
            echo -1;
        }
        exit();
    }
    $assignedtoidsAfter = array();
    $assignedtoidsAfter2 = array();
    $assignedtonamesAfter = array();
    //$assignedTo = $_POST['resourceId'];
    $arrAssignedTo = json_decode($_POST['arr']);

    if (!isset($_POST['assigs'])) {
        echo -2;
        exit();
    } else {
        $assignedTo = $arrAssignedTo;
    }
    foreach ($assignedTo as $value) {
        if (strpos($value, 'con') === false && strpos($value, 'cus') === false) {
            array_push($assignedtoidsAfter, $value);
        }
    }

    $j = 0;
    if (count($assignedtoidsAfter) == 0) {
        echo -3;
        exit();
    }
    $myDataObjectResourcesIDS = json_decode($_POST['resourcesID']);
    $myDataObjectResourcesNames = json_decode($_POST['resourcesName']);


    $i = 0;
    foreach ($myDataObjectResourcesIDS as $value) {
        if (is_numeric(array_search($value, $assignedtoidsAfter)) !== false) {
            $assignedtoidsAfter2[$j] = $value;
            array_push($assignedtonamesAfter, $myDataObjectResourcesNames[$i]);
            $j = $j + 1;
        }
        $i = $i + 1;
    }

    $assignedToids = implode(',', $assignedtoidsAfter2);//to convert it to string
  $assignedTonames = implode(',', $assignedtonamesAfter);//to convert it to string

  $todaydatetime = date("Y-m-d H:i:s"); //dateCreated

    $todayDate = date("Y-m-d");
    $startDate = new DateTime($todayDate); //current datetime
    $deadline = $startDate->modify('+'.$duration.' day') ->format('Y-m-d');
    $requester = $_SESSION['username'];

    $insertQuery = "INSERT INTO ".$table." (id, taskNo, taskName, description, assignedTo, dateCreated, deadline ,status, projectCode, requester, documents, assignedToNames)
VALUES (NULL,'".$taskNo."', '".$taskName."','".$description."', '".$assignedToids."','".$todaydatetime."','".$deadline."','".$status."','".$projectCode."','".$requester."','".$myFilesData."','".$assignedTonames."')";//0 in progress / 1 completed / 2 declined



    $result = mysqli_query($connect, $insertQuery);

    // $data['jsonString'] = $row[0];
    // $data['lastEditedBy'] = $row[1];
    // $data['dateCreated'] = $row[2];
    // $data['projectCode'] = $row[3];

    if ($result) {
        echo 0;
    } else {
        echo -1;
    }
}

if (!empty($_POST['action']) && $_POST['action'] == 'deleteMultibleRecords') {
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'projectTable') {
        $recordsTable = "projects";
        if (!empty($_POST['deleteItemsData'])) {
            $deletedItems = $_POST['deleteItemsData'];
            // these data to be user for delete all items
            $j = 0;
            foreach ($deletedItems as $deletedItem) {
                $deleteQuery = "DELETE FROM ".$recordsTable."
  WHERE projectCode ='".$deletedItem."'";
                if (mysqli_query($connect, $deleteQuery)) {
                    echo 0;
                } else {
                    echo -1; //error with your data
                }
                $j++;
            }
        } else {
            echo -2;
        }
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'uploadProjectDocuments') {
    include "session.php";
    $table = 'projectsdocuments';
    $projectCode = $_POST['projectCode'];
    $titleOfFile = $_POST['titleOfFile'];
    $datetime = date("Y-m-d H:i:s"); //dateCreated
    $currentusername = $_SESSION['username'];

    $myDocuments =  $_POST['allFilesNames'];
    if (!empty($myDocuments)) {
        $myFilesData =  $myDocuments;
    } else {
        $myFilesData = '';
    }
    $insertQuery = "INSERT INTO ".$table." (id, projectCode, sender, documents, dateCreated, title)
VALUES (NULL,'".$projectCode."','".$currentusername."', '".$myFilesData."','".$datetime."','".$titleOfFile."')";//0 in progress / 1 completed / 2 declined

    $result = mysqli_query($connect, $insertQuery);

    if ($result) {
        echo 0;
    } else {
        echo -1;
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'bringData') {
    $table = 'gantt';
    $idt = $_POST['element'];
    $array = array();

    $query = "SELECT  `gantJson`,`employeename` ,`dateCreated` , `projectCode` FROM ".$table." WHERE id=(SELECT MAX(id) FROM ".$table." WHERE projectCode = '".mysqli_real_escape_string($connect, $idt)."') "; // to take the first record of request process to insert it

    $result = mysqli_query($connect, $query);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);

    if (COUNT($row) > 0) {
        $data['jsonString'] = $row[0];
        $data['lastEditedBy'] = $row[1];
        $data['dateCreated'] = $row[2];
        $data['projectCode'] = $row[3];
    } else {
        $data['jsonString'] = '';
        $data['lastEditedBy'] = '';
        $data['dateCreated'] = '';
        $data['projectCode'] = '';
    }

    if ($result) {
        echo json_encode($data);
    } else {
        echo json_encode(1);
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'bringEmployees') {
    $empids = array();
    $empids2 = array();
    $empidsAfter = array();
    $empnames = array();
    $empnames2 = array();
    $table = 'projects';
    $projectCode = $_POST['element'];
    $query = "SELECT `teamemp` , `teamempname` ,`teamcontra` , `teamcontraname` FROM ".$table." WHERE projectCode = '".mysqli_real_escape_string($connect, $projectCode)."'"; // to take the first record of request process to insert it
    $result = mysqli_query($connect, $query);
    $rowsteam = mysqli_fetch_all($result, MYSQLI_NUM);
    foreach ($rowsteam as $rowteam) {
        array_push($empids, $rowteam[0]);
        array_push($empnames, $rowteam[1]);
        array_push($empids2, $rowteam[2]);
        array_push($empnames2, $rowteam[3]);
    }
    $empidsAfter = array();
    $empnamesAfter = array();
    $empidsAfter2 = array();
    $empnamesAfter2 = array();
    $str = implode('', $empids);
    $str2 = implode('', $empnames);
    $str3 = implode('', $empids2);
    $str4 = implode('', $empnames2);
    $commanamesids = explode(",", $str);
    ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
    foreach ($commanamesids as $singleValue) {
        array_push($empidsAfter, $singleValue);
    }
    $commanamesids2 = explode(",", $str3);
    ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
    foreach ($commanamesids2 as $singleValue) {
        array_push($empidsAfter2, $singleValue);
    }

    $commanames = explode(",", $str2);
    ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
    foreach ($commanames as $singleValue) {
        array_push($empnamesAfter, $singleValue);
    }
    $commanames2 = explode(",", $str4);
    ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
    foreach ($commanames2 as $singleValue) {
        array_push($empnamesAfter2, $singleValue);
    }


    if (count($empnames) > 0) {
        $json['ids'] = array_unique($empidsAfter);
        $json['names'] = array_unique($empnamesAfter);
        //  $json['ids2'] = $empids2After;
        //  $json['names2'] = $empnames2After;
        $j = 0;
        $t = 0;
        $finalArray1 = array();
        $finalArray2 = array();


        foreach ($empidsAfter as $sValue) {
            $final = array('id' => $sValue , 'name' => $empnamesAfter[$j]);
            array_push($finalArray1, $final);
            $j++;
        }
        foreach ($empidsAfter2 as $sValue) {
            $final = array('id' => $sValue , 'name' => $empnamesAfter2[$t]);
            array_push($finalArray2, $final);
            $t++;
        }


        $finalArray = array_merge($finalArray1, $finalArray2);

        //$uniqueArray = array_unique($finalArray);
        //var_dump(implode('',$uniqueArray));
        echo json_encode($finalArray);
    } else {
        echo json_encode(-1);
    }
}
if (!empty($_POST['action']) && $_POST['action'] == 'showProjects') {
    $table = 'projects';
    //ORDER BY dateCreated DESC
if (isset($_POST['element'])) { //if this index exit
  $sqlQuery = "SELECT * FROM ".$table."
WHERE projectCode = '".$_POST["element"][0]."'";
    $result = mysqli_query($connect, $sqlQuery);
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    if ($result) {
        recordSession($connect);
        echo json_encode($rows);
    }
} else {
    $sqlQuery = "SELECT * FROM ".$table.""; // to take the first record of request process to insert it
    $result = mysqli_query($connect, $sqlQuery);
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    if ($result) {
        recordSession($connect);
        echo json_encode($rows);
    }
}
}
if (!empty($_POST['action']) && $_POST['action'] == 'showAllRequests') {
    $table = 'projectrequests';
    //ORDER BY dateCreated DESC
if (isset($_POST['element'])) { //if this index exit
  $sqlQuery = "SELECT * FROM ".$table."
WHERE projectCode = '".$_POST["element"][0]."'";
    $result = mysqli_query($connect, $sqlQuery);
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    if ($result) {
        recordSession($connect);
        echo json_encode($rows);
    }
} else {
    $sqlQuery = "SELECT * FROM ".$table.""; // to take the first record of request process to insert it
    $result = mysqli_query($connect, $sqlQuery);
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    if ($result) {
        recordSession($connect);
        echo json_encode($rows);
    }
}
}
if (!empty($_POST['action']) && $_POST['action'] == 'showAllDocuments') {
    $table = 'projectsdocuments';
    //ORDER BY dateCreated DESC
if (isset($_POST['element'])) { //if this index exit
  $sqlQuery = "SELECT * FROM ".$table."
WHERE projectCode = '".$_POST["element"][0]."'";
    $result = mysqli_query($connect, $sqlQuery);
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    if ($result) {
        recordSession($connect);
        echo json_encode($rows);
    }
} else {
    $sqlQuery = "SELECT * FROM ".$table.""; // to take the first record of request process to insert it
    $result = mysqli_query($connect, $sqlQuery);
    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    if ($result) {
        recordSession($connect);
        echo json_encode($rows);
    }
}
}
if (!empty($_POST['action']) && $_POST['action'] == 'saveMultibleRecords') {
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == "projectTable") {
        $recordsTable = "projects";

        $datetime = date("Y-m-d H:i:s"); //dateCreated
        if (!empty($_POST['addRecordsData'])) {
            $rowsaddRecords = $_POST['addRecordsData'];
            foreach ($rowsaddRecords[0] as $rowaddRecords) {
                $insertQuery = "INSERT INTO ".$recordsTable." (projectCode, pname, leadby, teamemp ,teamcontra,progress,customers,tasksAcheived,documents,dateCreated,teamempname,teamcontraname , customername,leadbyName)
                VALUES ('".$rowaddRecords[1]."','".$rowaddRecords[2]."','".$rowaddRecords[10]."','".$rowaddRecords[4]."','".$rowaddRecords[6]."','0','".$rowaddRecords[8]."','','','$datetime','".$rowaddRecords[5]."','".$rowaddRecords[7]."','".$rowaddRecords[9]."','".$rowaddRecords[11]."')";
                if (mysqli_query($connect, $insertQuery)) {
                    echo 0;
                } else {
                    echo -2; //error with your data
                }
            }
        } else {
            echo -1;
        }
    }
}


if (!empty($_POST['action']) && $_POST['action'] == 'DeleteOneItem') {
    if ($_POST["deletedItem"]) {
        if ($_POST["tableNameData"] == "projectTable") {
            $recordsTable = "projects";
            $deleteQuery = "DELETE FROM ".$recordsTable."
      WHERE projectCode ='".$_POST["deletedItem"]."'";
            if (mysqli_query($connect, $deleteQuery)) {
                echo 0;
                exit;
            } else {
                echo -1;
            }
        }
    }
}

if (!empty($_POST['action']) && $_POST['action'] == 'editRecords') {
    if (!empty($_POST['tableNameData']) && $_POST['tableNameData'] == 'projectTable') {
        $recordsTable = "projects";
        if (!empty($_POST['editedRecordsData'])) {
            $rowseditRecords = $_POST['editedRecordsData'];
            $editedItems = $_POST['editedItemsData'];
            // these data to be user for delete all items

            $updateQuery = "UPDATE ".$recordsTable." SET pname = '".$rowseditRecords[0][1]."', teamemp = '".$rowseditRecords[0][4]."' , teamempname = '".$rowseditRecords[0][5]."' , teamcontra = '".$rowseditRecords[0][6]."' , teamcontraname = '".$rowseditRecords[0][7]."'  ,  customers = '".$rowseditRecords[0][8]."' , customername = '".$rowseditRecords[0][9]."' , leadby = '".$rowseditRecords[0][10]."', leadbyName = '".$rowseditRecords[0][11]."'
      WHERE projectCode ='".$editedItems[0]."'";
            if (mysqli_query($connect, $updateQuery)) {
                echo json_encode(0);
            } else {
                echo -2;
            }
        } else {
            echo -1;
        }
    }
}
