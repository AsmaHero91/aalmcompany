<?php

include "connect.php";


function closeSession()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    session_unset();
    session_destroy();

    //echo"you have successfully logout. Click here to <a herf='login.html'>login again</a>";
}

if (!empty($_POST['action']) && $_POST['action'] == 'validateSession') {
    validateSession();
}
if (!empty($_POST['action']) && $_POST['action'] == 'logout') {
    closeSession();
}

  if (!empty($_POST['action']) && $_POST['action'] == 'AddCustomActions') {
      include "session.php";
      $table = 'requestprocesses';
      $table2 = 'requests';
      $table3 = 'requesthistorylog';
      $myDocuments =  $_POST['allFilesNames'];
      if (!empty($myDocuments)) {
          $myFilesData =  $myDocuments;
      } else {
          $myFilesData = '';
      }
      $reqid = intval($_POST["reqidg"]);
      $currentUserId = $_SESSION["employeeid"];
      // select all log order to get the max only .
    $datetime = date("Y-m-d H:i:s"); //dateCreated
    if (!isset($_POST["idg"])) {
        $updateQueryM = "UPDATE ".$table3." SET isActionTaken = '1' WHERE id = '".mysqli_real_escape_string($connect, $_POST["idg"])."'"; // to take the first record of request process to insert it
    } else {
        $maxQuery = "SELECT MAX(id) AS maximum FROM ".$table3." WHERE reqid = '$reqid'"; // to take the first record of request process to insert it
        $resultMax = mysqli_query($connect, $maxQuery);
        $rowmax = mysqli_fetch_array($resultMax, MYSQLI_NUM);
        $max = intval($rowmax[0]);
        $updateQueryM = "UPDATE ".$table3." SET isActionTaken = '1' WHERE id = '$max'"; // to take the first record of request process to insert it
    }
      if ($_POST["checkedValue"] === '1') { //fot complete
          $todayDate = date("Y-m-d");

          $insertQuery = "INSERT INTO ".$table3." (id, reqid, requestTCode, actionRequired, documents, deadline ,dateCreated, status, notes, empid, assignedTo, isActionTaken, logType, readen)
    VALUES (NULL,'$reqid', '".$_POST["selectRequestg"]."','".$_POST["actionRequiredm"]."', '".$myFilesData."','".$todayDate."','".$datetime."','completed','".$_POST["description"]."','$currentUserId','".$_POST["selectionsEmployees"]."','1','Custom','0')";//0 in progress / 1 completed / 2 declined
    $updateQuery = "UPDATE ".$table2." SET status=1 WHERE reqid = '$reqid'"; // to take the first record of request process to insert it
    mysqli_query($connect, $updateQuery);
      } else {
          //to calculate deadline
          $todayDate = date("Y-m-d");
          //   $durationIndays = $valuesToInsert[5];
$startDate = new DateTime($todayDate); //current datetime

                //to do specify strat time
          $deadline = $startDate->modify('+'.$_POST["duration"].' day') ->format('Y-m-d');
          //insert in progress
                $insertQuery = "INSERT INTO ".$table3." (id, reqid, requestTCode, actionRequired, documents, deadline ,dateCreated, status, notes, empid, assignedTo, isActionTaken, logType, readen)
                VALUES (NULL,'$reqid', '".$_POST["selectRequestg"]."', '".$_POST["actionRequiredm"]."', '".$myFilesData."','".$deadline."','".$datetime."','in progress','".$_POST["description"]."','$currentUserId','".$_POST["selectionsEmployees"]."','0','Custom','0')"; //0 in progress / 1 completed / 2 declined
      }

      if (mysqli_query($connect, $insertQuery) && mysqli_query($connect, $updateQueryM)) {
          echo 0; //success (Your request has been succcssfullly added)
      } else {
          echo -1;// (error/ please check your data or internet connection)
      }
  }

if (!empty($_POST['action']) && $_POST['action'] == 'DeclineRequest') {
    include "session.php";
    $table2 = 'requests';
    $table3 = 'requesthistorylog';

    $todayDate = date("Y-m-d");
    $datetime = date("Y-m-d H:i:s"); //dateCreated

    $insertQuery = "INSERT INTO ".$table3." (id, reqid, requestTCode, actionRequired, documents, deadline ,dateCreated, status, notes, empid, assignedTo, isActionTaken, logType, readen)
  VALUES (NULL,'".$_POST["reqidg"]."', '".$_POST["selectRequestg"]."','', '','".$todayDate."','".$datetime."','Declined','".$_POST["description"]."','".$_SESSION["employeeid"]."','".$_POST["assignedToToInsert"]."','1','Automatic','0')";//0 in progress / 1 completed / 2 declined
  $updateQuery = "UPDATE ".$table2." SET status=3 WHERE reqid = '".mysqli_real_escape_string($connect, $_POST["reqidg"])."'"; // to take the first record of request process to insert it
  mysqli_query($connect, $updateQuery);

    $updateQueryM = "UPDATE ".$table3." SET isActionTaken=1 WHERE id = '".mysqli_real_escape_string($connect, $_POST["idg"])."'"; // to take the first record of request process to insert it

    if (mysqli_query($connect, $insertQuery) && mysqli_query($connect, $updateQueryM)) {
        echo 0; //success (Your request has been succcssfullly added)
    } else {
        echo -1;// (error/ please check your data or internet connection)
    }
}
  if (!empty($_POST['action']) && $_POST['action'] == 'AcceptRequest') {
      include "session.php";
      $table = 'requestprocesses';
      $table2 = 'requests';
      $table3 = 'requesthistorylog';
      $myDocuments =  $_POST['allFilesNames'];
      if (!empty($myDocuments)) {
          $myFilesData =  $myDocuments;
      } else {
          $myFilesData = '';
      }
      $queryToCountPrimary = "SELECT `logType` FROM ".$table3." WHERE reqid = '".mysqli_real_escape_string($connect, $_POST["reqidg"])."' AND logType='Automatic'"; // to take the first record of request process to insert it
      $result = mysqli_query($connect, $queryToCountPrimary);
      $pOrderToselectForInsert = mysqli_num_rows($result) + 1;

      $query = "SELECT * FROM ".$table." WHERE requestTCode = '".mysqli_real_escape_string($connect, $_POST['selectRequestg'])."' AND poderNo=$pOrderToselectForInsert"; // to take the first record of request process to insert it

      $result = mysqli_query($connect, $query);
      $row = mysqli_fetch_array($result, MYSQLI_NUM);
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      //to calculate deadline
      $todayDate = date("Y-m-d");
      //   $durationIndays = $valuesToInsert[5];
              $startDate = new DateTime($todayDate); //current datetime


                if ($_POST["checkedValue"] === '1') {
                    $insertQuery = "INSERT INTO ".$table3." (id, reqid, requestTCode,actionRequired, documents, deadline ,dateCreated, status, notes, empid, assignedTo, isActionTaken, logType, readen)
              VALUES (NULL,'".$_POST["reqidg"]."', '".$_POST["selectRequestg"]."','', '".$myFilesData."','".$todayDate."','".$datetime."','completed','".$_POST["description"]."','".$_SESSION["employeeid"]."','".$_POST["assignedToToInsert"]."','1','Automatic','0')";//0 in progress / 1 completed / 2 declined
              $updateQuery = "UPDATE ".$table2." SET status=2 WHERE reqid = '".mysqli_real_escape_string($connect, $_POST["reqidg"])."'"; // to take the first record of request process to insert it
              mysqli_query($connect, $updateQuery);
                } else {
                    if (mysqli_num_rows($result) > 0) { //for in progress
                        $deadline = $startDate->modify('+'.$row[5].' day') ->format('Y-m-d');
                        //insert in progress
                          $insertQuery = "INSERT INTO ".$table3." (id, reqid, requestTCode, actionRequired, documents, deadline ,dateCreated, status, notes, empid, assignedTo, isActionTaken, logType, readen)
                          VALUES (NULL,'".$_POST["reqidg"]."', '".$_POST["selectRequestg"]."' ,'".$row[4]."', '".$myFilesData."','".$deadline."','".$datetime."','in progress','".$_POST["description"]."','".$_SESSION["employeeid"]."','".$row[3]."','0','Automatic','0')"; //0 in progress / 1 completed / 2 declined
                    } else {
                        $todayDate = date("Y-m-d");

                        //insert completed
                          $insertQuery = "INSERT INTO ".$table3." (id, reqid, requestTCode, actionRequired, documents, deadline ,dateCreated, status, notes, empid, assignedTo, isActionTaken, logType, readen)
                          VALUES (NULL,'".$_POST["reqidg"]."', '".$_POST["selectRequestg"]."','', '".$myFilesData."','".$todayDate."','".$datetime."','completed','".$_POST["description"]."','".$_SESSION["employeeid"]."','".$_POST["assignedToToInsert"]."','1','Automatic','0')";//0 in progress / 1 completed / 2 declined
                          $updateQuery = "UPDATE ".$table2." SET status=2 WHERE reqid = '".mysqli_real_escape_string($connect, $_POST["reqidg"])."'"; // to take the first record of request process to insert it
                          mysqli_query($connect, $updateQuery);
                    }
                }

      $updateQueryM = "UPDATE ".$table3." SET isActionTaken=1 WHERE id = '".mysqli_real_escape_string($connect, $_POST["idg"])."'"; // to take the first record of request process to insert it

      if (mysqli_query($connect, $insertQuery) && mysqli_query($connect, $updateQueryM)) {
          echo 0; //success (Your request has been succcssfullly added)
      } else {
          echo -1;// (error/ please check your data or internet connection)
      }
  }


                                       if (!empty($_POST['action']) && $_POST['action'] == 'passSelectedreqtypeProcess') {
                                           $empids = array();
                                           $empnames = array();
                                           if ($_POST["element"]) {
                                               $element = $_POST["element"];
                                               // $query=$connect->prepare("select * from requesthistorylog where reqid= 2 order by requesthistorylog.dateCreated desc");
                                               $table = "requestprocesses";
                                               //ORDER BY dateCreated DESC
                                     $sqlQuery = "SELECT * FROM ".$table." WHERE requestTCode = '".mysqli_real_escape_string($connect, $_POST['element'])."' ORDER BY `requestprocesses`.`poderNo` ASC"; // to take the first record of request process to insert it
                                             $result = mysqli_query($connect, $sqlQuery);
                                               $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

                                               $assignedtoQuery = "SELECT `assignedTo` FROM ".$table." WHERE requestTCode = '".mysqli_real_escape_string($connect, $_POST['element'])."' ORDER BY `requestprocesses`.`poderNo` ASC";
                                               $table2 = "employees";
                                               $resultassignedto = mysqli_query($connect, $assignedtoQuery);
                                               $rowsassignedto = mysqli_fetch_all($resultassignedto, MYSQLI_ASSOC);
                                               foreach ($rowsassignedto as $rowassignedto) {
                                                   array_push($empids, $rowassignedto);
                                               }
                                               foreach ($empids as $empid) {
                                                   $commanamesAfterhandeled = array();
                                                   $str = implode('', $empid);
                                                   if (strpos($str, ',') !== false) { //to check if  contains this character
                                                       $commanames = explode(",", $str);

                                                       ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
                                                       foreach ($commanames as $singleValue) {
                                                           $empnamecommassquery = "SELECT `name` FROM ".$table2." WHERE id = '".$singleValue."'";
                                                           $resultempcommanames = mysqli_query($connect, $empnamecommassquery);
                                                           $rowempname = $resultempcommanames->fetch_row();
                                                           array_push($commanamesAfterhandeled, $rowempname[0]);
                                                       }
                                                       $str2 = implode(",", $commanamesAfterhandeled);
                                                       ///////////////////////////////////////////////////////
                                           array_push($empnames, $str2); //the value taken from the function above
                                                   } else {
                                                       $empnamesquery = "SELECT `name` FROM ".$table2." WHERE id = '".$str."'";
                                                       $resultempnames = mysqli_query($connect, $empnamesquery);
                                                       $rowempname = $resultempnames->fetch_row();
                                                       array_push($empnames, $rowempname[0]);
                                                   }
                                               }

                                               $json['data1'] = $rows;
                                               $json['data2'] = $empnames;


                                               echo json_encode($json);
                                           }
                                       }


                                     if (!empty($_POST['action']) && $_POST['action'] == 'showProcessesTable') {
                                         $empids = array();
                                         $empnames = array();
                                         if ($_POST["element"][0]) {
                                             $element = mysqli_real_escape_string($connect, $_POST['element'][0]);
                                             // $query=$connect->prepare("select * from requesthistorylog where reqid= 2 order by requesthistorylog.dateCreated desc");
                                             $table = "requestprocesses";
                                             //ORDER BY dateCreated DESC
                                             $sqlQuery = "SELECT r.*, CASE WHEN r.requestTCode = ty.requestTCode
                                                      THEN ty.name
                                                    END as requestTypeName
                                     FROM requestprocesses as r
                                     LEFT JOIN requesttype as ty ON ty.requestTCode=r.requestTCode WHERE r.requestTCode='$element'
                                     ORDER BY r.poderNo ASC";


                                             $result = mysqli_query($connect, $sqlQuery);
                                             $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
                                             $assignedtoQuery = "SELECT `assignedTo` FROM ".$table." WHERE requestTCode = '".mysqli_real_escape_string($connect, $_POST['element'][0])."' ORDER BY `requestprocesses`.`poderNo` ASC";
                                             $table2 = "employees";
                                             $resultassignedto = mysqli_query($connect, $assignedtoQuery);
                                             $rowsassignedto = mysqli_fetch_all($resultassignedto, MYSQLI_ASSOC);
                                             foreach ($rowsassignedto as $rowassignedto) {
                                                 array_push($empids, $rowassignedto);
                                             }
                                             foreach ($empids as $empid) {
                                                 $commanamesAfterhandeled = array();
                                                 $str = implode('', $empid);
                                                 if (strpos($str, ',') !== false) { //to check if  contains this character
                                                     $commanames = explode(",", $str);

                                                     ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
                                                     foreach ($commanames as $singleValue) {
                                                         $empnamecommassquery = "SELECT `name` FROM ".$table2." WHERE id = '".$singleValue."'";
                                                         $conn = $connect;
                                                         $resultempcommanames = mysqli_query($conn, $empnamecommassquery);
                                                         $rowempname = $resultempcommanames->fetch_row();
                                                         array_push($commanamesAfterhandeled, $rowempname[0]);
                                                     }
                                                     $str2 = implode(",", $commanamesAfterhandeled);
                                                     ///////////////////////////////////////////////////////
                                         array_push($empnames, $str2); //the value taken from the function above
                                                 } else {
                                                     $empnamesquery = "SELECT `name` FROM ".$table2." WHERE id = '".$str."'";
                                                     $resultempnames = mysqli_query($connect, $empnamesquery);
                                                     $rowempname = $resultempnames->fetch_row();
                                                     array_push($empnames, $rowempname[0]);
                                                 }
                                             }

                                             $json['data1'] = $rows;
                                             $json['data2'] = $empnames;
                                             $json['data3'] = $empids; //forempids 1076609104,1076609103


                                             echo json_encode($json);
                                         }
                                     }
  if (!empty($_POST['action']) && $_POST['action'] == 'passSelectedreqid') {
      if ($_POST["element"]) {
          $element = $_POST["element"][0];
          $int = (int)$element;
          // $query=$connect->prepare("select * from requesthistorylog where reqid= 2 order by requesthistorylog.dateCreated desc");
          $table = "requesthistorylog";

          $sqlQuery = "SELECT r.*, CASE WHEN r.empid = e1.id
                     THEN e1.name
                   END as employeeName
    FROM requesthistorylog as r
    LEFT JOIN employees as e1 ON r.empid=e1.id
    WHERE (reqid = '$element') ORDER BY r.dateCreated DESC";


          $result = mysqli_query($connect, $sqlQuery);
          $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
          $empids = array();
          $empnames = array();
          $empids2 = array();
          $empnames2 = array();
          $assignedtoQuery = "SELECT `assignedTo` FROM ".$table." WHERE reqid = $element ORDER BY dateCreated DESC";
          $table2 = "employees";
          $resultassignedto = mysqli_query($connect, $assignedtoQuery);
          $rowsassignedto = mysqli_fetch_all($resultassignedto, MYSQLI_ASSOC);
          foreach ($rowsassignedto as $rowassignedto) {
              array_push($empids, $rowassignedto);
          }
          foreach ($empids as $empid) {
              $commanamesAfterhandeled = array();
              $str = implode('', $empid);
              if (strpos($str, ',') !== false) { //to check if  contains this character
                  $commanames = explode(",", $str);

                  ///handle comma xml_set_end_namespace_decl_handler  $commanamesAfterhandeled = array();
                  foreach ($commanames as $singleValue) {
                      $empnamecommassquery = "SELECT `name` FROM ".$table2." WHERE id = '".$singleValue."'";
                      $resultempcommanames = mysqli_query($connect, $empnamecommassquery);
                      $rowempname = $resultempcommanames->fetch_row();
                      array_push($commanamesAfterhandeled, $rowempname[0]);
                  }
                  $str2 = implode(",", $commanamesAfterhandeled);
                  ///////////////////////////////////////////////////////
      array_push($empnames, $str2); //the value taken from the function above
              } else {
                  $empnamesquery = "SELECT `name` FROM ".$table2." WHERE id = '".$str."'";
                  $resultempnames = mysqli_query($connect, $empnamesquery);
                  $rowempname = $resultempnames->fetch_row();
                  array_push($empnames, $rowempname[0]);
              }
          }
          $myminuplatedassignedto =  json_encode($empnames);


          $json['data1']= $rows;
          $json['data2']= $empnames;
          echo json_encode($json);
      }
  }

if (!empty($_POST['action']) && $_POST['action'] == 'getEmployeeDetails') {
    $table = "employees";
    $query = "SELECT * FROM employees e
  LEFT JOIN contacts c on e.id = c.id LEFT JOIN eprogress p on e.id = p.id LEFT JOIN users u on e.id = u.employeeid LEFT JOIN ejobinfo j on e.id = j.empid WHERE e.id = ".$_POST['idt']."";

    $result = mysqli_query($connect, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    echo json_encode($row);
}



  if (!empty($_POST['actionm']) && $_POST['actionm'] == 'addARequest') {
      include "session.php";
      $table = 'requestprocesses';
      $table2 = 'requests';
      $table3 = 'requesthistorylog';
      $myDocuments =  $_POST['allFilesNames'];
      if (!empty($myDocuments)) {
          $myFilesData =  $myDocuments;
      } else {
          $myFilesData = '';
      }
      $query = "SELECT * FROM ".$table." WHERE requestTCode = '".mysqli_real_escape_string($connect, $_POST['selectRequest'])."' AND poderNo=1"; // to take the first record of request process to insert it


      $result = mysqli_query($connect, $query);
      $row = mysqli_fetch_array($result, MYSQLI_NUM);
      // $days = $result->fetch_object()->durationInDays;


      $datetime = date("Y-m-d H:i:s"); //dateCreated
      //to calculate deadline
      $maxQuery = "SELECT MAX(reqid) AS maximum FROM ".$table2.""; // to take the first record of request process to insert it
      $resultMax = mysqli_query($connect, $maxQuery);
      $rowmax = mysqli_fetch_array($resultMax, MYSQLI_NUM);
      $max = intval($rowmax[0]) + 1;

      if (mysqli_num_rows($result) > 0) {
          $todayDate = date("Y-m-d");
          //   $durationIndays = $valuesToInsert[5];
      $startDate = new DateTime($todayDate); //current datetime

      $deadline = $startDate->modify('+'.$row[5].' day') ->format('Y-m-d');

          $currentuserId = $_SESSION['employeeid'];
          $insertQuery = "INSERT INTO ".$table2." (reqid, requestTCode, empid, documents, dataCreated, description, status)
                                   VALUES ('$max','".$_POST["selectRequest"]."','$currentuserId','".$myFilesData."','".$datetime."' ,'".$_POST["description"]."','0')"; //0 in progress / 1 completed / 2 declined
                                   $insertQuery2 = "INSERT INTO ".$table3." (id, reqid, requestTCode, actionRequired, documents, deadline ,dateCreated, status, notes, empid, assignedTo, isActionTaken, logType, readen)
                                   VALUES (NULL,'$max', '".$_POST["selectRequest"]."','".$row[4]."', '".$myFilesData."','".$deadline."','".$datetime."','in progress','".$_POST["description"]."','".$_SESSION["employeeid"]."','".$row[3]."','0','Automatic','0')";//0 in progress / 1 completed / 2 declined

                                 if (mysqli_query($connect, $insertQuery) && mysqli_query($connect, $insertQuery2)) {
                                     echo 0; //success (Your request has been succcssfullly added)
                                 } else {
                                     // echo -1;
                                         echo -1;// (error/ please check your data or internet connection)
                                 }
      } else {
          echo -2; //this requests not initalized yet ask the top manager to add its process
      }
  }

if (!empty($_POST['action']) && $_POST['action'] == 'getRecordRequest') {
    $table = "requesthistorylog";
    if ($_POST["idt"]) {
        $sqlQuery = "
				SELECT * FROM ".$table."
				WHERE id = '".mysqli_real_escape_string($connect, $_POST["idt"])."'";
        $result = mysqli_query($connect, $sqlQuery);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        echo json_encode($row);
    }
}
