<?php

include "connect.php";

  if (!empty($_POST['action']) && $_POST['action'] == 'fillTemplates') {
      $query= "select * from smstemplates";
      $result = mysqli_query($connect, $query);
      $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
      if ($result) {
          echo json_encode($rows);
      } else {
          echo json_encode(-1);
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'showAllSMS') {
      $querys= "select * from smstemplates";
      $results = mysqli_query($connect, $querys);
      $rowss = mysqli_fetch_all($results, MYSQLI_ASSOC);
      if ($results) {
          echo json_encode($rowss);
      } else {
          echo json_encode(-1);
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'editSms') {
      $id = $_POST['id'];
      $title = $_POST['title'];
      $message = $_POST['template'];
      $query= "UPDATE smstemplates SET smsType = '$title', smsMessage = '$message' WHERE id = '$id'";
      $result = mysqli_query($connect, $query);
      if ($result) {
          echo json_encode(0);
      } else {
          echo json_encode(-1);
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'deleteSms') {
      $id = $_POST['id'];
      $query= "DELETE FROM smstemplates WHERE id = '$id'";
      $result = mysqli_query($connect, $query);
      if ($result) {
          echo json_encode(0);
      } else {
          echo json_encode(-1);
      }
  }

  if (!empty($_POST['action']) && $_POST['action'] == 'addSmsTemplate') {
      $smstype = $_POST['title'];
      $smsmessage = $_POST['smstemplates'];
      $table = 'smstemplates';
      $insertquery= "INSERT INTO ".$table." (id, smsType, smsMessage)
       VALUES (NULL,'".$_POST["title"]."','".$_POST["smstemplates"]."')";
      $result = mysqli_query($connect, $insertquery);
      if ($result) {
          echo json_encode(0);
      } else {
          echo json_encode(-1);
      }
  }

  if (!empty($_POST['action']) && $_POST['action'] == 'getPassedIDmobile1No') {
      $element = base64_decode($_POST['element']);
      if (strpos('con', $element) == false) {
          $query = "SELECT mobile1 FROM customers WHERE customerid = '$element'";
      } elseif (strpos('cus', $element) == false) {
          $query = "SELECT mobile1 FROM customers WHERE customerid = '$element'";
      }

      $queryid = "SELECT mobile1 FROM contacts WHERE id = '$element'";
      $resultid = mysqli_query($connect, $queryid);
      $rowsid = mysqli_fetch_all($resultid, MYSQLI_ASSOC);
      if ($resultid && COUNT($rowsid) > 0) {
          echo json_encode($rowsid);
          exit();
      } else {
          $result = mysqli_query($connect, $query);
          $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
          if ($result) {
              echo json_encode($rows);
          } else {
              echo json_encode(-1);
          }
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'fillMobilesAndMessages') {
      include "db.php";
      if ($_POST["idt"]) {
          $recordsTable = "smstemplates";
          //take the value of selected value related to select tag with name (select2)
          $sqlQuery = "
       SELECT * FROM ".$recordsTable."
       WHERE id = '".$_POST["idt"]."'";
          $result = mysqli_query($connect, $sqlQuery);
          $rows1 = mysqli_fetch_array($result, MYSQLI_ASSOC);
      } else {
          $rows1 = '';
      }
      $allMobiles = array();

      if (isset($_POST["elements"])) {
          $values = $_POST['elements'];
          foreach ($values as $val) {
              if ($val == "customers") {
                  $query=$conn->prepare("select mobile1 from customers where customerid like 'cus%'");
                  $query->setFetchMode(PDO::FETCH_OBJ);
                  $query->execute();
                  while ($row=$query->fetch()) {
                      if ($row->mobile1 != '') {
                          array_push($allMobiles, $row->mobile1);
                      }
                  }
              }
              if ($val == "employees") {
                  $query2=$conn->prepare("select mobile1 from contacts");
                  $query2->setFetchMode(PDO::FETCH_OBJ);
                  $query2->execute();
                  while ($row=$query2->fetch()) {
                      if ($row->mobile1 != '') {
                          array_push($allMobiles, $row->mobile1);
                      }
                  }
              }

              if ($val == "contractors") {
                  $query3=$conn->prepare("select mobile1 from customers where customerid like 'con%'");
                  $query3->setFetchMode(PDO::FETCH_OBJ);
                  $query3->execute();
                  while ($row=$query3->fetch()) {
                      if ($row->mobile1 != '') {
                          array_push($allMobiles, $row->mobile1);
                      }
                  }
              }
          }
      }
      if (COUNT($allMobiles) > 1) {
          $json['allMobiles'] = implode(',', $allMobiles);
      } else {
          $json['allMobiles'] = implode('', $allMobiles);
      }
      $json['smsMessage'] = $rows1;

      echo json_encode($json);
  }
