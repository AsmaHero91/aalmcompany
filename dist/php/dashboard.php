<?php

include "connect.php";

  function recordSession($connect)
  {
      include "session.php";
      $recordsTable = "session";
      $datetime = date("Y-m-d H:i:s"); //dateCreated
      $insertquery = "INSERT INTO ".$recordsTable." (sessionid, empid, activityName, status, timeActivity)
  VALUES (NULL,'".$_SESSION['employeeid']."','read','online','".$datetime."')";
      mysqli_query($connect, $insertquery);
  }
  recordSession($connect);

  if (!empty($_POST['action']) && $_POST['action'] == 'loadLogsData') {
      include "session.php";
      $currentUserId = $_SESSION['employeeid'];
      $selectQuery1 = "SELECT * FROM bonusdeductpt
      WHERE idcard = '$currentUserId' AND type = 'bonus'
      ORDER BY issuedDate DESC";
      $selectQuery2 = "SELECT * FROM bonusdeductpt
      WHERE idcard = '$currentUserId' AND type = 'deduct'
      ORDER BY issuedDate DESC";
      $selectQuery3 = "SELECT * FROM bonusdeductpt
      WHERE idcard = '$currentUserId' AND type = 'appraisals'
      ORDER BY issuedDate DESC";
      $result1 = mysqli_query($connect, $selectQuery1);
      $result2 = mysqli_query($connect, $selectQuery2);
      $result3 = mysqli_query($connect, $selectQuery3);
      if ($result1 && $result2 && $result3) {
          $rows1 = mysqli_fetch_all($result1, MYSQLI_ASSOC);
          $rows2 = mysqli_fetch_all($result2, MYSQLI_ASSOC);
          $rows3 = mysqli_fetch_all($result3, MYSQLI_ASSOC);
          $json['bonus'] = $rows1;
          $json['deduct'] = $rows2;
          $json['appraisals'] = $rows3;
          echo json_encode($json);
          exit();
      } else {
          echo json_encode('هناك مشكلة بالبيانات ارجو التأكد من سلامتها');
          exit();
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'ShowemployeeInfo') {
      $recordsTable = "ejobinfo";
      include "session.php";
      $currentUserId = $_SESSION['employeeid'];
      $datetime = date("Y-m-d"); //dateCreated
      $Query1 = "SELECT * FROM ".$recordsTable." WHERE empid = '$currentUserId'";
      $result1 = mysqli_query($connect, $Query1);
      if ($result1) {
          $rows1 = mysqli_fetch_all($result1, MYSQLI_ASSOC);
          echo json_encode($rows1); //error with your data
      } else {
          echo json_encode(-1); //error with your data
      }
  }
  if (!empty($_POST['action']) && $_POST['action'] == 'requestsCounter') {
      $recordsTable = "requesthistorylog";
      include "session.php";
      $datetime = date("Y-m-d"); //dateCreated
      $Query1 = "SELECT * FROM ".$recordsTable." WHERE status='in progress' AND assignedTo LIKE '%".$_SESSION['employeeid']."%' AND isActionTaken = '0' AND dateCreated LIKE '".$datetime."%'";
      $Query2 = "SELECT * FROM ".$recordsTable." WHERE empid = ".$_SESSION['employeeid']." AND isActionTaken = '1' AND dateCreated LIKE '".$datetime."%'";
      $Query3 = "SELECT COUNT(id) AS requestsCounter FROM ".$recordsTable." WHERE empid = ".$_SESSION['employeeid']." AND isActionTaken = '1'";
      $Query4 = "SELECT COUNT(id) AS projectsCounter FROM projectrequests WHERE assignedTo = ".$_SESSION['employeeid']." AND (status = 'STATUS_DONE' OR status = 'STATUS_FAILED')";

      $result1 = mysqli_query($connect, $Query1);
      $result2 = mysqli_query($connect, $Query2);
      $result3 = mysqli_query($connect, $Query3);
      $result4 = mysqli_query($connect, $Query4);

      if ($result1 && $result2 && $result3) {
          $rows1 = mysqli_fetch_all($result1, MYSQLI_ASSOC);
          $rows2 = mysqli_fetch_all($result2, MYSQLI_ASSOC);
          $rows3 = mysqli_fetch_all($result3, MYSQLI_ASSOC);
          $rows4 = mysqli_fetch_all($result4, MYSQLI_ASSOC);

          $json['new'] = COUNT($rows1);

          $json['handeled'] = COUNT($rows2);
          if (COUNT($rows1) == 0) {
              $json['precentage'] = 0;
          } else {
              $json['precentage'] = (COUNT($rows2) / COUNT($rows1)) * 100;
          }
          $json['All'] = intval($rows3[0]['requestsCounter']) + intval($rows4[0]['projectsCounter']);

          echo json_encode($json);
      } else {
          echo json_encode(-1); //error with your data
      }
  }


  if (!empty($_POST['action']) && $_POST['action'] == 'loadSalaryLogsData') {
      include "session.php";
      $currentUserId = $_SESSION['employeeid'];
      $selectQuery1 = "SELECT * FROM epaymentslogs WHERE empid = '$currentUserId' ORDER BY issuedDate DESC";
      $result1 = mysqli_query($connect, $selectQuery1);
      if ($result1) {
          $rows1 = mysqli_fetch_all($result1, MYSQLI_ASSOC);
          echo json_encode($rows1);
      } else {
          echo json_encode('هناك مشكلة بالبيانات ارجو التأكد من سلامتها');
      }
  }
