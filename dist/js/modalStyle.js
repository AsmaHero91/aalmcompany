$(document).ready(function() {

    /* Centering the modal vertically */
    function alignModal() {
        var modalDialog = $(this).find(".modal-dialog");
        modalDialog.css("margin-top", Math.max(0,
        ($(window).height() - modalDialog.height()) / 12));
        modalDialog.css("margin-left", Math.max(0,
        ($(window).width() - modalDialog.width()) / 2));
    }
    $(".modal.custom").on("shown.bs.modal", alignModal);

    /* Resizing the modal according the screen size */
    $(window).on("resize", function() {
        $(".modal:visible").each(alignModal);
    });
});
