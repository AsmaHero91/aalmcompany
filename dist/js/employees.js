


$(document).on('click','button.copyButton', function(event){
  console.log('is clicked copy');
  //  const link = document.querySelector('a.sharelink');

    var allLinks = document.querySelector('p.sharelinks');
    const range = document.createRange();
    range.selectNode(allLinks);
    const selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    const successful = document.execCommand('copy');

});
function showAllPermissions(){

  var action = 'showAllPermissions';
  $.ajax({
    url: '../dist/php/employees.php',
    type: 'POST',
    data: {
      action: action,
    },
    dataType: "json",
    success: function(data) {
      var myData = data;
      var t = defineTable("#permissionTable");
t.clear().draw();
      if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
        var count = 0;
        myData.forEach(function(rowd) {
          count = count + 1;
          var row = t.row.add([rowd.levelid,  rowd.UserGCategory , '']).draw().node();
          $(row).find('td:eq(2)').html('<button data-id1="' + count + '" data-id2="' + rowd.levelid + '" data-id3="' + rowd.UserGCategory + '" data-id3="permissionTable" class="EDITPermissions btn btn-warning btn-sm">Edit</button>');
return row;
      });
      }
    }
  });

}
showAllPermissions();
function showVacations(){

  var action = 'showAllVacations';
  $.ajax({
    url: '../dist/php/employees.php',
    type: 'POST',
    data: {
      action: action,
    },
    dataType: "json",
    success: function(data) {
      var myData = data;
      var t = defineTable("#vacationsTable");
t.clear().draw();
      if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
        var count = 0;
        let rdata = myData.map(function (o) {
                  //o.priority = "asma";
                  switch (o.status) {
                  case '0':
                      o.status = 'جاري الانتظار';
                      break;
                      case '1':
                          o.status = 'مكتمل';
                          break;
                  case '2':
                      o.status = 'مرفوض';
                      break;

                  }
                  return o;
              });

        rdata.forEach(function(rowd) {
          var arrayd = [];
          var arrayhtml = [];
          var value = rowd.documents
          var valueString = value.toString();

          var dlink = '../upload/vacations/';
          strx = valueString.split(',');
          arrayd = arrayd.concat(strx);
          //formattedDocuments.split(',');
          for (element of arrayd) {
            var el = element.trim();
            var fairel = el.substring(el.lastIndexOf('+') + 1);
            arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + fairel.trim() + '</a>');
          }
          var arraydString = arrayhtml;
          count = count + 1;
          var row = t.row.add([count ,  rowd.vName,  rowd.employeeName , rowd.dateCreated ,  rowd.Notes,  rowd.documents,  rowd.status, '']).draw().node();
          $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.id + '" data-id1="' + rowd.documents + '" type="checkbox">' + count + '</label>');
          $(row).find('td:eq(5)').html(arraydString.join());
          $(row).find('td:eq(7)').html('<button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="vacationsTable" class="acceptV btn btn-warning btn-sm"><span class="ion-checkmark" aria-hidden="true"></span></button><br><br><button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="vacationsTable" class="declineV btn btn-danger btn-sm"><span class="ion-close" aria-hidden="true"></span></button>');
return row;
      });
      }
    }
  });

}
function showMessageSuccess(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'success',
    title: maessageResponse,
    showConfirmButton: true,
    timer: 1500
  })
}
function showMessageSuccessWithHTML(maessageResponse,html){

Swal.fire({
  position: 'center',
  type: 'success',
  title: maessageResponse,
  html: html,
  showConfirmButton: true
})
}
function showMessageError(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'error',
    title: maessageResponse,
    showConfirmButton: false,
    timer: 1500
  })
}
$(document).ready(function() {
  function getChechedValuesDocuments(tableName){
    var selectedChecked = [];
    var t = defineTable("#" + tableName + "");

    var filteredInputsCheckboxes = $("#" + tableName + "").find("input[name='checkboxtd']:checked");
    $.each($(filteredInputsCheckboxes), function() {
      if ($(this).data('id1') != ''){
      selectedChecked.push($(this).data('id1'));
    }
     });

    return selectedChecked;
  }
            var t = defineTable('#employeesTable');
           var t2 = defineTable('#ejobinfoTable');
          var t3 = defineTable('#contactsTable');


  $('#inputSearch2').on('keyup click', function () {
    t.search( this.value ).draw();
    t2.search( this.value ).draw();
        t3.search( this.value ).draw();

  });
//  $("a#custom-tabs-one-home-tab").click();

  //your code here
  $('.checkall').change(function() {
    $('.checkboxtd').each(function() {
      if ($('.checkall').is(':checked')) {
        $(this).attr("disabled", false);
        $(this).prop("checked", true);
      } else {
        $(this).prop("checked", false);
      }
    });
  });



  let employeeidProfile;
  $(document).on('click', 'a.employeesProfileLinks', function(event) {

    var mine = event.target;
    var employeeidProfile = $(mine).attr('data-id1')
    var element = employeeidProfile;
    var action = 'showEmployeProfileDiv';
    $.ajax({
      url: '../dist/php/employees.php',
      type: 'POST',
      data: {
        element: element,
        action: action,
      },
      dataType: "json",
      success: function(data) {
        var myData = data;
        if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
          var count = 0;

          myData.forEach(function(rowd) {

            $('#myprofileContent').html('<p class="col-5 text-center">' + '<img src="../dist/img/' + rowd.userPic + '" alt="" class="img-circle img-fluid"></p><p><h2 class="lead"><b>' + rowd.name + '</b></p></h2><p class="text-muted text-sm"><b>الوصف: </b>' + rowd.userNote + '<ul class="ml-4 mb-0 fa-ul text-muted"><li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #:' + rowd.officeNo + '</li></ul></p>')
            $('#profileHeader').html(rowd.departmentName + ' -' + rowd.jobTitle);
            $('#profilefooter').html('<div class="text-right"><a href="../sms/form.php?uid=' + btoa(rowd.id) + '" class="btn btn-sm bg-teal">SMS  <i class="ion-android-textsms"></i></a> <button type="button" name="btn btn-danger btn-s" style="width: 38px;" data-id1="' + btoa(rowd.id) + '" value="Chat" class="btn btn-s btn-delete btn_chat btn-info"><span class="ion-chatboxes" aria-hidden="true"></span> </button>  <a href="../employees/profile.php?uid=' + btoa(rowd.id) + '" class="btn btn-sm btn-primary">رؤية البروفايل <i class="ion-ios-person"></i> </a></div>');
          });
        }
      }
    });


  });
  var array = [];


  $('a#custom-tabs-one-profile-tab').on('shown.bs.tab', function() {
    //var myselection = getChechedValues("employeesTable");

    showsettingtbody("", "progressTable");


  });
  $("a#custom-tabs-one-profile-tab").click(function() {
    $(this).tab('show');
  });

  $('a#custom-tabs-one-messages-tab').on('shown.bs.tab', function() {
    showsettingtbody("", "employeesPTable");

  });



  $('a#custom-tabs-one-settings-tab').on('shown.bs.tab', function() {
    showsettingtbody("", "vacationsTable");

  });
  $(function() {
    showsettingtbody("", "employeesTable");
    showsettingtbody("", "contactsTable");
    showsettingtbody("", "ejobinfoTable");
    showsettingtbody("", "bonusDeductSalaryTable");
    showsettingtbody("", "vacationsTable");

    //console.log('hasbeen clicked');
  })

  function showsettingtbody(myselection, tableName) {
    if (myselection != ""){
    localStorage.setItem('myselectionreq', myselection)
}
    var element = myselection;


    if (tableName == "employeesTable") {

      initializeArrays();
      var t = $('#employeestbody').html('')

      var t = defineTable('#employeesTable');
      t.clear().draw();
      //  htmltoAppend.innerHTML = '';
      var action = 'showEmployees';
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          action: action,
        },
        dataType: "json",
        success: function(data) {
          var t = defineTable('#employeesTable');
          t.clear().draw();

          var myData = data;
          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
            var count = 0;

            myData.forEach(function(rowd) {
              count = count + 1;
              var arrayd = [];
              var arrayhtml = [];
              var value = rowd.documents
              var valueString = value.toString();

              var dlink = '../upload/hr/';
              console.log(valueString);
              strx = valueString.split(',');
              arrayd = arrayd.concat(strx);
              //formattedDocuments.split(',');
              for (element of arrayd) {
                var el = element.trim();
                var fairel = el.substring(el.lastIndexOf('+') + 1);
                arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + fairel.trim() + '</a>');
              }
              var arraydString = arrayhtml;
              var row = t.row.add(['<td class="#">' + count + ' </td>', '<td class="idcard">' + rowd.id + '</td>', '<td class="name">' + rowd.name + '</td>', '<td class="m1">' + rowd.nationality + '</td>', '<td class="m2">' + rowd.skills + '</td>', '<td class="off">' + rowd.experience + '</td>', '<td class="email">' + rowd.lastCer + '</td>', '<td class="notes">' + rowd.documents + '</td>', '<td class="notes">' + rowd.joinDate + '</td>', '<td class="notes">' + rowd.status + '</td>', '<td></td>', ]).draw().node();



              $(row).find('td:eq(2)').html('<a data-toggle="modal" class="employeesProfileLinks" data-target="#profileModal" href="#" data-id1="' + rowd.id + '" >' + rowd.name + ' </a>');

              $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.id + '" data-id1="' + rowd.documents  + '" type="checkbox">' + count + '</label>');
            //  $(row).find('td:eq(3)').html('<select name="nationality" class="custom-select" style="width: 90px;" value="' + rowd.nationality + '" disabled><option value="Saudi">Saudi</option><option value="Hindi">Hindi</option><option value="American">American</option><option value="Egyptian">Egyptian</option></select>');
            $(row).find('td:eq(7)').html(arraydString.join());

            //  $(row).find('td:eq(5)').html('<select name="experience" class="custom-select" style="width: 150px;" value="' + rowd.experience + '" disabled><option value="Less than 1 year"> Less than 1 year </option><option value="1-2 years">1-2 years</option><option value="2-4 years">2-4 years</option><option value="More than 4 years">More than 4 years</option></select>');
              $(row).find('td:eq(8)').html('<input type="text" class="joinDate" name="joinDate" value="' + rowd.joinDate + '" disabled>');

      //        $(row).find('td:eq(9)').html('<select name="status" class="custom-select" style="width: 130px;" value="' + rowd.status + '" disabled><option value="Employed">Employed</option><option value="Retired">Retired</option></select>');

              $(row).find('td:eq(10)').html('<button type="button" class="btn btn-warning btn-s update" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id1="' + rowd.id + '"> <span class="ion-edit" aria-hidden="true"></span> </button><button type="button" name="btn btn-danger btn-s btn_delete" style="margin-right:10px; margin-bottom:10px; width: 38px;" data-id1="' + rowd.id + '" data-id2="' + rowd.documents + '" data-id3="employeesTable" data-id4="' + count + '" class="btn btn-s btn-danger btn_delete"><span class="ion-trash-a"' + ' aria-hidden="true"></span> </button>'+'<button type="button" name="btn btn-danger btn-s btn_share" style="margin-right:10px; margin-bottom:10px; width: 38px;"  data-id1="' + rowd.documents + '"  class="btn btn-s btn-primary btn_share"><span class="ion-android-share" aria-hidden="true"></span> </button>');

              row.id = count;
              return row;

            });
          }
        }
      });
    }

    if (tableName == "contactsTable") {
      initializeArrays();
      var t = $('#contactstbody').html('')

      var t = defineTable('#contactsTable');
      t.clear().draw();
      //  htmltoAppend.innerHTML = '';
      var action = 'showContacts';
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          action: action,
        },
        dataType: "json",
        success: function(data) {
          var myData = data;
          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
            var count = 0;
            myData.forEach(function(rowd) {
              count = count + 1;
              var t = defineTable('#contactsTable');
              var row = t.row.add(['<td class="#">' + count + ' </td>', '<td class="idcard">' + rowd.id + '</td>', '<td class="name">' + rowd.name + '</td>', '<td class="m1">' + rowd.mobile1 + '</td>', '<td class="m2">' + rowd.mobile2 + '</td>', '<td class="off">' + rowd.officeNo + '</td>', '<td class="email">' + rowd.email + '</td>', '<td class="notes">' + rowd.notes + '</td>', '<td></td>', ]).draw().node();
              $(row).find('td:eq(8)').html('<button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="contactsTable" class="editbtn btn btn-warning btn-sm">Edit</button>');
              //$(row).find('td:eq(8)').addClass('assignedTom'); //to add class or mainpulate specific cell
              row.id = count;
              return row;

            });

          }
        }
      });
    }
    if (tableName == "ejobinfoTable") {
      initializeArrays();
      $('#ejobinfoTbody').html('')

      var t = defineTable('#ejobinfoTable');
      t.clear().draw();
      //  htmltoAppend.innerHTML = '';
      var action = 'showjobInfo';
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          action: action,
        },
        dataType: "json",
        success: function(data) {
          var myData = data;
          var t = defineTable('#ejobinfoTable');
          t.clear().draw();
          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
            var count = 0;
            myData.forEach(function(rowd) {
              count = count + 1;


              var row = t.row.add(['<td class="#">' + count + ' </td>', '<td class="idcard">' + rowd.empid + '</td>', '<td class="name">' + rowd.jobTitle + '</td>', '<td class="m1">' + rowd.salary + '</td>', '<td class="m2">' + rowd.benefits + '</td>', '<td class="off">' + rowd.departmentName + '</td>', '<td class="email">' + rowd.datebegin + '</td>', '<td class="notes">' + rowd.idEnd + '</td>', '<td class="notes">' + rowd.vacationDate + '</td>', '<td></td>', ]).draw().node();
              $(row).find('td:eq(6)').html('<input type="text" class="datebegin" name="datebegin" value="' + rowd.datebegin + '" style="width: 130px;" disabled>');
              $(row).find('td:eq(7)').html('<input type="text" class="idEnd" name="idEnd" value="' + rowd.idEnd + '" style="width: 130px;" disabled>');
              $(row).find('td:eq(8)').html('<input type="text" class="vacationDate" name="vacationDate" value="' + rowd.vacationDate + '" style="width: 130px;" disabled>');

              $(row).find('td:eq(9)').html('<button data-id1="' + count + '" data-id2="' + rowd.empid + '" data-id3="ejobinfoTable" class="editbtn btn btn-warning btn-sm">Edit</button>');
              //$(row).find('td:eq(8)').addClass('assignedTom'); //to add class or mainpulate specific cell
              row.id = count;
              return row;

            });


          }
        }
      });
    }

    if (tableName == "progressTable") {

      initializeArrays();
      var t = $('#progressTbody').html('')

      var t = defineTable('#progressTable');
      t.clear().draw();
      //  htmltoAppend.innerHTML = '';
      var action = 'showEmployeesProgress';
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          action: action,
        },
        dataType: "json",
        success: function(data) {
          var myData = data;
          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
            var count = 0;
            var t = defineTable('#progressTable');
            t.clear().draw();

            myData.forEach(function(rowd) {
              count = count + 1;

              if (rowd.noOfProjectsLead == null) {
                rowd.noOfProjectsLead = '0';
              }
              if (rowd.tasksAssigned == null) {
                rowd.tasksAssigned = '0';
              }
              if (rowd.tasksHandled == null) {
                rowd.tasksHandled = '0';
              }
              if (rowd.honorspts == null) {
                rowd.honorspts = '0';
              }
              if (rowd.deductpts == null) {
                rowd.deductpts = '0';

              }
              if (rowd.Appraisals == null) {
                rowd.Appraisals = '0';

              }




              var row = t.row.add(['<td class="#">' + count + ' </td>', '<td class="name">' + rowd.name + '</td>', '<td class="m1">' + rowd.noOfProjectsLead + '</td>', '<td class="m2">' + rowd.tasksAssigned + '</td>', '<td class="off">' + rowd.tasksHandled + '</td>', '<td class="email">' + rowd.honorspts + '</td>', '<td class="notes">' + rowd.deductpts + '</td>', '<td class="notes">' + rowd.Appraisals + '</td>', '<td>' + '' + '</td>']).draw().node();

              $(row).find('td:eq(0)').html('<label><input name="checkboxtd" class="checkboxtd" value ="' + rowd.id + '" type="checkbox">' + count + '</label>');
              $(row).find('td:eq(8)').html('<button type="button" class="btn btn-warning btn-s appraisals" data-id1="' + rowd.id + '" style="margin-right:10px; margin-bottom:10px; width: 80px;"> <span class="ion-happy" aria-hidden="true">تقدير</span> </button>' +
                '<button type="button" class="btn btn-primary btn-s bonus" style="margin-right:10px; margin-bottom:10px; width: 80px;" data-id1="' + rowd.id + '">+  بونس </button>' + '<button type="button" class="btn btn-danger btn-s deduct" style="margin-right:10px; margin-bottom:10px; width: 80px;" data-id1="' + rowd.id + '">- خصم</button>' +
                '<button type="button" class="btn btn-success btn-s upgrade" style="margin-right:10px; margin-bottom:10px; width: 80px;" data-id1="' + rowd.id + '">  ^ترقية</button>');

              row.id = count;
              return row;

            });

          }
        }
      });
    }
    if(tableName == "vacationsTable"){
      initializeArrays();
showVacations();

    }
    if (tableName == "employeesPTable") {

      initializeArrays();
      $('#employeesPtbody').html('')
      $('#userstbody').html('')
      $('#employeesLogsTimelineTbody').html('')


      var t = defineTable('#employeesPTable');
      t.clear().draw();

      var t2 = defineTable('#usersTable')
      t2.clear().draw();
      var t3 = defineTable('#employeesLogsTimeline');
      t3.clear().draw();

      //  htmltoAppend.innerHTML = '';
      var action = 'showPreviligesAndTimeLine';
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          action: action,
        },
        dataType: "json",
        success: function(data) {
          var myData = data['users'];
          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
            var count = 0;
            var t = defineTable('#employeesPTable');
            t.clear().draw();
            var t2 = defineTable('#usersTable');
            t2.clear().draw();
            var t3 = defineTable('#employeesLogsTimeline');

            t3.clear().draw();

            myData.forEach(function(rowd) {
              count = count + 1;



              var row = t.row.add([count, rowd.employeeid, rowd.username, rowd.levelid, '']).draw().node();
              var row2 = t2.row.add([count, rowd.employeeid, rowd.username, rowd.userPic, '']).draw().node();
              $(row2).find('td:eq(3)').html('<img src="../dist/img/'+rowd.userPic+'" class="img-circle elevation-2" alt="User Image" width=70 height=70>')
    //          $(row2).find('td:eq(4)').html('<button data-id1="' + count + '" data-id2="' + rowd.employeeid + '" data-id3="employeesPTable" class="editbtn btn btn-warning btn-sm">Edit</button>');

              $(row).find('td:eq(3)').html('<select class="selectPreviliges" style="width: 150px;" disabled><option value="1">عالي الصلاحية</option><option value="2">متوسط الصلاحية</option><option value="3">منخفض الصلاحية</option></select>')
              $(row).find('td:eq(3)').find('select').val(rowd.levelid);
              $(row).find('td:eq(4)').html('<button data-id1="' + count + '" data-id2="' + rowd.employeeid + '" data-id3="employeesPTable" class="editbtn btn btn-warning btn-sm">Edit</button>');

              row.id = count;
              return row;

            });
          }
            var myData2 = data['empTimeline'];

            if (typeof myData2 != "undefined" && myData2 != null && myData2.length != null && myData2.length > 0) {
              var count = 0;


              myData2.forEach(function(rowd) {
                count = count + 1;
                var arrayd = [];
                var arrayhtml = [];
                var value = rowd.documents
                var valueString = value.toString();

                var dlink = '../upload/hr/certifications/';
                console.log(valueString);
                strx = valueString.split(',');
                arrayd = arrayd.concat(strx);
                //formattedDocuments.split(',');
                for (element of arrayd) {
                  var el = element.trim();
                  var fairel = el.substring(el.lastIndexOf('+') + 1);
                  arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + fairel.trim() + '</a>');
                }

                var row = t3.row.add([count, rowd.empid, rowd.activityName, rowd.date, rowd.documents, '']).draw().node();
                $(row).find('td:eq(4)').html(arrayhtml.join());

                row.id = count;
                return row;

              });
            }

          var t = defineTable('#usersTable');
         var t2 = defineTable('#employeesPTable');
        var t3 = defineTable('#employeesLogsTimeline');




$('#inputSearch').on('keyup click', function () {
  t.search( this.value ).draw();
  t2.search( this.value ).draw();
  t3.search( this.value ).draw();


});



          }
      });
    }

    if (tableName == "bonusDeductSalaryTable") {

      initializeArrays();
      $('#bonustbody').html('')
      $('#deductTbody').html('')
      $('#epaymentsTbody').html('')

      var t = defineTable('#bonusTable');
      t.clear().draw();

      var t2 = defineTable('#deductTable');
      t2.clear().draw();
      var t3 = defineTable('#epaymentsTable');
      t3.clear().draw();

      //  htmltoAppend.innerHTML = '';
      var action = 'showBonusDeductSalaryLogs';
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          action: action,
        },
        dataType: "json",
        success: function(data) {
          var t = defineTable('#bonusTable');
          t.clear().draw();
          var t2 = defineTable("#deductTable");
            t2.clear().draw();
            var t3 = defineTable('#epaymentsTable');
            t3.clear().draw();
          var myDatab = data['bonus'];
          if (typeof myDatab != "undefined" && myDatab != null && myDatab.length != null && myDatab.length > 0) {
            var count = 0;
            myDatab.forEach(function(rowd) {
              count = count + 1;

              var row = t.row.add([count, rowd.idcard, rowd.pt, rowd.amountPayment, rowd.reason, rowd.issuedDate, '']).draw().node();

              // var row = t.row.add([count, rowd.logid, rowd.empid, rowd.amount, rowd.typeOfPayment,rowd.issuedDate. '']).draw().node();

              row.id = count;
              return row;

            });
          }
            var myDatad = data['deduct'];




            if (typeof myDatad != "undefined" && myDatad != null && myDatad.length != null && myDatad.length > 0) {
              var count = 0;
              myDatad.forEach(function(rowd) {
                count = count + 1;
                var row = t2.row.add([count, rowd.idcard, rowd.pt, rowd.amountPayment, rowd.reason, rowd.issuedDate, '']).draw().node();

                // var row = t.row.add([count, rowd.logid, rowd.empid, rowd.amount, rowd.typeOfPayment,rowd.issuedDate. '']).draw().node();

                row.id = count;
                return row;

              });
            }
              var myDatae = data['epayments'];
              if (typeof myDatae != "undefined" && myDatae != null && myDatae.length != null && myDatae.length > 0) {
                var count = 0;
                myDatae.forEach(function(rowd) {
                  count = count + 1;

                  var row = t3.row.add([count, rowd.empid, rowd.amount, rowd.typeOfPayment,rowd.issuedDate, '']).draw().node();

                  // var row = t.row.add([count, rowd.logid, rowd.empid, rowd.amount, rowd.typeOfPayment,rowd.issuedDate. '']).draw().node();

                  row.id = count;
                  return row;

                });
}
//
// $('#inputSearch').on('keyup click', function () {
//   t.search( this.value ).draw();
//   t2.search( this.value ).draw();
//   console.log('is searching');
//
// });

        }
      });
    }

  }
  var addRecords = [];
  var addRecordsRowsNo = [];
  var editedRecords = [];

  function initializeArrays() {
    addRecords = [];
    addRecordsRowsNo = [];
    editedRecords = [];
  }
  $(document).on('click', 'button.declineV', function(event) {
    event.preventDefault();
    $('#declineVModal').modal('show');
    $('.modal-title').html("<i></i> رفض الطلب");

  //  $('#declineVForm')[0].reset();
  var id = $(this).data('id2');
  $('#idV').val(''+id+'');
  });
  $(document).on('click', 'button.acceptV', function(event) {
    event.preventDefault();
    var action = 'acceptVacation';
  //  $('#declineVForm')[0].reset();
  var idV = $(this).data('id2');
  var fd = new FormData();
fd.append('action', action);
fd.append('idV', idV);

$.ajax({
  url: '../dist/php/employees.php',
  type: 'post',
  data: fd,
  contentType: false,
  processData: false,
  success: function(data) {
     if(data == 0){
       showMessageSuccess('تمت العملية بنجاح');
window.location.reload();
    }else{
      showMessageError("تحقق من صحة البيانات أو الانترنت لديك");

    }
  }
});
  });


  $(document).on('click', 'button.EDITPermissions', function(event) {
    event.preventDefault();
    var action = 'EDITPermissions';
    var currentTD = $(this).parents('tr').find('td');

    if ($(this).html() == 'Edit') {
      //console.log($(this).data("id2"));
      $.each(currentTD, function() {
        $(this).prop('contenteditable', true)

      });
      $(this).html('Save');
    } else {
      $(this).html('Edit');

      $.each(currentTD, function() {
        $(this).prop('contenteditable', false)

      });


  //  $('#declineVForm')[0].reset();
  var id = $(this).data('id2');
  var idttoEdit = $(this).parents('tr').index() + 1;
  var table = document.getElementById("permissionTable");
var data = table.rows[idttoEdit].cells[1].innerText;
  var fd = new FormData();
fd.append('action', action);
fd.append('id', id);
fd.append('data', data);

$.ajax({
  url: '../dist/php/employees.php',
  type: 'post',
  data: fd,
  contentType: false,
  processData: false,
  success: function(data) {
     if(data == 0){
       showMessageSuccess('تمت العملية بنجاح');
    }else{
      showMessageError("تحقق من صحة البيانات أو الانترنت لديك");

    }
  }
});
}
  });
  $(document).on('submit', '#declineVForm', function(event) {
event.preventDefault();
      var fd = new FormData();
      var poData = jQuery('#declineVForm').serializeArray();
      jQuery.each(poData, function(i, pd) {
        fd.append('' + pd.name + '', pd.value);
      });
    var action = 'declineVacation';
    fd.append('action', action);
    $.ajax({
      url: '../dist/php/employees.php',
      type: 'post',
      data: fd,
      contentType: false,
      processData: false,
      success: function(data) {
         if(data == 0){
           showMessageSuccess('تمت العملية بنجاح');
         $('#declineVForm')[0].reset();
          $('#declineVModal').modal('hide');
        }else{
          showMessageError("تحقق من صحة البيانات أو الانترنت لديك");

        }
      }
    });
  });

  $(document).on('click', '.ddelete', function(event) {
    var mine = event.target;
  var link = '../../upload/hr/';
  var fileToDelete = $(mine).data('filename');
deleteFile(link,fileToDelete,mine);
  });

  $(document).on('click', 'button.documentdelete', function(event) {
    var mine = event.target;
    var formID = $(mine).closest('form')[0].id;
    var link;

if(formID == 'bdForm'){
  link = '../../upload/hr/certifications/';
}
if(formID == 'recordForm'){
  link = '../../upload/hr/';
}
  var fileToDelete = $(mine).data('filename');
deleteFile(link,fileToDelete,mine);

  });

  $('#btn_upload_cer').click(function() {
    event.preventDefault();
  uploadFiles('userfile2','#documentsm','../../upload/hr/certifications/');
  });

  $('#btn_upload').click(function() {
    event.preventDefault();
    uploadFiles('userfile','#documentsu','../../upload/hr/');
  });
  $('#joinDate').datepicker({
    format: 'yyyy-mm-dd'
  });



  $('#addRecord').click(function() {
    $('#recordModal').modal('show');
    $('#recordForm')[0].reset();
    $('.modal-title').html("<i></i> Add Record");
    $('#action').val('addRecord'); //from modal footer to send
    $('#save').val('Add');
  });

  function gettodaydate() {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }
  var test = [];
  var addRecords = [];
  var editedRecords = [];
  var addRecordsRowsNo = [];
  var deletedRecordsRowsNo = [];
  var editedItems = [];
  var values = [];
  $(document).on('click', '.editbtn', function() {
    var currentTD = $(this).parents('tr').find('td');
    if ($(this).html() == 'Edit') {
      editedItems[0] = $(this).data("id2");

      //console.log($(this).data("id2"));
      $.each(currentTD, function() {
        $(this).prop('contenteditable', true),
          $(this).find('select').prop('disabled', false);
        $(this).find('input').prop('disabled', false);

        $(this).find('.datebegin').datepicker({
          format: 'yyyy-mm-dd',
          value: gettodaydate()
        });

        $(this).find('.idEnd').datepicker({
          format: 'yyyy-mm-dd',
          value: gettodaydate()

        });
        $(this).find('.vacationDate').datepicker({
          format: 'yyyy-mm-dd',
          value: gettodaydate()

        });
      });

      $(this).html('Save');
    } else {

      var idttoEdit = $(this).parents('tr').index() + 1;
      editedItems[0] = $(this).data("id2");
      var tableName = $(this).data("id3");
      $(this).html('Edit');
      var idttoEdit = $(this).parents('tr').index() + 1;

      var tableName = $(this).data("id3")
      if (tableName == "ejobinfoTable") {
        editsettingtbody(idttoEdit, 'ejobinfoTable');
      }
      if (tableName == "contactsTable") {
        editsettingtbody(idttoEdit, 'contactsTable');
      }
      if (tableName == "employeesPTable") {
        editsettingtbody(idttoEdit, 'employeesPTable');
      }

      $.each(currentTD, function() {
        $(this).prop('contenteditable', false),
          $(this).find('select').prop('disabled', true);

      });

      var editedRecordsData = editedRecords;
      var action = "editRecords";
      var editedItemsData = editedItems;

      var tableNameData;
      if (tableName == "ejobinfoTable") {
        ///this is for our table stting 3 table
        tableNameData = "ejobinfoTable";
      }
      if (tableName == "contactsTable") {
        ///this is for our table stting 3 table


        tableNameData = "contactsTable";
      }
      if (tableName == "employeesPTable") {
        ///this is for our table stting 3 table


        tableNameData = "employeesPTable";
      }
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          editedItemsData: editedItemsData,
          editedRecordsData: editedRecordsData,
          action: action,
          tableNameData: tableNameData,
        },
        dataType: "text",
        success: function(data) {
          // alert(data);
          if (data == 0) {
            maessageResponse = 'تم التعديل بنجاح';
            Swal.fire({
              position: 'center',
              type: 'success',
              title: maessageResponse,
              showConfirmButton: false,
              timer: 1500
            })
            if (tableName == "ejobinfoTable") {
              var t = $('#ejobinfoTbody').html('');
              showsettingtbody("", "ejobinfoTable");
            }
            if (tableName == "contactsTable") {
              var t = $('#contactstbody').html('');
              showsettingtbody("", "contactsTable");
            }

          } else {
            maessageResponse = 'لم يتم التعديل بنجاح ، تأكد من البيانات المدخلة';
            Swal.fire({
              position: 'center',
              type: 'error',
              title: maessageResponse,
              showConfirmButton: false,
              timer: 1500
            })
          }


        }
      });

    }
  });

  function editsettingtbody(idttoEdit, tableName) {
    editedRecords = [];
    var values = [];
    var table = document.getElementById("" + tableName + "");

    if (idttoEdit != null || idttoEdit != 'undefined') {
      //if table name
      if (tableName == "ejobinfoTable") {
        //  var table = document.getElementById("settingstableTwo");
        for (i = 0; i < 9; i++) { //assumin we have 4 cells in our example

          if (i == 6) {
            var m = $(table.rows[idttoEdit]).find('td:eq(6) input').val();
            values[i] = m;

          } else if (i == 7) {
            var m = $(table.rows[idttoEdit]).find('td:eq(7) input').val();
            values[i] = m;

          } else if (i == 8) {
            var m = $(table.rows[idttoEdit]).find('td:eq(8) input').val();
            values[i] = m;

          } else {
            values[i] = table.rows[idttoEdit].cells[i].innerText;
          }
          //   console.log(table.rows[idt].cells[i].outerHTML);
        }

        editedRecords.push(values);
      }
      if (tableName == "contactsTable") {
        //  var table = document.getElementById("settingstableTwo");
        for (i = 0; i < 8; i++) { //assumin we have 4 cells in our example

          values[i] = table.rows[idttoEdit].cells[i].innerText;

          //   console.log(table.rows[idt].cells[i].outerHTML);
        }

        editedRecords.push(values);
      }
      if (tableName == "employeesPTable") {
        //  var table = document.getElementById("settingstableTwo");
        for (i = 0; i < 4; i++) { //assumin we have 4 cells in our example
          if (i == 3) {
            var m = $(table.rows[idttoEdit]).find('td:eq(3) select').val();
            values[i] = m;

          } else {
            values[i] = table.rows[idttoEdit].cells[i].innerText;
          }
          //   console.log(table.rows[idt].cells[i].outerHTML);
        }

        editedRecords.push(values);
      }

    }
  }

  $(document).on('click', '.update', function() {
    console.log('has been clicked');
    var idt = $(this).data("id1");
    var action = 'getRecord'; //it will call the record with selected editbutton id
    $.ajax({
      url: '../dist/php/employees.php',
      method: "POST",
      data: {
        idt: idt,
        action: action
      },
      dataType: "json",
      success: function(data) { //the data return in json format from db to be used in the modal.
        $('#recordModal').modal('show');
        $('#idt').val(idt);
        $('#id').val(data.id);
        $('#name').val(data.name);
        $('#nationality').val(data.nationality);
        $('#skills').val(data.skills);
        $('#experience').val(data.experience);
        $('#lastCer').val(data.lastCer);
        $('#joinDate').val(data.joinDate);
        $('#status').val(data.status);
        $('#documents').attr('data-documents',data.documents);

        //these lines for
        var array1 = [];
        var valueString = $('#documents').attr("data-documents");
        if(valueString.length > 1){
          var strx = valueString.split(',');
          array1 = array1.concat(strx); //this the input original values
          var htmlFiles = [];
          var el = document.getElementById("documents");
          $('#documents').html('');
          for (file of array1) {
            var el = file.trim();
        		var fairel = el.substring(el.lastIndexOf('+') + 1);
          $('#documents').append('<span class="documentdeletespan"><span id="filename">' + fairel + '  </span><button type="button" class="ddelete ion-trash-a" name="btn_delete"  data-filename="' + file + '" ></button>' + '</span>');
            // $('#documentsui').append('<span class="documentdeletespan"> <a id=documentsa href=upload/hr/'+response+'>'+ response +'</a> <button type="button" id="documentdelete" name="btn btn-danger btn-xs btn_delete" class="ion-trash-a" data-id5="'+ response + '" > </button></span>');
          }


        }

        $('.modal-title').html("<i></i> تحرير السجل");
        $('#action').val('updateRecord');
        $('#save').val('حفظ');
            $('.ddelete').on('click', function(e){
              e.preventDefault();
              $(e).closest('.documentdeletespan').remove(); //to remove it in real time

            });

}
        });

  });
  Array.prototype.nonMatchingItems = function(array1,array2) {

var nonMatchingItems = array1.filter(item => array2.indexOf(item) === -1);
//if array1 = [1,2,3] and array2 = [1] the output will be [2,3] from array 1
return nonMatchingItems;

};
  Array.prototype.contains = function(v) {
    for (var i = 0; i < this.length; i++) {
      if (this[i] === v) return true;
    }
    return false;
  };

  Array.prototype.unique = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
      if (!arr.contains(this[i])) {
        arr.push(this[i]);
      }
    }
    return arr;
  };


  $('#bonusDeductModal').on('submit', '#bdForm', function(event) {
    event.preventDefault();
    var targ = event.target;
    var fd = new FormData();
    var poData = jQuery(targ).serializeArray();
    jQuery.each(poData, function(i, pd) {
      fd.append('' + pd.name + '', pd.value);
    });
var uploadedFiles = document.querySelectorAll("button.documentdelete");
var allFilesNames = [];
uploadedFiles.forEach((item) => {
	var el = item.getAttribute('data-filename');
allFilesNames.push(el);
});
//this the uploaded files before
fd.append('allFilesNames', allFilesNames);
    $.ajax({
      url: '../dist/php/employees.php',
      type: 'post',
      data: fd,
      contentType: false,
      processData: false,
      success: function(response) {
        isUploadClicked = false;
        if (response == 0) {
          $('#bonusDeductModal').modal('hide');
          $('#bdForm')[0].reset();
          $('#documentsm').html('');
          var maessageResponse = 'تم إرسال الطلب بنجاح';
          showMessageSuccess(maessageResponse);
          return;
        } else if(response == -1) {
          var maessageResponse = 'لم يتم إرسال الطلب بنجاح تأكد من البيانات المدخلة';
          showMessageError(maessageResponse);
          // alert(response);
          return;
        }else{
          showMessageError(response);

        }


      }
    })
  });
  $("#recordModal").on('submit', '#recordForm', function(event) {
    event.preventDefault();
    var targ = this;
    var fd = new FormData();
      var deletedFiles = [];
      var existingFiles = [];
    //these lines for
    var array1 = [];
    var valueString = $('#documents').attr("data-documents");
    var array1 = valueString.split(',');
    $('#save').attr('disabled', false);
    var existingFiles = document.querySelectorAll("button.ddelete");
    var length = existingFiles.length;

    var allFilesNamesExist = [];
    existingFiles.forEach((item) => {
    	var el = item.getAttribute('data-filename');
    allFilesNamesExist.push(el);
    });
console.log(allFilesNamesExist+'existing');

     deletedFiles = array1.nonMatchingItems(array1,allFilesNamesExist);


    console.log(deletedFiles+'deletedFiles');


var uploadedFiles = document.querySelectorAll("button.documentdelete");
var allFilesNames = [];
uploadedFiles.forEach((item) => {
	var el = item.getAttribute('data-filename');
allFilesNames.push(el);
});
//this the uploaded files before
fd.append('allFilesNames', allFilesNames);

console.log(allFilesNames+'uploaded');

//these files exist before
fd.append('existingFiles', allFilesNamesExist);

fd.append('deletedFiles', deletedFiles);


var poData = jQuery(targ).serializeArray();
jQuery.each(poData, function(i, pd) {
  fd.append('' + pd.name + '', pd.value);
});

    $.ajax({
      url: "../dist/php/employees.php",
      type: 'post',
      data: fd,
      contentType: false,
      processData: false,
      success: function(data) {
        if (data == 0) {
          $('#recordModal').modal('hide');
          $('#recordForm')[0].reset();
          $('#save').attr('disabled', false);
          showMessageSuccess('تمت العملية بنجاح');
          window.location.reload();
        } else {
          showMessageError('هناك مشكلة بالبيانات');
        }

      }
    });
  });



  $(document).on('click', '.btn_delete', function(event) {
    var mine = event.target;
    var idt = $(this).data("id1");
    var idocuments = $(this).data("id2");
    var tableName = $(this).data("id3");
    var deleteidt = $(this).data("id4");
    var deletedFiles = idocuments;

var link = '../../upload/hr/';
    var action = "deleteRecord";
    var tableNameData;
    if (tableName == "employeesTable" || tableName == "employeesPTable") {
      ///this is for our table stting 3 table
      tableNameData = "employees";
    }

    var t = defineTable("#" + tableName + "");
    $.ajax({
      url: "../dist/php/employees.php",
      method: "POST",
      data: {
        idt: idt,
        deletedFiles: deletedFiles,
        link: link,
        action: action,
        tableNameData: tableNameData
      },
      dataType: "text",
      success: function(data) {
        if (data == 0) {
          maessageResponse = 'تم الحذف بنجاح';
          Swal.fire({
            position: 'center',
            type: 'success',
            title: maessageResponse,
            showConfirmButton: false,
            timer: 1500
          })
          t.row(['#'+ deleteidt]).remove().draw(true);

          //  t.rows[deleteindex].remove(); //to remove row
        } else {
          maessageResponse = 'لم يتم الحذف بنجاح';
          Swal.fire({
            position: 'center',
            type: 'error',
            title: maessageResponse,
            showConfirmButton: false,
            timer: 1500
          })
        }
      }
    });
  });


  $(document).on('click', '.appraisals', function(event) {
    var mine = event.target;
    $('#bonusDeductModal').modal('show');
    $('.modal-title').html("<i></i> شكر وتقدير");
    var id = $(this).data("id1");
    $('#ptsid').val(id);
    $('#actionp').val('addAppraisals');
    $('#saveb').val('حفظ');
    $('#pts').attr('required', false);
    $('#ptsDiv').hide();
    $('#selectFileDiv').show();

  });

  $(document).on('click', '.bonus', function(event) {
    var mine = event.target;
    $('#bonusDeductModal').modal('show');
    $('.modal-title').html("<i></i> بونس");
    var id = $(this).data("id1");
    $('#ptsid').val(id);
    $('#actionp').val('addBonus');
    $('#saveb').val('حفظ');
    $('#pts').attr('required', true);
    $('#selectFileDiv').hide();
    $('#ptsDiv').show();
  });
  $(document).on('click', '.deduct', function(event) {
    var mine = event.target;
    $('#bonusDeductModal').modal('show');
    $('.modal-title').html("<i></i> خصم");
    var id = $(this).data("id1");
    $('#ptsid').val(id);
    $('#actionp').val('addDeduct');
    $('#saveb').val('حفظ');
    $('#pts').attr('required', true);
    $('#ptsDiv').show();
    $('#selectFileDiv').hide();
  });
  $(document).on('click', '.upgrade', function(event) {
    var mine = event.target;
    $('#bonusDeductModal').modal('show');
    $('.modal-title').html("<i></i> ترقية");
    var id = $(this).data("id1");
    $('#ptsDiv').hide();
    $('#ptsid').val(id);
    $('#actionp').val('addUpgrade');
    $('#pts').attr('required', false);
    $('#saveb').val('حفظ');
    $('#selectFileDiv').show();
  });

  $(document).on('click', '#payEmp', function(event) {
    var mine = event.target;
    var selections = getChechedValues("progressTable","");
                    var action = 'payEmployees';

                        Swal.fire({
                          title: 'هل انت متأكد من إضافة سجلات دفع الرواتب للموظفين؟؟',
                          text: "لن تستطيع التراجع!",
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: '!نعم قم بإضافته'
                        }).then((result) => {
                          if (result.value) {
                            $.ajax({
                              url: "../dist/php/employees.php",
                              method: "POST",
                              data: {
                                selections: selections,
                                action: action,
                              },
                              dataType: "text",
                              success: function(data) {
                          //   if (data === -1) {
                          //     maessageResponse = "هناك مشكلة في إضافة البيانات";
                          //     Swal.fire({
                          //       position: 'center',
                          //       type: 'error',
                          //       title: maessageResponse,
                          //       showConfirmButton: false,
                          //       timer: 1500
                          //     })
                          //   }
                          //   maessageResponse = "تم حذف الملف بنجاح";
                          //   Swal.fire({
                          //     position: 'center',
                          //     type: 'success',
                          //     title: maessageResponse,
                          //     showConfirmButton: false,
                          //     timer: 1500
                          //   })
                          // }
                        // })
                        // Toast.fire({
                        // 	type: 'success',
                        // 	title: maessageResponse
                        // })
                      }
                    });
                  }
                });

  });
  $(document).on('click', '#deleteSelect', function(event) {
    var mine = event.target;
    var selections = getChechedValues("employeesTable","");
                    var action = 'deleteMultibleRecords';
                    var tableNameData = 'employees';
                    var deletedFiles = getChechedValuesDocuments("employeesTable");
                    console.log(deletedFiles);

                    var deletedFiles = deletedFiles.join(',');
                    var link = '../../upload/hr/';
                        Swal.fire({
                          title: 'هل انت متأكد من حذف الموظفين المحددين وكل ما يخصهم؟',
                          text: "لن تستطيع التراجع!",
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: '!نعم قم بالحذف'
                        }).then((result) => {
                          if (result.value) {
                            $.ajax({
                              url: "../dist/php/employees.php",
                              method: "POST",
                              data: {
                                selections: selections,
                                action: action,
                                tableNameData: tableNameData,
                                deletedFiles: deletedFiles,
                                link: link
                              },
                              dataType: "text",
                              success: function(data) {
                                if (data == 0){
                                  getChechedValues("employeesTable","delete");
                                  showMessageSuccess("تمت العملية بنجاح");
                                  return;

                                }
                                  showMessageError('تأكد من وجود بعض المستندات المرفقة');

                          //     maessageResponse = "هناك مشكلة في إضافة البيانات";
                          //     Swal.fire({
                          //       position: 'center',
                          //       type: 'error',
                          //       title: maessageResponse,
                          //       showConfirmButton: false,
                          //       timer: 1500
                          //     })
                          //   }
                          //   maessageResponse = "تم حذف الملف بنجاح";
                          //   Swal.fire({
                          //     position: 'center',
                          //     type: 'success',
                          //     title: maessageResponse,
                          //     showConfirmButton: false,
                          //     timer: 1500
                          //   })
                          // }
                        // })
                        // Toast.fire({
                        // 	type: 'success',
                        // 	title: maessageResponse
                        // })
                      }
                    });
                  }
                });

  });


  /*
   * -------
   * Here we will create multible select, daterange ,
   */
  function getChechedValues(tableName,actionNeeded){
    var selectedChecked = [];
    var t = defineTable("#" + tableName + "");

    var filteredInputsCheckboxes = $("#" + tableName + "").find("input[name='checkboxtd']:checked");
    $.each($(filteredInputsCheckboxes), function() {
      selectedChecked.push($(this).val());
      if(actionNeeded == 'delete'){
         t.row($(this).closest('tr')).remove().draw(); //to remove row
      }
    });

    return selectedChecked;
  }


  $(document).on('click','button.btn_share' , function(event) {
    var el = $(this).attr('data-id1');
    if (el == null || el == undefined || el === ""){
      return;
    }else{
     shareSelectedDocuments(el);
    }

});

function shareSelectedDocuments(el){

      var filenames = el;
      var action = 'shareSelectedDocuments';
      $.ajax({
        url: '../dist/php/employees.php',
        type: 'POST',
        data: {
          filenames: el,
          action: action,
        },
        dataType: "json",
        success: function(data) {
          var text;

          if (data == -1){
showMessageError('هناك مشكلة بالملف إما غير موجود أو تم حذفه مسبقاً');
            return;
          }else{
            var i = 0;
            if (data['link'].length == 1){
              text = '<a href="../public/' + data['link']+ '" >'+'http://aalmlandmarks.com/public/'+data['link']+'</a><br>';
              showMessageSuccessWithHTML('تم إدراج الروابط بنجاح','<p class="sharelinks">'+text+'</p><br><button class="copyButton">copy</button>');

              return;
            }else{

              var i = 0;
              data['link'].forEach(function(rowd) {
                if (i == 0){
                  text = '<a href="../public/' + rowd+ '" >'+'http://aalmlandmarks.com/public/'+rowd+'</a><br>';

                }else{
                  text += '<a href="../public/' + rowd+ '" >'+' http://aalmlandmarks.com/public/'+rowd+'</a><br>';

                }
    i = i + 1;
            //    showMessageSuccess('<p class="col-5 text-center">' + '<img src="../dist/img/' + rowd.userPic + '" alt="" class="img-circle img-fluid"></p><p><h2 class="lead"><b>' + rowd.name + '</b></p></h2><p class="text-muted text-sm"><b>الوصف: </b>' + rowd.userNote + '<ul class="ml-4 mb-0 fa-ul text-muted"><li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #:' + rowd.officeNo + '</li></ul></p>')
                });
            }


           showMessageSuccessWithHTML('تم إدراج الروابط بنجاح','<p class="sharelinks">'+text+'</p><br><button class="copyButton">copy</button>');

        }
      }
      });
}

  $(function() {

    //Initialize Select2 Elements
    //Money Euro
    $('[data-mask]').inputmask()
    //Date range as a button
    $('#daterange-btn').daterangepicker({
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate: moment()
    }, function(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    });
    //Bootstrap Duallistbox
  });

});
