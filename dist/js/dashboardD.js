function checkauthority(){
  var action = 'checkAuthority';
  $.ajax({
    url: 'dist/php/sessionData.php',
    type: 'POST',
    data: {
        action: action
    }
    , dataType: "json"
    , success: function(data) {
      if (data == 1){
$('#admin-section').css('display','block');
$('#midadmin-section').css('display','block');

}
if (data == 2){
$('#midadmin-section').css('display','block');
$('#admin-section').html('');
}
 if (data == 3){
  $('#admin-section').html('');
  $('#midadmin-section').html('');

}
}
});
}


function get_time_diff( datetime )
{
    var datetime = typeof datetime !== 'undefined' ? datetime : "2014-01-01 01:02:03.123456";

    var datetime = new Date( datetime ).getTime();
    var now = new Date().getTime();

    if( isNaN(datetime) )
    {
        return "";
    }


    if (datetime < now) {
        var milisec_diff = now - datetime; //always here with blogs
    }else{
        var milisec_diff = datetime - now;
    }

    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));

    var date_diff = new Date( milisec_diff );
    var dateCreated = new Date( datetime );

    if(days <= 0){
      return " today "+  formatAMPM(dateCreated);

      //  return "اليوم في"+  dateCreated.getHours() + " : " + dateCreated.getMinutes() + " : " + dateCreated.getSeconds();


    }else{
      return "since" +  days + " days " +  formatAMPM(dateCreated);

    }
}
function formatAMPM(date) {
  var hours = date.getHours() + 1;
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}
function pagifyli(itemsToSearch,perPage,paginationSel){

var items = $(itemsToSearch);
    var numItems = items.length;
    var perPage = perPage;

    items.slice(perPage).hide();

    $(paginationSel).pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
}

function showMessageSuccess(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'success',
    title: maessageResponse,
    showConfirmButton: false,
    timer: 1500
  })
}
function showMessageError(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'error',
    title: maessageResponse,
    showConfirmButton: false,
    timer: 1500
  })
}
$(document).ready(function(){
checkauthority();
setInterval(function(){
  try {
  setLatestChatid();
  } catch (e) {

  }
  try {
  //  setLatestChatid();
 viewLatestMessages(); //to update a list in real time.
  } catch (e) {

  }
  try {
  //  setLatestChatid();
  viewLatestChatYouOpenedOnLoad();
  } catch (e) {

  }


}, 1500);



});

$('#logout').click(function(event) {
   event.preventDefault();
var action = "logout";
$.ajax({
  url: 'dist/php/sessionData.php',
  type: 'post',
  data: {
      action: action
  }
  , dataType: "text"
  , success: function(data) {
  window.location.replace("login.html");

  }

 });
});

$('div#receiverInfo').append('<p id="lastSeen"></p>');

//to assign userStatus
var latestconversation_Id;

function handleJsonviewLatestChatYouopen(jsonarrays){

  arrayhtmlContent = [];
  jsonarrays.forEach(row => {
        var currentchatlogid = [];
        if(row.fromUserId == currentUserId ) //this is a sender
        {
          if(row.readen == 0 ){
            messageChat =   '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+currentUserName+'</span><span class="direct-chat-timestamp float-left">'+row.timestamp+'</span></div><img class="direct-chat-img" src="dist/img/'+currentUserPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'<i class="fas fa-check"></i></div></div>';

          }else{
            messageChat =   '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+currentUserName+'</span><span class="direct-chat-timestamp float-left">'+row.timestamp+'</span></div><img class="direct-chat-img" src="dist/img/'+currentUserPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'  <i class="fas fa-check"></i><i class="fas fa-check"></i></div></div>';

          }
        }else{
          if(row.readen == 0 ){
            messageChat =   '<div class="direct-chat-msg"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">'+row.participantUsername+'</span><span class="direct-chat-timestamp float-right">'+row.timestamp+'</span></div><img class="direct-chat-img" src="dist/img/'+row.participantPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'<i class="fas fa-check"></i></div></div>';

}else{
  messageChat =   '<div class="direct-chat-msg"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">'+row.participantUsername+'</span><span class="direct-chat-timestamp float-right">'+row.timestamp+'</span></div><img class="direct-chat-img" src="dist/img/'+row.participantPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'<i class="fas fa-check"></i><i class="fas fa-check"></i></div></div>';

}                                    //this is a reciver

        }
        arrayhtmlContent.push(messageChat);
        messageChat = '';
        return arrayhtmlContent;
              });
}

function displayUserPanel(jsonarrays){

  var receiverInfo = document.getElementById("receiverInfo");
  var receiverpicpath = 'dist/img/'+jsonarrays.receiverpic;
 document.getElementById("receiverpic").setAttribute("src",receiverpicpath);
  document.getElementById("receivername").innerText = jsonarrays.receivername;
  //document.getElementById("lastSeen").innerText = jsonarrays.timeActivity;

  $('p#lastSeen').html('Last seen'+get_time_diff(jsonarrays.timeActivity)+'');


  if(jsonarrays.status === 'online'){
  //  receiverInfo.innerhtml = '<a href="" class="d-block"  id="receivernamem">'+jsonarrays.receivername+'</a><a href="#" id="statusr2"></a><p id="lastSeen">kk</p>';
  $('a#statusr').html('<i class="fa fa-circle text-success"></i> متصل');
  }else{
  $('a#statusr').html('<i class="fa fa-circle text-offline"></i> غير متصل');

  }
}
 //LATEST ONE CHAT OPEN CALLONLOAD AND ITS FUNCTION NEED TO BE CALLED WHEN OPEN ANOTHER CHAT TO CHAT. //LATEST USER PROFILE(NAME,PIC) AND RECEIVER PROFILE(NAME,PIC) CALLONLAD AND ITS FUNCTION NEED TO BE CALLED WHEN OPEN ANOTHER CHAT TO CHAT.
//to view latestMessage
var jsonarrayForUpdate;
var updatehtmlContent;
var listMessages;
//
var jsonarrays;
var messageChat;
var arrayhtmlContent;
///

function handleJsonviewLatestMessages(jsonarrayForUpdate){
  updatehtmlContent = [];
  var count = 0;
  jsonarrayForUpdate.forEach(row => {
    count = count + 1;
    if(row.counter!= null){
          listMessages = '<li id="'+count+'" class="conversation-list" data-conversation_id="'+row.conversation_id+'"><img class="contacts-list-img" src="dist/img/'+row.participantPic+'"><div class="contacts-list-info"><span class="contacts-list-name">'+row.participantUsername+'<small class="contacts-list-date float-right">'+row.timestamp+'</small></span><span class="contacts-list-msg">'+row.chatMessage+'</span><i class="fas fa-check"></i></div>';
        } else{
          listMessages = '<li id="'+count+'" class="conversation-list" data-conversation_id="'+row.conversation_id+'"><img class="contacts-list-img" src="dist/img/'+row.participantPic+'"><div class="contacts-list-info"><span class="contacts-list-name">'+row.participantUsername+'<small class="contacts-list-date float-right">'+row.timestamp+'</small></span><span class="contacts-list-msg">'+row.chatMessage+'<i class="fas fa-check"></i><i class="fas fa-check"></i></span></div>';
        }
        updatehtmlContent.push(listMessages);
        listMessages = '';
        return updatehtmlContent;
        if (count == jsonarrays.length){
          latestconversation_Id = row.conversation_id;
        }
              });
}


loadData();
function loadData(){
  var action = 'loadSessionData';
  $.ajax({
    url: 'dist/php/sessionData.php',
    type: 'POST',
    data: {
        action: action
    }
    , dataType:"json"
    , success: function(data) {
  //to conver it to Json after contactit as string  to be used together
  currentUserId = data.currentUserId;
  currentUserName = data.currentUserName;
  currentUserPic = data.currentUserPic;
  }
   });
}
function viewLatestMessages(){
var action = 'viewLatestMessages';
$.ajax({
  url: 'dist/php/chats.php',
  type: 'POST',
  data: {
      action: action
  }
  , dataType:"json"
  , success: function(data) {
//to conver it to Json after contactit as string  to be used together
jsonarrayForUpdate = data;
if (typeof jsonarrayForUpdate != "undefined" && jsonarrayForUpdate != null && jsonarrayForUpdate.length != null && jsonarrayForUpdate.length > 0 ){
handleJsonviewLatestMessages(jsonarrayForUpdate);
var htmlToAppend2 = document.getElementById("direct-chat-contacts");
htmlToAppend2.innerHTML = updatehtmlContent.join('');
}

}
 });
}
function setLatestChatid(){
var action = 'setLatestChatid';
$.ajax({
  url: 'dist/php/chats.php',
  type: 'POST',
  data: {
      action: action
  }
  , dataType:"json"
  , success: function(data) {
//this is to set session of last conversation_id;
//to conver it to Json after contactit as string  to be used together
if (data != null || data != undefined){
// $('#chatMessage').focus();
}
}

 });
}
function setClickedChatid(element){
var action = 'setClickedChatid';
$.ajax({
  url: 'dist/php/chats.php',
  type: 'POST',
  data: {
      action: action
      ,element: element
  }
  , dataType:"json"
  , success: function(data) {
$('.fas.fa-comments').click();//this is to set session of last conversation_id;

//to conver it to Json after contactit as string  to be used together
}

 });
}
 setLatestChatid();
 viewLatestMessages();
viewLatestChatYouOpenedOnLoad();

function updateReadenMessagesOnCurrentChat(){
  var action = 'updateReadenMessagesOnCurrentChat';
  $.ajax({
    url: 'dist/php/chats.php',
    type: 'POST',
    data: {
        action: action
    }
    , success: function(data) {


  //to conver it to Json after contactit as string  to be used together
}
});
}
function viewLatestChatYouClicked(element){
var action = 'viewClickedChat';
$.ajax({
  url: 'dist/php/chats.php',
  type: 'POST',
  data: {
      action: action
  }
  , dataType:"json"
  , success: function(data) {

//to conver it to Json after contactit as string  to be used together
jsonarrays = data;
$('#chatMessage').focus();
if (typeof jsonarrays != "undefined" && jsonarrays != null && jsonarrays.length != null && jsonarrays.length > 0 ){
displayUserPanel(jsonarrays);

handleJsonviewLatestChatYouopen(jsonarrays);
var htmlToAppend = document.getElementById("direct-chat-messages");
htmlToAppend.setAttribute("data-conversation_id", btoa(element));
htmlToAppend.innerHTML = arrayhtmlContent.join('');
//to scroll to bottom
var objDiv = htmlToAppend;
objDiv.scrollTop = objDiv.scrollHeight;
$('#chatMessage').focus();
try {
  setTimeout(updateReadenMessagesOnCurrentChat, 3000);
} catch (e) {

}

}
}
 });
}
//to view latestMessages
function viewLatestChatYouOpenedOnLoad(){
var action = 'viewLatestChatOnLoad';
$.ajax({
  url: 'dist/php/chats.php',
  type: 'POST',
  data: {
      action: action
  }
  , dataType:"json"
  , success: function(data) {

//to conver it to Json after contactit as string  to be used together
var jsonarrays = data['rows'];

if (typeof jsonarrays != "undefined" && jsonarrays != null && jsonarrays.length != null && jsonarrays.length > 0 ){
displayUserPanel(data['new']);

handleJsonviewLatestChatYouopen(jsonarrays);
var htmlToAppend = document.getElementById("direct-chat-messages");
htmlToAppend.setAttribute("data-conversation_id", btoa(jsonarrays[0]['conversation_id']));
htmlToAppend.innerHTML = arrayhtmlContent.join('');
//to scroll to bottom
var objDiv = htmlToAppend;
objDiv.scrollTop = objDiv.scrollHeight;
setTimeout(updateReadenMessagesOnCurrentChat, 1500);
setTimeout(viewLatestMessages, 1500);

}
}
 });
}
function loadlistTasks() {
    var idt = '';
    $.ajax({
        url: 'dist/php/projectsNotifications.php'
        , type: 'POST'
        , data: {
            idt: idt
        }
        , dataType: "json"
        , success: function (data) {
            $('#todolistP').html(data.notification2);
          //  $('#projectsList').html(data.projects);

//$("#projectsList").pagify(9, ".projectlist");
      //  $("#todolistP").pagify(9, ".projectTask","pagination");
        pagifyli("#projectsList .projectTask",5,"#pagination");


        }
    });
}
function loadlistProjects() {
    var idt = '';
    $.ajax({
        url: 'dist/php/projectList.php'
        , type: 'POST'
        , data: {
            idt: idt
        }
        , dataType: "json"
        , success: function (data) {
      $('#projectsList').html(data.lists);
      pagifyli("#projectsList .project",5,"#pagination2");


        }
    });
}
function loadVacationsSummary() {
    var action = 'loadVacationsSummary';
    $.ajax({
        url: 'dist/php/vacations.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , dataType: "json"
        , success: function (data) {
if (data[0] > 0){
  $('#sickVn').html(data[0]);
}else{
  $('#sickVn').html('0');
}
if (data[1] > 0){
  $('#yearVn').html(data[1]);
}else{
  $('#yearVn').html('0');
}
if (data[2] > 0){
  $('#vacationN').html(data[2]);
}else{
  $('#vacationN').html('0');
}

        }
    });
}
var tableContent;
var tableContent2;
var tableContent3;

function loadVacationsData() {
    var action = 'loadVacationsData';
    $.ajax({
        url: 'dist/php/vacations.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , beforeSend: function (data) {
          tableContent = $('#vacationsTableDiv').html();
          $('#vacationsTableDiv').html('');
          $('#tablesBody').html(tableContent);
        }
        , dataType: "json"
        , success: function (data) {
          $('#tablesLabel').html('جدول الإجازات');

          //all tables process //////////////
          $('#vacationsTbody').html('')

          var t = defineTable('#vacationsTable');
          t.clear().draw();
              var myData = data;
              if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
                var count = 0;
                let rdata = myData.map(function (o) {
                    //o.priority = "asma";
                    switch (o.status) {
                    case '0':
                        o.status = 'جاري الانتظار';
                        break;
                        case '1':
                            o.status = 'مكتمل';
                            break;
                    case '2':
                        o.status = 'مرفوض';
                        break;

                    }
                    return o;
                });
                rdata.forEach(function(rowd) {
                  count = count + 1;

                    var row = t.row.add([count, rowd.Notes , rowd.vName , rowd.status , '']).draw().node();

                    row.id = count;
                  return row;

                });

              }else {
                  var t = defineTable('#vacationsTable');
                  t.clear().draw();
                }

}
});
}
function requestsCounter() {
    var action = 'requestsCounter';
    $.ajax({
        url: 'dist/php/dashboard.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , dataType: "json"
        , success: function (data) {

$('#allr').html(data['All']);
$('#new').html(data['new']+'طلبات');
$('#handeled').html(data['handeled']+'من');
$('#precentage').html(data['precentage']);

}
});
}
function ShowemployeeInfo() {
    var action = 'ShowemployeeInfo';
    $.ajax({
        url: 'dist/php/dashboard.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , dataType: "json"
        , success: function (data) {
//
$('#endCard').html(''+data[0].idEnd+'');
$('#vacationNext').html(''+data[0].vacationDate+'');

$('#joinDate').html(''+data[0].datebegin+'');

}
});
}
ShowemployeeInfo();
requestsCounter();
function loadSalaryLogsData() {
    var action = 'loadSalaryLogsData';
    $.ajax({
        url: 'dist/php/dashboard.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , beforeSend: function (data) {
          tableContent3 = $('#salaryTableDiv').html();
          $('#salaryTableDiv').html('');
          $('#tablesBody').html(tableContent3);
        }
        , dataType: "json"
        , success: function (data) {


var myData = data;
          //all tables process //////////////
          $('#salaryTbody').html('');

          var t = defineTable('#salaryTable');
          t.clear().draw();
          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0 ){
var count = 0;
                myData.forEach(function(rowd) {
                  count = count + 1;
                    var row = t.row.add([count, rowd.amount , rowd.issuedDate , '']).draw().node();

                  return row;

                });
              }else{
                var t = defineTable('#salaryTable');
                t.clear().draw();
              }



}
});
}
function loadLogsData(elementID) {
    var action = 'loadLogsData';
    $.ajax({
        url: 'dist/php/dashboard.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , beforeSend: function (data) {
          tableContent2 = $('#logsTableDiv').html();
          $('#logsTableDiv').html('');
          $('#tablesBody').html(tableContent2);
        }
        , dataType: "json"
        , success: function (data) {


if(elementID == 'bonus'){
myData = data['bonus'];
$('#tablesLabel').html('جدول البونس');
}
if(elementID == 'deduct'){
myData = data['deduct'];
$('#tablesLabel').html('جدول الخصم');

}
if(elementID == 'appraisals'){
myData = data['appraisals'];
$('#tablesLabel').html('جدول التقديرات');

}
          //all tables process //////////////
          $('#logsTbody').html('');

          var t = defineTable('#logsTable');
          t.clear().draw();
          if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0 ){
var count = 0;
                myData.forEach(function(rowd) {
                  count = count + 1;
                    var row = t.row.add([rowd.pt , rowd.reason , rowd.issuedDate , rowd.amountPayment , '']).draw().node();

                  return row;

                });
              }



}
});
}
function countLogsData() {
    var action = 'loadLogsData';
    $.ajax({
        url: 'dist/php/dashboard.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , dataType: "json"
        , success: function (data) {
var bonusc = 0;
var deductc = 0;
var appraisalsc = 0;

if(data['bonus'].length > 0){
bonusc = data['bonus'].length;
}
if(data['deduct'].length > 0){
deductc = data['deduct'].length;
}
if(data['appraisals'].length > 0){
appraisalsc = data['appraisals'].length;
}
$('#innerbonus').html(''+bonusc+'');
$('#innerdeduct').html(''+deductc+'');
$('#innerappraisals').html(''+appraisalsc+'');


}
});
}
function countLogsSalaryData() {
    var action = 'loadSalaryLogsData';
    $.ajax({
        url: 'dist/php/dashboard.php'
        , type: 'POST'
        , data: {
            action: action
        }
        , dataType: "json"
        , success: function (data) {
var salaries = 0;

salaries = data.length;

$('#innersalary').html(''+salaries+'');


}
});
}
loadVacationsSummary();
countLogsSalaryData();
// function fillAssignedToSelect(){
//
//   $.ajax({
//       url: 'dist/php/assignedToSelect.php'
//       , type: 'POST'
//       , dataType: "json"
//       , success: function (data) {
//         var el = document.querySelector('.assignedTo');
//         if (!el.classList.contains('custom-select')){
//         el.classList = 'assignedTo custom-select';
//       }
//         var text;
//         var i = 0;
//         data.forEach(row => {
// if (i==0){
//   text = '<option value="'+btoa(row.id)+'">'+row.name+'</option>';
//
// }else{
//   text += '<option value="'+btoa(row.id)+'">'+row.name+'</option>';
//
// }
// i = i + 1;
//             });
//         el.innerHTML = ''+text+'';
//
//
//       }
//   });
// }

$('#btn_upload').click(function() {
  uploadFiles('userfile','#documentsm','../../upload/vacations/');


});
$(document).on('click', 'button.documentdelete', function(event) {
  var mine = event.target;
  var formID = $(mine).closest('form')[0].id;
  var link;

link = '../../upload/vacations/';

var fileToDelete = $(mine).data('filename');
deleteFile(link,fileToDelete,mine);

});
function sendSickVacation(fd){
  $.ajax({
    url: 'dist/php/vacations.php',
    type: 'post',
    data: fd,
    contentType: false,
    processData: false,
    success: function(response) {

      if (response == 0) {
        $('#rVacationsModal').modal('hide');
        $('#rvForm')[0].reset();
        $('#documentsm').html('');
        var maessageResponse = 'تم إرسال الطلب بنجاح';
        showMessageSuccess(maessageResponse);
        return;
      } else {
        var maessageResponse = 'لم يتم إرسال الطلب بنجاح';
        showMessageError(maessageResponse);
        return;
      }


    }
  });
}
function sendYearVacation(fd){
  $.ajax({
    url: 'dist/php/vacations.php',
    type: 'post',
    data: fd,
    contentType: false,
    processData: false,
    success: function(response) {
      if (response == 0) {
        $('#rVacationsModal').modal('hide');
        $('#rvForm')[0].reset();
        $('#documentsm').html('');
        var maessageResponse = 'تم إرسال الطلب بنجاح';
        showMessageSuccess(maessageResponse);
        return;
      } else {
        var maessageResponse = 'لم يتم إرسال الطلب بنجاح';
        showMessageError(maessageResponse);
        return;
      }


    }
  });
}

loadlistProjects();
loadlistTasks();
$(document).on('click', '.conversation-list', function (event) {
  var element = $(this).attr('data-conversation_id');
setClickedChatid(element);
viewLatestChatYouClicked(element);
});
$('#sickV').click(function (event) {
event.preventDefault();
$('#rVacationsModal').modal('show');
$('#rVacationsLabel').html('طلب إجازة مرضية');
});
$('#yearV').click(function (event) {
event.preventDefault();
$('#rVacationsModal').modal('show');
$('#rVacationsLabel').html('طلب إجازة سنوية');
});

$('#rVacationsModal').on('submit', '#rvForm', function(event) {
event.preventDefault();
var targ = event.target;
var fd = new FormData();
var poData = jQuery(targ).serializeArray();
jQuery.each(poData, function(i, pd) {
  fd.append('' + pd.name + '', pd.value);
});
var uploadedFiles = document.querySelectorAll("button.documentdelete");
var allFilesNames = [];
uploadedFiles.forEach((item) => {
	var el = item.getAttribute('data-filename');
allFilesNames.push(el);
});
//this the uploaded files before
fd.append('allFilesNames', allFilesNames);
var modalTitle = $('#rVacationsLabel').html();
if($('#noteV').val() == null || $('#noteV').val() == 'undefined' || $('#noteV').val() == ''  ){
showMessageError('لم تكتب تفاصيل الاجازة');
  return;
}
if(modalTitle == 'طلب إجازة مرضية'){
  var action = 'sendSickVacation';
  fd.append('action', action);
  sendSickVacation(fd);
  return;
}
if(modalTitle == 'طلب إجازة سنوية'){
  var action = 'sendYearVacation';
  fd.append('action', action);
  sendYearVacation(fd);
  return;
}

});

$('#vacationsInfo').click(function (event) {
  event.preventDefault();
loadVacationsData();
$('#tablesModal').modal('show');



});
$('#salary').click(function (event) {
  event.preventDefault();

loadSalaryLogsData();
$('#tablesModal').modal('show');
$('#tablesLabel').html('جدول الرواتب');


});

$(document).on('click','a.logs', function(event) {
  event.preventDefault();
  var elementID = $(this).attr('id');
loadLogsData(''+elementID+'');
$('#tablesModal').modal('show');


});
//loadLogsData('');
$(document).on('submit','#myForm',function(event) {
event.preventDefault();

});
$(document).on('hidden.bs.modal','#tablesModal', function(event) {
  event.preventDefault();
var title = $('#tablesLabel').html();
if (title == 'جدول البونس'|| title == 'جدول الخصم'|| title == 'جدول التقديرات'){
  $('#logsTableDiv').html(tableContent2);
}
if (title == 'جدول الإجازات'){
  $('#vacationsTableDiv').html(tableContent);

}

if (title == 'جدول الرواتب'){
  $('#salaryTableDiv').html(tableContent3);

}

});
$(document).on('keyup', '#chatMessage', function(event) {
  event.preventDefault();
    if (event.keyCode === 13) {
event.preventDefault();
var textMessage = document.getElementById("chatMessage").value;
if (textMessage == null || textMessage == undefined || textMessage === ""){
  return;
}
var fdm = new FormData();
var conversation_id = $('#direct-chat-messages').attr('data-conversation_id');
fdm.append('conversation_id', conversation_id);
var formData = jQuery('#myForm').serializeArray();
jQuery.each(formData, function(i, fd) {
  fdm.append('' + fd.name + '', fd.value);
});

//message

$.ajax({
  url: 'dist/php/chats.php',
  type: 'post',
  data: fdm
  ,contentType: false,
  processData: false,
  success: function(response) {
    $('#myForm')[0].reset();
    var htmlToAppend = document.getElementById("direct-chat-messages");

        var node = document.createElement("div");
        var nodeAttribute = node.setAttribute("class","direct-chat-msg right")
           // Create a <button> element
        node.innerHTML = '<div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+currentUserName+'</span><span class="direct-chat-timestamp float-left">'+new Date().getTime()+'</span></div><img class="direct-chat-img" src="dist/img/'+currentUserPic+'" alt="message user image"><div class="direct-chat-text">'+textMessage+'<i class="fas fa-check"></i></div>';                   // Insert text

        htmlToAppend.appendChild(node);
  htmlToAppend.scrollTop = htmlToAppend.scrollHeight;



}
 });
}
});
countLogsData();
