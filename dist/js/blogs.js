function getParameterValueFromUrl(parameterName){
  var currentUrl = window.location.href;
var url = new URL(currentUrl);
var parameterValue = url.searchParams.get(''+parameterName+'');
console.log(parameterValue);
return parameterValue;
}
function showTimeline(){
  var action = 'showProfileTimeline';
  console.log('has been call');
  let uid;
  if (window.location.href.indexOf("uid") > -1) {
   uid = getParameterValueFromUrl('uid');
}
  $.ajax({
      url: '../dist/php/employees.php'
      , type: 'POST'
      , data: {
        uid: uid,
          action: action
      }
      , dataType: "json"
      , success: function (data) {
        var mydata = data;
        var text = '';
        data.forEach((data) => {
          if (!data.activityName.includes('شكر')){
                  text += '<div class="time-label"><span class="bg-danger">'+
                  data.date+'</span></div><div><i class="fas fa-user bg-info"></i><div class="timeline-item"><h3 class="timeline-header border-0">'+
                  data.activityName+'</h3></div></div>';
          }else{

          let documentsHTML = '';
          var documents = data.documents;
          if (documents != null || documents != 'undefined') {
            let documentsValue = [];
            if(documents.includes(',') == true){

              let array = documents.split(',');
              documentsHTML = '';
              var i = 0;
            //  documentsHTML = '<img src="../upload/hr/certifications/'+array[0]+'" alt="..." width="150" height="100">'

            while (i < array.length) {
              if ( i == 0){
                documentsHTML = '<img src="../upload/hr/certifications/'+array[i]+'" alt="..." width="150" height="100">'

              }else{
                documentsHTML += '<img src="../upload/hr/certifications/'+array[i]+'" alt="..." width="150" height="100">'

              }
              i++;
            }

            }else{
              documentsHTML = '<img src="../upload/hr/certifications/'+documents+'" alt="..." width="150" height="100">'

            }
          }
            text += '<div class="time-label"><span class="bg-danger">'+
            data.date+'</span></div><div><i class="fas fa-camera bg-purple"></i><div class="timeline-item"><h3 class="timeline-header"><a href="#">'+
            data.activityName+'</a><br><br>'+'<div class="timeline-body">'+documentsHTML+'</div></h3></div></div>';
          }
        });


$('#timelinediv').html(text+'<div><i class="far fa-clock bg-gray"></i></div>');
checklink();
      }
  });
}
showTimeline();
function showAllrelatedEmpInfo(){
  console.log('has been call');
  var action = 'showAllProfile';
  let uid;
  if (window.location.href.indexOf("uid") > -1) {
   uid = getParameterValueFromUrl('uid');
}
  $.ajax({
      url: '../dist/php/employees.php'
      , type: 'POST'
      , data: {
          uid: uid,
          action: action
      }
      , dataType: "json"
      , success: function (data) {
var el = document.getElementById("profileInfor");
        el.innerHTML = '<div class="text-center"><img class="profile-user-img img-fluid img-circle" src="../dist/img/'+data.userPic+'"'+'alt="User profile picture"></div><h3 class="profile-username text-center">'+data.username+'</h3><p class="text-muted text-center">'+data.jobTitle+'-'+data.departmentName+'</p>';
        var el2 = document.getElementById("aboutMe");
        el2.innerHTML = '<strong><i class="fas fa-book mr-1"></i> التعليم</strong><p class="text-muted">'+data.education+'</p><hr><strong><i class="fas fa-pencil-alt mr-1"></i> المهارات</strong><p class="text-muted"><span class="tag tag-danger">'+
        data.skills+'</span></p><hr><strong><i class="far fa-file-alt mr-1"></i> ملاحظات</strong><p class="text-muted">'+
        data.userNote+'</p><hr>';
      }
  });
}
showAllrelatedEmpInfo();

function get_time_diff( datetime )
{
    var datetime = typeof datetime !== 'undefined' ? datetime : "2014-01-01 01:02:03.123456";

    var datetime = new Date( datetime ).getTime();
    var now = new Date().getTime();

    if( isNaN(datetime) )
    {
        return "";
    }

    console.log( datetime + " " + now);

    if (datetime < now) {
        var milisec_diff = now - datetime; //always here with blogs
    }else{
        var milisec_diff = datetime - now;
    }

    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));

    var date_diff = new Date( milisec_diff );
    var dateCreated = new Date( datetime );

    if(days <= 0){
      return "today at "+  dateCreated.getHours() + " : " + dateCreated.getMinutes() + " : " + dateCreated.getSeconds();


    }else{
      return "since " +  days + "days at "+  dateCreated.getHours() + " : " + dateCreated.getMinutes() + " : " + dateCreated.getSeconds();

    }
}


function displayAllBlogsWithComments(){
  var action = 'viewMyBlogs';
  let uid;
  if (window.location.href.indexOf("uid") > -1) {
   uid = getParameterValueFromUrl('uid');
}
  //action  sendComments
  $.ajax({
    url: '../dist/php/blogs.php',
    type: 'post',
    data: {
      uid: uid,
        action: action
    }
    , dataType: "json"
    , success: function(data) {

      var jsonarrays = data['blogs'];
      var i = 0;
      var length =  Object.keys(jsonarrays).length;
      jsonarrays.forEach(rowd => {
        var datetime = rowd.dateCreated;
var htmlToAppend = document.getElementById("MainDivPosts");
var div = document.createElement('div');
div.classList = "post"; ////***** here div to search
div.id = "post"+rowd.blogid;
var divBlogger = document.createElement('div');
divBlogger.classList = "user-block";
divBlogger.innerHTML = ' <img class="img-circle img-bordered-sm" src="../dist/img/'+rowd.partcipantPic+'" width="32px;" height="32px;" alt="user image">'+
'<span class="username">'+
'<a href="#">'+rowd.participantName+'</a>'+
'<a href="#" class="deleteBlog float-right btn-tool"><i class="fas fa-times"></i></a>'+
'</span><span class="description"> Shared publicly -'+get_time_diff( datetime );+'</span>';
var pMessage = document.createElement('p');
pMessage.classList = 'bID';
pMessage.dataset.blogid = rowd.blogid;

if ( i == 0){
pMessage.id = 'lastblogid';
pMessage.tabindex = 0;

console.log('========');
}

pMessage.innerHTML = ""+'<h3>'+rowd.title+'</h3><br>'+rowd.message+"";
var pcomments = document.createElement('p');
var counter;

pcomments.innerHTML = '<span class="float-right"><a href="" class="text-sm"><i class="far fa-comments commentsLink mr-1">عدد التعليقات</i><span class="commentsCounter" data-counter="'+rowd.commentsCounter+'">'+ rowd.commentsCounter +'</span></a></span></br>';
var form = document.createElement("form");
form.classList = "commentsForm";
form.action = "";
var input = document.createElement("input");
input.classList = "sendComments form-control form-control-sm";
input.name = "comment";
input.type = "text";
input.placeholder = "اكتب تعليقك";
var commentsDiv = document.createElement('div');
commentsDiv.classList = "commentsDiv";
function apppendFirst(){

  div.appendChild(divBlogger);
  div.appendChild(pMessage);

  div.appendChild(pcomments);
  div.appendChild(commentsDiv);

  form.appendChild(input);

  div.appendChild(form);

  htmlToAppend.appendChild(div);
  var imageList = pMessage.querySelectorAll('img');
  imageList.forEach(imagel =>{
imagel.classList = "blogImage";
  });


}

var promise = (new Promise(apppendFirst)).then(_ => {
  // Autoplay started!
  console.log('started1');

}).catch(error => {
  // Autoplay was prevented.
  // Show a "Play" button so that user can start playback.
});
i = i + 1 ;
});
checklink();
}
});


}
$(document).on('keyup', '.sendComments', function(event) {
  event.preventDefault();
  console.log('ggg');
    if (event.keyCode === 13) {
      event.preventDefault();
      var formData = $(this).closest('.commentsForm').serializeArray();
      var bID = $(this).closest('div.post').find('p.bID').attr('data-blogid');
      console.log(bID);
      var action = 'sendComment';
      var fdm = new FormData();
      jQuery.each(formData, function(i, fd) {
        fdm.append('' + fd.name + '', fd.value);
      });
      fdm.append('action',action);
      fdm.append('bID',bID);

      //action  sendComments
      $.ajax({
        url: '../dist/php/blogs.php',
        type: 'post',
        data: fdm
        ,contentType: false,
        processData: false,
        success: function(response) {
          let uid;
          if (window.location.href.indexOf("uid") > -1) {
           uid = getParameterValueFromUrl('uid');
           window.location.replace('profile.php?uid='+uid);

        }else{
          window.location.replace("profile.php");
        }
          $('#lastblogid').focus();

    }
    });

        // Cancel the default action, if needed
        // Trigger the button element with a click
        console.log("enter has ben clicked");
        // jQuery.each(formData, function(i, fd) {
        //   fdm.append('' + fd.name + '', fd.value);
        // });
        //action  sendComments
      }
  });
  $(document).on('submit', '.commentsForm', function(event) {
    event.preventDefault();
  console.log('stop submit');
    });
    $(document).on('click', '.commentsLink', function(event) {
      event.preventDefault();
      var targetel = event.target;
      var counter = $(targetel).closest('div').find('span.commentsCounter')[0].innerHTML;
      if(parseInt(counter)> 0 ){

      var element =  $(targetel).closest('div').find('p.bID').attr('data-blogid');

      if (targetel.classList.contains('isOpened')){
        targetel.classList.remove("isOpened");
        targetel.classList.toggle("isClosed");
        var targetcommentsDiv = $('#post'+ element).find('div.commentsDiv')[0];
        targetcommentsDiv.innerHTML = '';
      }else{

      targetel.classList.remove("isClosed");

      targetel.classList.toggle("isOpened");

var action = "displayComments";
$.ajax({
  url: '../dist/php/blogs.php',
  type: 'GET',
  data: {
       element: element
      ,action: action
  }
  , dataType: "json"
  , success: function(data) {
    if (data != -1){
    var targetcommentsDiv = $('#post'+ element).find('div.commentsDiv')[0];
    console.log(targetcommentsDiv);
    var jsonarrays = data;
    var i = 0;
    jsonarrays.forEach(rowd => {
      var datetime = rowd.dateCreated;
var div = document.createElement('div');
div.classList = "post";
var divBlogger = document.createElement('div');
divBlogger.classList = "user-block";
divBlogger.innerHTML = ' <img class="img-circle img-bordered-sm" src="../dist/img/'+rowd.partcipantPic+'" width="32px;" height="32px;" alt="user image">'+
'<span class="username">'+
'<a href="#">'+rowd.participantName+'</a>'+
'</span><span class="description"> Shared publicly -'+get_time_diff( datetime );+'</span>';
var pMessage = document.createElement('p');
pMessage.classList = 'comment';
pMessage.id = rowd.id;
pMessage.innerHTML = rowd.comment;
console.log( rowd.comment);
function apppendFirst(){

div.appendChild(divBlogger);
div.appendChild(pMessage);

targetcommentsDiv.appendChild(div);

}

var promise = (new Promise(apppendFirst)).then(_ => {
// Autoplay started!
console.log('started1');

}).catch(error => {
// Autoplay was prevented.
// Show a "Play" button so that user can start playback.
});

});
}
}

});

} //else block
}else{
  return;

}
//ifblock e
});

function checklink(){
  let uid;
  if (window.location.href.indexOf("uid") > -1) {
    $('.contacts-section').hide();
    $('#writing').hide();
    $('a.deleteBlog').hide();

}else{
  document.getElementById("blogTitleSendDiv").style.display = 'block';
}
}
checklink();
function showMessageError(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'error',
    title: maessageResponse,
    showConfirmButton: false,
    timer: 1500
  })
}
function showMessageSuccess(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'success',
    title: maessageResponse,
    showConfirmButton: true,
    timer: 1500
  })
}

$(document).ready(function(){
  displayAllBlogsWithComments();



$(document).on('click','a.deleteBlog',function(event) {
event.preventDefault();
var bID = $(this).closest('div.post').find('p.bID').attr('data-blogid');
var thisel = $(this).closest('div.post');
var action = 'deleteBlog';
var fdm = new FormData();
fdm.append('bID',bID);
fdm.append('action',action);
$.ajax({
  url: '../dist/php/blogs.php',
  type: 'post',
  data: fdm
  ,contentType: false,
  processData: false,
  success: function(data) {
    if(data == 0){
      showMessageSuccess('تمت العملية بنجاح');
      thisel.remove();

   }else{
     showMessageError("تحقق من صحة البيانات أو الانترنت لديك");

   }

  }
});
});

$('#sendblog').click(function(event) {
event.preventDefault();
var val = $('#lastblogid').attr('data-blogid');
var title = $('#blogTitleSend').val();
var fdm = new FormData();
var formData = jQuery('#blogForm').serializeArray();
jQuery.each(formData, function(i, fd) {
  fdm.append('' + fd.name + '', fd.value);
});
fdm.append('lastblogid',val);
fdm.append('title',title);

if (typeof title == null || title == 'undefined'|| title == ''){
showMessageError('يجب كتابة العنوان الخاص بالمقالة');
  return;
}
var blogArea = $('#blogArea').val();
if (typeof blogArea == null || blogArea == 'undefined'|| blogArea == ''){
showMessageError('يجب كتابة المقالة');
  return;
}
//message
$.ajax({
  url: '../dist/php/blogs.php',
  type: 'post',
  data: fdm
  ,contentType: false,
  processData: false,
  success: function(response) {

    window.location.replace("profile.php");
    $('#lastblogid').focus();





}
 });
});
});
