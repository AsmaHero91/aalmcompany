function checkauthority(){
  var action = 'checkAuthority';
  $.ajax({
    url: '../dist/php/sessionData.php',
    type: 'POST',
    data: {
        action: action
    }
    , dataType: "json"
    , success: function(data) {
      if (data == 1){
$('#admin-section').css('display','block');
$('#midadmin-section').css('display','block');

}
if (data == 2){
$('#midadmin-section').css('display','block');
$('#admin-section').html('');
}
 if (data == 3){
  $('#admin-section').html('');
  $('#midadmin-section').html('');

}
}
});
}
function displayUserPanelN(jsonarrays){
  var image = document.getElementById("receiverpicm");
  image.src = '../dist/img/'+jsonarrays.receiverpic;
  document.getElementById("receivernamem").innerText = jsonarrays.receivername;
  if(jsonarrays.status === 'online'){
  $('a#statusr2').html('<i class="fa fa-circle text-success"></i> متصل');
}else{
  $('a#statusr2').html('<i class="fa fa-circle text-offline"></i> غير متصل');

}

}

function updateReadenMessagesOnCurrentClicked(el){
  var action = 'updateReadenMessagesOnCurrentChate';
  $.ajax({
    url: '../dist/php/chats.php',
    type: 'POST',
    data: {
      element: el
        ,action: action
    }
    , success: function(data) {

  //to conver it to Json after contactit as string  to be used together
}
});
}
var arrayhtmlContent;
function handleJsonviewLatestChatClicked(jsonarrays){

    arrayhtmlContent = [];
    jsonarrays.forEach(row => {
          var currentchatlogid = [];
          if(row.fromUserId == currentUserId ) //this is a sender
          {
            if(row.readen == 0 ){
              messageChat =   '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+currentUserName+'</span><span class="direct-chat-timestamp float-left">'+row.timestamp+'</span></div><img class="direct-chat-img" src="../dist/img/'+currentUserPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'<i class="fas fa-check"></i></div></div>';

            }else{
              messageChat =   '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+currentUserName+'</span><span class="direct-chat-timestamp float-left">'+row.timestamp+'</span></div><img class="direct-chat-img" src="../dist/img/'+currentUserPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'  <i class="fas fa-check"></i><i class="fas fa-check"></i></div></div>';

            }
          }else{
            if(row.readen == 0 ){
              messageChat =   '<div class="direct-chat-msg"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">'+row.participantUsername+'</span><span class="direct-chat-timestamp float-right">'+row.timestamp+'</span></div><img class="direct-chat-img" src="../dist/img/'+row.participantPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'<i class="fas fa-check"></i></div></div>';

  }else{
    messageChat =   '<div class="direct-chat-msg"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">'+row.participantUsername+'</span><span class="direct-chat-timestamp float-right">'+row.timestamp+'</span></div><img class="direct-chat-img" src="../dist/img/'+row.participantPic+'" alt="message user image"><div class="direct-chat-text">'+row.chatMessage+'<i class="fas fa-check"></i><i class="fas fa-check"></i></div></div>';

  }                                    //this is a reciver

          }
          arrayhtmlContent.push(messageChat);
          messageChat = '';
          return arrayhtmlContent;
                });
}

function viewClickedChatCID(el){

  var action = "viewClickedChatel";
  $.ajax({
    url: '../dist/php/chats.php',
    type: 'post',
    data: {
         element: el
        ,action: action
    }
    , dataType: "json"
    , success: function(data) {
      
      $('#chatModal').modal('show');
      var dm = document.getElementById("direct-chat-messages2");
  dm.setAttribute('data-conversation_id',el);

      //to conver it to Json after contactit as string  to be used together
      if (data['new'] != null){
        var jsonarrays = data['new'];
        displayUserPanelN(jsonarrays);


      }
      var jsonarrays = data['rows'];
      if (typeof jsonarrays != "undefined" && jsonarrays != null && jsonarrays.length != null && jsonarrays.length > 0 ){
        handleJsonviewLatestChatClicked(jsonarrays);
        var htmlToAppend = document.getElementById("direct-chat-messages2");
        htmlToAppend.setAttribute("data-conversation_id", jsonarrays[0]['conversation_id']);
        htmlToAppend.innerHTML = arrayhtmlContent.join('');
        //to scroll to bottom
        var objDiv = htmlToAppend;
        objDiv.scrollTop = objDiv.scrollHeight;
        setTimeout(updateReadenMessagesOnCurrentClicked, 3000, jsonarrays[0]['conversation_id']); // Hello, John

      }

    }, error: function(err) {
      console.log('err'+err);
  }
   });
}

function viewClickedChatID(el){
  var action = "viewClickedChat";
  $.ajax({
    url: '../dist/php/chats.php',
    type: 'post',
    data: {
         element: el
        ,action: action
    }
    , dataType: "json"
    , success: function(data) {
      console.log(data);
      $('#chatModal').modal('show');
      var dm = document.getElementById("direct-chat-messages2");

      //to conver it to Json after contactit as string  to be used together
      if (data['new'] != null){
        var jsonarrays = data['new'];
        displayUserPanelN(jsonarrays);
        var el = jsonarrays.receiverid;
         dm.setAttribute('data-conversation_id',btoa(currentUserId+'-'+el));


      }
      var jsonarrays = data['rows'];
      if (typeof jsonarrays != "undefined" && jsonarrays != null && jsonarrays.length != null && jsonarrays.length > 0 ){
        handleJsonviewLatestChatClicked(jsonarrays);
        var htmlToAppend = document.getElementById("direct-chat-messages2");
        htmlToAppend.setAttribute("data-conversation_id", jsonarrays[0]['conversation_id']);
        htmlToAppend.innerHTML = arrayhtmlContent.join('');
        //to scroll to bottom
        var objDiv = htmlToAppend;
        objDiv.scrollTop = objDiv.scrollHeight;
        var el = jsonarrays[0]['conversation_id'];
        updateReadenMessagesOnCurrentClicked(el);
      //  setTimeout(updateReadenMessagesOnCurrentClicked, 3000, jsonarrays[0]['conversation_id']); // Hello, John

      }



    }, error: function(err) {
      console.log('err'+err);
  }
   });
}
function get_time_diff( datetime )
{
    var datetime = typeof datetime !== 'undefined' ? datetime : "2014-01-01 01:02:03.123456";

    var datetime = new Date( datetime ).getTime();
    var now = new Date().getTime();

    if( isNaN(datetime) )
    {
        return "";
    }


    if (datetime < now) {
        var milisec_diff = now - datetime; //always here with blogs
    }else{
        var milisec_diff = datetime - now;
    }

    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));

    var date_diff = new Date( milisec_diff );
    var dateCreated = new Date( datetime );

    if(days <= 0){
      return "today at "+  dateCreated.getHours() + " : " + dateCreated.getMinutes() + " : " + dateCreated.getSeconds();


    }else{
      return "since " +  days + "days at "+  dateCreated.getHours() + " : " + dateCreated.getMinutes() + " : " + dateCreated.getSeconds();

    }
}

function viewClickedBlogAlert(element){
  var action = 'ViewAndupdateReadenBlogs';
  //action  sendComments
  $.ajax({
    url: '../dist/php/chats.php',
    type: 'post',
    data: {
      element: element
      ,action: action
    }
    , dataType: "json"
    , success: function(data) {
//     alert(JSON.stringify(data['comments']));
      var jsonarrays = data;
      var i = 0;
      var text = '';
      var htmlToAppend = document.getElementById("MainDivPostsm");
text = '<h4><strong>'+jsonarrays.title+'</strong></h4><p>'+jsonarrays.message+'</p>';


htmlToAppend.innerHTML = text;
var imageList = htmlToAppend.querySelectorAll('img');
imageList.forEach(imagel =>{
imagel.classList = "blogImage";
});
}
});


}

function load_unseen_notification() {
    var idt = '';
    console.log('has been call');
    $.ajax({
        url: '../dist/php/notifications.php'
        , type: 'POST'
        , data: {
            idt: idt
        }
        , dataType: "json"
        , success: function (data) {
            $('#dropdown-menu2').html(data.notification);
            $('.countbdg1').html(data.unseen_notification);
            $('.countbdg2').html(data.unseen_notification);
        }
    });
}
function load_unseen_notification2() {
    var idt = '';
    $.ajax({
        url: '../dist/php/projectsNotifications.php'
        , type: 'POST'
        , data: {
            idt: idt
        }
        , dataType: "json"
        , success: function (data) {
        //  alert(JSON.stringify(data));
            $('#dropdown-menu3').html(data.notification);
            $('.countbdg3').html(data.unseen_notification);
            $('.countbdg4').html(data.unseen_notification);
  }
    });
}



var alertUnreadListMessagesText;
var alertUnreadListMessagesArray;
function alertUnreadListMessages(){
var action = 'alertUnreadListMessages';
$.ajax({
  url: '../dist/php/chats.php',
  type: 'POST',
  data: {
      action: action
  }
  , dataType:"json"
  , success: function(data) {

    if(data != -1){
//to conver it to Json after contactit as string  to be used together
var json1 = data['rows'];
var json2 = data['rowsBlogs'];

if (typeof json1 != "undefined" && json1 != null && json1.length != null && json1.length > 0  || json2 != "undefined" && json2 != null && json2.length != null && json2.length > 0){
  var value = parseInt(json1.length) + parseInt(json2.length);
  $('span#blogsCounting').html(''+json2.length+'');

  $('#cc').html('<span class="badge badge-warning navbar-badge" id="messagesAlert">'+value+'</span>')
handleJsonUnreadListMessages(json1,json2);
var htmlToAppend = document.getElementById("displayAlertUnreadmessages");
htmlToAppend.innerHTML = alertUnreadListMessagesArray.join('');

}

}else{
  $('#cc').html('<span class="badge badge-warning navbar-badge" id="messagesAlert">0</span>');

  var htmlToAppend = document.getElementById("displayAlertUnreadmessages");
  htmlToAppend.innerHTML = '<a href="" class="dropdown-item">لا توجد رسائل غير مقروء</a>';
}
}
});
}
function handleJsonUnreadListMessages(json1,json2){
  alertUnreadListMessagesArray = [];
var text = '';
var text2 = '';

  var j = 0;
  var m = 0;

  let promise = new Promise(function(resolve, reject) {
    // the function is executed automatically when the promise is constructed
    json2.forEach(row => {
            text = '<div class="media blog" data-blog="'+row.blogid+'"><img src="../dist/img/'+row.writerPic+'" alt="User Avatar" class="img-blogl img-size-50 mr-3 img-circle"><div class="media-body"><h3 class="dropdown-item-title">'+row.writerName+'<span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span></h3><p class="text-sm">'+row.title+'كتب مقالة بعنوان</p><p class="text-sm text-muted"><i class="far fa-clock mr-1"></i>'+get_time_diff(row.dateCreated)+'</p></div></div>';
          alertUnreadListMessagesArray.push(text);
          text = '';
          m = m + 1;
                });
    // after 1 second signal that the job is done with the result "done"

    json1.forEach(row => {
            text2 = '<div class="media chat" data-id1="'+row.conversation_id+'"><img src="../dist/img/'+row.participantPic+'" alt="User Avatar" class="img-chatl img-size-50 mr-3 img-circle"><div class="media-body"><h3 class="dropdown-item-title">'+row.participantUsername+'<span class="float-right text-sm text-danger">'+row.counter+'<i class="fas fa-star"></i>  </span></h3><p class="text-sm">'+row.chatMessage+'</p><p class="text-sm text-muted"><i class="far fa-clock mr-1"></i>'+get_time_diff(row.timestamp)+'</p></div></div>';
          alertUnreadListMessagesArray.push(text2);
          text2 = '';
          j = j + 1;
        });
        return alertUnreadListMessagesArray;

  });


}


var alert1;
var alert2;
var alert3;

function playSound(){
  try {
  alert1 = $('#messagesAlert')[0].innerText;
  alert2 = $('.countbdg1')[0].innerText;
  alert3 = $('.countbdg3')[0].innerText;



}
catch(err) {
alert1 = null;
alert2 = null;
alert3 = null;

}
  if(alert1 != undefined || alert1 != null || alert2 != undefined || alert2 != null || alert3 != undefined || alert3 != null){

    if(parseInt(alert1) > 0 || parseInt(alert2) > 0  || parseInt(alert3) > 0){

    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', '../dist/beep.mp3');
    audioElement.setAttribute('allow', 'autoplay');
    audioElement.setAttribute('muted', true);



    audioElement.load();
    var promise = audioElement.play();

  if (promise !== undefined) {
    promise.then(_ => {
      // Autoplay started!
    }).catch(error => {
      // Autoplay was prevented.
      // Show a "Play" button so that user can start playback.
    });
  }
    }

  }


}
let  currentUserId, currentUserName,currentUserPic,currentUserStatus;
loadData();
function loadData(){
  var action = 'loadSessionData';
  $.ajax({
    url: '../dist/php/sessionData.php',
    type: 'POST',
    data: {
        action: action
    }
    , dataType:"json"
    , success: function(data) {
  //to conver it to Json after contactit as string  to be used together
  currentUserId = data.currentUserId;
  currentUserName = data.currentUserName;
  currentUserPic = data.currentUserPic;
  $('#userName')[0].textContent = currentUserName;
  $('#userPic')[0].src = '../dist/img/'+ currentUserPic;
  $('#userStatus').html('<i class="fa fa-circle text-success"></i> Online');


  }
   });
}
var alerts;
$(document).ready(function () {
  checkauthority();

alerts =  setInterval(function () {
  try {
    load_unseen_notification2();



  } catch (e) {

  }
  try {
    load_unseen_notification();



  } catch (e) {

  }
  try {
    alertUnreadListMessages(); //to display messages alert in realtime

  } catch (e) {

  }
   //to display all requests that not taken action
}, 1500);

setInterval(function(){
  try {
    playSound(); //to display messages alert in realtime

  } catch (e) {

  }
// // play sound if is more than 0 notifications
}, 3600000);


$('.al').on('show.bs.dropdown', function () {
  clearInterval(alerts);

  });
  $('.al').on('hide.bs.dropdown', function () {
    alerts =  setInterval(function () {
      try {
        load_unseen_notification2();
          } catch (e) {

      }
      try {
      load_unseen_notification(); //to display all requests that not taken action
      } catch (e) {

      }
      try {
        alertUnreadListMessages(); //to display messages alert in realtime
      } catch (e) {

      }
    }, 1500);
    });


var liveChat;

$('#logout').click(function(event) {
   event.preventDefault();
var action = "logout";
$.ajax({
  url: '../dist/php/sessionData.php',
  type: 'post',
  data: {
      action: action
  }
  , dataType: "text"
  , success: function(data) {
    logout();
  //  window.location.replace("../login.html");

  }

 });
});

});
function logout(){
  var action = "logout";
  $.ajax({
    url: '../dist/php/ajax_action.php',
    type: 'post',
    data: {
        action: action
    }
    , dataType: "text"
    , success: function(data) {
      window.location.replace("../login.html");

    }

   });
}

$(document).on('click','div.media.chat', function(event) {
  event.preventDefault();
  console.log('is btn cht cluicked');
 var el = $(this).attr('data-id1');
 $('#myForm2')[0].reset();

   liveChat = setInterval(function(){
     try {
viewClickedChatCID(el);

     } catch (e) {

     }
  }, 1500);

});
$(document).on('click','button.btn_chat' , function(event) {
   event.preventDefault();
   console.log('is btn cht cluicked');
   var el = $(this).attr('data-id1');

   $('#myForm2')[0].reset();
    liveChat = setInterval(function(){
      try {
        viewClickedChatID(el);

      } catch (e) {

      }
   }, 1500);


});

$(document).on('click','div.media.blog', function(event) {
  event.preventDefault();
  $('#blogModal').modal('show');

var element = $(this).attr('data-blog');
viewClickedBlogAlert(element);
});
$(document).on('hidden.bs.modal','#chatModal', function(event) {
  event.preventDefault();
  clearInterval(liveChat);

});
$(document).on('submit','#chatModal', function(event) {
  event.preventDefault();

});
$(document).on('keyup', '#chatMessage2', function(event) {
  event.preventDefault();
    if (event.keyCode === 13) {
event.preventDefault();

var textMessage = document.getElementById("chatMessage2").value;
if (textMessage == null || textMessage == undefined || textMessage === ""){
  return;
}
var fdm = new FormData();
var conversation_id = $('#direct-chat-messages2').attr('data-conversation_id');
fdm.append('conversation_id', btoa(conversation_id));
var formData = jQuery('#myForm2').serializeArray();
jQuery.each(formData, function(i, fd) {
  fdm.append('' + fd.name + '', fd.value);
});


$.ajax({
  url: '../dist/php/chats.php',
  type: 'post',
  data: fdm
  ,contentType: false,
  processData: false,
  success: function(response) {
    $('#myForm2')[0].reset();
    var htmlToAppend = document.getElementById("direct-chat-messages2");

        var node = document.createElement("div");
        var nodeAttribute = node.setAttribute("class","direct-chat-msg right")
           // Create a <button> element
        node.innerHTML = '<div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+currentUserName+'</span><span class="direct-chat-timestamp float-left">'+new Date().getTime()+'</span></div><img class="direct-chat-img" src="../dist/img/'+currentUserPic+'" alt="message user image"><div class="direct-chat-text">'+textMessage+'<i class="fas fa-check"></i></div>';                   // Insert text

        htmlToAppend.appendChild(node);
  htmlToAppend.scrollTop = htmlToAppend.scrollHeight;
  //  viewLatestMessages();



}
 });
}
});
