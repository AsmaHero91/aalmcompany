function fillTemplates(){
  var action = 'fillTemplates';
  $.ajax({
      url: '../dist/php/sms.php'
      , method: "POST"
      , data: {
          action: action
      }
      , dataType: "json"
      , success: function (data) {
        if (typeof data != "undefined" && data != null && data.length != null && data.length > 0) {
var text;
var i = 0;
          data.forEach(function(item) {
            if ( i == 0){
              text = '<option value="">من فضلك اختر قالب</option><option value="'+item.id+'">'+item.smsType+'</option>';
            }else{
              text += '<option value="'+item.id+'">'+item.smsType+'</option>';

            }
i = i + 1;
          });
          $('#selecto').html(''+text+'');



}
}

});
}
function defineTable(tableName){
  var t = $(tableName).DataTable({
  "order": [],
  "columnDefs": [{
    "targets": [0],
    "orderable": false,
  }] ,  dom: 'lBfrtip',
          buttons: [
            { extend: 'copyHtml5',
text: '<i class="fas fa-copy"></i>',
className: 'btn-primary',
titleAttr: 'Copy to Clipboard'
},

{ extend: 'excelHtml5',
text: '<i class="fas fa-file-excel"></i>',
className: 'btn-primary',
titleAttr: 'Export to Excel'
},
{ extend: 'pdfHtml5',
text: '<i class="fas fa-file-pdf"></i>',
className: 'btn-primary',
titleAttr: 'Export to PDF',
// messageTop: 'hgg',
title: 'تقرير',
className: 'dt-body-right',
exportOptions: {columns: ':visible'},
customize: function (doc) {
doc.defaultStyle.font = 'Arial';
doc.defaultStyle.alignment = 'right';
doc.styles.tableHeader.alignment = 'right';
doc.styles.direction = 'rtl';}
},
{ extend: 'print',
text: '<i class="fas fa-print"></i>',
className: 'btn btn-primary',
titleAttr: 'Print Table'
},
{ extend: 'colvis',
text: '<i class="fas fa-columns"></i>',
className: 'btn btn-primary',
titleAttr: 'Show/Hide Columns'
}
 ],
 retrieve: true,
 processing: true,
      } );
      t.buttons().nodes().css('background', '#007bff');
      t.buttons().nodes().css('color', 'White');

$(tableName + '_paginate').click(function() {
var t = $(tableName).DataTable({
  "order": [],
  "columnDefs": [{
    "targets": [0],
    "orderable": false,
  }],
  retrieve: true,
  processing: true,
});
});

return t;
}
function showallsms(){

    var action = 'showAllSMS';
    $.ajax({
      url: '../dist/php/sms.php',
      type: 'POST',
      data: {
        action: action,
      },
      dataType: "json",
      success: function(data) {
        var myData = data;
        var t = defineTable("#smsTable");
  t.clear().draw();
        if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
          var count = 0;
          myData.forEach(function(rowd) {
            count = count + 1;
            var row = t.row.add([count ,  rowd.smsType,  rowd.smsMessage, '']).draw().node();
            $(row).find('td:eq(3)').html('<button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="smsTable" class="editbtn btn btn-warning btn-sm">Edit</button><br><br><button data-id1="' + count + '" data-id2="' + rowd.id + '" data-id3="smsTable" class="deletebtn btn btn-danger btn-sm"><span class="ion-close" aria-hidden="true"></span></button>');
            row.id = rowd.id;
  return row;
        });
        }
      }
    });
}



$(document).ready(function (){
  pdfMake.fonts = {
  Roboto: {
     normal: 'Roboto-Regular.ttf',
     bold: 'Roboto-Medium.ttf',
     italics: 'Roboto-Italic.ttf',
     bolditalics: 'Roboto-MediumItalic.ttf'
  }, Arial: {
            normal: 'arial.ttf',
            bold: 'arialbd.ttf',
            // italics: 'ariali.ttf',
            // bolditalics: 'arialbi.ttf'
    }
};

$('#addTemplate').on('click', function(event){
  event.preventDefault();
  $('#smsModal').modal('show');
});
$(document).on('click','button.editbtn', function(event){
  event.preventDefault();
  var currentTD = $(this).parents('tr').find('td');
  if ($(this).html() == 'Edit') {
      $.each(currentTD, function() {
      $(this).prop('contenteditable', true)
    });

    $(this).html('Save');
  } else {
    $(this).html('Edit');

    $.each(currentTD, function() {
    $(this).prop('contenteditable', false)

  });

  var id = $(this).data('id2');
  var el = $(this).closest('tr').attr('id');
  var t = defineTable("#smsTable");
var index = t.row(['#'+el]).index() + 1;
var table = document.getElementById("smsTable");
var title = table.rows[index].cells[1].innerText;
var template = table.rows[index].cells[2].innerText;

  //t.row(['#'+el]).data();
  var action = 'editSms';
  var fd = new FormData();
  fd.append('id',id);
  fd.append('el',el);
fd.append('title',title);
fd.append('template',template);
  fd.append('action',action);
  $.ajax({
      url: '../dist/php/sms.php',
      type: 'post',
      data: fd,
      contentType: false,
      processData: false,
      success: function(data) {
        if(data == 0){
          showMessageSuccess('تمت العملية بنجاح');
       }else{
         showMessageError("تحقق من صحة البيانات أو الانترنت لديك");

       }
}
});
}
});
$(document).on('click','button.deletebtn', function(event){
  event.preventDefault();
  var id = $(this).data('id2');
  console.log(id);
  var el = $(this).closest('tr').attr('id');
  var action = 'deleteSms';
  var fd = new FormData();
  fd.append('id',id);
  fd.append('action',action);
  $.ajax({
      url: '../dist/php/sms.php',
      type: 'post',
      data: fd,
      contentType: false,
      processData: false,
      success: function(data) {
        if(data == 0){
          showMessageSuccess('تمت العملية بنجاح');
          var t = defineTable("#smsTable");
          t.row(['#'+ el]).remove().draw(true);
       }else{
         showMessageError("تحقق من صحة البيانات أو الانترنت لديك");

       }
}
});
});
$(document).on('submit','#smsFormt', function(event){
  event.preventDefault();
  var fd = new FormData();
  var poData = jQuery('#smsFormt').serializeArray();
  jQuery.each(poData, function(i, pd) {
    fd.append('' + pd.name + '', pd.value);
  });
  $.ajax({
      url: '../dist/php/sms.php',
      type: 'post',
      data: fd,
      contentType: false,
      processData: false,
      success: function(data) {
        if(data == 0){
          showMessageSuccess('تمت العملية بنجاح');
        $('#smsFormt')[0].reset();
         $('#smsModal').modal('hide');
       }else{
         showMessageError("تحقق من صحة البيانات أو الانترنت لديك");

       }
}
});
});


  function getDataOfLink(){
    var varFromPhp = $(this).attr("data");

  }

  $('input.charcounter-control').charcounter({
    placement: 'bottom-right'
  });
  $('textarea.charcounter-control').charcounter({
    placement: 'top-d'
  });


      $(document).on('click','#submit' ,function(event){

      //to get all the contacts related to that values from the database

      event.preventDefault();
      var e = document.getElementById("selecto");
      var selectedValue = e.options[e.selectedIndex].value;
      let values = $('.duallistbox').val(); //I will deal with it as array
      var action = 'fillMobilesAndMessages';
      var elements = values.join(',');

var valueMobile;
    $.ajax({
        url: '../dist/php/sms.php'
        , method: "POST"
        , data: {
            idt: selectedValue
            ,elements: values
            ,action: action

        }
        ,beforeSend: function (){
          if (window.location.href.indexOf("uid") > -1) {
             valueMobile = $('textarea#mobileta').html();
             $('textarea#mobileta').val(valueMobile);}
        }
        , dataType: "json"
        , success: function (data) {



if (data['smsMessage'] != 'undefined' && data['smsMessage'] != null ){
          var message = data['smsMessage'].smsMessage;
          var messageTextArea = document.getElementById("Text");
          messageTextArea.innerHTML = message;

}
//to take php value inside js(Note: can not take js value inside php without ajax)

//
//
// //to get value attributes of selected multible values
if (data['allMobiles'] != 'undefined' && data['allMobiles'] != null){
      var allMobiles = data['allMobiles'];
      var strx = allMobiles.split(',');

      var uniqueMobiles =[];
      //to check all mobiles is not repeated because maybe a customer is employee
removeDuplicateValues(strx,uniqueMobiles);
var mobTextArea = document.getElementById("mobileta");
mobTextArea.innerHTML = uniqueMobiles;
}
}
    });
});

function checkPassedvalues(){

if (window.location.href.indexOf("uid") > -1) {
let uid = getParameterValueFromUrl('uid');
let element = uid;
getPassedIDmobileNo(element);
}
}
checkPassedvalues();
function getPassedIDmobileNo(element){
var action = 'getPassedIDmobile1No';
$.ajax({
  url: '../dist/php/sms.php'
  , method: "POST"
  , data: {
      element: element,
      action: action
  }
  , dataType: "json"
  , success: function (data) {
    if (typeof data != "undefined" && data != null && data.length != null && data.length > 0) {
$('#mobileta').html(data[0].mobile1);

    }
}
});

}


function getParameterValueFromUrl(parameterName){
  var currentUrl = window.location.href;
var url = new URL(currentUrl);
var parameterValue = url.searchParams.get(''+parameterName+'');
return parameterValue;
}
function showMessageSuccess(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'success',
    title: maessageResponse,
    showConfirmButton: true,
    timer: 1500
  })
}

function showMessageError(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'error',
    title: maessageResponse,
    showConfirmButton: false,
    timer: 1500
  })
}

//function to remove duplicate values from array
function removeDuplicateValues (repeatedValuesArray, uniqueArray ){
$.each(repeatedValuesArray, function(i, el){
if($.inArray(el,uniqueArray) === -1) uniqueArray.push(el);
});
}
        //Bootstrap Duallistbox
$('.duallistbox').bootstrapDualListbox()
$('.select2').select2()


});
var imported = document.createElement('script');
imported.src = '//cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js';
document.head.appendChild(imported);

var imported2 = document.createElement('script');
imported2.src = '//cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js';
document.head.appendChild(imported2);

var imported3 = document.createElement('script');
imported3.src = '//cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js';
document.head.appendChild(imported3);
showallsms();
fillTemplates();
