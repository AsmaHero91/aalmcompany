function get_time_diff( datetime )
{
    var datetime = typeof datetime !== 'undefined' ? datetime : "2014-01-01 01:02:03.123456";

    var datetime = new Date( datetime ).getTime();
    var now = new Date().getTime();

    if( isNaN(datetime) )
    {
        return "";
    }

    console.log( datetime + " " + now);

    if (datetime < now) {
        var milisec_diff = now - datetime; //always here with blogs
    }else{
        var milisec_diff = datetime - now;
    }

    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));

    var date_diff = new Date( milisec_diff );
    var dateCreated = new Date( datetime );

    if(days <= 0){
      return "today at "+  dateCreated.getHours() + " : " + dateCreated.getMinutes() + " : " + dateCreated.getSeconds();


    }else{
      return "since " +  days + "days at "+  dateCreated.getHours() + " : " + dateCreated.getMinutes() + " : " + dateCreated.getSeconds();

    }
}


function displayAllBlogsWithComments(){
  var action = 'viewAllBlogs';
  let uid;

  //action  sendComments
  $.ajax({
    url: '../dist/php/blogs.php',
    type: 'post',
    data: {
      uid: uid,
        action: action
    }
    , dataType: "json"
    , success: function(data) {
      var jsonarrays = data['blogs'];
      var i = 0;
      var length =  Object.keys(jsonarrays).length;
      jsonarrays.forEach(rowd => {
        var datetime = rowd.dateCreated;
var htmlToAppend = document.getElementById("MainDivPosts");
var div = document.createElement('div');
div.classList = "post"; ////***** here div to search
div.id = "post"+rowd.blogid;
var divBlogger = document.createElement('div');
divBlogger.classList = "user-block";
divBlogger.innerHTML = ' <img class="img-circle img-bordered-sm" src="../dist/img/'+rowd.partcipantPic+'" width="32px;" height="32px;" alt="user image">'+
'<span class="username">'+
'<a href="#">'+rowd.participantName+'</a>'+
'</span><span class="description"> Shared publicly -'+get_time_diff( datetime );+'</span>';
var pMessage = document.createElement('p');
pMessage.classList = 'bID';
pMessage.dataset.blogid = rowd.blogid;

if ( i == 0){
pMessage.id = 'lastblogid';
pMessage.tabindex = 0;

console.log('========');
}
pMessage.innerHTML = ""+'<h3>'+rowd.title+'</h3><br>'+rowd.message+"";
var pcomments = document.createElement('p');
var counter;

pcomments.innerHTML = '<span class="float-right"><a href="" class="text-sm"><i class="far fa-comments commentsLink mr-1">عدد التعليقات</i><span class="commentsCounter" data-counter="'+rowd.commentsCounter+'">'+ rowd.commentsCounter +'</span></a></span></br>';
var form = document.createElement("form");
form.classList = "commentsForm";
form.action = "";
var input = document.createElement("input");
input.classList = "sendComments form-control form-control-sm";
input.name = "comment";
input.type = "text";
input.placeholder = "اكتب تعليقك";
var commentsDiv = document.createElement('div');
commentsDiv.classList = "commentsDiv";
function apppendFirst(){

  div.appendChild(divBlogger);
  div.appendChild(pMessage);

  div.appendChild(pcomments);
  div.appendChild(commentsDiv);

  form.appendChild(input);

  div.appendChild(form);

  htmlToAppend.appendChild(div);
  var imageList = pMessage.querySelectorAll('img');
  imageList.forEach(imagel =>{
imagel.classList = "blogImage";
  });


}

var promise = (new Promise(apppendFirst)).then(_ => {
  // Autoplay started!
  console.log('started1');

}).catch(error => {
  // Autoplay was prevented.
  // Show a "Play" button so that user can start playback.
});
i = i + 1 ;
});

}
});


}
$(document).on('keyup', '.sendComments', function(event) {
  event.preventDefault();
  console.log('ggg');
    if (event.keyCode === 13) {
      event.preventDefault();
      var formData = $(this).closest('.commentsForm').serializeArray();
      var bID = $(this).closest('div.post').find('p.bID').attr('data-blogid');
      console.log(bID);
      var action = 'sendComment';
      var fdm = new FormData();
      jQuery.each(formData, function(i, fd) {
        fdm.append('' + fd.name + '', fd.value);
      });
      fdm.append('action',action);
      fdm.append('bID',bID);

      //action  sendComments
      $.ajax({
        url: '../dist/php/blogs.php',
        type: 'post',
        data: fdm
        ,contentType: false,
        processData: false,
        success: function(response) {

          window.location.replace("news.php");

          $('#lastblogid').focus();

    }
    });

        // Cancel the default action, if needed
        // Trigger the button element with a click
        console.log("enter has ben clicked");
        // jQuery.each(formData, function(i, fd) {
        //   fdm.append('' + fd.name + '', fd.value);
        // });
        //action  sendComments
      }
  });
  $(document).on('submit', '.commentsForm', function(event) {
    event.preventDefault();
  console.log('stop submit');
    });
    $(document).on('click', '.commentsLink', function(event) {
      event.preventDefault();
      var targetel = event.target;
      var counter = $(targetel).closest('div').find('span.commentsCounter')[0].innerHTML;
      if(parseInt(counter)> 0 ){

      var element =  $(targetel).closest('div').find('p.bID').attr('data-blogid');

      if (targetel.classList.contains('isOpened')){
        targetel.classList.remove("isOpened");
        targetel.classList.toggle("isClosed");
        var targetcommentsDiv = $('#post'+ element).find('div.commentsDiv')[0];
        targetcommentsDiv.innerHTML = '';
      }else{

      targetel.classList.remove("isClosed");

      targetel.classList.toggle("isOpened");

var action = "displayComments";
$.ajax({
  url: '../dist/php/blogs.php',
  type: 'GET',
  data: {
       element: element
      ,action: action
  }
  , dataType: "json"
  , success: function(data) {
    if (data != -1){
    var targetcommentsDiv = $('#post'+ element).find('div.commentsDiv')[0];
    console.log(targetcommentsDiv);
    var jsonarrays = data;
    var i = 0;
    jsonarrays.forEach(rowd => {
      var datetime = rowd.dateCreated;
var div = document.createElement('div');
div.classList = "post";
var divBlogger = document.createElement('div');
divBlogger.classList = "user-block";
divBlogger.innerHTML = ' <img class="img-circle img-bordered-sm" src="../dist/img/'+rowd.partcipantPic+'" width="32px;" height="32px;" alt="user image">'+
'<span class="username">'+
'<a href="#">'+rowd.participantName+'</a>'+
'</span><span class="description"> Shared publicly -'+get_time_diff( datetime );+'</span>';
var pMessage = document.createElement('p');
pMessage.classList = 'comment';
pMessage.id = rowd.id;
pMessage.innerHTML = rowd.comment;
console.log( rowd.comment);
function apppendFirst(){

div.appendChild(divBlogger);
div.appendChild(pMessage);

targetcommentsDiv.appendChild(div);

}

var promise = (new Promise(apppendFirst)).then(_ => {
// Autoplay started!
console.log('started1');

}).catch(error => {
// Autoplay was prevented.
// Show a "Play" button so that user can start playback.
});

});
}
}

});

} //else block
}else{
  return;

}
//ifblock e
});


function showMessageError(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'error',
    title: maessageResponse,
    showConfirmButton: false,
    timer: 1500
  })
}
function showMessageSuccess(maessageResponse){
  Swal.fire({
    position: 'center',
    type: 'success',
    title: maessageResponse,
    showConfirmButton: true,
    timer: 1500
  })
}

$(document).ready(function(){
  displayAllBlogsWithComments();




});
