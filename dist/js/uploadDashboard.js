function fillDocumentsDiv (divSelector, filesnames){

var uploadtopre = '';

	for (file of filesnames) {
		var el = file.trim();
		var fairel = el.substring(el.lastIndexOf('+') + 1);
		uploadtopre += '<span class="documentdeletespan">' + fairel + '  <button type="button" class="documentdelete ion-trash-a" name="btn_delete"  data-filename="' + file + '" ></button>' + '</span></br>';

		// $('#documentsui').append('<span class="documentdeletespan"> <a id=documentsa href=upload/hr/'+response+'>'+ response +'</a> <button type="button" id="documentdelete" name="btn btn-danger btn-xs btn_delete" class="ion-trash-a" data-id5="'+ response + '" > </button></span>');
	}


$(divSelector).append(uploadtopre);
}
function StartLoaingInsideDiv (divSelector){
$(divSelector).html('');
$(divSelector)[0].classList.add('dots');

var loadinghtml = '<div class="dots"><div class="dot dot-1"></div><div class="dot dot-2"></div><div class="dot dot-3"></div></div>';

$(divSelector).append(loadinghtml);
}
function StopLoaingInsideDiv (divSelector){
	$(divSelector)[0].classList.remove('dots');

$(divSelector).html('');
}
  function uploadFiles(uploadFileID,divSelector,link){
  	var fd = new FormData();
  var fileSelectInput = document.getElementById(uploadFileID);

  // Get the selected files from the input.
  var files = fileSelectInput.files;

  // Loop through each of the selected files.
   for (var i = 0; i < files.length; i++) {
  	 var file = files[i];
  	 // Add the file to the request.
  	fd.append('userfile[]', file, file.name);
   }
   var link = link;

   fd.append('link', link);

  	 $.ajax({
  			 url: "dist/php/upload.php",
  			 type: 'post',
  			data: fd,
  			contentType: false,
  			processData: false,
  			beforeSend: function () {
  	// ... your initialization code here (so show loader) ...
  StartLoaingInsideDiv(divSelector);

  },
  		success: function(data) {
  			StopLoaingInsideDiv(divSelector);
  			var jsonData = JSON.parse(data);
				var maessageResponse = jsonData.output;
				Swal.fire({
					position: 'center',
					title: maessageResponse,
					showConfirmButton: true
				});
  			if (jsonData.filesnames.length > 0){
  				var filesnames = jsonData.filesnames;

  fillDocumentsDiv(divSelector,filesnames);

  }
  }
  		});
  }

function deleteFile(link,fileToDelete,mine){
  var link = link;
  var fileToDelete = fileToDelete;
  	$.ajax({
  			url: "dist/php/upload.php"
  			, method: "POST"
  			, data: {
  					fileToDelete: fileToDelete,
  					link: link
  			}
  			, dataType: "text"
  			, success: function (data) {
					var maessageResponse = 'لقد تم حذف الملف بنجاح';
					Swal.fire({
						position: 'center',
						type: 'success',
						title: maessageResponse,
						showConfirmButton: true
					});
  				$(mine).closest('span').remove();

  }
  });
}
