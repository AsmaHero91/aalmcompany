function showCompletedRequests(){
//  htmltoAppend.innerHTML = '';
var action = 'completedRequests';
$.ajax({
  url: '../dist/php/requestsActions.php',
  type: 'POST',
  data: {
    action: action,
  },
  dataType: "json",
  success: function(data) {
    $('#completedTbody').html('');
    var t = defineTable('#completedTable');
    t.clear().draw();
    var myData = data;
    if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
      var count = 0;

      myData.forEach(function(rowd) {
        count = count + 1;
        var arrayd = [];
        var arrayhtml = [];
        var value = rowd.documents
        var valueString = value.toString();
        var dlink = '../upload/requests/'
        strx = valueString.split(',');
        arrayd = arrayd.concat(strx);
        //formattedDocuments.split(',');
        for (element of arrayd) {
          arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + element.trim() + '</a>');
          console.log(element.trim());
        }
        var arraydString = arrayhtml;
        var row = t.row.add([count , rowd.reqid , rowd.requestName , rowd.requester ,  rowd.documents , rowd.dateCreated, rowd.deadline, rowd.status,  '' ]).draw().node();
        $(row).find('td:eq(0)').html('<label>' + count + '</label>');
        $(row).find('td:eq(2)').attr('data-id1',rowd.requestTCode);
        $(row).find('td:eq(4)').html(arraydString.join());
        $(row).find('td:eq(8)').html('<button type="button" class="btn btn-info btn-s update" style="margin-right:10px; margin-bottom:10px; width: 48px;" data-id1="'+rowd.id+'"> <span class="ion-search" aria-hidden="true"></span> </button>');
        row.id = count;
        return row;

      });

    } else {
      var t = defineTable('#completedTable');
      t.clear().draw();
    }
  }
});
}

function showDeclinedRequests(){
//  htmltoAppend.innerHTML = '';
var action = 'declinedRequests';
$.ajax({
  url: '../dist/php/requestsActions.php',
  type: 'POST',
  data: {
    action: action,
  },
  dataType: "json",
  success: function(data) {
    $('#declinedTbody').html('')
    var t = defineTable('#declinedTable');
    t.clear().draw();
    var myData = data;
    if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
      var count = 0;

      myData.forEach(function(rowd) {
        count = count + 1;
        var arrayd = [];
        var arrayhtml = [];
        var value = rowd.documents
        var valueString = value.toString();
        var dlink = '../upload/requests/'
        strx = valueString.split(',');
        arrayd = arrayd.concat(strx);
        //formattedDocuments.split(',');
        for (element of arrayd) {
          arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + element.trim() + '</a>');
          console.log(element.trim());
        }
        var arraydString = arrayhtml;
        var row = t.row.add([count , rowd.reqid , rowd.requestName , rowd.requester ,  rowd.documents , rowd.dateCreated, rowd.deadline, rowd.status,  '' ]).draw().node();
        $(row).find('td:eq(0)').html('<label>' + count + '</label>');
        $(row).find('td:eq(2)').attr('data-id1',rowd.requestTCode);
        $(row).find('td:eq(4)').html(arraydString.join());
        $(row).find('td:eq(8)').html('<button type="button" class="btn btn-info btn-s update" style="margin-right:10px; margin-bottom:10px; width: 48px;" data-id1="'+rowd.id+'"> <span class="ion-search" aria-hidden="true"></span> </button>');
        row.id = count;
        return row;

      });

    } else {
      var t = defineTable('#declinedTable');
      t.clear().draw();
    }
  }
});
}


function showNeedsYourAction(){
//  htmltoAppend.innerHTML = '';

    $('#needsYourActionTbody').html('');

var action = 'needsYourAction';
$.ajax({
  url: '../dist/php/requestsActions.php',
  type: 'POST',
  data: {
    action: action,
  },
  dataType: "json",
  success: function(data) {
    var myData = data['rows'];

    if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
      var t = defineTable('#needsYourActionTable');
      t.clear().draw();
      var count = 0;
var j = 0 ;
      myData.forEach(function(rowd) {
        count = count + 1;
        var arrayd = [];
        var arrayhtml = [];
        var value = rowd.documents
        var valueString = value.toString();
        var dlink = '../upload/requests/'
        strx = valueString.split(',');
        arrayd = arrayd.concat(strx);
        //formattedDocuments.split(',');
        for (element of arrayd) {
          arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + element.trim() + '</a>');
          console.log(element.trim());
        }
        var arraydString = arrayhtml;
        var row = t.row.add([count , rowd.reqid , rowd.requestName , rowd.requester , rowd.actionRequired,  rowd.documents , rowd.dateCreated, rowd.deadline, rowd.notes, data['empNames'][j],  '' ]).draw().node();
        $(row).find('td:eq(0)').html('<label>' + count + '</label>');
        $(row).find('td:eq(2)').attr('data-id1',rowd.requestTCode);
        $(row).find('td:eq(5)').html(arraydString.join());



        $(row).find('td:eq(10)').html('<button type="button" class="btn btn-warning btn-s accept" style="margin-right:10px; margin-bottom:10px;  width: 90px; height: 40px" data-id1="'+rowd.id+'" data-id2="'+rowd.reqid+'" data-id3="'+rowd.requestTCode+'" data-id5="'+rowd.empid+'"> <span class="ion-checkmark" aria-hidden="true"></span> </button>'+
        '<button type="button" class="btn btn-warning btn-s decline" style="margin-right:10px; margin-bottom:10px;  width: 90px; height: 40px;" data-id1="'+rowd.id+'" data-id2="'+rowd.reqid+'" data-id3="'+rowd.requestTCode+'" data-id5="'+rowd.empid+'"><span class="ion-close" aria-hidden="true"></span> </button>'+
        '<button type="button" class="btn btn-warning btn-s ccaction" style="margin-right:10px; margin-bottom:10px; width: 90px; height: 40px;" data-id1="'+rowd.id+'" data-id2="'+rowd.reqid+'" data-id3="'+rowd.requestTCode+'" data-id5="'+rowd.empid+'"> تخصيص </button>');
        row.id = count;
        j = j + 1;
        return row;

      });

    } else {
      var t = defineTable('#needsYourActionTable');
      t.clear().draw();
    }
  }
});
}

function showprogresstable(){
  var action = 'showInProgressTable';
  $.ajax({
    url: '../dist/php/requestsActions.php',
    type: 'POST',
    data: {
      action: action,
    },
    dataType: "json",
    success: function(data) {
      $('#inProgressTbody').html('');
      var t = defineTable('#inProgressTable');
      t.clear().draw();
      var myData = data;
      if (typeof myData != "undefined" && myData != null && myData.length != null && myData.length > 0) {
        var count = 0;

        myData.forEach(function(rowd) {
          count = count + 1;
          var arrayd = [];
          var arrayhtml = [];
          var value = rowd.documents
          var valueString = value.toString();
          var dlink = '../upload/requests/'
          strx = valueString.split(',');
          arrayd = arrayd.concat(strx);
          //formattedDocuments.split(',');
          for (element of arrayd) {
            arrayhtml.push('<a href="' + dlink + element.trim() + '" target="_blank">' + element.trim() + '</a>');
            console.log(element.trim());
          }
          var arraydString = arrayhtml;
          var row = t.row.add([count , rowd.reqid , rowd.requestName , rowd.requester ,  rowd.documents , rowd.dateCreated, rowd.deadline, rowd.status,  '' ]).draw().node();
          $(row).find('td:eq(0)').html('<label>' + count + '</label>');
          $(row).find('td:eq(2)').attr('data-id1',rowd.requestTCode);
          $(row).find('td:eq(4)').attr(arraydString.join());



          $(row).find('td:eq(8)').html('<button type="button" class="btn btn-info btn-s update" style="margin-right:10px; margin-bottom:10px; width: 48px;" data-id1="'+rowd.id+'"> <span class="ion-search" aria-hidden="true"></span> </button>');
          row.id = count;
          return row;

        });

      } else {
        var t = defineTable('#inProgressTable');
        t.clear().draw();
      }
    }
  });
}

function defineTable(tableName){
  var t = $(tableName).DataTable({
  "order": [],
  "columnDefs": [{
    "targets": [0],
    "orderable": false,
  }],
          dom: 'lBfrtip',
          buttons: [
            { extend: 'copyHtml5',
text: '<i class="fas fa-copy"></i>',
className: 'btn-primary',
titleAttr: 'Copy to Clipboard'
},

{ extend: 'excelHtml5',
text: '<i class="fas fa-file-excel"></i>',
className: 'btn-primary',
titleAttr: 'Export to Excel'
},
{ extend: 'pdfHtml5',
text: '<i class="fas fa-file-pdf"></i>',
className: 'btn-primary',
titleAttr: 'Export to PDF',
// messageTop: 'hgg',
title: 'تقرير',
className: 'dt-body-right',
exportOptions: {columns: ':visible'},
customize: function (doc) {
doc.defaultStyle.font = 'Arial';
doc.defaultStyle.alignment = 'right';
doc.styles.tableHeader.alignment = 'right';
doc.styles.direction = 'rtl';}
},
{ extend: 'print',
text: '<i class="fas fa-print"></i>',
className: 'btn btn-primary',
titleAttr: 'Print Table'
},
{ extend: 'colvis',
text: '<i class="fas fa-columns"></i>',
className: 'btn btn-primary',
titleAttr: 'Show/Hide Columns'
}
 ],
 retrieve: true,
 processing: true,
      } );
      t.buttons().nodes().css('background', '#007bff');
      t.buttons().nodes().css('color', 'White');

$(tableName + '_paginate').click(function() {
var t = $(tableName).DataTable({
  "order": [],
  "columnDefs": [{
    "targets": [0],
    "orderable": false,
  }],
  retrieve: true,
  processing: true,
});
});

return t;
}


function defineTableWithCounter(tableName){
  var t = $(tableName).DataTable({
    "order": [],
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    },
    {
      "targets": [1],
      "orderable": false,
    },
  ],
  dom: 'lBfrtip',
  buttons: [
    { extend: 'copyHtml5',
text: '<i class="fas fa-copy"></i>',
className: 'btn-primary',
titleAttr: 'Copy to Clipboard'
},

{ extend: 'excelHtml5',
text: '<i class="fas fa-file-excel"></i>',
className: 'btn-primary',
titleAttr: 'Export to Excel'
},
{ extend: 'pdfHtml5',
text: '<i class="fas fa-file-pdf"></i>',
className: 'btn-primary',
titleAttr: 'Export to PDF',
// messageTop: 'hgg',
title: 'تقرير',
className: 'dt-body-right',
exportOptions: {columns: ':visible'},
customize: function (doc) {
doc.defaultStyle.font = 'Arial';
doc.defaultStyle.alignment = 'right';
doc.styles.tableHeader.alignment = 'right';
doc.styles.direction = 'rtl';}
},
{ extend: 'print',
text: '<i class="fas fa-print"></i>',
className: 'btn btn-primary',
titleAttr: 'Print Table'
},
{ extend: 'colvis',
text: '<i class="fas fa-columns"></i>',
className: 'btn btn-primary',
titleAttr: 'Show/Hide Columns'
}
],
retrieve: true,
processing: true,
} );
t.buttons().nodes().css('background', '#007bff');
t.buttons().nodes().css('color', 'White');


  $(tableName + '_paginate').click(function() {
  var t = $(tableName).DataTable({
    "order": [],
    "columnDefs": [{
      "targets": [0],
      "orderable": false,
    }],
    retrieve: true,
    processing: true,
  });
});
  return t;
}

$(document).ready(function () {
  pdfMake.fonts = {
  Roboto: {
     normal: 'Roboto-Regular.ttf',
     bold: 'Roboto-Medium.ttf',
     italics: 'Roboto-Italic.ttf',
     bolditalics: 'Roboto-MediumItalic.ttf'
  }, Arial: {
            normal: 'arial.ttf',
            bold: 'arialbd.ttf',
            // italics: 'ariali.ttf',
            // bolditalics: 'arialbi.ttf'
    }
};

});
